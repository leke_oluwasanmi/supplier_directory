﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class AuthenticatedUserServiceTests
    {
        [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);

            

            var item = new Data.DTOs.AuthenticatedUser()
            {
                Id = "1",
                Title = "CEO",
                UserName = "userName1",
                CreatedOn = datetime,
                UpdatedOn = datetime
            };
            var item2 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "2",
                 Title="Analyst",
                  UserName="userName2",
                CreatedOn = datetime,
                UpdatedOn = datetime
            };
            IQueryable<Data.DTOs.AuthenticatedUser> list = new List<Data.DTOs.AuthenticatedUser>() { item, item2 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.AuthenticatedUser>()).Return(list);


         
            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
            var result = service.FindByIds(settings, "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("userName2", result[0].UserName);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.AuthenticatedUser()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime
            };
            var item2 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime
            };
            var item3 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime
            };
            IQueryable<Data.DTOs.AuthenticatedUser> list = new List<Data.DTOs.AuthenticatedUser>() { item, item2, item3 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.AuthenticatedUser>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            
            //ACT
            var result = service.GetCount();


            //ASSERT
            Assert.AreEqual(3, result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SearchCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.AuthenticatedUser()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserName = "michael",
                 Title="userName",
                GivenName = "Joey Tribbinani"
            };
            var item2 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserName="userName",
                Title = "CEO",
                GivenName = "Rachel"
            };

            var item3 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserName = "Jhonny",
                Title = "Analyst",
                GivenName = "Joey"
            };
            IQueryable<Data.DTOs.AuthenticatedUser> list = new List<Data.DTOs.AuthenticatedUser>() { item, item2, item3 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.AuthenticatedUser>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
            var result = service.Search("Joey", 10, new AuthenticatedUserHydrationSettings() { CreatedBy = new UserHydrationSettings() { }, UpdatedBy = new UserHydrationSettings() });


            //ASSERT
            Assert.AreEqual(2, result.Count());
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetPagedOrderedBySurnameCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.AuthenticatedUser()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserName = "michael",
                Title = "userName",
                GivenName = "Ross"
            };
            var item2 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserName = "userName",
                Title = "CEO",
                GivenName = "Rachel"
            };

            var item3 = new Data.DTOs.AuthenticatedUser()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserName = "Jhonny",
                Title = "Analyst",
                GivenName = "Joey"
            };
            IQueryable<Data.DTOs.AuthenticatedUser> list = new List<Data.DTOs.AuthenticatedUser>() { item, item2, item3 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.AuthenticatedUser>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
            var result = service.GetPagedOrderedBySurname(0, 10);


            //ASSERT
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual("Joey", result[0].GivenName);
            Assert.AreEqual("Rachel", result[1].GivenName);
            Assert.AreEqual("Ross", result[2].GivenName);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SaveCheckInsert()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);


            mockRepository.Expect(x => x.Insert(Arg<Data.Model.AuthenticatedUser>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
           service.Save(new  AuthenticatedUser(){ GivenName="Joey" , Title="Title" , Telephone="07240460931", UserEmail = "Email@email.com", UserName ="userName"});


            //ASSERT
           
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SaveCheckUpdate()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);


            mockRepository.Expect(x => x.Update(Arg<Data.Model.AuthenticatedUser>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
            service.Save(new AuthenticatedUser() { Id="1", GivenName = "Joey", Title = "Title", Telephone = "07240460931", UserEmail = "Email@email.com", UserName = "userName" });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByEmailCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.AuthenticatedUser()
            {
                Id = "1",
                Title = "CEO",
                UserName = "userName1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserEmail = "user@email.com"
            };
            var item2 = new Data.Model.AuthenticatedUser()
            {
                Id = "2",
                Title = "Analyst",
                UserName = "userName2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserEmail = "email@email.com"
            };
            IQueryable<Data.Model.AuthenticatedUser> list = new List<Data.Model.AuthenticatedUser>() { item, item2 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.Model.AuthenticatedUser>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
            var result = service.FindByEmail("email@email.com");


            //ASSERT

         
            Assert.AreEqual("2", result.Id);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByUniqueIDCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.AuthenticatedUser()
            {
                Id = "1",
                Title = "CEO",
                UserName = "userName1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserEmail = "user@email.com",
                UserIdentification = "Identification1"
            };
            var item2 = new Data.Model.AuthenticatedUser()
            {
                Id = "2",
                Title = "Analyst",
                UserName = "userName2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                UserEmail = "email@email.com",
                UserIdentification = "Identification2"
            };
            IQueryable<Data.Model.AuthenticatedUser> list = new List<Data.Model.AuthenticatedUser>() { item, item2 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.Model.AuthenticatedUser>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthenticatedUserHydrationSettings settings = new AuthenticatedUserHydrationSettings();

            //ACT
            var result = service.FindByUniqueID("Identification2");


            //ASSERT


            Assert.AreEqual("2", result.Id);

            mockRepository.VerifyAllExpectations();
        }
    }
}
