﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SD.Core.Services.Caching;
using SD.Core.Data;
using Rhino.Mocks;
using SD.Core.Data.DTOs;
using SD.Core.Services.CMS;
using SD.Core.Services.Helpers;
using SD.Core.Common.Constants;
using SD.Core.Data.Model.HS;
using MongoDB.Driver;
using System.Threading;
using SD.Core.Services.Core.Wrapped;
using MongoDB.Bson;

//using VWG.Core.Common.Config;

namespace SD.Core.Services.Tests
{
    [TestFixture]
    public class ArticleServiceTestsCore
    {

        [SetUp]
        public void SetupInit()
        {
          
        }

        [Test]
        public void FindBySlugCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();
            

            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            //articles.InsertOne(key);
            
            var hsettings=new ArticleHydrationSettings();
            articleServiceWrapped.Expect(x => x.FindBySlug("urlSlug", hsettings)).Return(item);

            //ACT
            var article = service.FindBySlug("urlSlug", hsettings);
                
            //ASSERT

            Assert.AreEqual("urlSlug", article.UrlSlug);
            mockRepository.VerifyAllExpectations();

        }

        [Test]
        public void FindByUrlSlugCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                Id="1",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
      

            var hsettings = new ArticleHydrationSettings();

            List<Data.Model.Article> list = new List<Data.Model.Article>() { item };
            articleServiceWrapped.Expect(x => x.FindByUrlSlugs("vw", hsettings, "urlSlug")).Return(list);

            //ACT
            var articles = service.FindByUrlSlugs("vw", hsettings, "urlSlug");

            //ASSERT

            Assert.AreEqual(1, articles.Count());
            Assert.AreEqual("1", articles.FirstOrDefault().Id);
            mockRepository.VerifyAllExpectations();

        }

        //---------------------
        [Test]
        [Ignore("ignore test")]
        public void FindByIdCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var datetime = new DateTime(2012, 10, 20);
            

            SD.Core.Services.Core.ArticleService service = new ArticleService(mockRepository);
            var item = new Data.DTOs.Article()
            {
                Id="1",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.DTOs.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
           

            //ACT
            var article = service.FindById("1", new ArticleHydrationSettings());
         

            //ASSERT
            Assert.AreEqual(article.Id, "1");
            

        }


        //---------------------
        [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.Model.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            var hsettings = new ArticleHydrationSettings();
            IList<Data.Model.Article> list = new List<Data.Model.Article>() { item, item2 };
            articleServiceWrapped.Expect(x => x.FindByIds(hsettings, "2")).Return(list);

            //ACT
            var articles = service.FindByIds("",hsettings,"2");

            //ASSERT

            Assert.AreEqual(2, articles.Count());
            mockRepository.VerifyAllExpectations();

        }
       
           

         //---------------------
        [Test]
        public void GetPagedOrderedByCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.Model.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            var hsettings = new ArticleHydrationSettings();
            IList<Data.Model.Article> list = new List<Data.Model.Article>() { item, item2 };
            articleServiceWrapped.Expect(x => x.GetPagedOrderedBy("vw", SortOrder.CreatedOn, 0, 10, new string[] { })).Return(list);

            //ACT
            var articles = service.GetPagedOrderedBy("vw", SortOrder.CreatedOn, 0,10, new string[]{});

            //ASSERT

           
            mockRepository.VerifyAllExpectations();

        }

         //---------------------
        [Test]
        public void GetCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.Model.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            var hsettings = new ArticleHydrationSettings();
            IList<Data.Model.Article> list = new List<Data.Model.Article>() { item, item2 };
            articleServiceWrapped.Expect(x => x.GetCount("vw", new string[] { })).Return(list.Count());

            //ACT
            var result = service.GetCount("vw", new string[] { } );

            //ASSERT

            Assert.AreEqual(2, result);
            mockRepository.VerifyAllExpectations();

        }

           

         //---------------------
        [Test]
        [Ignore("ignore test")]
        public void GetPopularCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var datetime = new DateTime(2012, 10, 20);


            SD.Core.Services.Core.ArticleService service = new ArticleService(mockRepository);
            var articleItem = new Data.DTOs.Article()
            {
                Id="123",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "1",
                Status = Common.Constants.PublicationStatus.Published
            };

            var item = new Data.DTOs.ArticleStatsGroup()
            {
                 Id="1",
                  Value=3
            };

            IQueryable<Data.DTOs.Article> list2 = new List<Data.DTOs.Article>() { articleItem }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list2);

            IQueryable<Data.DTOs.ArticleStatsGroup> list = new List<Data.DTOs.ArticleStatsGroup>() { item }.AsQueryable();
            

            //mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Article>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.ArticleStatsGroup>()).Return(list);
            
            //ACT
            var articles = service.GetPopular(ArticleStatType.Share,10, ArticleType.article, new ArticleHydrationSettings());
                
          

            //ASSERT

            Assert.AreEqual("123", articles[0].Id);
            mockRepository.VerifyAllExpectations();

        }
           
           

          

         //---------------------
        [Test]
        public void GetPopularTagsListCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            
            var datetime = new DateTime(2012, 10, 20);


            SD.Core.Services.Core.ArticleService service = new ArticleService(mockRepository);
            var item = new Data.DTOs.Tags()
            {
                Id="123",
                DisplayText="Text",
                 ParentId="2",
                  ParentSlug="Slug",
                UrlSlug = "UrlSlug"
               
            };
            var item2 = new Data.DTOs.Tags()
            {
                Id = "1234",
                DisplayText = "Text",
                ParentId = "2",
                ParentSlug = "Slug",
                UrlSlug = "noUrlSlug"

            }; 
            var item3 = new Data.DTOs.Tags()
            {
                Id = "12345",
                DisplayText = "Text",
                ParentId = "2",
                ParentSlug = "Slug",
                UrlSlug = "UrlSlug"

            };

            IQueryable<Data.DTOs.Tags> list = new List<Data.DTOs.Tags>() { item, item2, item3 }.AsQueryable();
           
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Tags>()).Return(list);
            

            //ACT
            var result = service.GetPopularTagsList("UrlSlug");
                
           


            //ASSERT
            Assert.AreEqual(2, result.Count);
            mockRepository.VerifyAllExpectations();

        }
           
           
        
         //---------------------
        [Test]
        public void FindRelatedTagsCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
         

            articleServiceWrapped.Expect(x => x.FindRelatedTags("vw", new string[] { }, new PublicationStatus[] { PublicationStatus.Published }, false, true)).Repeat.Once();

            //ACT
            var articles = service.FindRelatedTags("vw", new string[] { }, new PublicationStatus[]{ PublicationStatus.Published }, false,true);

            //ASSERT

         
            mockRepository.VerifyAllExpectations();
             

        }


        //---------------------
        [Test]
        public void SearchCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                Id="1",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.Model.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IList<Data.Model.Article> list = new List<Data.Model.Article>() { item, item2 };
            articleServiceWrapped.Expect(x => x.Search("searchText", 0, 10)).Return(list);

            //ACT
            var articles = service.Search("searchText", 0, 10);

            //ASSERT

            Assert.AreEqual(2, articles.Count());
            Assert.AreEqual("1", articles.FirstOrDefault().Id);
            mockRepository.VerifyAllExpectations();

        }


        //---------------------
        [Test]
        public void SearchDifferentParametersCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            
            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();


            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            var item = new Data.Model.Article()
            {
                Id = "1",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.Model.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IList<Data.Model.Article> list = new List<Data.Model.Article>() { item, item2 };
            articleServiceWrapped.Expect(x => x.Search("searchText")).Return(list);

            //ACT
            var articles = service.Search("searchText");

            //ASSERT

            Assert.AreEqual(2, articles.Count());
            Assert.AreEqual("1", articles.FirstOrDefault().Id);
            mockRepository.VerifyAllExpectations();

        }


        //---------------------
        [Test]
        public void GetFavoriteArticlesCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);

            var articleServiceWrapped = MockRepository.GenerateMock<IArticleServiceWrapped>();
            SD.Core.Services.Core.ArticleService service = new SD.Core.Services.Core.ArticleService(mockRepository, articleServiceWrapped);
            
            var item = new Data.Model.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
                 
            };
            var item2 = new Data.Model.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };

            var itemStat = new FavoriteStat()
            {
                Id = "3",
                UserId = "2",
                StatisticType = FavoriteStatType.Yes,
                ArticleId = "articleId"
            };

            var hsettings = new ArticleHydrationSettings();
            IList<Data.Model.Article> list = new List<Data.Model.Article>() { item, item2 };
            
            articleServiceWrapped.Expect(x => x.FindByUrlSlugs("vw", hsettings, "urlSlug")).Return(list);

            IQueryable<Data.DTOs.FavoriteStat> listQueryable = new List<Data.DTOs.FavoriteStat>() { itemStat }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.FavoriteStat>()).Return(listQueryable);
           

            //ACT
            var articles = service.GetFavoriteArticles("vw", "2", hsettings, 0, 20);

            //ASSERT

            Assert.AreEqual(2, articles.Count());
            mockRepository.VerifyAllExpectations();

        }
       
           
           
      


        

    }
}
