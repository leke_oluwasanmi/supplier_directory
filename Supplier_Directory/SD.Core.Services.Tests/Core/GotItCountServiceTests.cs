﻿using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Services.Core;
using SD.Core.Services.Core.Wrapped;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class GotItCountServiceTests
    {

        [Test]
        public void AllCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.GotItCount>();
            
            var serviceWrapped = MockRepository.GenerateMock<IGotItCountServiceWrapped>();

            GotItCountService service = new GotItCountService(mockRepository, serviceWrapped);
            
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.GotItCount()
            {
                Id = "1",
               
                YesCount = 4,
                NoCount = 1
            };

            var item2 = new Data.Model.GotItCount()
            {
                Id = "2",
               
                YesCount = 5,
                NoCount = 0
            };

            listItems.Add(item);
            listItems.Add(item2);



            serviceWrapped.Expect(x => x.All()).Return(listItems);


          



            //ACT
            var result = service.All();


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);
            Assert.AreEqual(4, result[0].YesCount);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            GotItCountService service = new GotItCountService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.GotItCount()
            {
                Id = "1",
                ArticleId = "3",
                YesCount = 4,
                NoCount = 1
            };

            var item2 = new Data.DTOs.GotItCount()
            {
                Id = "2",
                ArticleId = "3",
                YesCount = 5,
                NoCount = 0
            };


            IQueryable<Data.DTOs.GotItCount> list = new List<Data.DTOs.GotItCount>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.GotItCount>()).Return(list);



           
            //ACT
            var result = service.FindByIds( "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual(5, result[0].YesCount);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByIdCheck()
        {
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var serviceWrapped = MockRepository.GenerateMock<IGotItCountServiceWrapped>();

            GotItCountService service = new GotItCountService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.GotItCount()
            {
                Id = "1",

                YesCount = 4,
                NoCount = 1
            };

            var item2 = new Data.Model.GotItCount()
            {
                Id = "2",

                YesCount = 5,
                NoCount = 0
            };

            serviceWrapped.Expect(x => x.FindById("2")).Return(item2);

            //ACT
            var result = service.FindById("2");


            //ASSERT

          
            Assert.AreEqual("2", result.Id);
            Assert.AreEqual(5, result.YesCount);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByArticleIdCheck()
        {
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var serviceWrapped = MockRepository.GenerateMock<IGotItCountServiceWrapped>();

            GotItCountService service = new GotItCountService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.GotItCount()
            {
                Id = "1",
                 Article = new Data.Model.Article(){ Id="3"},
                YesCount = 4,
                NoCount = 1
            };

            var item2 = new Data.Model.GotItCount()
            {
                Id = "2",
                Article = new Data.Model.Article() { Id = "1" },
                YesCount = 5,
                NoCount = 0
            };

            serviceWrapped.Expect(x => x.FindByArticleId("1")).Return(item2);

            //ACT
            var result = service.FindByArticleId("1");


            //ASSERT


            Assert.AreEqual("2", result.Id);
            Assert.AreEqual(5, result.YesCount);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SaveCheckNew()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            GotItCountService service = new GotItCountService(mockRepository);





            mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.GotItCount>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.GotItCount()
            {
                Article = new Data.Model.Article(),
                YesCount = 3,
                NoCount = 2
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();

        }


        [Test]
        public void SaveCheckExisting()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            GotItCountService service = new GotItCountService(mockRepository);





            //mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.GotItCount>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();
            mockRepository.Expect(x => x.Update(Arg<Data.DTOs.GotItCount>.Is.Anything)).Return(replaceOneResult1).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.GotItCount()
            {
                Id="1",
                Article = new Data.Model.Article(),
                YesCount = 3,
                NoCount = 2
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();
           

        }


         [Test]
         public void DeleteCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var gotItCount = MockRepository.GenerateStub<IMongoCollection<Data.Model.GotItCount>>();
            GotItCountService service = new GotItCountService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.GotItCount()
            {
                Id = "1",
               
                YesCount = 4,
                NoCount = 1
            };

            var item2 = new Data.Model.GotItCount()
            {
                Id = "2",
               
                YesCount = 5,
                NoCount = 0
            };

            gotItCount.InsertOne(item);
            gotItCount.InsertOne(item2);

            mockRepository.Stub(x => x.GetCollection<Data.Model.GotItCount>()).Return(gotItCount);


            gotItCount.Expect(x => x.DeleteOne(Arg<FilterDefinition<Data.Model.GotItCount>>.Is.Anything, Arg<CancellationToken>.Is.Anything)).Repeat.Once();



            //ACT
            service.Delete("2");


            //ASSERT

         

            mockRepository.VerifyAllExpectations();
            //var filter = Builders<GotItCount>.Filter.Eq("Id", id);
            //MongoRepository.GetCollection<GotItCount>().DeleteOne(filter);
        }

       


    }
}
