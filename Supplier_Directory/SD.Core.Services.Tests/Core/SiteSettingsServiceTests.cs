﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class SiteSettingsServiceTests
    {
        [Test]
        public void GetAllCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            SiteSettingsService service = new SiteSettingsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.SiteSettings()
            {
                Id = "1",
                Key ="Key 1",
                Value = "Value 1",
                 Object = new object()
            };

            var item2 = new Data.Model.SiteSettings()
            {
                Id = "2",
                Key = "Key 2",
                Value = "Value 2",
                Object = new object()
            };

            IQueryable<Data.Model.SiteSettings> list = new List<Data.Model.SiteSettings>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.Model.SiteSettings>()).Return(list);



          

            //ACT
            var result = service.GetAll();


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);
            Assert.AreEqual("Value 1", result[0].Value);

            mockRepository.VerifyAllExpectations();
         
        }

        [Test]
        public void ByIdCheck(string id)
        {


            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            SiteSettingsService service = new SiteSettingsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.SiteSettings()
            {
                Id = "1",
                Key = "Key 1",
                Value = "Value 1",
                Object = new object()
            };

            var item2 = new Data.Model.SiteSettings()
            {
                Id = "2",
                Key = "Key 2",
                Value = "Value 2",
                Object = new object()
            };

            IQueryable<Data.Model.SiteSettings> list = new List<Data.Model.SiteSettings>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.Model.SiteSettings>()).Return(list);





            //ACT
            var result = service.ById("2");


            //ASSERT

            Assert.AreEqual("2", result.Id);
            Assert.AreEqual("Value 2", result.Value);

            mockRepository.VerifyAllExpectations();
         

         
        }

        [Test]
        public void ByKeyCheck(string Key)
        {



            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            SiteSettingsService service = new SiteSettingsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.SiteSettings()
            {
                Id = "1",
                Key = "Key 1",
                Value = "Value 1",
                Object = new object()
            };

            var item2 = new Data.Model.SiteSettings()
            {
                Id = "2",
                Key = "Key 2",
                Value = "Value 2",
                Object = new object()
            };

            IQueryable<Data.Model.SiteSettings> list = new List<Data.Model.SiteSettings>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.Model.SiteSettings>()).Return(list);





            //ACT
            var result = service.ByKey("Key 2");


            //ASSERT

            Assert.AreEqual("2", result.Id);
            Assert.AreEqual("Value 2", result.Value);

            mockRepository.VerifyAllExpectations();
         

         
            
        }


        [Test]
        public void GetCacheBusterCheck()
        {




            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            SiteSettingsService service = new SiteSettingsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.SiteSettings()
            {
                Id = "1",
                Key = "Key 1",
                Value = "Value 1",
                Object = new object()
            };

            var item2 = new Data.Model.SiteSettings()
            {
                Id = "2",
                Key = "CacheBuster",
                Value = "Value 2",
                Object = new object()
            };

            IQueryable<Data.Model.SiteSettings> list = new List<Data.Model.SiteSettings>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.Model.SiteSettings>()).Return(list);





            //ACT
            var result = service.GetCacheBuster();


            //ASSERT

        
            Assert.AreEqual("Value 2", result);

            mockRepository.VerifyAllExpectations();
         

         
        }

       
    }
}
