﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Core.Wrapped;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class JobRoleServiceTests
    {
        [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.JobRole()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "CEO"
            };

            var item2 = new Data.DTOs.JobRole()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Account Manager"
            };

            IQueryable<Data.DTOs.JobRole> list = new List<Data.DTOs.JobRole>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.JobRole>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            JobRoleHydrationSettings settings = new JobRoleHydrationSettings();

            //ACT
            var result = service.FindByIds(settings, "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Account Manager", result[0].JobRoleName);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByParentIDCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.JobRole()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "CEO"
            };

            var item2 = new Data.DTOs.JobRole()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Account Manager",
                ParentRoleId = "1"
            };

            var item3 = new Data.DTOs.JobRole()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Senior Executive",
                ParentRoleId = "2"
            };

            IQueryable<Data.DTOs.JobRole> list = new List<Data.DTOs.JobRole>() { item, item2, item3 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.JobRole>()).Return(list);



          
            JobRoleHydrationSettings settings = new JobRoleHydrationSettings();

            //ACT
            var result = service.FindByParentID("2", settings);


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("3", result[0].Id);
            Assert.AreEqual("Senior Executive", result[0].JobRoleName);

            mockRepository.VerifyAllExpectations();

          
        }

        [Test]
        public void FindByNameCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.JobRole()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "CEO"
            };

            var item2 = new Data.DTOs.JobRole()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Account Manager"
            };

            IQueryable<Data.DTOs.JobRole> list = new List<Data.DTOs.JobRole>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.JobRole>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            JobRoleHydrationSettings settings = new JobRoleHydrationSettings();

            //ACT
            var result = service.FindByName("Account Manager", settings);


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Account Manager", result[0].JobRoleName);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByIDCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.JobRole()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "CEO"
            };

            var item2 = new Data.DTOs.JobRole()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Account Manager"
            };

            IQueryable<Data.DTOs.JobRole> list = new List<Data.DTOs.JobRole>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.JobRole>()).Return(list);



           
            JobRoleHydrationSettings settings = new JobRoleHydrationSettings();

            //ACT
            var result = service.FindByIds(settings, "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Account Manager", result[0].JobRoleName);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetAllCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.JobRole>();
            
            var serviceWrapped = MockRepository.GenerateMock<IJobRoleServiceWrapped>();

            JobRoleService service = new JobRoleService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.JobRole()
            {
                Id = "1",

                JobRoleName ="Analyst"
            };

            var item2 = new Data.Model.JobRole()
            {
                Id = "2",

                JobRoleName = "CEO"
            };

            listItems.Add(item);
            listItems.Add(item2);


            var settings = new JobRoleHydrationSettings();
            serviceWrapped.Expect(x => x.GetAll(settings)).Return(listItems);






            //ACT
            var result = service.GetAll(settings);


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);
           
            mockRepository.VerifyAllExpectations();

        }

        [Test]
        public void GetTopLevelCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.JobRole>();
            
            var serviceWrapped = MockRepository.GenerateMock<IJobRoleServiceWrapped>();

            JobRoleService service = new JobRoleService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.JobRole()
            {
                Id = "1",

                JobRoleName = "CEO"
            };

          

            listItems.Add(item);
            

            var settings = new JobRoleHydrationSettings();
            serviceWrapped.Expect(x => x.GetTopLevel(settings)).Return(listItems);






            //ACT
            var result = service.GetTopLevel(settings);


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("1", result[0].Id);

            mockRepository.VerifyAllExpectations();
        }

      
        [Test]
        public void GetCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.JobRole()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "CEO"
            };

            var item2 = new Data.DTOs.JobRole()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Account Manager"
            };

            IQueryable<Data.DTOs.JobRole> list = new List<Data.DTOs.JobRole>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.JobRole>()).Return(list);



           
            
            //ACT
            var result = service.GetCount();


            //ASSERT

            Assert.AreEqual(2, result);
           

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetPagedOrderedByNameCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.JobRole()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "CEO"
            };

            var item2 = new Data.DTOs.JobRole()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                JobRoleName = "Account Manager"
            };

            IQueryable<Data.DTOs.JobRole> list = new List<Data.DTOs.JobRole>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.JobRole>()).Return(list);





            //ACT
            var result = service.GetPagedOrderedByName(0,10);


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Account Manager", result[0].JobRoleName);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SaveCheckNew()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);


            mockRepository.Expect(x => x.Insert(Arg<Data.Model.JobRole>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();





            //ACT
            service.Save(new Data.Model.JobRole() { JobRoleName="CEO", ParentRoleId="2"  });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }



        [Test]
        public void SaveCheckUpdate()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            JobRoleService service = new JobRoleService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);


            mockRepository.Expect(x => x.Update(Arg<Data.Model.JobRole>.Is.Anything)).Repeat.Once();





            //ACT
            service.Save(new Data.Model.JobRole() { Id="2", JobRoleName = "CEO", ParentRoleId = "2" });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }
    }
}
