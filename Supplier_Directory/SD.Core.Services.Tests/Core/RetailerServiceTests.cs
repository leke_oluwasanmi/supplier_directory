﻿using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class RetailerServiceTests
    {

        [Test]
        public void FindByIdsCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            RetailerService service = new RetailerService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Retailer()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                 RetailerName = "Retailer Name"
            };

            var item2 = new Data.DTOs.Retailer()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                RetailerName = "Retailer Name 2"
            };

            IQueryable<Data.DTOs.Retailer> list = new List<Data.DTOs.Retailer>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Retailer>()).Return(list);
            
            RetailerHydrationSettings settings = new RetailerHydrationSettings();

            //ACT
            var result = service.FindByIds(settings, "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Retailer Name 2", result[0].RetailerName);

            mockRepository.VerifyAllExpectations();

         
        }

        [Test]
        public void GetCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            RetailerService service = new RetailerService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Retailer()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                RetailerName = "Retailer Name"
            };

            var item2 = new Data.DTOs.Retailer()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                RetailerName = "Retailer Name 2"
            };

            IQueryable<Data.DTOs.Retailer> list = new List<Data.DTOs.Retailer>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Retailer>()).Return(list);

           
            //ACT
            var result = service.GetCount();


            //ASSERT

            Assert.AreEqual(2, result);
           
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetPagedOrderedByNameCheck()
        {


            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            RetailerService service = new RetailerService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Retailer()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                RetailerName = "Zet Retailer Name"
            };

            var item2 = new Data.DTOs.Retailer()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                RetailerName = "Retailer Name"
            };
            
            var item3 = new Data.DTOs.Retailer()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                RetailerName = "Home Retailer"
            };

            IQueryable<Data.DTOs.Retailer> list = new List<Data.DTOs.Retailer>() { item, item2, item3 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Retailer>()).Return(list);

            RetailerHydrationSettings settings = new RetailerHydrationSettings();

            //ACT
            var result = service.GetPagedOrderedByName(0, 10);


            //ASSERT

            Assert.AreEqual(3, result.Count());
            Assert.AreEqual("3", result[0].Id);
            Assert.AreEqual("Home Retailer", result[0].RetailerName);
            Assert.AreEqual("2", result[1].Id);
            Assert.AreEqual("Retailer Name", result[1].RetailerName);
            Assert.AreEqual("1", result[2].Id);
            Assert.AreEqual("Zet Retailer Name", result[2].RetailerName);

            mockRepository.VerifyAllExpectations();


          
        }

        [Test]
        public void SaveCheckNew()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);
            
            

            RetailerService service = new RetailerService(mockRepository);





            mockRepository.Expect(x => x.Insert(Arg<Data.Model.Retailer>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.Retailer()
            {
                RetailerName = "Retailer Name"
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void SaveCheckUpdate()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            RetailerService service = new RetailerService(mockRepository);





            mockRepository.Expect(x => x.Update(Arg<Data.Model.Retailer>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.Retailer()
            {
                Id="1",
                RetailerName = "Retailer Name"
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }
    }
}