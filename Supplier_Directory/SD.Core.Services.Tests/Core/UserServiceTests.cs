﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Services.Core;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class UserServiceTests
    {
        [Test]
         public void FindById()
            {


                EntityMapper.InitialiseMappings();
                //ARRANGE
                var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
                
            
                UserService service = new UserService(mockRepository);
                var datetime = new DateTime(2012, 10, 20);



                var item = new Data.Model.User()
                {
                    Id = "1",
                    Email ="Email@email.com" , 
                     Surname = "Surname" ,
                      Forename = "Forename"
                };

                var item2 = new Data.Model.User()
                {
                    Id = "2",
                    Email = "AnotherEmail@email.com",
                    Surname = "Joey",
                    Forename = "Tribbiani"
                };


                IQueryable<Data.Model.User> list = new List<Data.Model.User>() { item, item2 }.AsQueryable();
                mockRepository.Expect(x => x.AsQueryable<Data.Model.User>()).Return(list);




                //ACT
                var result = service.FindById("2");


                //ASSERT

     
                Assert.AreEqual("2", result.Id);
                Assert.AreEqual("AnotherEmail@email.com", result.Email);

                mockRepository.VerifyAllExpectations();
            }
        
    }
}
