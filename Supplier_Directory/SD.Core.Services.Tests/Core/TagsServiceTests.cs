﻿using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.DTOs;
using SD.Core.Services.Core;
using SD.Core.Services.Core.Wrapped;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class TagsServiceTests
    {
        //[Test]
        //public void AllCheck()
        //{


        //    //EntityMapper.InitialiseMappings();
        //    ////ARRANGE
        //    //var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
        //    //var tagsCollection = MockRepository.GenerateMock<IMongoCollection<Tags>>();
           
        //    //TagsService service = new TagsService(mockRepository);
        //    //var datetime = new DateTime(2012, 10, 20);



        //    //var item = new Data.DTOs.Tags()
        //    //{
        //    //    Id = "1",
        //    //   DisplayText= "Tag 1" ,
        //    //    UrlSlug = "urlSlug 1",
        //    //     ParentSlug = "parentSlug 1"
        //    //};

        //    //var item2 = new Data.DTOs.Tags()
        //    //{
        //    //    Id = "2",
        //    //    DisplayText = "Tag 2",
        //    //    UrlSlug = "urlSlug 2",
        //    //    ParentSlug = "parentSlug 2"
        //    //};

        //    //IQueryable<Data.DTOs.Tags> list = new List<Data.DTOs.Tags>() { item, item2 }.AsQueryable();
        //    //mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Tags>()).Return(list);

        //    //tagsCollection.InsertOne(item);
        //    //tagsCollection.InsertOne(item2);



        //    //mockRepository.Expect(x => x.GetCollection<Data.DTOs.Tags>()).Return(tagsCollection);
           

        //    ////ACT
        //    //var result = service.All();


        //    ////ASSERT

        //    //Assert.AreEqual("2", result.Count());
        //    //Assert.AreEqual("Tag 1", result[0].DisplayText);

        //    //mockRepository.VerifyAllExpectations();

           
        //}


        [Test]
        public void AllCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.Tags>();
            
            var serviceWrapped = MockRepository.GenerateMock<ITagsServiceWrapped>();

            var service = new TagsService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "tag 1"
            };

            var item2 = new Data.Model.Tags()
            {
                Id = "2",
                DisplayText = "tag 2"
            };

            listItems.Add(item);
            listItems.Add(item2);


            serviceWrapped.Expect(x => x.All()).Return(listItems);

            //ACT
            var result = service.All();


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SearchCheck()
        {


            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            
            var service = new TagsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "Tag 1",
                UrlSlug = "urlSlug 1",
                ParentSlug = "parentSlug 1"
            };

            var item2 = new Data.Model.Tags()
            {
                Id = "2",
                DisplayText = "Tag 2",
                UrlSlug = "urlSlug 2",
                ParentSlug = "parentSlug 2"
            };

            var list = new List<Data.Model.Tags>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.Model.Tags>()).Return(list);

        
            //ACT
            var result = service.Search("Tag 2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Tag 2", result[0].DisplayText);

            mockRepository.VerifyAllExpectations();


        }

         [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.Tags>();
            
            var serviceWrapped = MockRepository.GenerateMock<ITagsServiceWrapped>();

            var service = new TagsService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "tag 1"
            };

            var item2 = new Data.Model.Tags()
            {
                Id = "2",
                DisplayText = "tag 2"
            };

            listItems.Add(item);
            listItems.Add(item2);


            serviceWrapped.Expect(x => x.FindByIds("1", "2")).Return(listItems);

            //ACT
            var result = service.FindByIds("1", "2");


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);

            mockRepository.VerifyAllExpectations();


        }

         [Test]
         public void FindByIdCheck(string id)
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var serviceWrapped = MockRepository.GenerateMock<ITagsServiceWrapped>();

            var service = new TagsService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "tag 1"
            };

       


            serviceWrapped.Expect(x => x.FindById("1")).Return(item);

            //ACT
            var result = service.FindById("1");


            //ASSERT

            Assert.AreEqual("1", result.Id);

            mockRepository.VerifyAllExpectations();
        }

         [Test]
         public void FindByUrlSlugCheck(string UrlSlug)
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var serviceWrapped = MockRepository.GenerateMock<ITagsServiceWrapped>();

            var service = new TagsService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "tag 1",
                UrlSlug = "urlSlug"
            };




            serviceWrapped.Expect(x => x.FindByUrlSlug("urlSlug")).Return(item);

            //ACT
            var result = service.FindByUrlSlug("urlSlug");


            //ASSERT

            Assert.AreEqual("1", result.Id);

            mockRepository.VerifyAllExpectations();
        }


         [Test]
         public void FindByUrlSlugsCheck()
         {
             EntityMapper.InitialiseMappings();
             //ARRANGE
             var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
             var listItems = new List<Data.Model.Tags>();
             
             var serviceWrapped = MockRepository.GenerateMock<ITagsServiceWrapped>();

             var service = new TagsService(mockRepository, serviceWrapped);

             var datetime = new DateTime(2012, 10, 20);



             var item = new Data.Model.Tags()
             {
                 Id = "1",
                 DisplayText = "tag 1",
                  UrlSlug = "urlSlug"
             };

             var item2 = new Data.Model.Tags()
             {
                 Id = "2",
                 DisplayText = "tag 2",
                 UrlSlug = "urlSlug 2"
             };

             listItems.Add(item);
             listItems.Add(item2);


             serviceWrapped.Expect(x => x.FindByUrlSlug(new string[] { "urlSlug" })).Return(listItems);

             //ACT
             var result = service.FindByUrlSlug(new string[] { "urlSlug" });


             //ASSERT

             Assert.AreEqual(2, result.Count());
             Assert.AreEqual("1", result[0].Id);

             mockRepository.VerifyAllExpectations();


         }

         [Test]
         public void FindByDisplayTextCheck(string displayText)
        {
             
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var serviceWrapped = MockRepository.GenerateMock<ITagsServiceWrapped>();

            var service = new TagsService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "tag 1",
                UrlSlug = "urlSlug"
            };




            serviceWrapped.Expect(x => x.FindByDisplayText("tag 1")).Return(item);

            //ACT
            var result = service.FindByDisplayText("tag 1");


            //ASSERT

            Assert.AreEqual("1", result.Id);

            mockRepository.VerifyAllExpectations();
        
        }

       

        /*
         [Test]
         public void SaveCheckNew()
         {
             EntityMapper.InitialiseMappings();
             //ARRANGE
             var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
             var service = new TagsService(mockRepository);
             var datetime = new DateTime(2012, 10, 20);


             mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Tags>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();





             //ACT
             service.Save(new Data.Model.Tags() {  DisplayText = "Tag Text", UrlSlug = "urlSlug" });


             //ASSERT

             mockRepository.VerifyAllExpectations();
         }



         [Test]
         public void SaveCheckUpdate()
         {
             EntityMapper.InitialiseMappings();
             //ARRANGE
             var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
             var service = new TagsService(mockRepository);
             var datetime = new DateTime(2012, 10, 20);


             mockRepository.Expect(x => x.Update(Arg<Data.DTOs.Tags>.Is.Anything)).Repeat.Once();





             //ACT
             service.Save(new Data.Model.Tags() { Id="2", DisplayText = "Tag Text", UrlSlug = "urlSlug" });


             //ASSERT

             mockRepository.VerifyAllExpectations();
         }

        */
         [Test]
         public void InsertCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            
            var service = new TagsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);


            mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Tags>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();





            //ACT
            service.Insert(new Data.Model.Tags() { Id = "2", DisplayText = "Tag Text", UrlSlug = "urlSlug" });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }

         [Test]
         public void DeleteCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var tagsCollection = MockRepository.GenerateMock<IMongoCollection<Data.Model.Tags>>();

            var service = new TagsService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Tags()
            {
                Id = "1",
                DisplayText = "Tag 1",
                UrlSlug = "urlSlug 1",
                ParentSlug = "parentSlug 1"
            };

            var item2 = new Data.Model.Tags()
            {
                Id = "2",
                DisplayText = "Tag 2",
                UrlSlug = "urlSlug 2",
                ParentSlug = "parentSlug 2"
            };

           
            tagsCollection.InsertOne(item);
            tagsCollection.InsertOne(item2);



            mockRepository.Expect(x => x.GetCollection<Data.Model.Tags>()).Return(tagsCollection);



            //TagsService service = new TagsService(mockRepository);
            //var datetime = new DateTime(2012, 10, 20);


            //tagsCollection.Expect(x => x.DeleteOne(Arg<Data.DTOs.Tags>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();
            tagsCollection.Expect(x => x.DeleteOne(Arg<FilterDefinition<Data.Model.Tags>>.Is.Anything, Arg<CancellationToken>.Is.Anything)).Repeat.Once();





            //ACT
            service.Delete("1");


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }


    }
}
