﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Core.Wrapped;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class MediaServiceTests
    {

      [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            MediaService service = new MediaService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Media()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                 Title = "Media Title 1"
            };

            var item2 = new Data.DTOs.Media()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Second"
            };

            IQueryable<Data.DTOs.Media> list = new List<Data.DTOs.Media>() { item, item2 }.AsQueryable();
            
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Media>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            MediaHydrationSettings settings = new MediaHydrationSettings();

            //ACT
            var result = service.FindByIds(settings, "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Media Title Second", result[0].Title);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByImportIdCheck()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            MediaService service = new MediaService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Media()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title 1"
            };

            var item2 = new Data.DTOs.Media()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Second",
                ImportId = "123"
            };

            var item3 = new Data.DTOs.Media()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Third",
                ImportId = "1234"
            };

            IQueryable<Data.DTOs.Media> list = new List<Data.DTOs.Media>() { item, item2 }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Media>()).Return(list);




            MediaHydrationSettings settings = new MediaHydrationSettings();

            //ACT
            var result = service.FindByImportId("123");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Media Title Second", result[0].Title);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetBySlugCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            MediaService service = new MediaService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Media()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title 1",
                UrlSlug = "urlSlug1"
            };

            var item2 = new Data.DTOs.Media()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Second",
                ImportId = "123",
                UrlSlug = "urlSlug2"
            };

            var item3 = new Data.DTOs.Media()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Third",
                ImportId = "1234",
                UrlSlug = "urlSlug3"
            };

            IQueryable<Data.DTOs.Media> list = new List<Data.DTOs.Media>() { item, item2, item3 }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Media>()).Return(list);




            MediaHydrationSettings settings = new MediaHydrationSettings();

            //ACT
            var result = service.GetBySlug("urlSlug2", settings);


            //ASSERT

           
            Assert.AreEqual("2", result.Id);
            Assert.AreEqual("Media Title Second", result.Title);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByYouTubeIdCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            MediaService service = new MediaService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Media()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title 1",
                UrlSlug = "urlSlug1",
                RemoteItemCode = "youtubeId1"
            };

            var item2 = new Data.DTOs.Media()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Second",
                ImportId = "123",
                UrlSlug = "urlSlug2",
                RemoteItemCode = "youtubeId2"
            };

            var item3 = new Data.DTOs.Media()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Title = "Media Title Third",
                ImportId = "1234",
                UrlSlug = "urlSlug3",
                RemoteItemCode = "youtubeId3"
            };

            IQueryable<Data.DTOs.Media> list = new List<Data.DTOs.Media>() { item, item2, item3 }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Media>()).Return(list);




            MediaHydrationSettings settings = new MediaHydrationSettings();

            //ACT
            var result = service.FindByYouTubeId("youtubeId2", settings);


            //ASSERT


            Assert.AreEqual("2", result.Id);
            Assert.AreEqual("Media Title Second", result.Title);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetPagedOrderedByCreatedOnCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.Media>();
            
            var serviceWrapped = MockRepository.GenerateMock<IMediaServiceWrapped>();

            MediaService service = new MediaService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Media()
            {
                Id = "1",
                 Title = "Media First"
            };

            var item2 = new Data.Model.Media()
            {
                Id = "2",
                Title = "Media Second"
            };

            listItems.Add(item);
            listItems.Add(item2);


            var settings = new MediaHydrationSettings();
            serviceWrapped.Expect(x => x.GetPagedOrderedByCreatedOn(0,10)).Return(listItems);






            //ACT
            var result = service.GetPagedOrderedByCreatedOn(0, 10);


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);

            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void GetPagedCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            var serviceWrapped = MockRepository.GenerateMock<IMediaServiceWrapped>();

            MediaService service = new MediaService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var settings = new MediaHydrationSettings();
            serviceWrapped.Expect(x => x.GetPagedCount()).Return(2);






            //ACT
            var result = service.GetPagedCount();


            //ASSERT

            Assert.AreEqual(2, result);
           

            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void GetForDayCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var listItems = new List<Data.Model.Media>();
            
            var serviceWrapped = MockRepository.GenerateMock<IMediaServiceWrapped>();

            MediaService service = new MediaService(mockRepository, serviceWrapped);

            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.Model.Media()
            {
                Id = "1",
                Title = "Media First"
            };

            var item2 = new Data.Model.Media()
            {
                Id = "2",
                Title = "Media Second"
            };

            listItems.Add(item);
            listItems.Add(item2);


            var settings = new MediaHydrationSettings();
            serviceWrapped.Expect(x => x.GetForDay(datetime, 10)).Return(listItems);

            //ACT
            var result = service.GetForDay(datetime, 10);


            //ASSERT

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("1", result[0].Id);

            mockRepository.VerifyAllExpectations();
        }


    }
}
