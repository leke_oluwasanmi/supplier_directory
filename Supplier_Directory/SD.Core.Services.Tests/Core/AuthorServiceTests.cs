﻿using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.Core
{
    [TestFixture]
    public class AuthorServiceTests
    {
        [Test]
        public void FindByIdsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthorService service = new AuthorService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Author()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                 Forename = "Joey",
                  Surname = "Tribbiani"
            };

            var item2 = new Data.DTOs.Author()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Rachel",
                Surname = "Green"
            };

            IQueryable<Data.DTOs.Author> list = new List<Data.DTOs.Author>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Author>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

            AuthorHydrationSettings settings = new AuthorHydrationSettings();

            //ACT
            var result = service.FindByIds(settings, "2");


            //ASSERT

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("2", result[0].Id);
            Assert.AreEqual("Rachel", result[0].Forename);

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            AuthorService service = new AuthorService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);




            var item = new Data.DTOs.Author()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Joey",
                Surname = "Tribbiani"
            };

            var item2 = new Data.DTOs.Author()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Rachel",
                Surname = "Green"
            };

            IQueryable<Data.DTOs.Author> list = new List<Data.DTOs.Author>() { item, item2 }.AsQueryable();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Author>()).Return(list);

           
            //ACT
            var result = service.GetCount();


            //ASSERT
            Assert.AreEqual(2, result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SearchCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            AuthorService service = new AuthorService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);



            var item = new Data.DTOs.Author()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Joey",
                Surname = "Tribbiani",
                 DisplayName = "Joe"
            };

            var item2 = new Data.DTOs.Author()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Rachel",
                Surname = "Green",
                DisplayName = "Rachel"
            };

            var item3 = new Data.DTOs.Author()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Joey",
                Surname = "Geller" ,
                DisplayName = "Joey"
            };

            IQueryable<Data.DTOs.Author> list = new List<Data.DTOs.Author>() { item, item2, item3 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Author>()).Return(list);



            //mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();

           
            //ACT
            var result = service.Search("Joey",true,true,true, 10, new AuthorHydrationSettings() { CreatedBy = new UserHydrationSettings() { }, UpdatedBy = new UserHydrationSettings() });


            //ASSERT
            Assert.AreEqual(2, result.Count());
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetPagedOrderedBySurnameCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            
            AuthorService service = new AuthorService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);




            var item = new Data.DTOs.Author()
            {
                Id = "1",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Joey",
                Surname = "Tribbiani",
                DisplayName = "Tribbiani"
            };

            var item2 = new Data.DTOs.Author()
            {
                Id = "2",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Rachel",
                Surname = "Green",
                DisplayName = "Rachel"
            };

            var item3 = new Data.DTOs.Author()
            {
                Id = "3",
                CreatedOn = datetime,
                UpdatedOn = datetime,
                Forename = "Joey",
                Surname = "Geller",
                DisplayName = "Joey"
            };

            IQueryable<Data.DTOs.Author> list = new List<Data.DTOs.Author>() { item, item2, item3 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Author>()).Return(list);



            
            //ACT
            var result = service.GetPagedOrderedBySurname(0, 10);


            //ASSERT
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual("Joey", result[0].DisplayName);
            Assert.AreEqual("Rachel", result[1].DisplayName);
            Assert.AreEqual("Tribbiani", result[2].DisplayName);
            mockRepository.VerifyAllExpectations();
        }

    }
}
