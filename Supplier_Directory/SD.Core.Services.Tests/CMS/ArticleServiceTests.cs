﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SD.Core.Services.Caching;
using SD.Core.Data;
using Rhino.Mocks;
using SD.Core.Data.DTOs;
using SD.Core.Services.CMS;
using SD.Core.Services.Helpers;
using SD.Core.Common.Constants;
//using VWG.Core.Common.Config;

namespace SD.Core.Services.Tests
{
    [TestFixture]
    public class ArticleServiceTests
    {

        [SetUp]
        public void SetupInit()
        {
          
        }
        [Test]
        public void SaveCheckNewArticle()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);


            ArticleService service = new ArticleService(mockRepository);
            var key = new Data.DTOs.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
            mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Article>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.Article()
            {
                Title = "New Title",
                ArticleType = Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                MetaDescription = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published,
                Id = ""
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();

        }


        [Test]
        public void SaveCheckExistingArticle()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);


            ArticleService service = new ArticleService(mockRepository);
            var key = new Data.DTOs.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
            mockRepository.Expect(x => x.UpdateWithComment(Arg<Data.DTOs.Article>.Is.Anything, Arg<string>.Is.Anything)).Return(null).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.Article()
            {
                Title = "New Title",
                ArticleType = Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                MetaDescription = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published,
                Id = "12"
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();

        }

        //--------------------------

        [Test]
        public void AddRelatedMediaCheckSimpleadd()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);


            var key = new Data.DTOs.Article();

            key.Id = "1";
            key.ArticleType = SD.Core.Common.Constants.ArticleType.article;
            key.Body = "";
            key.BrandArticleNumber = "1";
            key.CreatedOn = datetime;
            key.Title = "Beautifull wheather";
            key.SEOTitle = "Nice";
            key.PlainTextBody = "Body";
            key.UrlSlug = null;
            key.Status = Common.Constants.PublicationStatus.Published;
            key.RelatedMediaIds = new List<string>() { };

            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
            mockRepository.Expect(x => x.Update<Data.DTOs.Article>(Arg<Data.DTOs.Article>.Is.Anything)).Return(null).Repeat.Once();


            //ACT
            SD.Core.Services.CMS.ArticleService service = new SD.Core.Services.CMS.ArticleService(mockRepository);

            service.AddRelatedMedia("1", "2");


            //ASSERT
            Assert.IsTrue(key.RelatedMediaIds.Contains("2"));
            mockRepository.VerifyAllExpectations();
        }


        //-----------------------------------

        
        [Test]
        public void DeleteByIdCheckSimpleadd()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);

            
            var key = new Data.DTOs.Article();
            
                key.Id="1";
                key.ArticleType = SD.Core.Common.Constants.ArticleType.article;
                key.Body = "";
                key.BrandArticleNumber = "1";
                key.CreatedOn = datetime;
                key.Title = "Beautifull wheather";
                key.SEOTitle = "Nice";
                key.PlainTextBody = "Body";
                key.UrlSlug = null;
                key.Status = Common.Constants.PublicationStatus.Published;
                key.RelatedMediaIds = new List<string>() { };
            
            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
            mockRepository.Expect(x => x.Update<Data.DTOs.Article>(Arg<Data.DTOs.Article>.Is.Anything)).Return(null).Repeat.Once();
            
           
            //ACT
            SD.Core.Services.CMS.ArticleService service = new SD.Core.Services.CMS.ArticleService(mockRepository);
            
            service.DeleteById("1");


            //ASSERT
            Assert.AreEqual(key.Status, PublicationStatus.Deleted);
            mockRepository.VerifyAllExpectations();
        }
        

    }
}
