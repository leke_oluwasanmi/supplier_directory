﻿using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Services.CMS;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.CMS
{
    
    [TestFixture]
    public class AuthenticatedUserServiceTests
    {
        [Test]
        public void DeleteByIdCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            var authenticatedUsers = MockRepository.GenerateMock<IMongoCollection<AuthenticatedUser>>();
            var filter2 = Builders<AuthenticatedUser>.Filter.Eq("Id", "1");
            
            
            AuthenticatedUserService service = new AuthenticatedUserService(mockRepository);
            var key = new Data.Model.AuthenticatedUser()
            {
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                 Id = "1"
            };

            var key2 = new Data.Model.AuthenticatedUser()
            {
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                Id = "2"
            };
          
            authenticatedUsers.InsertMany(new [] {key,key2});

            mockRepository.Expect(x => x.GetCollection<AuthenticatedUser>()).Return(authenticatedUsers);
            authenticatedUsers.Expect(x => x.DeleteOne(Arg<FilterDefinition<AuthenticatedUser>>.Is.Anything, Arg<CancellationToken>.Is.Anything)).Repeat.Once();

            //ACT
            service.DeleteById("1");


            //ASSERT


            //Assert.AreEqual(1, authenticatedUsers.Count(filter2));
            mockRepository.VerifyAllExpectations();




          
        }

       
    }
}
