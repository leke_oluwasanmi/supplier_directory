﻿using MongoDB.Driver;
using NUnit.Framework;
using Omu.Encrypto;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Services.CMS;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.CMS
{
    [TestFixture]
    public class UserServiceTests
    {

        [Test]
        public void SaveCheckNewUser()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            UserService service = new UserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);

            mockRepository.Expect(x => x.Insert(Arg<Data.Model.User>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.User()
            {
                Email="email@email.com",
                 CreatedOn = datetime,
                  Forename="John",
                    Password = "pass",
                     Surname="Michael",
                      UpdatedOn = datetime
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SaveCheckUpdateUser()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            
            UserService service = new UserService(mockRepository);
            var datetime = new DateTime(2012, 10, 20);

            mockRepository.Expect(x => x.Update(Arg<Data.Model.User>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void SaveCheckUserWithoutPassword()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);

            mockRepository.Expect(x => x.Update(Arg<Data.Model.User>.Is.Anything)).Return(replaceOneResult1).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
               
                Surname = "Michael",
                UpdatedOn = datetime
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }


        //--------------------
        [Test]
        public void DeleteByIdCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var users = MockRepository.GenerateMock<IMongoCollection<User>>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };

            users.InsertMany(new[] { itemArticle, item2Article });

            mockRepository.Expect(x => x.GetCollection<User>()).Return(users);
            users.Expect(x => x.DeleteOne(Arg<FilterDefinition<User>>.Is.Anything, Arg<CancellationToken>.Is.Anything)).Repeat.Once();


         
            //ACT
            service.DeleteById("1");


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindByEmailCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);
            
            

            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
            //mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Media>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            var user = service.FindByEmail("emailakar@email.com");


            //ASSERT
            Assert.AreEqual("2", user.Id);
            mockRepository.VerifyAllExpectations();
        }

        //-----------

        [Test]
        public void FindWithCredentialsCheckNormal()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);

            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password =  hasher.Encrypt("password1234"),
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password =  hasher.Encrypt("password1234"),
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
          

            //ACT
            var result = service.FindWithCredentials("email@email.com", "password1234");


            //ASSERT
            Assert.AreEqual(list.FirstOrDefault(), result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindWithCredentialsCheckEmptyPassword()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);

            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.FindWithCredentials("email@email.com", "");


            //ASSERT
            Assert.IsNull(result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void FindWithCredentialsCheckIncorrectPassword()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);

            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.FindWithCredentials("email@email.com", "umatchedPassword");


            //ASSERT
            Assert.IsNull(result);
            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void FindWithCredentialsCheckPermissions()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);

            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Michael",
                UpdatedOn = datetime ,
                Permissions = new List<Permission> (){ Permission.EditMedia , Permission.UseCMS, Permission.ViewMedia }
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Akar",
                UpdatedOn = datetime,
               
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.FindWithCredentials("email@email.com", "password1234", Permission.UseCMS, Permission.ViewMedia);


            //ASSERT
            Assert.AreEqual(list.FirstOrDefault(), result);
            mockRepository.VerifyAllExpectations();
        }



        [Test]
        public void FindWithCredentialsCheckPermissionsNotFound()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);

            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Michael",
                UpdatedOn = datetime,
                Permissions = new List<Permission>() { Permission.EditMedia, Permission.UseCMS, Permission.ViewMedia }
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = hasher.Encrypt("password1234"),
                Surname = "Akar",
                UpdatedOn = datetime,

            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.FindWithCredentials("email@email.com", "password1234", Permission.UseCMS, Permission.ViewMedia, Permission.EditUsers);


            //ASSERT
            Assert.IsNull(result);
            mockRepository.VerifyAllExpectations();
        }

        //-----------------

        [Test]
        public void GetPagedOrderedBySurnameCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();
            var expected = new List<Data.Model.User>(){item2Article,itemArticle};

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
            //mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Media>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            IList<User> result = service.GetPagedOrderedBySurname(0,10);


            //ASSERT
            Assert.AreEqual(expected, result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void GetCountCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
           

            //ACT
            var result = service.GetCount();


            //ASSERT
            Assert.AreEqual(2, result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SearchCheckNotFoundTest()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();
            var expected = new List<User>() { };

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
            

            //ACT
            var result = service.Search("Michael",true,false,true,10);


            //ASSERT
            Assert.AreEqual(expected, result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void SearchCheckFoundTest()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();
            var expected = new List<User>() { itemArticle };

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.Search("Michael", true, true, true, 10);


            //ASSERT
            Assert.AreEqual(expected, result);
            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void SearchCheckFoundEmailTest()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();
            var expected = new List<User>() { itemArticle };

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.Search("l@email.com", true, true, true, 10);


            //ASSERT
            Assert.AreEqual(expected, result);
            mockRepository.VerifyAllExpectations();
        }
        [Test]
        public void SearchCheckFoundContainEmailTest2Found()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();
            var expected = new List<User>() { itemArticle, item2Article };

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.Search("@email.com", true, true, true, 10);


            //ASSERT
            Assert.AreEqual(expected, result);
            mockRepository.VerifyAllExpectations();
        }

        //-------

        [Test]
        public void UsernameExistsCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
          

            //ACT
            var result=service.UsernameExists("email@email.com");


            //ASSERT
            Assert.IsTrue(result);
            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void UsernameExistsCheckDoesNotExists()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.UsernameExists("emailnotfound@email.com");


            //ASSERT
            Assert.IsFalse(result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void UsernameExistsCheckDoesNotExistsExcluded()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemArticle = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };
            var item2Article = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };
            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);


            //ACT
            var result = service.UsernameExists("email@email.com", "1");


            //ASSERT
            Assert.IsFalse(result);
            mockRepository.VerifyAllExpectations();
        }
        //-------------

        [Test]
        public void CreatePasswordResetTicketCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var itemUser = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };

            var item2User = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };


            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemUser, item2User }.AsQueryable();


            var passTicket = new Data.DTOs.PasswordResetTicket()
            {
                Id = "1",
                UserId = "1",
                CreatedOn = datetime,
                 Key="key1"
              
            };

            var passTicket2 = new Data.DTOs.PasswordResetTicket()
            {
                Id = "2",
               UserId="1",
                CreatedOn = datetime,
                Key = "key2"
              
            };

            IQueryable<Data.DTOs.PasswordResetTicket> listTickets = new List<Data.DTOs.PasswordResetTicket>() { passTicket, passTicket2 }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
            mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.PasswordResetTicket>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.PasswordResetTicket>()).Return(listTickets);

            //ACT
            service.CreatePasswordResetTicket("email@email.com");


            //ASSERT

            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void PasswordResetTicketIsValidCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var passTicket = new Data.DTOs.PasswordResetTicket()
            {
                Id = "1",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key1"

            };

            var passTicket2 = new Data.DTOs.PasswordResetTicket()
            {
                Id = "2",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key2"

            };

            IQueryable<Data.DTOs.PasswordResetTicket> listTickets = new List<Data.DTOs.PasswordResetTicket>() { passTicket, passTicket2 }.AsQueryable();

          
             mockRepository.Expect(x => x.AsQueryable<Data.DTOs.PasswordResetTicket>()).Return(listTickets);

            //ACT
             var result = service.PasswordResetTicketIsValid("key2");


            //ASSERT
             Assert.IsTrue(result);
            mockRepository.VerifyAllExpectations();
        }


        [Test]
        public void PasswordResetTicketIsValidCheckIsFalse()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            UserService service = new UserService(mockRepository);



            var passTicket = new Data.DTOs.PasswordResetTicket()
            {
                Id = "1",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key1"

            };

            var passTicket2 = new Data.DTOs.PasswordResetTicket()
            {
                Id = "2",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key2"

            };

            IQueryable<Data.DTOs.PasswordResetTicket> listTickets = new List<Data.DTOs.PasswordResetTicket>() { passTicket, passTicket2 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.PasswordResetTicket>()).Return(listTickets);

            //ACT
            var result = service.PasswordResetTicketIsValid("key21");


            //ASSERT
            Assert.IsFalse(result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void PasswordResetTicketIsActiveCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = DateTime.Now;


            UserService service = new UserService(mockRepository);



            var passTicket = new Data.DTOs.PasswordResetTicket()
            {
                Id = "1",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key1"

            };

            var passTicket2 = new Data.DTOs.PasswordResetTicket()
            {
                Id = "2",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key2"

            };

            IQueryable<Data.DTOs.PasswordResetTicket> listTickets = new List<Data.DTOs.PasswordResetTicket>() { passTicket, passTicket2 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.PasswordResetTicket>()).Return(listTickets);

            //ACT
            var result = service.PasswordResetTicketIsActive("key2");


            //ASSERT
            Assert.IsTrue(result);
            mockRepository.VerifyAllExpectations();
        }

        [Test]
        public void ChangeUserPasswordCheck()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = DateTime.Now;


            var itemUser = new Data.Model.User()
            {
                Id = "1",
                Email = "email@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Michael",
                UpdatedOn = datetime
            };

            var item2User = new Data.Model.User()
            {
                Id = "2",
                Email = "emailakar@email.com",
                CreatedOn = datetime,
                Forename = "John",
                Password = "pass",
                Surname = "Akar",
                UpdatedOn = datetime
            };


            IQueryable<Data.Model.User> list = new List<Data.Model.User>() { itemUser, item2User }.AsQueryable();

            UserService service = new UserService(mockRepository);



            var passTicket = new Data.DTOs.PasswordResetTicket()
            {
                Id = "1",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key1"

            };

            var passTicket2 = new Data.DTOs.PasswordResetTicket()
            {
                Id = "2",
                UserId = "1",
                CreatedOn = datetime,
                Key = "key2"

            };


            IQueryable<Data.DTOs.PasswordResetTicket> listTickets = new List<Data.DTOs.PasswordResetTicket>() { passTicket, passTicket2 }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<User>()).Return(list);
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.PasswordResetTicket>()).Return(listTickets);
            mockRepository.Expect(x => x.Update(Arg<Data.Model.User>.Is.Anything)).Return(replaceOneResult1).Repeat.Once();
            mockRepository.Expect(x => x.Update(Arg<Data.DTOs.PasswordResetTicket>.Is.Anything)).Return(replaceOneResult1).Repeat.Once();
            
            //ACT
            service.ChangeUserPassword("key2","newPassword");

              
            //ASSERT
           
            mockRepository.VerifyAllExpectations();
        }
    }
}
