﻿using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Services.CMS;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.CMS
{
    [TestFixture]
    public class RetailerServiceTests
    {
        [Test]
        public void DeleteByIdCheck()
        {
          
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var datetime = new DateTime(2012, 10, 20);
            var retailers = MockRepository.GenerateMock<IMongoCollection<Retailer>>();
            
            
            RetailerService service = new RetailerService(mockRepository);
            var key = new Data.Model.Retailer()
            {
                CreatedOn = datetime,
                 RetailerName = "retailer",
                Id = "1"
            };
            var key2 = new Data.Model.Retailer()
            {
                CreatedOn = datetime,
                RetailerName = "saisbury",
                Id = "2"
            };

            retailers.InsertMany(new[] { key, key2 });

            mockRepository.Expect(x => x.GetCollection<Retailer>()).Return(retailers);
            retailers.Expect(x => x.DeleteOne(Arg<FilterDefinition<Retailer>>.Is.Anything, Arg<CancellationToken>.Is.Anything)).Repeat.Once();


            //ACT

            service.DeleteById("1");


            //ASSERT

            mockRepository.VerifyAllExpectations();

        }

       
    }
}
