﻿using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Services.CMS;
using SD.Core.Services.Helpers;

namespace SD.Core.Services.Tests.CMS
{
    [TestFixture]
    public class MediaServiceTests
    {



        [SetUp]
        public void SetupInit()
        {

        }

        [Test]
        public void SaveCheckNew()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);
            
            

            MediaService service = new MediaService(mockRepository);



            var itemArticle = new Data.DTOs.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2Article = new Data.DTOs.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { itemArticle, item2Article }.AsQueryable();


            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
            mockRepository.Expect(x => x.Insert(Arg<Data.DTOs.Media>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.Media()
            {
                Title = "New Title",

                CreatedOn = datetime,
                MetaDescription = "Beautifull wheather",
                SEOTitle = "Nice",

                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();

        }


        [Test]
        public void SaveCheckExisting()
        {
            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            MediaService service = new MediaService(mockRepository);

            

            var itemArticle = new Data.DTOs.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2Article = new Data.DTOs.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { itemArticle, item2Article }.AsQueryable();

            
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
            mockRepository.Expect(x => x.UpdateWithComment(Arg<Data.DTOs.Media>.Is.Anything, Arg<string>.Is.Anything)).Return(replaceOneResult1).Repeat.Once();


            //ACT
            service.Save(new SD.Core.Data.Model.Media()
            {
                Title = "New Title",

                CreatedOn = datetime,
                MetaDescription = "Beautifull wheather",
                SEOTitle = "Nice",

                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published,
                Id = "1"
            });


            //ASSERT

            mockRepository.VerifyAllExpectations();

        }


        //[Test]
        //public void SaveCheckExisting()
        //{
        //    EntityMapper.InitialiseMappings();
        //    //ARRANGE
        //    var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
        //    var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
        //    var datetime = new DateTime(2012, 10, 20);


        //    MediaService service = new MediaService(mockRepository);



        //    var itemArticle = new Data.DTOs.Article()
        //    {
        //        ArticleType = VWG.Core.Common.Constants.ArticleType.general,
        //        Body = "",
        //        BrandArticleNumber = "1",
        //        CreatedOn = datetime,
        //        Title = "Beautifull wheather",
        //        SEOTitle = "Nice",
        //        PlainTextBody = "Body",
        //        UrlSlug = "urlSlug",
        //        Status = Common.Constants.PublicationStatus.Published
        //    };
        //    var item2Article = new Data.DTOs.Article()
        //    {
        //        Id = "2",
        //        ArticleType = VWG.Core.Common.Constants.ArticleType.general,
        //        Body = "",
        //        BrandArticleNumber = "1",
        //        CreatedOn = datetime,
        //        Title = "Beautifull weather",
        //        SEOTitle = "Nice",
        //        PlainTextBody = "Body",
        //        UrlSlug = null,
        //        Status = Common.Constants.PublicationStatus.Published
        //    };
        //    IQueryable<Data.DTOs.Article> list = new List<Data.DTOs.Article>() { }.AsQueryable();

        //    var media = new VWG.Core.Data.Model.Media()
        //    {
        //        Title = "New Title",
        //        CreatedOn = datetime,
        //        MetaDescription = "Beautifull wheather",
        //        SEOTitle = "Nice",
        //        LiveFrom = datetime,
        //        UrlSlug = "second urlSlug ",
        //        Status = Common.Constants.PublicationStatus.Published,
        //        Id = "1"
        //    };
        //    //media.UrlSlug = media.Title.GenerateSlug();
        //    //mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(list);
        //    mockRepository.Expect(x => x.UpdateWithComment(service.deHydrate(media))).Repeat.Once();


        //    //ACT
        //    service.Save(media);


        //    //ASSERT

        //    mockRepository.VerifyAllExpectations();

        //}
        //--------------------


        [Test]
        public void DeleteByIdCheckSoftDelete()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            MediaService service = new MediaService(mockRepository);



            var item = new Data.DTOs.Media()
            {
                Id = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
              
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.DTOs.Media()
            {
               Id="2",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
              
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Media> list = new List<Data.DTOs.Media>() { item, item2 }.AsQueryable();
            var itemArticle = new Data.DTOs.Article()
            {
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2Article = new Data.DTOs.Article()
            {
                Id = "2",
                ArticleType = SD.Core.Common.Constants.ArticleType.article,
                Body = "",
                BrandArticleNumber = "1",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",
                PlainTextBody = "Body",
                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Article> listArticle = new List<Data.DTOs.Article>() { itemArticle, item2Article }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Article>()).Return(listArticle);
           
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Media>()).Return(list);
            mockRepository.Expect(x => x.UpdateWithComment(Arg<Data.DTOs.Media>.Is.Anything, Arg<string>.Is.Anything)).Return(replaceOneResult1).Repeat.Once();


            //ACT

            service.DeleteById("1");


            //ASSERT

            mockRepository.VerifyAllExpectations();

          
        }





        [Test]
        public void DeleteByIdCheckSoftIsFalseDelete()
        {

            EntityMapper.InitialiseMappings();
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoRepository1>();
            var replaceOneResult1 = MockRepository.GenerateStub<ReplaceOneResult>();
            var datetime = new DateTime(2012, 10, 20);


            MediaService service = new MediaService(mockRepository);



            var item = new Data.DTOs.Media()
            {
                Id = "1",
                CreatedOn = datetime,
                Title = "Beautifull wheather",
                SEOTitle = "Nice",

                UrlSlug = "urlSlug",
                Status = Common.Constants.PublicationStatus.Published
            };
            var item2 = new Data.DTOs.Media()
            {
                Id = "2",
                CreatedOn = datetime,
                Title = "Beautifull weather",
                SEOTitle = "Nice",

                UrlSlug = null,
                Status = Common.Constants.PublicationStatus.Published
            };
            IQueryable<Data.DTOs.Media> list = new List<Data.DTOs.Media>() { item, item2 }.AsQueryable();
          
            mockRepository.Expect(x => x.AsQueryable<Data.DTOs.Media>()).Return(list);
            mockRepository.Expect(x => x.Remove(Arg<Data.DTOs.Media>.Is.Anything, Arg<string>.Is.Anything)).Repeat.Once();


            //ACT

            service.DeleteById("1",false);


            //ASSERT

            mockRepository.VerifyAllExpectations();


        }



     
    }
}
