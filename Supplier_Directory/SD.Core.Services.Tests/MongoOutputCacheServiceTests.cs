﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SD.Core.Services.Caching;
using SD.Core.Data;
using Rhino.Mocks;
using SD.Core.Data.DTOs;
using System.IO;

namespace SD.Core.Services.Tests
{
    [TestFixture]
    public class MongoOutputCacheServiceTests
    {


        [Test]
        public void FindByKeyCheckSimpleKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime= new DateTime(2012,10,20);
            var itemTest = new byte[200] ;
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);
            
            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindByKey("key");

           
            //ASSERT

            Assert.AreEqual(memStream.ToArray(), result);
          
        }

        [Test]
        public void FindByKeyCheckNotFoundKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
           

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() {  }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindByKey("key");


            //ASSERT

            Assert.AreEqual(null, result);

        }

        [Test]
        public void FindByKeyCheckExpiredKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MinValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindByKey("key");


            //ASSERT

            Assert.AreEqual(null, result);
           

        }



        [Test]
        public void FindExistingCheckSimpleKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindExistingKey("key");


            //ASSERT

            Assert.IsFalse( result);

        }

        [Test]
        public void FindExistingCheckNotFoundKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();


            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindExistingKey("key");


            //ASSERT

            Assert.IsTrue(result);

        }

        //------------------------

        [Test]
        public void FindExistingUnexpiredCheckSimpleKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindExistingUnexpired("key");


            //ASSERT

            Assert.AreEqual(memStream.ToArray(), result);

        }

        [Test]
        public void FindExistingUnexpiredCheckNotFoundKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();


            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindExistingUnexpired("key");


            //ASSERT

            Assert.AreEqual(null, result);

        }

        [Test]
        public void FindExistingUnexpiredCheckExpiredKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MinValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);


            //ACT
            var result = service.FindExistingUnexpired("key");


            //ASSERT

            Assert.AreEqual(null, result);


        }

        //--------------------------

        [Test]
        public void InsertCheckNotInsertingAlreadyExistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();
          
            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Insert(Arg<MongoOutputCacheItem>.Is.Anything)).Repeat.Never();
            //ACT
            service.Insert("key", MongoOutputCacheService.Serialize(memStream.ToArray()),DateTime.MaxValue);


            //ASSERT
            mockRepository.VerifyAllExpectations();

           

        }

        [Test]
        public void InsertCheckSimpleNonexistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Insert(Arg<MongoOutputCacheItem>.Is.Anything)).Repeat.Once();
            //ACT
            service.Insert("key", MongoOutputCacheService.Serialize(memStream.ToArray()), datetime);


            //ASSERT
            mockRepository.VerifyAllExpectations();



        }


        //--------------------------

        [Test]
        public void InsertOrUpdateCheckNotInsertingAlreadyExistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Insert(Arg<MongoOutputCacheItem>.Is.Anything)).Repeat.Never();
            mockRepository.Expect(x => x.Save(Arg<MongoOutputCacheItem>.Is.Anything)).Repeat.Once();
            //ACT
            service.InsertOrUpdate("key", MongoOutputCacheService.Serialize(memStream.ToArray()), DateTime.MaxValue);


            //ASSERT
            mockRepository.VerifyAllExpectations();



        }

        [Test]
        public void InsertOrUpdateCheckSimpleNonexistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Insert(Arg<MongoOutputCacheItem>.Is.Anything)).Repeat.Once();
            mockRepository.Expect(x => x.Save(Arg<MongoOutputCacheItem>.Is.Anything)).Repeat.Never();
            //ACT
            service.InsertOrUpdate("key", MongoOutputCacheService.Serialize(memStream.ToArray()), datetime);


            //ASSERT
            mockRepository.VerifyAllExpectations();



        }



        //--------------------------

        [Test]
        public void DeleteByKeyCheckDeleteExistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MaxValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Remove("1")).Repeat.Once();
            //ACT
            service.DeleteByKey("key");


            //ASSERT
            mockRepository.VerifyAllExpectations();



        }

        [Test]
        public void DeleteByKeyCheckSimpleNonexistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
           

            
            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Remove("1")).Repeat.Never();
            //ACT
            service.DeleteByKey("key");

            //ASSERT
            mockRepository.VerifyAllExpectations();



        }

        //--------------------------

        [Test]
        public void RemoveExpiredCheckDeleteExistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();
            var datetime = new DateTime(2012, 10, 20);
            var itemTest = new byte[200];
            itemTest[0] = 1;
            itemTest[1] = 1;

            MemoryStream memStream = new MemoryStream(itemTest);

            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            var key = new MongoOutputCacheItem() { KeyUrl = "key", Expiration = DateTime.MinValue, Id = "1", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            var key2 = new MongoOutputCacheItem() { KeyUrl = "key2", Expiration = DateTime.MinValue, Id = "2", LastModified = datetime, Item = MongoOutputCacheService.Serialize(memStream.ToArray()) };
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { key, key2 }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Remove("1")).Repeat.Once();
            mockRepository.Expect(x => x.Remove("2")).Repeat.Never();
            //ACT
            service.RemoveExpired("key");


            //ASSERT
            mockRepository.VerifyAllExpectations();



        }

        [Test]
        public void RemoveExpiredCheckSimpleNonexistingKey()
        {
            //ARRANGE
            var mockRepository = MockRepository.GenerateMock<IMongoOutputCacheRepository>();



            MongoOutputCacheService service = new MongoOutputCacheService(mockRepository);
            IQueryable<MongoOutputCacheItem> list = new List<MongoOutputCacheItem>() { }.AsQueryable();

            mockRepository.Expect(x => x.AsQueryable()).Return(list);

            mockRepository.Expect(x => x.Remove("1")).Repeat.Never();
            //ACT
            service.RemoveExpired("key");

            //ASSERT
            mockRepository.VerifyAllExpectations();



        }



    }
}
