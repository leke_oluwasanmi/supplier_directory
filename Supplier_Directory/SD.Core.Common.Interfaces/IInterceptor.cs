﻿namespace SD.Core.Common.Interfaces
{
    public interface IInterceptor
    {
        bool IsInterestedIn(object entity);

        void Intercept(object entity);
    }
}