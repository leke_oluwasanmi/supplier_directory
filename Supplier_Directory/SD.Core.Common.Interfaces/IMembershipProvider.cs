﻿using SD.Core.Data.Model;

namespace SD.Core.Common.Interfaces
{
    public interface IMembershipProvider
    {
        User ActiveUser { get; }

        string ActiveUserId { get; }
    }
}