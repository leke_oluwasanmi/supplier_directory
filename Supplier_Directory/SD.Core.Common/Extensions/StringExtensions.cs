﻿using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace SD.Core.Common.Extensions.Strings
{
    public static class StringExtensions
    {
        private static readonly Dictionary<string, TextInfo> _textInfos;
        private static object _lockObj = new object();

        static StringExtensions()
        {
            lock (_lockObj) _textInfos = new Dictionary<string, TextInfo>();
        }

        public static string ConvertStringArrayToString(this string[] array)
        {
            //
            // Use string Join to concatenate the string elements.
            //
            var result = string.Join(", ", array);
            return result;
        }

        public static string ToTitleCase(this string input, string culture = "en-GB")
        {
            if (string.IsNullOrEmpty(input))
                return null;

            lock (_lockObj)
                if (!_textInfos.ContainsKey(culture))
                    _textInfos.Add(culture, new CultureInfo(culture).TextInfo);

            return _textInfos[culture].ToTitleCase(input.ToLower()); //for some reason, have to toLower it first?!
        }

        public static string CalculateMD5Hash(this string input)
        {
            // step 1, calculate MD5 hash from input
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            var sb = new StringBuilder();

            for (var i = 0; i < hash.Length; i++)
                sb.Append(hash[i].ToString("X2"));

            return sb.ToString();
        }

        public static string Truncate(this string inputString, int targetSize)
        {
            if (inputString.Length > targetSize) return inputString.Substring(0, targetSize) + "...";
            else return inputString;
        }
    }
}