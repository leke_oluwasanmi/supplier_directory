﻿using System;
using System.Configuration;

namespace SD.Core.Common
{
    public class ConfigHelper
    {
        public static string RewriteIgnoreExtensions
        {
            get { return GetAppSettingsValue<string>("RewriteIgnoreExtensions", string.Empty); }
        }

        public static string CDNImageLocation
        {
            get { return GetAppSettingsValue<string>("CDNImageLocation", string.Empty); }
        }

        public static string DocumentDir
        {
            get { return GetAppSettingsValue<string>("DocumentDir", string.Empty); }
        }

        public static string ArticlePublished
        {
            get { return GetAppSettingsValue<string>("ArticlePublished", string.Empty); }
        }

        public static int Published
        {
            get { return GetAppSettingsValue<int>("Published", 0); }
        }

        public static int UnPublished
        {
            get { return GetAppSettingsValue<int>("UnPublished", 0); }
        }

        public static int Archieved
        {
            get { return GetAppSettingsValue<int>("Archieved", 0); }
        }

        public static int MediaImage
        {
            get { return GetAppSettingsValue<int>("MediaImage", 0); }
        }

        public static int MediaVideo
        {
            get { return GetAppSettingsValue<int>("MediaVideo", 0); }
        }

        public static int MediaDocument
        {
            get { return GetAppSettingsValue<int>("MediaDocument", 0); }
        }

        public static int MaxPage
        {
            get { return GetAppSettingsValue<int>("MaxPage", 0); }
        }

        public static int MaxFavourite
        {
            get { return GetAppSettingsValue<int>("MaxFavourite", 0); }
        }

        public static int AlertHigh
        {
            get { return GetAppSettingsValue<int>("AlertHigh", 0); }
        }

        public static int AlertMedium
        {
            get { return GetAppSettingsValue<int>("AlertMedium", 0); }
        }

        public static int AlertLow
        {
            get { return GetAppSettingsValue<int>("AlertLow", 0); }
        }

        public static int MaxGalleryPage
        {
            get { return GetAppSettingsValue<int>("MaxGalleryPage", 0); }
        }

        public static int AlertGeneral
        {
            get { return GetAppSettingsValue<int>("AlertGeneral", 0); }
        }

        public static int AlertSystem
        {
            get { return GetAppSettingsValue<int>("AlertSystem", 0); }
        }

        public static string BrandName
        {
            get { return GetAppSettingsValue<string>("BrandName", string.Empty); }
        }

        public static int ArticleWorkloadType
        {
            get { return GetAppSettingsValue<int>("ArticleWorkloadType", 0); }
        }

        public static string CurrentBrand
        {
            get { return GetAppSettingsValue<string>("CurrentBrand", string.Empty); }
        }

        public static string CurrentSite
        {
            get { return GetAppSettingsValue<string>("CurrentSite", string.Empty); }
        }

        public static string KnowledgeBase
        {
            get { return GetAppSettingsValue<string>("KnowledgeBase", string.Empty); }
        }

        public static string ServerEnvironment
        {
            get { return GetAppSettingsValue<string>("ServerEnvironment", string.Empty); }
        }

        public static string MailServer
        {
            get { return GetAppSettingsValue<string>("MailServer", string.Empty); }
        }

        public static string TargetedEmail
        {
            get { return GetAppSettingsValue<string>("TargetedEmail", string.Empty); }
        }

        public static string GASetAccount
        {
            get { return GetAppSettingsValue<string>("GASetAccount", string.Empty); }
        }

        public static int TextQuestion
        {
            get { return GetAppSettingsValue<int>("TextQuestion", 0); }
        }

        public static int MultipleSelect
        {
            get { return GetAppSettingsValue<int>("MultipleSelect", 0); }
        }

        public static int SingleSelect
        {
            get { return GetAppSettingsValue<int>("SingleSelect", 0); }
        }

        public static int RadioSelect
        {
            get { return GetAppSettingsValue<int>("RadioSelect", 0); }
        }

        public static int FileAttachment
        {
            get { return GetAppSettingsValue<int>("FileAttachment", 0); }
        }

        public static int InfoText
        {
            get { return GetAppSettingsValue<int>("InfoText", 0); }
        }

        public static string FileUploadExt
        {
            get { return GetAppSettingsValue<string>("FileUploadExt", string.Empty); }
        }

        public static string UploadFiles
        {
            get { return GetAppSettingsValue<string>("UploadFiles", string.Empty); }
        }

        public static int MaxFileSize
        {
            get { return GetAppSettingsValue<int>("MaxFileSize", 0); }
        }

        public static string RewriteIgnorePage
        {
            get { return GetAppSettingsValue<string>("RewriteIgnorePage", string.Empty); }
        }

        public static string IMLogOutUrl
        {
            get { return GetAppSettingsValue<string>("IMLogOutUrl", string.Empty); }
        }

        public static string GSA
        {
            get { return GetAppSettingsValue<string>("GSA", string.Empty); }
        }

        public static int DefaultCount
        {
            get { return GetAppSettingsValue<int>("DefaultCount", 0); }
        }

        public static int KBCacheHours
        {
            get { return GetAppSettingsValue<int>("KBCacheHours", 1); }
        }

        public static int KBCacheMinutes
        {
            get { return GetAppSettingsValue<int>("KBCacheMinutes", 1); }
        }

        public static int KBCacheSeconds
        {
            get { return GetAppSettingsValue<int>("KBCacheSeconds", 1); }
        }

        public static int OracleImageSize
        {
            get { return GetAppSettingsValue<int>("OracleImageSize", 1); }
        }

        public static string GTMID
        {
            get { return GetAppSettingsValue<string>("GTMID", "KZ433P"); }
        }

        #region Social media settings

        public static string Facebook
        {
            get { return GetAppSettingsValue<string>("FacebookSocialMedia", string.Empty); }
        }

        public static string Twitter
        {
            get { return GetAppSettingsValue<string>("TwitterSocialMedia", string.Empty); }
        }

        public static string Google
        {
            get { return GetAppSettingsValue<string>("GoogleSocialMedia", string.Empty); }
        }

        public static string YouTube
        {
            get { return GetAppSettingsValue<string>("YouTubeSocialMedia", string.Empty); }
        }

        public static string Flickr
        {
            get { return GetAppSettingsValue<string>("FlickrSocialMedia", string.Empty); }
        }

        #endregion Social media settings

        public static string GatedAllowIPAddress
        {
            get { return GetAppSettingsValue<string>("GatedAllowIPAddress", string.Empty); }
        }

        public static T GetAppSettingsValue<T>(string appSettingsKey, T defaultValue)
        {
            if (ConfigurationManager.AppSettings[appSettingsKey] == null)
                return defaultValue;
            else
                return (T)Convert.ChangeType(ConfigurationManager.AppSettings[appSettingsKey], typeof(T));
        }
    }
}