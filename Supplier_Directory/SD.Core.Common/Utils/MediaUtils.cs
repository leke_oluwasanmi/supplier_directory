﻿using Omu.Drawing;
using System;
using System.Drawing;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;

namespace SD.Core.Common.Utils
{
    public class MediaUtils
    {
        public static MediaProfile MediaProfileFromName(string profileName)
        {
            var profile = MediaProfile.size_300x200;
            var foundProfile = false;
            foreach (MediaProfile mediaProfile in Enum.GetValues(typeof(MediaProfile)))
            {
                if (mediaProfile.ToString().ToLower() == profileName.ToLower())
                {
                    profile = mediaProfile;
                    foundProfile = true;
                    break;
                }
            }
            if (!foundProfile) throw new Exception(string.Format("Couldn't find profile called {0}", profileName));
            return profile;
        }

        public static bool MediaRatioHasProfiles(MediaRatio ratio)
        {
            return SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio).Count > 0;
        }

        public static Bitmap CropImage(Image originalImage,
            decimal targetWidth,
            decimal targetHeight,
            decimal cropX1,
            decimal cropX2,
            bool maintainAspectRatio,
            decimal cropY1,
            decimal cropY2)
        {
            var aspectRatio = targetHeight / targetWidth;

            //if (maintainAspectRatio) cropY1 = Math.Round(cropX1 * aspectRatio, 0);
            if (maintainAspectRatio) cropY2 = cropY1 + Math.Floor((cropX2 - cropX1) * aspectRatio);

            var cropWidth = cropX2 - cropX1;
            var cropHeight = cropY2 - cropY1;

            // Make sure values arent outside the bounds of the original image . Hacky by you only lose a couple of pixels max...
            var verticalDeficit = originalImage.Height - (cropY1 + cropHeight);
            var horizontalDeficit = originalImage.Width - (cropX1 + cropWidth);
            if (verticalDeficit < 0) cropHeight += verticalDeficit;
            if (horizontalDeficit < 0) cropWidth += horizontalDeficit;

            var img = Imager.Crop(originalImage, new Rectangle((int)cropX1, (int)cropY1, (int)cropWidth, (int)cropHeight));

            return (Bitmap)img;
        }

        public static Bitmap ResizeImage(Image originalImage,
           decimal targetWidth,
           decimal targetHeight
           )
        {
            var aspectRatio = targetHeight / targetWidth;
            var newWidth = (int)targetWidth;
            var newHeight = originalImage.Height * (int)targetWidth / originalImage.Width;
            if (newHeight > (int)targetHeight)
            {
                newWidth = originalImage.Width * (int)targetHeight / originalImage.Height;
                newHeight = (int)targetHeight;
            }

            var img = Imager.Resize(originalImage, newWidth, newHeight, false);

            return (Bitmap)img;
        }

        public static string GetMediaUri(MediaProfileElement profileConfig, string filename)
        {
            return string.Format("{0}/{1}/{2}/{3}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Media.Directory,
                    profileConfig.DirectoryName,
                    filename);
        }

        public static string GetAudioUri(string filename)
        {
            return string.Format("{0}/{1}/{2}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Audio.Directory,
                    filename);
        }

        public static string GetAudioCoverImageUri(MediaProfileElement profileConfig)
        {
            return string.Format("{0}/{1}/{2}/{3}",
                SDConfig.Instance.Media.baseUri,
                SDConfig.Instance.Media.Directory,
                profileConfig.DirectoryName,
                SDConfig.Instance.Audio.CoverImageFilename);
        }

        public static string GetDefaultMediaUri(MediaProfileElement profileConfig)
        {
            return string.Format("{0}/{1}/{2}/{3}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Media.Directory,
                    profileConfig.DirectoryName,
                    profileConfig.DefaultFilename);
        }

        public static string GetYouTubeThumbnailUri(string videoCode)
        {
            return string.Format("http://img.youtube.com/vi/{0}/1.jpg", videoCode);
        }

        public static string GetBrightCoveThumbnailUri(string videoCode)
        {
            //throw new NotImplementedException(); //TODO: Get the BrightCove thumnail uri
            return "";
        }

        public static string GetDocumentUri(string filename)
        {
            return string.Format("{0}/{1}/{2}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Document.Directory,
                    filename);
        }

        public static string GetDocumentUri(string filename, out string iconFilePath)
        {
            iconFilePath = string.Format("{0}/{1}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Document.Icon);

            return string.Format("{0}/{1}/{2}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Document.Directory,
                    filename);
        }
    }
}