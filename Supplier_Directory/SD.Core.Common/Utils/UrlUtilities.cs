﻿using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;

namespace SD.Core.Common.Utils
{
    public static partial class UrlUtilities
    {
        public static string FromUrlFriendly(string friendlyTitle)
        {
            return friendlyTitle.Replace('-', ' ');
        }

        public static string UrlSafe(string urlSlug)
        {
            urlSlug = urlSlug.Replace(" ", "-").ToLower();
            return Regex.Replace(urlSlug, "[^a-zA-Z0-9_.-]+", "", RegexOptions.Compiled);
        }

        /// <summary>
        /// TODO: Configuration should not be defiened in AppSettings. Remove references to System.Config and use the HN.Core.Common.Config setup instead.
        /// </summary>
        /// <param name="contentPath"></param>
        /// <returns></returns>
        public static string CdnContent(string contentPath)
        {
            if (contentPath.StartsWith("~"))
                return (ConfigurationManager.AppSettings["CDN_Available"] == "True") ?
                    contentPath.Replace("~", ConfigurationManager.AppSettings["CDN_ContentRootUrl"]) :
                    VirtualPathUtility.ToAbsolute(contentPath);
            else return contentPath;
        }

        public static string NotCdnContent(string contentPath)
        {
            if (contentPath.StartsWith("~"))
                return VirtualPathUtility.ToAbsolute(contentPath);
            else return contentPath;
        }
    }
}