﻿using System;

namespace SD.Core.Common.Utils
{
    public static class DateTimeUtilities
    {
        /// <summary>
        /// Turns a local datetime into a utc datetime while preserving the original values.
        /// <example>23:00:00 GMT+1 becomes 23:00:00 GMT</example>
        /// </summary>
        /// <param name="datetime"></param>
        public static DateTime ToUtcPreserved(DateTime datetime)
        {
            datetime = DateTime.SpecifyKind(datetime, DateTimeKind.Utc); // All dates are forced into DateTimeKind.Utc even if they're of DateTimeKind.Local.
            //if(datetime.Kind == DateTimeKind.Local) datetime = datetime.ToUniversalTime(); // Convert kind and values will occur when DateTime.Now() is used
            //else datetime = DateTime.SpecifyKind(datetime, DateTimeKind.Utc); // Convert kind only. occurs when values are deserialised from mongodb
            return datetime;
        }

        public static DateTime? ToUtcPreserved(DateTime? datetime)
        {
            if (datetime.HasValue) return ToUtcPreserved(datetime.Value);
            return null;
        }

        public static string RecordAgeFormat(DateTime? athleteDob, DateTime? resultDate)
        {
            if (athleteDob != null)
            {
                athleteDob = athleteDob.Value.ToLocalTime();
                if (resultDate != null)
                {
                    resultDate = resultDate.Value.ToLocalTime();
                    var age = resultDate.Value.Year - athleteDob.Value.Year;
                    if (resultDate.Value < (athleteDob.Value).AddYears(age)) age--;
                    return String.Format("{0} ({1})", athleteDob.Value.ToString("dd MMM yyyy").ToUpper(), age.ToString());
                }
                else return athleteDob.Value.ToString("dd MMM yyyy").ToUpper();
            }
            else return "";
        }

        public static string DateFormat(DateTime? dateToFormat, Boolean showTime = false)
        {
            if (dateToFormat != null)
            {
                if(showTime)
                    return dateToFormat.Value.ToLocalTime().ToString("dd MMM yyyy HH:mm:ss").ToUpper();
                else
                    return dateToFormat.Value.ToLocalTime().ToString("dd MMM yyyy").ToUpper();
            }
           
            else
                return "";
        }

        public static string DateFormatISO(DateTime? dateToFormat, Boolean showTime = false)
        {
            if (dateToFormat != null)
            {
                if(showTime)
                    return dateToFormat.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:ss").ToUpper();
                else
                    return dateToFormat.Value.ToLocalTime().ToString("yyyy-MM-dd").ToUpper();
            }
            else
                return "";
        }

        /// <summary>
        /// Returns the currentdatetime for a timeZone
        ///
        /// </summary>
        /// <param name="timeZone">timezone e.g "Pacific Standard Time", "Russian Standard Time"</param>
        /// <returns></returns>
        public static DateTime GetCurrentDateTimeByTimeZone(string timeZone)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, timeZone);
        }

        /// <summary>
        /// Returns the currentdatetime for a timeZone
        ///
        /// </summary>
        /// <param name="timeZone">timezone e.g "Pacific Standard Time", "Russian Standard Time"</param>
        /// <returns></returns>
        public static void GetCurrentDateTimeByTimeZone(string timeZone, out string offSet, out DateTime candidateTimeZoneDateTime)
        {
            candidateTimeZoneDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, timeZone);
            offSet = String.Format("{0:+0hrs;-0hrs}", TimeZoneInfo.FindSystemTimeZoneById(timeZone).BaseUtcOffset.Hours);
        }

        public static string TimeAgo(DateTime? dt)
        {
            if (!dt.HasValue)
                return String.Empty;

            var span = DateTime.Now - dt.Value;
            if (span.Days > 365)
            {
                var years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} {1} ago",
                    years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                var months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("{0} {1} ago",
                    months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format("{0} {1} ago",
                    span.Days, span.Days == 1 ? "day" : "days");
            if (span.Hours > 0)
                return String.Format("{0} {1} ago",
                    span.Hours, span.Hours == 1 ? "hour" : "hours");
            if (span.Minutes > 0)
                return String.Format("{0} {1} ago",
                    span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format("{0} seconds ago", span.Seconds);
            if (span.Seconds <= 5)
                return "just now";
            return String.Empty;
        }
    }
}