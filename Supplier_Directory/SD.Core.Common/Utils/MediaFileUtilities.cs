﻿
using ImageResizer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Configuration;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;

namespace SD.Core.Common.Utils
{
    public static partial class MediaFileUtilities
    {
     

       

     

        public static void SaveImage(Bitmap croppedImage, MediaProfileElement profileConfig, String filename)
        {
            saveImageFile(croppedImage, profileConfig, filename, FileFormatGroup.Image);
        }

        public static void SaveImageNoResize(Bitmap croppedImage, MediaProfileElement profileConfig, String filename)
        {
            saveImageFile(croppedImage, profileConfig, filename, FileFormatGroup.Image, true);
        }

        public static void SaveImage(Stream croppedImageStream, MediaProfileElement profileConfig, string filename)
        {
            var bm = new Bitmap(croppedImageStream);
            saveImageFile(bm, profileConfig, filename, FileFormatGroup.Image);
        }

        public static bool MediaExists(MediaProfileElement profileConfig, string filename)
        {

            string curFile = string.Format("~/{0}/{1}/{2}",
                SDConfig.Instance.Media.Directory,
                profileConfig.DirectoryName,
                filename);


         return   File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(curFile)) ? true : false;


            
        }

        public static void DeleteMedia(MediaProfileElement profileConfig, string filename)
        {
            string curFile = string.Format("~/{0}/{1}/{2}",
               SDConfig.Instance.Media.Directory,
               profileConfig.DirectoryName,
               filename);
            
            DeleteObject(curFile);
        }

        public static Image GetImage(MediaProfileElement profileConfig, string filename)
        {
            if (filename.EndsWith(".doc") || filename.EndsWith(".docx") || filename.EndsWith(".pdf") || filename.EndsWith(".xls") || filename.EndsWith(".xlsx"))
            {
                return Image.FromFile(System.Web.Hosting.HostingEnvironment.MapPath("~/Assets/Images/doc-icon.png"));
            }
            var img = GetImg(profileConfig, filename);

            var elapsed = new TimeSpan();
            while ((img == null) && (elapsed < TimeSpan.FromSeconds(10)))
            {
                Thread.Sleep(1000);
                elapsed.Add(TimeSpan.FromSeconds(1));
                img = GetImg(profileConfig, filename);
            }

            return img;
        }

        private static Image GetImg(MediaProfileElement profileConfig, string filename)
        {
            string key = string.Format("{3}/{0}/{1}/{2}",
                    SDConfig.Instance.Media.Directory,
                    profileConfig.DirectoryName,
                    filename,
                    SDConfig.Instance.Media.baseUri);

            string key2 = string.Format("~/{0}/{1}/{2}",
                   SDConfig.Instance.Media.Directory,
                   profileConfig.DirectoryName,
                   filename);

            //TODO: If Document
            //var img = Image.FromStream(new MemoryStream(new WebClient().DownloadData(key)));
            return Image.FromFile(System.Web.Hosting.HostingEnvironment.MapPath(key2));
          
        }

        public static void SaveAudio(Stream stream, string filename)
        {
            putObject(stream,
                string.Format("{0}/{1}",
                    SDConfig.Instance.Audio.Directory,
                    filename));
        }

        public static void DeleteAudio(string filename)
        {

            string curFile = string.Format("~/{0}/{1}",
               SDConfig.Instance.Audio.Directory,
               filename);

            File.Delete(curFile);


           
        }

        public static void SaveDocument(System.Web.HttpPostedFileBase file, string fileName)
        {
            putObject(file.InputStream,
                string.Format("{0}/{1}",
                    SDConfig.Instance.Document.Directory,
                    fileName));
        }

        public static void SaveOriginal(System.Web.HttpPostedFileBase file, string fileName)
        {
            putObject(file.InputStream,
                string.Format("{0}/{1}/{2}",
                    SDConfig.Instance.Media.Directory,
                    "Original",
                    fileName));
        }

        public static void SaveDocument(Stream stream, string fileName)
        {
            putObject(stream, string.Format("{0}/{1}",
                    SDConfig.Instance.Document.Directory,
                    fileName));
        }

        public static bool DocumentExists(string filename)
        {

            string curFile = string.Format("~/{0}/{1}",
              SDConfig.Instance.Document.Directory,
              filename);


            return File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(curFile)) ? true : false;

          
        }

        public static void DeleteDocument(string fileName)
        {
            string curFile = string.Format("~/{0}/{1}",
            SDConfig.Instance.Document.Directory,
            fileName);

            DeleteObject(curFile);           
        }

        public static void DeleteObject(string fileName)
        {
            string fullPath = System.Web.Hosting.HostingEnvironment.MapPath(fileName);
            File.Delete(fullPath);
        }
        public static void SaveToLocation(System.Web.HttpPostedFileBase file, string fileName)
        {
            putObject(file.InputStream,
                string.Format("{0}",
                    fileName));
        }

        public static void DeleteFromLocation(string fileName)
        {

            File.Delete(fileName);

          
        }

        public static Stream DownloadDocument(string fileName)
        {

            string curFile = string.Format("~/{0}/{1}",
             SDConfig.Instance.Document.Directory,
             fileName);

            Stream fs = File.OpenRead(curFile);
            return fs;
            
        }

        #region Private Methods

        private static void saveImageFile(Bitmap croppedImage, MediaProfileElement profileConfig, string filename, FileFormatGroup fileFormatGroup)
        {
            saveImageFile(croppedImage, profileConfig, filename, fileFormatGroup, true);
        }

        private static void saveImageFile(Bitmap croppedImage, MediaProfileElement profileConfig, string filename, FileFormatGroup fileFormatGroup, bool DoNotStretch)
        {
            using (var fileStream = new MemoryStream())
            {
                var bm = new Bitmap(croppedImage);
                var fileExtension = Path.GetExtension(filename);
                var resizeSettings = new ResizeSettings();
                if (!DoNotStretch)
                    resizeSettings.Mode = FitMode.Stretch;
                else
                    resizeSettings.Mode = FitMode.Pad;

                resizeSettings.Width = profileConfig.Width;
                resizeSettings.Scale = ScaleMode.Both;
                resizeSettings.PaddingColor = Color.White;
                resizeSettings.BackgroundColor = Color.White;
                var imageFormat = ImageFormat.Jpeg;
                switch (fileExtension)
                {
                    case ".png":
                        imageFormat = ImageFormat.Png;
                        resizeSettings.Format = "png";
                        resizeSettings.Width = profileConfig.Width;
                        resizeSettings.Height = profileConfig.Height;
                        break;

                    case ".jpeg":
                    case ".jpg":
                        imageFormat = ImageFormat.Jpeg;
                        resizeSettings.Format = "jpg";
                        resizeSettings.Width = profileConfig.Width;
                        resizeSettings.Height = profileConfig.Height;
                        break;

                    default:
                        break;
                }

                if (profileConfig.ProfileName != MediaProfile.Original
                    && fileFormatGroup == FileFormatGroup.Image)
                    bm = ImageBuilder.Current.Build(croppedImage, resizeSettings, false);

                bm.Save(fileStream, imageFormat);

               

                    putObject(fileStream,
                    string.Format("{0}/{1}/{2}",
                        SDConfig.Instance.Media.Directory,
                        profileConfig.DirectoryName,
                        filename));
               
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fileSteam">File to save</param>
        /// <param name="keyName">Directory and file name to save to</param>
        /// <returns></returns>
        private static bool putObject(Stream fileSteam, string keyName)
        {
            using (var fileStream = File.Create(System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}", keyName))))
            {
                fileSteam.Seek(0, SeekOrigin.Begin);
                fileSteam.CopyTo(fileStream);
            }

           
            return true;
        }

     
        #endregion Private Methods

        public static void MoveFile(MediaProfileElement current, MediaProfileElement move, string fileName)
        {
            //get image
            var imgToMove = GetImage(current, fileName);

            var Key = string.Format("{0}/{1}/{2}",
                    SDConfig.Instance.Media.Directory,
                    move.DirectoryName,
                    fileName);

            using (var stream = new MemoryStream())
            {
                // Save image to stream.
                imgToMove.Save(stream, ImageFormat.Bmp);
                //save img
                putObject(stream, Key);
            }
        }
    }
}