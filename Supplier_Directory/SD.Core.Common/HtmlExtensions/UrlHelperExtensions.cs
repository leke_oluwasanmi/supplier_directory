﻿using System.Web.Mvc;
using SD.Core.Common.Config;
using SD.Core.Common.Utils;

namespace SD.Core.Common.HtmlExtensions
{
    public static class UrlHelperExtensions
    {
        public static string CdnContent(this UrlHelper helper, string contentPath)
        {
            return UrlUtilities.CdnContent(contentPath);
        }

        public static string GetCdnUri(this UrlHelper helper, string filename)
        {
            return string.Format("{0}/{1}",
                    SDConfig.Instance.Media.baseUri,
                    filename);
        }
    }
}