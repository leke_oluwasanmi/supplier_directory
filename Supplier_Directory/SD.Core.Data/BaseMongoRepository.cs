﻿using MongoDB.Driver;

namespace SD.Core.Data
{
    public class BaseMongoRepository
    {
        protected static IMongoDatabase _db;

        public IMongoDatabase MongoDatabase
        {
            get { return _db; }
        }
    }
}
