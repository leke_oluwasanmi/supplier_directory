﻿using System;
using SD.Core.Common.Constants;
using SD.Core.Common.Interfaces;

namespace SD.Core.Data.VersionInterceptors
    {
    public class UpdateVersionInterceptor<T> : IVersionInterceptor where T : class, IVersionable
        {
        private IMembershipProvider _membershipProvider;
        private readonly MongoRepository _mongoRepo = new MongoRepository();
        private VersioningMongoRepository<T> _mongoVersionRepo = new VersioningMongoRepository<T>();
        private readonly Func<T, object> _idExpr;

        public UpdateVersionInterceptor(IMembershipProvider membershipProvider, Func<T, object> idExpr)
            {
            _membershipProvider = membershipProvider;
            _idExpr = idExpr;
            }

        public void Intercept(object entity, string versionComment)
            {
            T typedEntity = entity as T;
            _mongoVersionRepo.SaveVersion(_idExpr(typedEntity), typedEntity, VersionRecordType.Update, _membershipProvider.ActiveUserId, versionComment);
            }
        }
    }