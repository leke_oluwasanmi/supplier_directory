﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SD.Core.Data.Model;

namespace SD.Core.Data.Mapping
    {
    public class UserMap
        {
        public UserMap()
            {
            BsonClassMap.RegisterClassMap<User>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
            }
        }
    }