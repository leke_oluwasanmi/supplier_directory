﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SD.Core.Data.DTOs;
namespace SD.Core.Data.Mapping
{
    public class SponsoredListingMap
    {
        public SponsoredListingMap()
        {
            BsonClassMap.RegisterClassMap<SponsoredListing>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());

            });
        }
    }
}
