﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SD.Core.Data.DTOs;

namespace SD.Core.Data.Mapping
{
    public partial class TagsMap
    {
        public TagsMap()
        {
            BsonClassMap.RegisterClassMap<Tags>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());

            });
        }
    }
}