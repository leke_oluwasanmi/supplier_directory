﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SD.Core.Data.Model;

namespace SD.Core.Data.Mapping
    {
    internal class DocumentMap
        {
        public DocumentMap()
            {
            BsonClassMap.RegisterClassMap<Document>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id)
                    .SetIdGenerator(new StringObjectIdGenerator());
            });
            }
        }
    }