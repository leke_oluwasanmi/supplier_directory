﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SD.Core.Data.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Mapping
{
    public partial class MemberMap
    {
        public MemberMap()
        {
            BsonClassMap.RegisterClassMap<Member>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());

                //cm.MapField(x => x.LiveFromDay);
                //cm.MapField(x => x.LiveFromMonth);
                //cm.MapField(x => x.LiveFromYear);
            });
        }
    }
}
