﻿using System.Configuration;
using System.Linq;
using SD.Core.Common.Config;
using SD.Core.Data.DTOs;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace SD.Core.Data
{
    public class MongoOutputCacheRepository : SD.Core.Data.IMongoOutputCacheRepository
    {
        private const string CachedItemsCollectionName = "CachedItems";
        private const string ConnectionStringKey = "mongo_cache";

        private static IMongoDatabase _db;
        private IMongoCollection<MongoOutputCacheItem> collection;
       
        static MongoOutputCacheRepository()
        {
            var connectionString = !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString)
                ? ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString
                : ConfigurationManager.AppSettings[ConnectionStringKey];

            var client = new MongoClient(connectionString);
            _db = client.GetDatabase(SDConfig.Instance.Database.Name);
          
            BsonClassMap.RegisterClassMap<MongoOutputCacheItem>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
        }

        public void Insert(MongoOutputCacheItem cacheItem)
        {
            collection.InsertOne(cacheItem);
        }

        public void Save(MongoOutputCacheItem cacheItem)
        {
            //var item = _cacheItems.AsQueryable().FirstOrDefault(m => m.Id == cacheItem.Id);

            var item = collection.AsQueryable().FirstOrDefault(m => m.Id == cacheItem.Id);

            if (item != null)
            {
                item.Item = cacheItem.Item;
                item.Expiration = cacheItem.Expiration;
                Update(item);
            }
            else
            {
                Insert(cacheItem);
            }
        }

       

        public bool Update(MongoOutputCacheItem entity)
        {
            if (entity.Id == null)
                Insert(entity);

            var result = collection
                .ReplaceOne(x => x.Id == entity.Id,
                    entity,
                    new UpdateOptions { IsUpsert = true });
            return result.MatchedCount > 0;
        }


        public void Remove(string cacheItemId)
        {
            collection.DeleteOne(x => x.Id == cacheItemId);
        }

        public IQueryable<MongoOutputCacheItem> AsQueryable()
        {
            return collection.AsQueryable();
        }

        public virtual IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class
        {
            return _db.GetCollection<T>(getCollectionName<T>(collectionName));
        }

        protected virtual string getCollectionName<T>(string collectionName)
        {
            return string.IsNullOrWhiteSpace(collectionName) ? typeof(T).Name.ToLower() : collectionName;
        }

       
             

        private void GetCollection()
        {
            collection = _db
                .GetCollection<MongoOutputCacheItem>(typeof(MongoOutputCacheItem).Name);
        }
    }
}