﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;
using SD.Core.Common.Interfaces;
using SD.Core.Data.DTOs;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace SD.Core.Data
{
    public class VersioningMongoRepository<T> where T : class, IVersionable
    {
        private const string CollectionPrefix = "versions_";
        private const string ConnectionStringKey = "mongo";
        private const int VersionItemLimit = 10;

        private static IMongoDatabase _db;

       
        static VersioningMongoRepository()
        {
            var connectionString = !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString)
                ? ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString
                : ConfigurationManager.AppSettings[ConnectionStringKey];

         
            var client = new MongoClient(connectionString);
            _db = client.GetDatabase(SDConfig.Instance.Database.Name); //ToDo:Change this
        }

        public void SaveVersion(object id, T entity, VersionRecordType recordType, string activeUserId, string versionComment)
        {
            ensureClassMapRegistration();
            if (id == null) throw new ArgumentException("Entity must have an id before a version can be saved. Ensure that you're trying to create a version record for an insert.", "entity");
            if (string.IsNullOrEmpty(activeUserId)) throw new ArgumentException("Active user Id is required to save a version.", "activeUserId");

            //// Retrieve the existing entity
            //var query = Query.EQ("_id", BsonValue.Create(id)); // helps us deal with inconsistent dto id naming/typing
            //T existingEntity = _mongoRepo.GetCollection<T>().FindOne(query);

            var version = new VersionWrapperFor<T>
            {
                Entity = entity,
                EntityId = id.ToString(),
                RecordType = recordType,
                Comment = versionComment,
                ModifiedById = activeUserId,
                ModifiedOn = DateTime.Now
            };
            getCollection().InsertOne(version);

            // If we've got more than the set number of versions delete the oldest one(s)
            if (getCollection().Count(_ => true) > VersionItemLimit)
            {
                IList<VersionWrapperFor<T>> itemsToRemove = asQueryable()
                    .OrderByDescending(x => x.ModifiedOn)
                    .Skip(VersionItemLimit)
                    .ToList();
                foreach (var item in itemsToRemove)
                {
                    RemoveVersion(item.Id);
                }
            }
        }

        public IList<VersionWrapperFor<T>> GetVersions(int entityId)
        {
            return GetVersions(entityId.ToString());
        }

        public IList<VersionWrapperFor<T>> GetVersions(string entityId)
        {
            if (string.IsNullOrEmpty(entityId)) return new List<VersionWrapperFor<T>>();
            ensureClassMapRegistration();
            IList<VersionWrapperFor<T>> allVersions = asQueryable().ToList();
            IList<VersionWrapperFor<T>> versions = asQueryable()
                .Where(x => x.EntityId == entityId)
                .OrderByDescending(x => x.ModifiedOn)
                .ToList();
            return versions;
        }

        public VersionWrapperFor<T> GetVersion(string versionId)
        {
            if (string.IsNullOrEmpty(versionId)) throw new ArgumentException("Version Id is required to get a particular version.", "versionId");
            ensureClassMapRegistration();
            var version = asQueryable()
                .FirstOrDefault(x => x.Id == versionId);
            return version;
        }

        public void RemoveOlderThan(DateTime dateTime)
        {
            ///TODO
            //ensureClassMapRegistration();
            //var query = Query.LT("ModifiedOn", new BsonDateTime(dateTime));
            //getCollection().Remove(query);
        }

        public void RemoveVersion(string versionId)
        {
            ///TODO
            //ensureClassMapRegistration();
            //var query = Query.EQ("_id", versionId);
            //getCollection().DeleteOne(query);
        }

        #region Private/Protected Methods

        private void ensureClassMapRegistration()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(VersionWrapperFor<T>)))
            {
                /*
                 * Register the type. Doing this in the constructor as opposed
                 * to loading static methods as per MongoRepository allows us
                 * to throw any type of class at this repo.
                 */
                BsonClassMap.RegisterClassMap<VersionWrapperFor<T>>(cm =>
                {
                    cm.AutoMap();
                    cm.MapIdProperty(e => e.Id)
                        .SetIdGenerator(new StringObjectIdGenerator());
                });
            }
        }

        //protected virtual string getCollectionName(string collectionName)
        //{
        //    return string.IsNullOrWhiteSpace(collectionName) ? typeof(T).Name.ToLower() : collectionName;
        //}

        private IMongoCollection<VersionWrapperFor<T>> getCollection()
        {

            return _db.GetCollection<VersionWrapperFor<T>>(CollectionPrefix + typeof(T).Name);
        }

        private IQueryable<VersionWrapperFor<T>> asQueryable()
        {
            return getCollection().AsQueryable<VersionWrapperFor<T>>();
        }

        #endregion Private/Protected Methods
    }
}