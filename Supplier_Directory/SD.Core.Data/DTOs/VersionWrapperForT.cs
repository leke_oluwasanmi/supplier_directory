﻿using System;
using SD.Core.Common.Constants;

namespace SD.Core.Data.DTOs
{
    public class VersionWrapperFor<T>
    {
        public string Id { get; set; }

        public string EntityId { get; set; }

        public T Entity { get; set; }

        public VersionRecordType RecordType { get; set; }

        public string Comment { get; set; }

        public string ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}