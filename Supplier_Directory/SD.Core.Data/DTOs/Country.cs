﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.DTOs
{
    [Serializable]
    public class Country
    {
        public virtual string Id { get; set; }
        public virtual string RegionName { get; set; }
        public virtual string RegionId { get; set; }
        public virtual string Name { get; set; }
        public virtual string UrlSlug { get; set; }
    }
}
