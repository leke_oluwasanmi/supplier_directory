﻿using SD.Core.Common.Utils;
using System;
using System.Collections.Generic;
using SD.Core.Common.Constants;
using SD.Core.Common.Interfaces;

namespace SD.Core.Data.DTOs
{
    [Serializable]
    public class Listing : IPublishable, IAuditable, IVersionable
    {
        public virtual string Id { get; set; }

        public virtual string Title { get; set; }

        public virtual string SEOTitle { get; set; }

        public virtual string UrlSlug { get; set; }

        public virtual string MetaDescription { get; set; }

        public virtual string StandFirst { get; set; }

        public virtual string Body { get; set; }

        public virtual string CompanyName { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }

        public virtual string Address3 { get; set; }

        public virtual string Address4 { get; set; }

        public virtual string Town { get; set; }

        public virtual string County { get; set; }

        public virtual string Country { get; set; }

        public virtual string Email { get; set; }

        public virtual string Telephone { get; set; }

        public virtual string Website { get; set; }

        public virtual string Facebook { get; set; }

        public virtual string Twitter { get; set; }

        public virtual string LinkedIn { get; set; }

        public virtual string GooglePlus { get; set; }

        public virtual string CompanyLogo { get; set; }

        public virtual string Grade { get; set; }

        public virtual string MemberId { get; set; }

        public virtual string CountryId { get; set; }

        public virtual string ListingType { get; set; }

        public virtual string Postcode { get; set; }

        public virtual int ViewCount { get; set; }

        public virtual string VideoTitle { get; set; }
        public virtual string VideoUrl { get; set; }

        public virtual string PrimaryMediaId { get; set; }

        public virtual bool IsModerated { get; set; }        

        public virtual IList<string> RelatedMediaIds { get; set; } // TODO: Return read-only list

        public IList<string> Tags { get; set; }

        public virtual PublicationStatus Status { get; set; }

        private DateTime _liveFrom;

        public virtual DateTime LiveFrom { get { return _liveFrom; } set { _liveFrom = DateTimeUtilities.ToUtcPreserved(value); } }

        private DateTime? _liveTo;

        public virtual DateTime? LiveTo { get { return _liveTo; } set { _liveTo = DateTimeUtilities.ToUtcPreserved(value); } }

        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn { get; set; }

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }

    }
}
