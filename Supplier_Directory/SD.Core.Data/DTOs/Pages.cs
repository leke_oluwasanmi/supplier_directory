﻿using SD.Core.Common.Constants;
using SD.Core.Common.Interfaces;
using SD.Core.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.DTOs
{
    [Serializable]
    public class Pages : IPublishable
    {
        public virtual string Id { get; set; }

        public virtual string Title { get; set; }

        public virtual string UrlSlug { get; set; }

        public virtual string Body { get; set; }

        public virtual string CreatedById { get; set; }

        public virtual string PageUrls { get; set; }


        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public virtual string UpdatedById { get; set; }

        private DateTime _updatedOn { get; set; }

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }


        public virtual PublicationStatus Status { get; set; }


        private DateTime _liveFrom;

        public virtual DateTime LiveFrom { get { return _liveFrom; } set { _liveFrom = DateTimeUtilities.ToUtcPreserved(value); } }


        private DateTime? _liveTo;

        public virtual DateTime? LiveTo { get { return _liveTo; } set { _liveTo = DateTimeUtilities.ToUtcPreserved(value); } }

    }
}
