﻿using System.Collections.Generic;
using SD.Core.Data.DTOs;

namespace Iaaf.Data.DTOs.EqualityComparers
    {
    public class MediaEqualityComparer : IEqualityComparer<Media>
        {
        public bool Equals(Media x, Media y)
            {
            return x.Id == y.Id;
            }

        public int GetHashCode(Media obj)
            {
            return base.GetHashCode();
            }
        }
    }