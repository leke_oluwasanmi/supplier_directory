﻿using System;
using SD.Core.Common.Utils;

namespace SD.Core.Data.DTOs
{
    public class PasswordResetTicket
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public string UserId { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        private DateTime? _completedOn;

        public DateTime? CompletedOn { get { return _completedOn; } set { _completedOn = DateTimeUtilities.ToUtcPreserved(value); } }
    }
}