﻿using SD.Core.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;

namespace SD.Core.Data.DTOs
{
    [Serializable]
    public class FeaturedListing : IPublishable
    {
        public virtual string Id { get; set; }
        
        public virtual Page PageName { get; set; }

        public virtual string ListingId { get; set; }

        public virtual int Order { get; set; }

        public virtual PublicationStatus Status { get; set; }

        private DateTime _liveFrom;

        public virtual DateTime LiveFrom { get { return _liveFrom; } set { _liveFrom = DateTimeUtilities.ToUtcPreserved(value); } }


        private DateTime? _liveTo;

        public virtual DateTime? LiveTo { get { return _liveTo; } set { _liveTo = DateTimeUtilities.ToUtcPreserved(value); } }

        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn { get; set; }

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }
    }
}
