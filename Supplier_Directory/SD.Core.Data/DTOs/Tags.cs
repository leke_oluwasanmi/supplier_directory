﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace SD.Core.Data.DTOs
{
    [Serializable]
    public class Tags
    {
        public virtual string Id { get; set; }

        public virtual string DisplayText { get; set; }

        public virtual string UrlSlug { get; set; }

        public virtual string ParentSlug { get; set; }

        public virtual string ParentId { get; set; }        

        public virtual string Description { get; set; }          

        public virtual int ViewCount { get; set; }        

    }
}
