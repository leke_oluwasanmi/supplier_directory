﻿using SD.Core.Common.Constants;
using SD.Core.Common.Interfaces;
using SD.Core.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.DTOs
{
    [Serializable]
    public class Member : IPublishable //, IAuditable
    {
        public virtual string Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Telephone { get; set; }
        public virtual AccountType AccountType { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string Address4 { get; set; }
        public virtual string Town { get; set; }
        public virtual string CountryId { get; set; }       
        public virtual string County { get; set; }
        public virtual string Postcode { get; set; }
        public virtual string Firstname { get; set; }
        public virtual string Surname { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string CompanyDescription { get; set; }
        public virtual string Email { get; set; }
        public virtual string Password { get; set; }
        public virtual string ActivationToken { get; set; }
        public virtual IList<string> ListingIds { get; set; }
        public virtual IList<string> SavedSearches { get; set; }
        public virtual int LoginAttempts { get; set; }
        public virtual IList<string> Permissions { get; set; }
        public virtual bool IsModerated { get; set; }
        private DateTime _createdOn { get; set; }
        public virtual DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        private DateTime _updatedOn { get; set; }

        public virtual DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } } 

        private DateTime _liveFrom; //Published date or activation date

        public virtual DateTime LiveFrom { get { return _liveFrom; } set { _liveFrom = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }
        public string CreatedById { get; set; }


        public Member()
        {
            ListingIds = new List<string>();
            SavedSearches = new List<string>();
            Permissions = new List<string>();
            Status = PublicationStatus.UnPublished;
            CreatedOn = DateTime.UtcNow;
            //LiveFrom = DateTime.UtcNow;
        }

        // IPublishable
        public virtual PublicationStatus Status { get; set; }

        public virtual Boolean IsActivated { get; set; }

        public virtual string PasswordToken { get; set; }

        public virtual DateTime? LiveTo
        {
            get;
            set;
        }
    }
}
