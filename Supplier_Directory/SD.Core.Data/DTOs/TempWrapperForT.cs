﻿namespace SD.Core.Data.DTOs
{
    public class TempWrapperFor<T>
    {
        public string Id { get; set; }

        public T Entity { get; set; }
    }
}