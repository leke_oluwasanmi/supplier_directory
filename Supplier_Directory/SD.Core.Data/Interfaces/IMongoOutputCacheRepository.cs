﻿using System;
namespace SD.Core.Data
{
    public interface IMongoOutputCacheRepository
    {
        System.Linq.IQueryable<SD.Core.Data.DTOs.MongoOutputCacheItem> AsQueryable();
        MongoDB.Driver.IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class;
        void Insert(SD.Core.Data.DTOs.MongoOutputCacheItem cacheItem);
        void Remove(string cacheItemId);
        void Save(SD.Core.Data.DTOs.MongoOutputCacheItem cacheItem);
        bool Update(SD.Core.Data.DTOs.MongoOutputCacheItem entity);
    }
}
