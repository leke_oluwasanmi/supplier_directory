﻿define(["knockout", "jquery", "knockout.validation", "jqueryui", "bootstrap", "bc", "bcga"], function (ko, $, validation) {

    // knockout model here with some knockout validation 
    console.log("knockout has been loaded");





    //check if json exists
    function returnData(data_url) {
        var ajaxData;
        $.ajax({
            dataType: "json",
            url: data_url,
            async: false,
            success: function (data) {
                ajaxData = data;
            }
        });
        return ajaxData;
    }



    ko.bindingHandlers.modal = {
        init: function (element, valueAccessor) {
            $(element).modal({
                show: false
            });

            var value = valueAccessor();
            if (typeof value === 'function') {
                $(element).on('hide.bs.modal', function () {
                    value(false);
                });
            }
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).modal("destroy");
            });

        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            if (ko.utils.unwrapObservable(value)) {
                $(element).modal('show');
            } else {
                $(element).modal('hide');
            }
        }
    }



    function VWModel() {





        self = this;


        //observables


        // functions

        // getRelatedTags 
        if (typeof relatedTags !== "undefined") {
            var relatedTagsArray = relatedTags.existingFilter.split("+");
            this.getRelatedTags = ko.observableArray(returnData(relatedTags.tagsUrl));
            this.getExistingFilter = ko.observable(relatedTags.existingFilter);
            this.query = ko.observable("");

            for (var rt = 0; rt < relatedTagsArray.length; rt++) {
                this.getRelatedTags.remove(function (tags) {
                    return tags.tagSlug === relatedTagsArray[rt].toLowerCase();
                });
            }

            //$('[data-toggle="tooltip"]').tooltip();
            this.searchResults = ko.computed(function () {
                var q = self.query().toLowerCase();
                //$('[data-toggle="tooltip"]').tooltip();
                return ko.utils.arrayFilter(self.getRelatedTags(), function (i) {
                    return i.tagName.toLowerCase().indexOf(q) >= 0 && i.tagSlug !== self.getExistingFilter();
                });
            });
        }




        // getNews
        var modifier = 20;
        var skip = 20;
        var take = 20;

        this.noMoreNews = ko.observable(true);
        this.loading = ko.observable(false);

        this.news = ko.observableArray();

        if (typeof newsAction !== 'undefined') {
            if (!!newsAction.paged) {
                if (newsAction.paged === false) {
                    this.noMoreNews(false);
                }
            }
        }

        this.getNews = function () {

            var newsCount;

            if (!!newsAction.count || newsAction.count === null) {
                newsCount = newsAction.count;
            } else {
                newsCount = newsAction.count;
            }

            //loading
            self.loading(true);

            var data_url = "";

            if (!!newsAction.getNewsUrl || newsAction.getNewsUrl === null) {
                data_url += newsAction.getNewsUrl + "?";
            }
            if (!!newsAction.filterId || newsAction.filterId === null) {
                data_url += newsAction.filterId + "?";
            }
            if (!!newsAction.type || newsAction.type === null) {
                data_url += "type=" + newsAction.type + "&";
            }
            data_url += "take=" + take + "&skip=" + skip;

            var returnArray = returnData(data_url);
            for (var i = 0; i < returnArray.length; i++) {
                this.news.push(returnArray[i]);
            }

            self.loading(false);

            skip = skip + modifier;
            newsCount = newsCount - modifier;

            if (newsCount < 0) {
                self.noMoreNews(false);
            }

        }
           


        this.runMarkup = function (code) {
            console.log(eval(code));
            //document.getElementById().innerHTML = eval(code);
        }









        $(document).on("click", "#collapseMain button, #collapseSub button", function () {
            $('#collapseMain').collapse('hide');
            $('#collapseSub').collapse('hide');
        })


        // filtering 
        this.labelFiltered = ko.observable("Important");
        this.labelFilteredDays = ko.observable(1);

        this.getFiltered = function (cat, num, label, checknum) {
            var data_url = "/data/" + cat;
            if (checknum) {
                data_url += "/" + num;
            }
            var returnArray = returnData(data_url + "?take=" + take);
            this.labelFiltered(label);
            this.labelFilteredDays(num);
            this.news(returnArray);
            if (returnArray.count < take) {
                this.noMoreNews(false);
            } else {
                this.noMoreNews(true);
            }
            skip = 20;
        }

        this.getMoreFiltered = function (cat) {
            var data_url = "/data/" + cat;
            var returnArray = returnData(data_url + "/" + this.labelFilteredDays() + "?take=" + take + "&skip=" + skip);
            ko.utils.arrayPushAll(this.news().data, returnArray.data);
            this.news.valueHasMutated();
            skip = skip + modifier;
            if (returnArray.count < take) {
                this.noMoreNews(false);
            } else {
                this.noMoreNews(true);
            }
        }

        if (typeof action !== "undefined") {
            if (typeof action.loadHomeNews !== "undefined" && typeof action.loadFilter !== "undefined") {

                // init check for Important
                this.news(returnData("/data/important/" + action.loadFilter));
                
                if (this.news().data.length === 0 && this.labelFiltered() === "Important") {
                    this.labelFiltered("Missed");
                    this.news(returnData("/data/missing/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));

                    if (this.news().data.length === 0 && this.labelFiltered() === "Missed" && this.labelFilteredDays() <= 1) {
                        this.labelFilteredDays(2);
                        this.news(returnData("/data/missing/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }
                    if (this.news().data.length === 0 && this.labelFiltered() === "Missed" && this.labelFilteredDays() <= 2) {
                        this.labelFilteredDays(7);
                        this.news(returnData("/data/missing/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }
                    if (this.news().data.length === 0 && this.labelFiltered() === "Missed" && this.labelFilteredDays() <= 7) {
                        this.labelFilteredDays(14);
                        this.news(returnData("/data/missing/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }
                    if (this.news().data.length === 0 && this.labelFiltered() === "Missed" && this.labelFilteredDays() <= 14) {
                        this.labelFiltered("Latest");
                        this.labelFilteredDays(1);
                        this.news(returnData("/data/latest/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }

                    if (this.news().data.length === 0 && this.labelFiltered() === "Latest" && this.labelFilteredDays() <= 1) {
                        this.labelFilteredDays(2);
                        this.news(returnData("/data/latest/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }
                    if (this.news().data.length === 0 && this.labelFiltered() === "Latest" && this.labelFilteredDays() <= 2) {
                        this.labelFilteredDays(7);
                        this.news(returnData("/data/latest/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }
                    if (this.news().data.length === 0 && this.labelFiltered() === "Latest" && this.labelFilteredDays() <= 7) {
                        this.labelFilteredDays(14);
                        this.news(returnData("/data/latest/" + action.loadFilter + "/" + this.labelFilteredDays() + "?take=20"));
                    }
                }

                if (this.news().count < take) {
                    this.noMoreNews(false);
                } else {
                    this.noMoreNews(true);
                }

            }
        }
        

























        // findArticle/Tags
        this.findArticle = ko.observable("");

        /**
        * Search autocomplete
        */
        $(function () {
            var cache = {};
            $("#txtSearch").autocomplete({
                minLength: 2,
                source: function (request, response) {
                    var term = request.term;
                    if (term in cache) {
                        response(cache[term]);
                        return;
                    }

                    $.getJSON("/data/GetTags/" + term, function (data, status, xhr) {
                        cache[term] = data;
                        response(data);
                    });
                },
                focus: function (event, ui) {
                    if (event.keyCode === undefined) {
                        return false;
                    } else {
                        console.log(ui.item.UrlSlug);
                        self.findArticle(ui.item.UrlSlug);
                    }
                },
                select: function (event, ui) {
                    console.log(ui.item.UrlSlug);
                    self.findArticle(ui.item.UrlSlug);
                },
            });
        });




































        // favourite
        this.isFavourite = ko.observable(false);
        if (typeof article !== "undefined") {
            this.articleIsFavourited = ko.observable(article.isFavourited); //
            $.get("/article/getfavorite/" + self.articleIsFavourited(),
            function (data) {
                if (data.Favorited) {
                    if (data.YesNo === 0) {
                        self.isFavourite(true);
                    } else if (data.YesNo === 1) {
                        self.isFavourite(false);
                    }
                }
            });
            this.favourite = function (urlSlug) {
                if (self.isFavourite()) {
                    self.isFavourite(false);
                } else {
                    self.isFavourite(true);
                }
                $.get("/article/favorite/" + urlSlug + "/" + self.isFavourite(),
                function (data) {
                    console.log(self.isFavourite());
                });
            }
        }

        // non-article, favourite
        $(document).on("click", "[data-favourite]", function () {
            var count = parseInt($(this).children(".favcount").attr("data-count"));
            if ($(this).attr("data-favourite") === "true") {
                $(this).attr("data-favourite", false);
                $(this).children("i.fa.fa-star").attr("class", "fa fa-star-o");
                $(this).children(".favcount").attr("data-count", parseInt(count - 1));
            } else {
                $(this).attr("data-favourite", true);
                $(this).children("i.fa.fa-star-o").attr("class", "fa fa-star");
                $(this).children(".favcount").attr("data-count", parseInt(count + 1));
            }
            $.get("/article/favorite/" + $(this).attr("data-urlslug") + "/" + $(this).attr("data-favourite"));
            return false;
        });






        // reading
        this.readingList = ko.observableArray(returnData("/data/readinglists"));
        this.articleUrlSlug = ko.observable();
        this.readingListId = ko.observable();
        this.showDialog = ko.observable(false);
        this.isReading = ko.observable(false);
        this.submitReading = function () {
            var count = parseInt($("#" + this.articleUrlSlug() + "RL").attr("data-count"));
            console.log(count);
            $.get("/article/reading/" + this.articleUrlSlug() + "/true/" + this.readingListId());
            self.showDialog(false);
            $("#" + this.articleUrlSlug() + "RL").attr("data-count", parseInt(count + 1));
        }
        $("#ReadingLists").on("change", function () {
            if (this.value === "") {
                $("#addToReading").attr("disabled", "disabled");
            } else {
                $("#addToReading").removeAttr("disabled");
            }
            self.readingListId(this.value);
            console.log(this.value);
        });
















    }
    //############  VWModel end ################
    



    ko.bindingHandlers.initializeValue = {
        init: function (element, valueAccessor) {
            valueAccessor()(element.getAttribute('value'));
        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            element.setAttribute('value', ko.utils.unwrapObservable(value))
        }
    };
        

    ko.applyBindings(new VWModel());





});