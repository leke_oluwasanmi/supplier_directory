﻿define(["jquery", "jqueryui", "bootstrap"], function ($) {
    


    // jquery has been loaded
    console.log("jquery has been loaded");



    /**
    * User Login
    */

    $("#PrimaryJobRole").on("change", function () {
        if ($(this).val() == "") {
            $("#confirm").addClass("disabled");
        } else {
            $("#confirm").removeClass("disabled");
        }
    });

    $("#confirm").click(function () {
        if (!$(this).hasClass("disabled")) {
            // tidy
            $("#primaryPlaceHolder").hide();
            $(this).hide();
            // function
            $("#confirmsecjobrole").show();
            var selectedID = $("#PrimaryJobRole").val();
            $.get("/home/secondaryjobroles/" + selectedID, function (data) {
                $("#secondaryPlaceHolder").html(data);
                $("#secondaryPlaceHolder").slideUp().fadeIn("fast");
            });
        }
    });

    $(document).on("change", "#secondaryPlaceHolder .checklist #secondaryJobRoles", function () {
        console.log("Value " + $("select[name='secondaryJobRoles']").val());
        if ($("select[name='secondaryJobRoles']").val() == "") {
            console.log("none");
            // function
            $("#confirmsecjobrole").addClass("disabled");
            $("#confirmsecjobrole").attr('disabled', 'disabled');
        } else {
            console.log("one or more than one");
            // function
            $("#confirmsecjobrole").removeClass("disabled");
            $("#confirmsecjobrole").removeAttr("disabled");
        }
    });

    $("#confirmsecjobrole").click(function () {
        if (!$(this).hasClass("disabled")) {
            // tidy
            $("#secondaryPlaceHolder").hide();
            $(this).hide();
            // function
            $("#enter").show();
            var selectedID = $("#PrimaryJobRole").val();
            $.get("/home/FavouriteTags/" + selectedID, function (data) {
                $("#tertiaryPlaceHolder").html(data);
                $("#tertiaryPlaceHolder").slideUp().fadeIn("fast");
            });
        }
    });

    $(document).on("change", "#tertiaryPlaceHolder .checklist input[type='checkbox']", function () {
        if ($("input[name='favouriteTags']:checked") == null || $("input[name='favouriteTags']:checked").length == 0) {
            console.log("none");
            // function
            $("#enter").addClass("disabled");
            $("#enter").attr('disabled', 'disabled');
        } else {
            console.log("one or more than one #2");
            // function
            $("#enter").removeClass("disabled");
            $("#enter").removeAttr("disabled");
        }
    });






    /**
    * Back to Top
    */
    $("#back-to-top > a.btn").click(function () {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            $("#back-to-top").show();
        } else {
            $("#back-to-top").hide();
        }
    });




    /**
    * Bootstrap Tooltip
    */
    $(document).ready(function () {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    });




    /**
    * Bootstrap Popover
    */
    $(function () {
        $('[data-toggle="popover"]').popover()
    });




    /**
    * Bootstrap Popover
    */
    $("#addtag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings("#removetag").removeClass("hide");
    });


    $("#removetag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings("#addtag").removeClass("hide");
    });













});