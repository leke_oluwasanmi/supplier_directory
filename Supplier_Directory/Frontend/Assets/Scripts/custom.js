﻿



/**
* jQuery Waypoints
*/

var scripts_to_load = new Function();

jQuery(document).ready(function($)
{
    scripts_to_load = function () {

        var article = $("#article .blog-post");
        var articleGetArticleName = $("#article .blog-post").attr("data-articletitle");

        console.log(articleGetArticleName);

        var articleWp25 = article.waypoint(function () {
            console.log("25%");
            this.destroy();
        }, {
            offset: '-25%'
        });

        var articleWp50 = article.waypoint(function () {
            console.log("50%");
            this.destroy();
        }, {
            offset: '-50%'
        });

        var articleWp75 = article.waypoint(function () {
            console.log("75%");
            this.destroy();
        }, {
            offset: '-100%'
        });

        var articleWpFull = article.waypoint(function () {
            console.log("100%");
            this.destroy();
        }, {
            offset: 'bottom-in-view'
        });

    }
    scripts_to_load();
}); 



/**
* jquerySmoothState
*/
//$(function () {

//    var $page = $('#transition'),
//        options = {
//            debug: true,
//            prefetch: true,
//            cacheLength: 2,
//            onStart: {
//                duration: 250, // Duration of our animation
//                render: function ($container) {
//                    // Add your CSS animation reversing class
//                    $container.addClass('is-exiting');
//                    // Restart your animation
//                    smoothState.restartCSSAnimations();


//                }
//            },
//            onReady: {
//                duration: 0,
//                render: function ($container, $newContent) {
//                    // Remove your CSS animation reversing class
//                    $container.removeClass('is-exiting');
//                    // Inject the new content
//                    $container.html($newContent);
//                    scripts_to_load();
//                }
//            }
//        },
//        smoothState = $page.smoothState(options).data('smoothState');
//});