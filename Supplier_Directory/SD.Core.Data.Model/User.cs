﻿using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class User
    {

        public string Id { get; set; }

        [Required(ErrorMessage = "Forename is required")]
        public virtual string Forename { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        public virtual string Surname { get; set; }

        [Required(ErrorMessage = "An email is required")]
        public virtual string Email { get; set; }

        /// <summary>
        /// Salt and hash!
        /// </summary>
        public virtual string Password { get; set; }

        public virtual int LoginAttempts { get; set; }

        public virtual IList<Permission> Permissions { get; set; }

        public virtual bool ReceiveNotifications { get; set; }

        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }
    }
}