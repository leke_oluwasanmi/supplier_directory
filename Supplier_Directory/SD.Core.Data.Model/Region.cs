﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class Region : Entity
    {
        public string Name { get; set; }        
        public List<Country> Countries { get; set;}
        public string UrlSlug { get; set; }
        public Region()
        {
            Countries = new List<Country>();
        }
    }
}
