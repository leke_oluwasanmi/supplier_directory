﻿using SD.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class SponsoredListing : Entity
    {
        public string ListingId { get; set; }
        public PublicationStatus Status { get; set; }
        public DateTime? LiveFrom { get; set; }
        [Required(ErrorMessage = "An expiry date is required")]
        public DateTime? LiveTo { get; set; }
        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public Tags RelatedTag { get; set; }
        public Listing RelatedListing { get; set; }
        public SponsoredListing()
        {
            Status = PublicationStatus.UnPublished;
            LiveFrom = DateTime.UtcNow;
            LiveTo = DateTime.UtcNow;
            RelatedTag = new Tags();
            RelatedListing = new Listing();
        }

    }
}
