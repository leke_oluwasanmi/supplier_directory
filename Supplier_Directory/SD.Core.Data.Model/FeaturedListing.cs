﻿using SD.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class FeaturedListing : Entity
    {
        public Page PageName { get; set; }
        
        public string ListingId { get; set; }

        public Listing RelatedListing { get; set; }
        public int Order { get; set; }

        public PublicationStatus Status { get; set; }
        [Required(ErrorMessage = "A live from date is required")]
        public DateTime? LiveFrom { get; set; }
        [Required(ErrorMessage = "An expiry date is required")]
        public DateTime? LiveTo { get; set; }
        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public FeaturedListing()
        {
            Status = PublicationStatus.UnPublished;
            LiveFrom = DateTime.UtcNow;
            LiveTo = DateTime.UtcNow;
            RelatedListing = new Listing();
        }
    }
}
