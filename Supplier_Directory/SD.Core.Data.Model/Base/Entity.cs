﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Xml.Serialization;

namespace SD.Core.Data.Model
{ // Summary:
    //     Abstract Entity for all the BusinessEntities.
    [Serializable]
    [BsonIgnoreExtraElements(Inherited = true)]
    public abstract class Entity : IEntity<string>
    {  // Summary:
        //     Gets or sets the id for this object (the primary record for an entity).
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public virtual string Id { get; set; }
    }

    public interface IEntity<TKey>
    {
        // Summary:
        //     Gets or sets the Id of the Entity.
        [BsonId]
        TKey Id { get; set; }
    }
}
