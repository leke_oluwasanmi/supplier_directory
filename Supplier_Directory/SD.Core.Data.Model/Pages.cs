﻿using SD.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Mvc.AllowHtmlAttribute;
//using System.Web.Helper;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class Pages : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A title is required")]
        public string Title { get; set; }

        public string UrlSlug { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Page body cannot be empty")]
        //[AllowHtml]
        [DataType(DataType.Html)]
        public string Body { get; set; }

        public string PageUrls { get; set; }

        public string UpdatedById { get; set; }

        public string CreatedById { get; set; }

        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
        
        public DateTime? LiveFrom { get; set; }

        public PublicationStatus Status { get; set; }

        public Pages()
        {
            Status = PublicationStatus.UnPublished;
            LiveFrom = DateTime.UtcNow;
        }
    }
}
