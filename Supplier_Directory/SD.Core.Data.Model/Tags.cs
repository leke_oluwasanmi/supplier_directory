﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class Tags : Entity
    {
        public string DisplayText { get; set; }

        public string UrlSlug { get; set; }

        public string ParentSlug { get; set; }

        public string ParentId { get; set; }

        public List<string> ArticleUrls { get; set; }

        public Tags ParentTag { get; set; }

        public string Description { get; set; }

        public int TagType { get; set; }
        public int ViewCount { get; set; }  

        public Tags()
        {
            
        }
    }
}
