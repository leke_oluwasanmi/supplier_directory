﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model.EqualityComparers
{
    public class TagEqualityComparer : IEqualityComparer<Tags>
    {
        public bool Equals(Tags x, Tags y)
        {
            if (x == null || y == null) return false;
            return x.UrlSlug == y.UrlSlug;
        }

        public int GetHashCode(Tags obj)
        {
            return obj.GetHashCode();
        }
    }
}
