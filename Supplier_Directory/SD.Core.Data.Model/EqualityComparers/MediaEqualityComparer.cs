﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model.EqualityComparers
    {
    public class MediaEqualityComparer : IEqualityComparer<Model.Media>
        {
        public bool Equals(Model.Media x, Model.Media y)
            {
            if (x == null || y == null) return false;
            return x.Id == y.Id;
            }

        public int GetHashCode(Model.Media obj)
            {
            return base.GetHashCode();
            }
        }
    }
