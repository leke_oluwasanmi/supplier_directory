﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SD.Core.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class Listing : Entity
    {
        //[Required(AllowEmptyStrings = false, ErrorMessage = "A title is required")]
        public string Title { get; set; }

        public IList<Tags> RelatedTags { get; set; }
                
        public string SEOTitle { get; set; }

        public string UrlSlug { get; set; }

        public string MetaDescription { get; set; }
        
        public string StandFirst { get; set; }

        [DataType(DataType.MultilineText)]        
        [Display(Name = "Business Description")]
        public string Body { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
         
        public string County { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Town { get; set; }

        public string PostCode { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public string Website { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string LinkedIn { get; set; }

        public string GooglePlus { get; set; }

        public string CompanyLogo { get; set; }

        public ListingType ListingType { get; set; }    
        
        public string MemberId { get; set; }
        public Member Member { get; set; }        
        public string Country { get; set; }
        public string CountryId { get; set; }
        public Country RelatedCountry { get; set; }

        public string VideoTitle { get; set; }
        public string VideoUrl { get; set; }

        public string Grade { get; set; }

        public bool IsModerated { get; set; }   

        public Media PrimaryMedia { get; set; }

        public PublicationStatus Status { get; set; }

        public IList<Media> RelatedMedia { get; set; }

        public IList<Media> RelatedGallery { get; set; } // TODO: Return read-only list

        public IList<Media> RelatedDownload { get; set; }
        public IList<String> Tags { get; set; }
        public int ViewCount { get; set; }

        public DateTime? LiveFrom { get; set; }
        public DateTime? LiveTo { get; set; }

        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }

        public DateTime CreatedOn { get; set; }
        
        public DateTime UpdatedOn { get; set; }
        

        public Listing()
        {            
            RelatedMedia = new List<Media>();                     
            Status = PublicationStatus.UnPublished;
            LiveFrom = DateTime.UtcNow;
            Tags = new List<string>();
            RelatedTags = new List<Tags>();
            RelatedGallery = new List<Media>();
            RelatedDownload = new List<Media>();
        }

    }
}
