﻿using SD.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model
{
    [Serializable]
   public class Member : Entity
    {
        public string Title { get; set; }
        public string Telephone { get; set; }
        public AccountType AccountType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Town { get; set; }
        public string CountryId { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ActivationToken { get; set; }    
        public virtual IList<Listing> Listings { get; set; }
        //public virtual IList<SavedSearch> SavedSearches { get; set; }
        public int LoginAttempts { get; set; }
        public PublicationStatus Status { get; set; }
        public Country RelatedCountry { get; set; }
        public bool IsModerated { get; set; }
        // Active, Not Active

        public string UpdatedById { get; set; }
        public DateTime CreatedOn { get; set; }

        public DateTime? LiveFrom { get; set; } //Published date or activation date
        public DateTime UpdatedOn { get; set; }
        public User UpdatedBy { get; set; }
        public User CreatedBy { get; set; }
        public Member()
        {
            Listings = new List<Listing>();
            //SavedSearches = new List<SavedSearch>();
            Status = PublicationStatus.UnPublished;
            LiveFrom = DateTime.UtcNow;
            RelatedCountry = new Model.Country();
        }

        public string PasswordToken { get; set; }

        public Boolean IsActivated { get; set; }
    }
}
