﻿using SD.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class Test : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A title is required")]
        public string Title { get; set; }

        public IList<String> Tags { get; set; }
        public string ListingType { get; set; }
        public Media PrimaryMedia { get; set; }

        public PublicationStatus Status { get; set; }

        public IList<Media> RelatedMedia { get; set; }

        public IList<Media> RelatedGallery { get; set; } // TODO: Return read-only list

        public IList<Media> RelatedDownload { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "An SEO title (page title) is required")]
        public string SEOTitle { get; set; }

        public string UrlSlug { get; set; }

        [Required(ErrorMessage = "Meta Description is required")]
        public string MetaDescription { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Stand first is required")]
        public string StandFirst { get; set; }

        public string Body { get; set; }

        public string CompanyName { get; set; }

        //public string Address1 { get; set; }

        //public string Address2 { get; set; }

        //public string Address3 { get; set; }

        //public string Town { get; set; }

        //public string PostCode { get; set; }

        //public string Telephone { get; set; }

        //public string Website { get; set; }

        //public string Email { get; set; }

        //public string MemberId { get; set; }        

        //public string Country { get; set; }



        public int ViewCount { get; set; }

        public DateTime? LiveFrom { get; set; }

        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }


    }
}
