﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model
{
    [Serializable]
    public class Country : Entity
    {
        public string Name { get; set; }        
        public Region Region { get; set; }
        public string RegionName { get; set; }
        public string UrlSlug { get; set; }
    }
}
