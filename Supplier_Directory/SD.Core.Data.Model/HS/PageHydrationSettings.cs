﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model.HS
{
    public class PageHydrationSettings
    {
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
    }
}
