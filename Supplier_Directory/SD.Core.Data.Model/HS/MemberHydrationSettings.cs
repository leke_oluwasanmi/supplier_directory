﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model.HS
{
    public class MemberHydrationSettings
    {
        public ListingHydrationSettings MemberListings= null;        
        public CountryHydrationSettings Country = null;
        public UserHydrationSettings UpdatedBy = null;
    }
}
