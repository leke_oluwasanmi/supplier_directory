﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model.HS
{
   public class ListingHydrationSettings
    {
        public TagHydrationSettings RelatedTags = null;
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
        public MediaHydrationSettings PrimaryMedia = null;
        public MediaHydrationSettings RelatedMedia = null;
        public MemberHydrationSettings Member = null;
        public CountryHydrationSettings Country = null;
    }
}
