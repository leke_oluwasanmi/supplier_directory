﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Data.Model.HS
{
    public class SponsoredListingHydrationSettings
    {
        public TagHydrationSettings RelatedTags = null;
        public ListingHydrationSettings RelatedListings = null;
    }
}
