﻿namespace SD.Core.Data.Model.HS
{
    public class TagHydrationSettings
    {
        public TagHydrationSettings ParentTag = null;
        public ListingHydrationSettings SponsoredListing = null;
    }
}
