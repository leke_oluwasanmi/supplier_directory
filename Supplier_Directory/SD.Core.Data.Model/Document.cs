﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;



namespace SD.Core.Data.Model
    {
    [Serializable]
    public class Document 
        {
        public virtual string Id { get; set; }

        [Required(ErrorMessage = "A title is required")]
        public virtual string Title { get; set; }

        public virtual string UrlSlug { get; set; }

        public virtual string Description { get; set; }

        private DateTime? _published;

        public virtual DateTime? Published { get { return _published; } set { _published = DateTimeUtilities.ToUtcPreserved(value); } } //if null not published

        public virtual FileFormat Format { get; set; } //e.g. PDF DOCX

        public virtual int Order { get; set; }

        

        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public Document()
            {
            Format = FileFormat.PDF;
            }

        public override int GetHashCode()
            {
            unchecked // Overflow is fine, just wrap
                {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                if (Id != null) hash = hash * 29 + Id.GetHashCode();
                if (Title != null) hash = hash * 29 + Title.GetHashCode();
                if (UrlSlug != null) hash = hash * 29 + UrlSlug.GetHashCode();
                if (Description != null) hash = hash * 29 + Description.GetHashCode();
                if (Published != null) hash = hash * 29 + Published.GetHashCode();
                hash = hash * 29 + Format.GetHashCode();
                hash = hash * 29 + Order.GetHashCode();
                //if (Files != null) hash = hash * 29 + Files.GetHashCode();

                return hash;
                }
            }

        public override bool Equals(object obj)
            {
            return this.Equals(obj as Document);
            }

        public bool Equals(Document d)
            {
            // If parameter is null, return false.
            if (Object.ReferenceEquals(d, null)) return false;

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, d)) return true;

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != d.GetType()) return false;

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return GetHashCode() == d.GetHashCode();
            }

        public static bool operator ==(Document ld, Document rd)
            {
            // Check for null on left side.
            if (Object.ReferenceEquals(ld, null))
                {
                if (Object.ReferenceEquals(rd, null)) return true; // null == null = true.
                return false; // Only the left side is null.
                }
            // Equals handles case of null on right side.
            return ld.Equals(rd);
            }

        public static bool operator !=(Document ld, Document rd)
            {
            return !(ld == rd);
            }
        }

    //public class DocumentView
    //    {
    //    public virtual string GroupTitle { get; set; }

    //    public virtual string GroupUrlSlug
    //        {
    //        get
    //            {
    //            return GroupTitle.GenerateSlug();
    //            }
    //        }

    //    public virtual string SectionTitle { get; set; }

    //    public virtual string SectionUrlSlug
    //        {
    //        get
    //            {
    //            return SectionTitle.GenerateSlug();
    //            }
    //        }

    //    public Document document { get; set; }
    //    }
    }
