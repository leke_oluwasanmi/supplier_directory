﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.ViewModels
{
    public class ImportResultsModel
    {
        public List<string> ImportedSuccessfullyEmails { get; set; }
        public List<string> AlreadyInTheGroupEmails { get; set; }
        public List<string> EmailNotFoundEmails { get; set; }
        public string UserGroupId { get; set; }
        public ImportResultsModel()
        {
            ImportedSuccessfullyEmails = new List<string>();
            AlreadyInTheGroupEmails = new List<string>();
            EmailNotFoundEmails = new List<string>();
        }
    }
}
