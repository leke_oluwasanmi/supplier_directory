﻿using System;

namespace SD.Core.ViewModels
{
    [Serializable]
    public class PaginationModel
    {
        public PaginationModel()
        {
            TotalItemCount = 0;
            CurrentPage = 1;
            PageRange = 5;
            PageSize = 1;
            DefaulPageSize = 5;
            BaseUrl = string.Empty;
            QueryString = string.Empty;
        }

        /// <summary>
        /// Total number of pageable items
        /// </summary>
        public int TotalItemCount { get; set; }

        /// <summary>
        /// How many items to display per page
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// REMEMBER: zero-based index!
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// The number of pages to display before and after the current page
        /// </summary>
        public int PageRange { get; set; }

        /// <summary>
        /// The BaseUrl is included so that we can build the paging url at runtime to include any filter/search parameters
        /// NOTE: Assumes that the paging section of the url is always at the end.
        /// </summary>
        public string BaseUrl { get; set; }

        public string QueryString { get; set; }

        public int DefaulPageSize { get; set; }
    }
}