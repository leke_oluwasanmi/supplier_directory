﻿using System;

namespace SD.Core.ViewModels
{
    [Serializable]
    public class UserResponseItem
    {
        public string Headline;
        public string Details;
        public ResponseType Type;
    }
    [Serializable]
    public enum ResponseType
    {
        Notice,
        Warning,
        Error
    }
}