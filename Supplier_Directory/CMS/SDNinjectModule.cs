﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Common;
using SD.Core.Data;

namespace CMS
{
    public class SDNinjectModule : NinjectModule
    {
        public override void Load()
        {
            AppKernel.kernel.Bind<IMongoRepository1>()
             .To<MongoRepository>();
            AppKernel.kernel.Bind<IMongoOutputCacheRepository>()
                .To<MongoOutputCacheRepository>();
            
        }
    }
}
