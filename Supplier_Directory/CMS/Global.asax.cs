﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SD.Core.Data.Interceptors;
using SD.Core.Services.Security;
using CMS.Helpers.MembershipProviders;
using SD.Core.Services.Helpers;
using System;
using System.Web;
using CMS.Controllers;

namespace CMS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            EntityMapper.InitialiseMappings();

            SDNinjectModule module = new SDNinjectModule();
            module.Load();

            // Register Membership Provider
            var membershipProvider = new MembershipProvider();
            LiveUser.SetMembershipProvider(membershipProvider);
            CMSAuthorizeAttribute.SetMembershipProvider(membershipProvider);

            // Configure versioning            
            SD.Core.Data.MongoRepository.ConfigureVersioning<SD.Core.Data.DTOs.Media>(membershipProvider, x => x.Id);       

            var mediaInterceptor = new MediaInterceptor();
            SD.Core.Data.MongoRepository.RegisterInsertInterceptor(mediaInterceptor);
            SD.Core.Data.MongoRepository.RegisterUpdateInterceptor(mediaInterceptor);
            SD.Core.Data.MongoRepository.RegisterRemoveInterceptor(new MediaRemoveInterceptor());
            
        }

        void Application_Error(Object sender, EventArgs e)
        {

            System.Diagnostics.Trace.WriteLine("Enter - Application_Error");

            var httpContext = ((MvcApplication)sender).Context;

            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));
            var currentController = " ";
            var currentAction = " ";

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null &&
                    !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null &&
                    !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

            var ex = Server.GetLastError();

            if (ex != null)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);

                if (ex.InnerException != null)
                {
                    System.Diagnostics.Trace.WriteLine(ex.InnerException);
                    System.Diagnostics.Trace.WriteLine(ex.InnerException.Message);
                }
            }

            var controller = new ErrorController();
            var routeData = new RouteData();
            var action = "Error";
            var statusCode = 500;

            if (ex is HttpException)
            {
                var httpEx = ex as HttpException;
                statusCode = httpEx.GetHttpCode();

                switch (httpEx.GetHttpCode())
                {
                    case 404:
                        action = "Error404";
                        break;

                    default:
                        action = "Error";
                        break;
                }
            }
            else
            {
                action = "Error";
            }

            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = statusCode;
            httpContext.Response.TrySkipIisCustomErrors = true;
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;

            controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }
    }
}
