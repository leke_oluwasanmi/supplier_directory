﻿

    // knockout model here with some knockout validation 
    console.log("knockout has been loaded");





    //check if json exists
    function returnData(data_url) {
        var ajaxData;
        $.ajax({
            dataType: "json",
            url: data_url,
            async: false,
            success: function (data) {
                ajaxData = data;
            }
        });
        return ajaxData;
    }






    function VWModel() {
        

        var modifier = 20;
        var skip = 20;
        var take = 20;

        self = this;


        //observables
        this.noMoreNews = ko.observable(true);
        this.loading = ko.observable(false);

        this.news = ko.observableArray();

        if (typeof newsAction !== 'undefined') {
            if (!!newsAction.paged) {
                if (newsAction.paged === false) {
                    this.noMoreNews(false);
                }
            }
        }

        this.getNews = function () {

            var newsCount;

            if (!!newsAction.count || newsAction.count === null) {
                newsCount = newsAction.count;
            } else {
                newsCount = newsAction.count;
            }

            //loading
            self.loading(true);

            var data_url = "";

            if (!!newsAction.getNewsUrl || newsAction.getNewsUrl === null) {
                data_url += newsAction.getNewsUrl + "?";
            }
            if (!!newsAction.filterId || newsAction.filterId === null) {
                data_url += newsAction.filterId + "?";
            }
            if (!!newsAction.type || newsAction.type === null) {
                data_url += "type=" + newsAction.type + "&";
            }
            data_url += "take=" + take + "&skip=" + skip;

            var returnArray = returnData(data_url);
            for (var i = 0; i < returnArray.length; i++) {
                this.news.push(returnArray[i]);
            }
            self.loading(false);

            skip = skip + modifier;
            newsCount = newsCount - modifier;

            console.log("News: " + skip + " ----- " + newsCount);
            console.log(data_url);

            if (newsCount < 0) {
                self.noMoreNews(false);
            }

        }

        this.UserGroupsPage_usersText = ko.observable();

        this.UserGroupsPage_users = ko.observableArray();


        this.getUserGroupsPage_users = function () {

          
            //loading
            self.loading(true);
            this.UserGroupsPage_users.splice(0, this.UserGroupsPage_users().length);
          
            var returnArray = returnData('/UserGroup/GetAuthenticatedUsers/?name=' + this.UserGroupsPage_usersText());
           
            
            



            if (returnArray === undefined || returnArray.length == 0) {
                if (this.UserGroupsPage_usersText().length>=3)
                    $('.CMSUsersNoResultsFound').show();
                else
                    $('.CMSUsersNoResultsFound').hide();
            }
            else {
                $('.CMSUsersNoResultsFound').hide();
                for (var i = 0; i < returnArray.length; i++) {
                    this.UserGroupsPage_users.push(returnArray[i]);
                }
            }
            self.loading(false);
            console.log(this.UserGroupsPage_users().length);


        }

        this.UserGroupsPage_usersText.subscribe(function () {
            self.getUserGroupsPage_users();
        });

        

    }
    //############  VWModel end ################
 

 
    ko.bindingHandlers.initializeValue = {
        init: function (element, valueAccessor) {
            valueAccessor()(element.getAttribute('value'));
        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            element.setAttribute('value', ko.utils.unwrapObservable(value))
        }
    };
        

    ko.applyBindings(new VWModel());
    




