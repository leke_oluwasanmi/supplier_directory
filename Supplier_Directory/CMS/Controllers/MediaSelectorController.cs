﻿using System.Linq;
using System.Web.Mvc;
using SD.Core.Common.Constants;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using CMS.Models;
using SD.Core.Data;
using SD.Core.Common;

namespace CMS.Controllers
{
    public class MediaSelectorController : BaseController
    {
        private const int SearchResultsAtATime = 50;

        private MediaService _mediaService;
        private IMongoRepository1 mongoRepository;


        public MediaSelectorController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _mediaService = new MediaService(mongoRepository);
         
        }

        public MediaSelectorController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _mediaService = new MediaService(mongoRepository);
        }

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="addMediaActionName">The action to call when the user adds a media item</param>
        ///// <param name="addMediaControllerName">The controller to call when the user adds a media item</param>
        ///// <param name="updateElementSelector">The element to update when the media item has been added.
        ///// The return value of the action specified above should be a partial view that can be inserted into this element.</param>
        ///// <returns></returns>
        //[HttpGet]
        //public JsonResult Load(string addMediaActionName, string addMediaControllerName, string updateElementSelector)
        //{
        //}
        [CMSAuthorize("UseCMS")]
        public PartialViewResult Load()
        {
            return PartialView("MediaSelector");
        }

        /// <summary>
        /// RenderAction behaves incoherently when the page is posted back to.
        /// When the page is first loaded the HttpGet Action is requested.
        /// If the page posts back to itself, RenderAction then requests the
        /// HttpPost action method...
        /// </summary>
        /// <param name="nowt">Will always be null. Included to provide a
        /// signature that's distinct from the HttpGet version of this method.</param>
        /// <returns></returns>
        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public PartialViewResult Load(string nowt)
        {
            return PartialView("MediaSelector");
        }

        [CMSAuthorize("UseCMS")]
        public JsonResult RecentMedia()
        {
            var model = new MediaResults { Type = ResultSetType.MediaRecent };
            model.Results = _mediaService.GetPagedOrderedByCreatedOn(0, SearchResultsAtATime, null, new[] { PublicationStatus.Published }, false, false)
                .Select(x => new MediaResultItem { Media = x })
                .ToList();
            //return PartialView("MediaSelector", model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[CMSAuthorize("UseCMS")]
        //public JsonResult RecentGalleries(string searchText)
        //{
        //    MediaResults model = new MediaResults { Type = ResultSetType.GalleryRecent };
        //    model.Results = _mediaGalleryService.Search(searchText)
        //        .Select(x => new MediaResultItem { Gallery = x })
        //        .ToList();
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}
        [CMSAuthorize("UseCMS")]
        public JsonResult SearchByTitle(string searchText, int skip)
        {
            var model = new MediaResults
            {
                Results =
                    _mediaService.Search(searchText, skip, SearchResultsAtATime, true, false,
                        new[] {PublicationStatus.Published}, true)
                        .Select(x => new MediaResultItem {Media = x})
                        .ToList()
            };

            //return PartialView("AvailableMedia", model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[CMSAuthorize("UseCMS")]
        //public JsonResult SearchGalleries(string searchText, int skip)
        //    {
        //    MediaResults model = new MediaResults { Type = ResultSetType.GallerySearchAny };
        //    // Find matching MediaGalleries
        //    IList<MediaGallery> foundGalleries = _mediaGalleryService.Search(searchText, true, SearchResultsAtATime, new[] { PublicationStatus.Published }, true, new MediaGalleryHydrationSettings
        //    {
        //        Media = new MediaHydrationSettings()
        //    });

        //    List<MediaResultItem> mediaResults = foundGalleries
        //        .SelectMany(g => g
        //            .Media
        //                .Select(m => new MediaResultItem { Media = m, Gallery = g }))
        //        .ToList();

        //    model.Results = mediaResults
        //        .Distinct(new MediaResultItemEqualityComparer())
        //        .Skip(skip)
        //        .Take(SearchResultsAtATime)
        //        .ToList();

        //    return Json(model, JsonRequestBehavior.AllowGet);

        //    //// Compile the list of media items
        //    //List<Media> galleryMedia = new List<Media>();
        //    //foreach (MediaGallery gallery in foundGalleries)
        //    //{
        //    //    galleryMedia.AddRange(gallery.Media);
        //    //    if (galleryMedia.Count >= SearchResultsAtATime) break;

        //    //    foreach (Media media in gallery.Media)
        //    //    {
        //    //        if (model.Results.Where(x => x.Media.Id == media.Id).Count() == 0) // Make sure we dont duplicate the results
        //    //        {
        //    //            model.Results.Add(new MediaResultItem
        //    //            {
        //    //                Media = media,
        //    //                Gallery = gallery,
        //    //            });
        //    //            if (model.Results.Count >= SearchResultsAtATime) break;
        //    //        }
        //    //    }
        //    //}

        //    //return PartialView("AvailableMedia", model);
        //    }
    }
}