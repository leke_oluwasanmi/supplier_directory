﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Models;
using SD.Core.Common;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data.Model;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using SD.Core.ViewModels;
using Omu.Encrypto;
using SD.Core.Data;
using System.Threading.Tasks;

namespace CMS.Controllers
{
    public class UserController : BaseController
    {
        private const int MaxResults = 12;
        private const string PaginationInstanceKey = "UserIndex";
        private const string UserResponseInstanceKey = "User";

        // Helpers
        private readonly EntityEditHelper<User> _userEditHelper = new EntityEditHelper<User>();

        private readonly PaginationHelper _pageHelper;
        private readonly UserResponseHelper _responseHelper;

        // Services

        private readonly UserService _userService ;// AppKernel.GetInstance<UserService>();
         private IMongoRepository1 mongoRepository;

        
      
        public UserController()
        {
              mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
              _userService = new UserService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, "/User/", MaxResults);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }
        
        public UserController(IMongoRepository1 _mongoRepository)
        {
               this.mongoRepository = _mongoRepository;
            _pageHelper = new PaginationHelper(PaginationInstanceKey, "/User/", MaxResults);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        #region User management

        #region Index & Page

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public ActionResult Index()
        {
            return Page();
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public ActionResult Page(int pageNumber = -1)
        {
            if (pageNumber != -1) _pageHelper.SetPage(pageNumber);
            return View("Index", getUserListModel());
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public PartialViewResult Search(string searchText)
        {
            var foundUsers = _userService.Search(searchText, true, true, true, MaxResults);
            return PartialView("UserList", foundUsers);
        }

        #endregion Index & Page

        #region Create & Edit

        [CMSAuthorize("UseCMS & EditUsers")]
        public ActionResult Create()
        {
            _userEditHelper.ClearCache();
            var model = new UserEditModel { User = _userEditHelper.Cache };
            model.AllPermissionConfigurations = SDConfig.Instance.Permissions.ToList();
            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public ActionResult Edit(string id)
        {
            var model = new UserEditModel();

            model.AllPermissionConfigurations = SDConfig.Instance.Permissions.ToList();

            if (_userEditHelper.Cache.Id == id)
            {
                model.User = _userEditHelper.Cache;
            }
            else
            {
                model.User = _userService.FindById(id);
                if (model.User == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user to edit.");
                    return RedirectToAction("Index");
                }
                else
                {
                    _userEditHelper.Cache = model.User;
                }
            }
            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        [HttpPost]
        public ActionResult EditAction(User user, string newPassword, string newPasswordConfirmation, string submit)
        {
            switch (submit)
            {
                case "Save": return save(user, newPassword, newPasswordConfirmation);
                case "Cancel": return cancel();
            }
            throw new Exception("Unrecognised EditAction");
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public ActionResult Delete(string id)
        {
            try
            {
                var user = _userService.FindById(id);

                if (user == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user to delete");
                    return RedirectToAction("Index");
                }
                _userService.DeleteById(id);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted user: {0} {1}", user.Forename, user.Surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public ActionResult AddCMSPermissions(string id)
        {
            try
            {
                var user = _userService.FindById(id);

                if (user == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user");
                    return RedirectToAction("Index");
                }

                var Permissions = new List<Permission>();

                Permissions.Add(Permission.UseCMS);
                Permissions.Add(Permission.ViewListings);
                Permissions.Add(Permission.EditListings);
                Permissions.Add(Permission.EditMedia);
                Permissions.Add(Permission.ViewMedia);

                user.Permissions = Permissions;

                _userService.Save(user);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully added basic CMS Permissions: {0} {1}", user.Forename, user.Surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        [CMSAuthorize("UseCMS & ViewUsers | EditUsers")]
        public ActionResult RemoveCMSPermissions(string id)
        {
            try
            {
                var user = _userService.FindById(id);

                if (user == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user to delete");
                    return RedirectToAction("Index");
                }

                user.Permissions.Remove(Permission.UseCMS);

                _userService.Save(user);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully removed CMS Permissions: {0} {1}", user.Forename, user.Surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        #endregion Create & Edit

        #region Private Methods

        private ActionResult save(User user, string newPassword, string newPasswordConfirmation)
        {
            mergeBasicChanges(user);

            var model = new UserEditModel();
            model.AllPermissionConfigurations = SDConfig.Instance.Permissions.ToList();
            model.User = _userEditHelper.Cache;

            if (!validate(newPassword, newPasswordConfirmation, model.User))
            {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }

            // If no password has been given for a new user then generate one for them
            if (string.IsNullOrEmpty(_userEditHelper.Cache.Id) && string.IsNullOrWhiteSpace(newPassword))
            {
                newPassword = Guid.NewGuid().ToString() + DateTime.Now.Ticks.ToString();
                _responseHelper.AddUserResponse(ResponseType.Warning, "Generated a random password for the new user.");
            }

            // If a new password has been given then change the users password
            if (!string.IsNullOrWhiteSpace(newPassword))
            {
                var cachedUser = _userEditHelper.Cache;
                var hasher = new Hasher();
                hasher.SaltSize = 10;
                cachedUser.Password = hasher.Encrypt(newPassword);
                _userEditHelper.Cache = cachedUser;
            }

            try
            {
                _userService.Save(model.User);
                _userEditHelper.Cache = model.User;
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated user: {0} {1}", model.User.Forename, model.User.Surname));
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }
        }

        private bool validate(string newPassword, string newPasswordConfirmation, User user)
        {
            if (!ModelState.IsValid)
            {
                _responseHelper.AddValidationResponses(ModelState);
                return false;
            }

            // Check email is unique
            if (_userService.UsernameExists(user.Email, user.Id))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("A user with the email '{0}' has already been registered.", user.Email.ToLower()));
                return false;
            }

            // Check password
            if (!string.IsNullOrWhiteSpace(newPassword) && !string.IsNullOrWhiteSpace(newPasswordConfirmation))
            {
                //Password is being changed
                if (newPassword != newPasswordConfirmation)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "The new password and new password confirmation that you supplied do not match.");
                    return false;
                }
                if (!ValidatePasswordStregth(newPassword))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "The password must contain Upper and Lowercase letters and a number. Min length 8 chars.");
                    return false;
                }
            }
            else if (string.IsNullOrWhiteSpace(newPassword) && string.IsNullOrWhiteSpace(newPasswordConfirmation))
            {
                // Password is being left alone
            }
            else
            {
                // One password is empty and one isn't. This is an error
                _responseHelper.AddUserResponse(ResponseType.Error, "The password and confirmation password do not match");
                return false;
            }

            // Any other validation...

            return true;
        }

        private bool ValidatePasswordStregth(string newPassword)
        {
            var reStength = new Regex(@"^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).*$");

            return reStength.IsMatch(newPassword);
        }

        private ActionResult cancel()
        {
            _userEditHelper.ClearCache();
            return RedirectToAction("Index");
        }

        private void mergeBasicChanges(User changedUser)
        {
            var user = _userEditHelper.Cache;
            user = Mapper.Map<User, User>(changedUser, user);            
            _userEditHelper.Cache = user;
        }

        private UserListModel getUserListModel()
        {
            var model = new UserListModel();
            model.Users = _userService.GetPagedOrderedBySurname(
                _pageHelper.PaginationModel.PageSize * (_pageHelper.PaginationModel.CurrentPage - 1),
                _pageHelper.PaginationModel.PageSize);

            _pageHelper.SetTotalItemCount(_userService.GetCount());

            model.Pagination = _pageHelper.PaginationModel;
            model.Responses = _responseHelper.UserResponses;
            return model;
        }

        #endregion Private Methods

        #endregion User management

        #region Login, Logout & Reset

        public ActionResult Login(string key)
        {
            if (!string.IsNullOrWhiteSpace(key) && _userService.PasswordResetTicketIsValid(key))
            {
                ViewBag.PasswordResetTicketKey = key.ToLower();
            }
            if (!string.IsNullOrWhiteSpace(key) && !_userService.PasswordResetTicketIsActive(key))
            {
                ViewBag.PasswordResetTicketHasTimedOut = true;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password, bool? rememberMe)
        {            
            if (MembershipHelper.Login(username, password, rememberMe == true))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ErrorMessage = "Invalid username and/or password. Please check the spelling and try again";
                return View();
            }
        }

        public ActionResult Logout()
        {
            MembershipHelper.Logout();
            return RedirectToAction("Login", "User");
        }

        [HttpPost]
        public JsonResult RequestPasswordReset(string email)
        {
            if (!_userService.UsernameExists(email)) return Json(new { Success = false, Message = String.Format("No user with email {0} could be found. Please make sure it's spelled correctly and that it's the email address associated with this account", email.ToLower()) });

            var ticketKey = HttpUtility.UrlEncode(_userService.CreatePasswordResetTicket(email));

            var user = _userService.FindByEmail(email);

            // Create and send The ticket
            try
            {
                //SESUtilities.SendTemplateEmail<PasswordResetModel>(
                //    email,
                //    "Your new HUB CMS password.",
                //    "~/Views/Email/PasswordResetText.cshtml",
                //    "~/Views/Email/PasswordResetHtml.cshtml",
                //    new PasswordResetModel { PasswordResetTicketKey = ticketKey, User = user, BaseUrl = ConfigurationManager.AppSettings["CMSurl"] },
                //    "andrew.macharg@haymarket.com",
                //    null);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = "There was a problem sending the confirmation email" });
            }
            return Json(new { Success = true, Message = "An email has been sent to your inbox with details about resetting your password" });
        }

        [HttpPost]
        public JsonResult CompletePasswordReset(string newPassword, string newPasswordConfirmation, string key)
        {
            if (newPassword.ToLower() != newPasswordConfirmation.ToLower()) return Json(new { Success = false, Message = "The new password and confirmation passwords don't match" });

            try
            {
                _userService.ChangeUserPassword(key, newPassword);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = "There was a problem resetting your password" });
            }

            return Json(new { Success = true, Message = "Changed your password" });
        }

        public ActionResult Forgot()
        {
            UserEditModel model = new UserEditModel();
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Reset(string username)
        {
            UserEditModel model = new UserEditModel();
            var user = _userService.FindByEmail(username);
            if(user == null)
            {
                model.Responses.Add(new UserResponseItem { Type = ResponseType.Notice, Headline = string.Format("No user with email {0} could be found. Please make sure it's spelled correctly and that it's the email address associated with this account", username) });                
                
                return View("Forgot", model);
            }
            var hasher = new Hasher();
            hasher.SaltSize = 10;
            string newPassword = UserService.RandomString(8).ToLower();
            user.Password = hasher.Encrypt(newPassword);
                        
            _userService.Save(user);
            string senderName = ConfigurationManager.AppSettings["EmailSenderName"];
            string senderEmail = ConfigurationManager.AppSettings["EmailSenderAddress"];
            string message = string.Format("Your new password is {0}", newPassword);
            await EmailHandler.SendMail(username, user.Forename, senderEmail, senderName, "Password Reset", message, System.Net.Mail.MailPriority.Low, true);
                                    
            model.Responses.Add(new UserResponseItem { Type = ResponseType.Notice, Headline = string.Format("Your new password has been sent to {0}", username) });

            return View("Forgot", model);
        }
        #endregion Login, Logout & Reset
    }
}