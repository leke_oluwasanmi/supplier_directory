﻿using System.Collections.Generic;
using System.Web.Mvc;
using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using System.Linq;
using SD.Core.Common;
using SD.Core.Data;

namespace CMS.Controllers
    {
    public class TagSelectorController : Controller
        {
        private const int MaxArticleSearchResults = 100;
      
        private const int MaxEventSearchResults = 100;
       
         private IMongoRepository1 mongoRepository;

        
        public TagSelectorController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
          
        }

        public TagSelectorController(IMongoRepository1 _mongoRepository)
        {
             this.mongoRepository = _mongoRepository;
           
        }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult Load()
            {
            return PartialView("TagSelector");
            }
        

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SearchTags(string searchText)
            {
                var tagsService = new TagsService(mongoRepository);
            var tags = tagsService.Search(searchText, true, MaxEventSearchResults);
            return PartialView("TagList", tags);
            }       
        }
    }