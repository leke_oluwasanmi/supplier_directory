﻿using System.Linq;
using System.Web.Mvc;
using CMS.Models;
using SD.Core.Common.Constants;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using SD.Core.Common;
using SD.Core.Data;

namespace Iaaf.Cms.Controllers
    {
    public class InContentMediaSelectorController : Controller
        {
        private const int SearchResultsAtATime = 50;
        private IMongoRepository1 mongoRepository;

        public InContentMediaSelectorController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _mediaService = new MediaService(mongoRepository);
        }

        public InContentMediaSelectorController(IMongoRepository1 _mongoRepository)
        {
             this.mongoRepository = _mongoRepository;
             _mediaService = new MediaService(mongoRepository);
        }

        private MediaService _mediaService;

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="addMediaActionName">The action to call when the user adds a media item</param>
        ///// <param name="addMediaControllerName">The controller to call when the user adds a media item</param>
        ///// <param name="updateElementSelector">The element to update when the media item has been added.
        ///// The return value of the action specified above should be a partial view that can be inserted into this element.</param>
        ///// <returns></returns>
        //[HttpGet]
        //public JsonResult Load(string addMediaActionName, string addMediaControllerName, string updateElementSelector)
        //{
        //}
        [CMSAuthorize("UseCMS")]
        public PartialViewResult Load()
            {
            return PartialView("InLineMediaSelector");
            }

        /// <summary>
        /// RenderAction behaves incoherently when the page is posted back to.
        /// When the page is first loaded the HttpGet Action is requested.
        /// If the page posts back to itself, RenderAction then requests the
        /// HttpPost action method...
        /// </summary>
        /// <param name="nowt">Will always be null. Included to provide a
        /// signature that's distinct from the HttpGet version of this method.</param>
        /// <returns></returns>
        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public PartialViewResult Load(string nowt)
            {
            return PartialView("InLineMediaSelector");
            }

        [CMSAuthorize("UseCMS")]
        public JsonResult RecentMedia()
            {
            var model = new MediaResults { Type = ResultSetType.MediaRecent };
            //model.Results = _mediaService.GetPagedOrderedByCreatedOn(0, SearchResultsAtATime, null, new[] { PublicationStatus.Published }, false, false)
            //    .Select(x => new MediaResultItem { Media = x })
            //    .ToList();
            //return PartialView("MediaSelector", model);
            return Json(model, JsonRequestBehavior.AllowGet);
            }

        [CMSAuthorize("UseCMS")]
        public JsonResult SearchByTitle(string searchText, int skip)
            {
            var model = new MediaResults
            {
                Results = _mediaService.Search(searchText, skip, SearchResultsAtATime, true, false, null, true)
                    .Select(x => new MediaResultItem {Media = x})
                    .ToList()
            };

            //return PartialView("AvailableMedia", model);
            return Json(model, JsonRequestBehavior.AllowGet);
            }
        }
    }