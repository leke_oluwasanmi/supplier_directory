﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Models;
using SD.Core.Common;
using SD.Core.Common.Constants;
using SD.Core.Common.Utilities;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using SD.Core.ViewModels;
using System.Data.Linq;
using System.Text;
using SD.Core.Common.Utilities.Extensions.Enums;
namespace CMS.Controllers
{
    public class FeaturedListingController : Controller
    {
        private const int MaxResultsPerTabView = 12;
        private const string PaginationMainIndexInstanceKey = "FeaturedListingMainIndex";
        private const string UserResponseInstanceKey = "FeaturedListing";
        private const string ShowDeletedListingsCacheKey = "ShowDeletedListingsCache";
        private const string ShowListingTypeCacheKey = "ShowListingTypeCacheKey";
        private const string PaginationBaseUrl = "/FeaturedListing/";
        private const string ListingSearchTextCacheKey = "FeaturedListingSearchTextCache";
        private const string ListingSearchCountCacheKey = "FeaturedListingSearchCountCache";

        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        //Carries Messages
        private readonly UserResponseHelper _responseHelper;

        //database entity
        private IMongoRepository1 mongoRepository;

        //
        private readonly MediaService _mediaService;

        //
        private readonly TagsService _tagsService;

        private readonly SD.Core.Services.Core.ListingService _listingService;

        private readonly UserService _userService;
        private readonly SD.Core.Services.Core.FeaturedListingService _featuredListingService;

        private static readonly FeaturedListingHydrationSettings _featuredListingHydrationSettings;

        private string SearchText
        {
            get
            {
                if (Session[ListingSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[ListingSearchTextCacheKey];
            }
            set
            {
                Session[ListingSearchTextCacheKey] = value;
            }
        }

        private int SearchCount
        {
            get
            {
                if (Session[ListingSearchCountCacheKey] == null)
                    Session[ListingSearchCountCacheKey] = MaxResultsPerTabView;
                return (int)Session[ListingSearchCountCacheKey];
            }
            set
            {
                Session[ListingSearchCountCacheKey] = value;
            }
        }

        private bool ShowDeletedListings
        {
            get
            {
                if (Session[ShowDeletedListingsCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedListingsCacheKey];
            }
            set
            {
                Session[ShowDeletedListingsCacheKey] = value;
            }
        }

        #region constructor
        static FeaturedListingController()
        {
            _featuredListingHydrationSettings = new FeaturedListingHydrationSettings { ListingHydrationSettings = new ListingHydrationSettings() };
        }

        public FeaturedListingController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _userService = new UserService(mongoRepository);
            _featuredListingService = new SD.Core.Services.Core.FeaturedListingService(mongoRepository);
            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        public FeaturedListingController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
            _featuredListingService = new SD.Core.Services.Core.FeaturedListingService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _userService = new UserService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }
        #endregion

        // GET: Listing
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public ActionResult Index(string page)
        {
            var model = new FeaturedListingModel();
            //populate dropdown
            var pages = (SD.Core.Common.Constants.Page[])Enum.GetValues(typeof(SD.Core.Common.Constants.Page));
            List<SelectListItem> selectItems = new List<SelectListItem>();
            Page pageName = SD.Core.Common.Constants.Page.Home;
            Enum.TryParse(page, true, out pageName);
            foreach (var p in pages)
            {
                int value = (int)p;
                bool selected = !string.IsNullOrEmpty(page) && pageName == p ? true : false;
                selectItems.Add(new SelectListItem() { Text = p.DisplayName(), Value = value.ToString(), Selected = selected });
            }
            model.SelectedPage = pageName.ToString();
            ViewBag.Pages = selectItems;

            //featured listings
            model.FeaturedListings = _featuredListingService.GetAllByPage(new[] { PublicationStatus.Published, PublicationStatus.UnPublished }, pageName, _featuredListingHydrationSettings);

            return View(model);
            //return Page(-1, null, showUnpublishedOnly);
        }



        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public ActionResult Edit(string id, string page)
        {
            using (var editCache = new EntityEditCache<FeaturedListing>(x => x.Id))
            {
                FeaturedListingEditModel model = new FeaturedListingEditModel();
                if (!string.IsNullOrEmpty(id) && id != "0")
                {
                    //get featuredListing
                    model.FeaturedListing = _featuredListingService.FindById(id, _featuredListingHydrationSettings);
                    if (model.FeaturedListing == null)
                    {
                        _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified featured listing to edit");
                        return View("Index");
                    }
                }
                if (string.IsNullOrEmpty(page))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "No Page has been selected");
                    return View("Index");
                }

                SD.Core.Common.Constants.Page pageName;
                if (Enum.TryParse<SD.Core.Common.Constants.Page>(page, true, out pageName))
                    model.FeaturedListing.PageName = pageName;

                editCache.SetEntity(model.FeaturedListing);
                model.Responses = _responseHelper.UserResponses;

                return View("Edit", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditAction(FeaturedListing featuredListing, string featuredListingId, string listingId, string submit)
        {
            if (submit.ToLower() == "cancel")
                return RedirectToAction("Index");
            else if (submit.ToLower() == "save")
            {
                FeaturedListingEditModel editModel = new FeaturedListingEditModel();
                var listing = _listingService.FindById(listingId, null);
                if (listing == null)
                {
                    return View("Edit", editModel);
                }
                using (var editHelper = new EntityEditCache<FeaturedListing>(x => x.Id))
                {
                    featuredListing.ListingId = listingId;
                    mergeBasicChanges(featuredListing, editHelper, featuredListingId);
                    editModel.FeaturedListing = editHelper.GetEntity(featuredListingId);
                    editModel.FeaturedListing.UpdatedBy = MembershipHelper.GetActiveUser();
                    editModel.FeaturedListing.UpdatedOn = DateTime.Now;
                    editModel.FeaturedListing.RelatedListing = listing;
                    if (!validate(editModel.FeaturedListing) || string.IsNullOrEmpty(editModel.FeaturedListing.ListingId))
                    {
                        if (string.IsNullOrEmpty(editModel.FeaturedListing.ListingId))
                            _responseHelper.AddUserResponse(ResponseType.Error, "A company is required");
                        editModel.Responses = _responseHelper.UserResponses;
                        return View("Edit", editModel);
                    }
                    //set the order
                    PublicationStatus[] showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };
                    if (string.IsNullOrEmpty(editModel.FeaturedListing.Id))
                    {
                        var relatedFeaturedListings = _featuredListingService.GetAllByPage(showWithStatus, editModel.FeaturedListing.PageName, null);
                        editModel.FeaturedListing.CreatedBy = MembershipHelper.GetActiveUser();
                        editModel.FeaturedListing.CreatedOn = DateTime.Now;
                        if (!string.IsNullOrEmpty(editModel.FeaturedListing.Id))
                            editModel.FeaturedListing.Order = relatedFeaturedListings.Count + 1;
                    }

                    _featuredListingService.Save(editModel.FeaturedListing);
                    //get back te successful response message
                    _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully saved featured listing");
                    editModel.Responses = _responseHelper.UserResponses;

                    return View("Edit", editModel);
                }
            }
            else
                throw new Exception("Unrecognised EditAction");
        }

        private bool validate(FeaturedListing featuredListing)
        {
            var isValid = true;
            if (!ModelState.IsValid)
            {
                _responseHelper.AddValidationResponses(ModelState);
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Persists aListing entity while skipping all properties that are managed
        /// </summary>
        /// <param name="changedArticle"></param>
        private void mergeBasicChanges(FeaturedListing changedFeaturedListing, EntityEditCache<FeaturedListing> editHelper, string featuredListingId)
        {
            var cachedFeaturedlisting = editHelper.GetEntity(featuredListingId);
            cachedFeaturedlisting = Mapper.Map<FeaturedListing, FeaturedListing>(changedFeaturedListing, cachedFeaturedlisting);
            cachedFeaturedlisting.Status = changedFeaturedListing.Status;

        }
        
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public PartialViewResult Search(string searchText, int skip, int take = 0)
        {
            var showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            SearchCount = skip + 15;

            var listings = _listingService.Search(searchText, skip, take, true, false, null);
            return PartialView("Listings", listings);
        }
     

        public ActionResult UpdateOrders(string orders, string selectedPage)
        {
            SD.Core.Common.Constants.Page pageName;
            if (Enum.TryParse<SD.Core.Common.Constants.Page>(selectedPage, true, out pageName))
                _featuredListingService.Reorder(orders.Split(',').ToList(), pageName, _featuredListingHydrationSettings);
            return Json("success", @"application/json", JsonRequestBehavior.AllowGet);
        }

        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public ActionResult Delete(string id, string page)
        {
            _featuredListingService.DeleteById(id);

            var model = new FeaturedListingModel();
            //populate dropdown
            var pages = (SD.Core.Common.Constants.Page[])Enum.GetValues(typeof(SD.Core.Common.Constants.Page));
            List<SelectListItem> selectItems = new List<SelectListItem>();
            Page pageName = SD.Core.Common.Constants.Page.Home;
            Enum.TryParse(page, true, out pageName);
            foreach (var p in pages)
            {
                int value = (int)p;
                bool selected = !string.IsNullOrEmpty(page) && pageName == p ? true : false;
                selectItems.Add(new SelectListItem() { Text = p.DisplayName(), Value = value.ToString(), Selected = selected });
            }
            model.SelectedPage = pageName.ToString();
            ViewBag.Pages = selectItems;

            //featured listings
            model.FeaturedListings = _featuredListingService.GetAllByPage(new[] { PublicationStatus.Published, PublicationStatus.UnPublished }, pageName, _featuredListingHydrationSettings);
            return View("Index", model);
        }
    }
}