﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using CMS.Helpers;
using CMS.Models;
using SD.Core.ViewModels;

namespace CMS.Controllers
{
    public class TagsController : Controller
    {
        private const string PaginationMainIndexInstanceKey = "TagsMainIndex";
        private const string PaginationBaseUrl = "/Tags/";
        private readonly TagsService _tagsService;        
        private readonly SD.Core.Services.Core.SponsoredListingService _sponsoredListingService;
        private IMongoRepository1 mongoRepository;
        private readonly SD.Core.Data.Model.HS.SponsoredListingHydrationSettings _sponsoredListingHydrationSettings =
            new SD.Core.Data.Model.HS.SponsoredListingHydrationSettings { RelatedTags = new SD.Core.Data.Model.HS.TagHydrationSettings(), RelatedListings = new SD.Core.Data.Model.HS.ListingHydrationSettings() };
        private readonly SD.Core.Services.Core.ListingService _listingService;
        private const string UserResponseInstanceKey = "Tags";
        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        //user response helper
        private readonly UserResponseHelper _responseHelper;

        public TagsController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);            
            _sponsoredListingService = new SD.Core.Services.Core.SponsoredListingService(mongoRepository);
            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        public TagsController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _tagsService = new TagsService(mongoRepository);            
            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }
        // GET: Tags
        public ActionResult Index()
        {
            return Page();
        }
        private ActionResult Page(int pageNumber = -1, bool? showDeletedListings = null)
        {
            if (pageNumber != -1)
            {
                _mainlistPageHelper.SetPage(pageNumber);
            }
            var tags = _tagsService.All().ToList();
            _mainlistPageHelper.SetTotalItemCount(tags.Count);

            TagsViewModel model = new TagsViewModel();
            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;

            //apply the pager
            int skip = _mainlistPageHelper.PaginationModel.PageSize * (_mainlistPageHelper.PaginationModel.CurrentPage - 1);
            model.Tags = tags.Skip(skip).Take(_mainlistPageHelper.PaginationModel.PageSize).ToList();
            return View("Index", model);
        }

        public void set(string beforeid, string childid)
        {
            var tag = _tagsService.FindByIds(new string[] { childid, beforeid });
            var childTag = tag.FirstOrDefault(i => i.Id == childid);
            var beforeTag = tag.FirstOrDefault(i => i.Id == beforeid);
            if (childTag != null)
            {
                if (beforeTag != null)
                {
                    childTag.ParentId = beforeTag.ParentId;
                    childTag.ParentSlug = beforeTag.ParentSlug;
                    _tagsService.Save(childTag);
                }
                else
                {
                    childTag.ParentId = "";
                    childTag.ParentSlug = "";
                    _tagsService.Save(childTag);

                }
            }

        }


        [CMSAuthorize("UseCMS & AdminFunctions")]
        public ActionResult List()
        {
            return View(_tagsService.All().OrderBy(x => x.DisplayText));
        }
        [CMSAuthorize("UseCMS & AdminFunctions")]
        [HttpPost]
        public ActionResult Create(Tags tag, string Id, FormCollection form)
        {
            try
            {
                var categoryWithSameUrlSlug = _tagsService.FindByUrlSlug(tag.UrlSlug);
                var categoryWithSameName = _tagsService.FindByDisplayText(tag.DisplayText);
                if (categoryWithSameUrlSlug != null || categoryWithSameUrlSlug != null)
                {
                    ViewBag.errMsg = "That urlSlug already exists, please try again. Your changes were NOT saved";

                    var selectListItems = from c in _tagsService.All()
                                          select new SelectListItem { Text = c.DisplayText, Value = c.Id };
                    ViewBag.TagsList = selectListItems.ToList();
                    return View(tag);
                }
                
                if (!string.IsNullOrEmpty(tag.ParentId))
                {
                    var parent = _tagsService.FindById(tag.ParentId);
                    tag.ParentSlug = parent.UrlSlug;
                    tag.ParentTag = parent;   
                    //ViewBag.Msg = tag.DisplayText + "was added successfully added as a sub-category of" + parent.DisplayText;
                }
                
                _tagsService.Save(tag);
                return RedirectToAction("Edit", new { id=tag.Id, isNew = true });
            }
            catch
            {
                return View();
            }
        }

        [CMSAuthorize("UseCMS & AdminFunctions")]
        public ActionResult Create()
        {
            Tags tag = new Tags();
            tag.TagType = 2;
            if (Request.QueryString["ParentId"] != null)
            {
                tag.ParentId = Request.QueryString["ParentId"];
                tag.ParentSlug = Request.QueryString["ParentSlug"];
            }

            List<Tags> parentTags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
            List<SelectListItem> selectedItems = new List<SelectListItem>();
            selectedItems.Add(new SelectListItem() { Text = "", Value = "", Selected = true });
            foreach (var parentTag in parentTags)
            {
                selectedItems.Add(new SelectListItem { Text = parentTag.DisplayText, Value = parentTag.Id });
            }


            ViewBag.TagsList = selectedItems;

            var tagTypeList = new List<SelectListItem>();
            tagTypeList.Add(new SelectListItem() { Text = "Primary Job Role", Value = "0" });
            tagTypeList.Add(new SelectListItem() { Text = "Secondary Job Role", Value = "1" });
            tagTypeList.Add(new SelectListItem() { Text = "Standard", Value = "2" });
            ViewBag.TypeList = tagTypeList.ToList();



            return View(tag);
        }

        [CMSAuthorize("UseCMS & AdminFunctions")]
        public ActionResult Edit(string id, bool isNew = false)
        {
            var tag = _tagsService.FindById(id);
            var selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "----", Value = "", Selected = true });
            var allTags = _tagsService.All();
            foreach (var t in allTags)
            {
                selectListItems.Add(new SelectListItem { Text = t.DisplayText, Value = t.Id });
            }
            ViewBag.TagsList = selectListItems.ToList();

            TagsEditModel model = new TagsEditModel();
            model.Tag = tag;
            var sponsoredListing = _sponsoredListingService.FindByTagId(model.Tag.Id, _sponsoredListingHydrationSettings);
            if (sponsoredListing != null)
                model.RelatedSponsoredListing = _sponsoredListingService.FindByTagId(model.Tag.Id, _sponsoredListingHydrationSettings);
            if (isNew)
            {
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully saved the category: {0}", tag.DisplayText));
                model.Responses = _responseHelper.UserResponses;
            }
            return View("Edit", model);
        }

        [ValidateInput(false)]
        [CMSAuthorize("UseCMS & AdminFunctions")]
        [HttpPost]
        public ActionResult Edit(string id, Tags tag, SponsoredListing relatedSponsoredListing, string sponsoredListingId, string listingId)
        {
            TagsEditModel model = new TagsEditModel();
            if (!string.IsNullOrEmpty(tag.ParentId))
            {
                var parent = _tagsService.FindById(tag.ParentId);
                tag.ParentSlug = parent.UrlSlug;
            }
            if (!string.IsNullOrEmpty(id))
            {
                var savedTag = _tagsService.FindById(id);
                savedTag.ParentId = tag.ParentId;
                savedTag.ParentSlug = tag.ParentSlug;
                savedTag.Description = tag.Description;
                savedTag.DisplayText = tag.DisplayText;
                _tagsService.Save(savedTag);
            }
            else
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified tag to edit");
                return View("Index");
            }
            model.Tag = _tagsService.FindById(id);
            //check if a company was selected
            if (string.IsNullOrEmpty(listingId))
                _responseHelper.AddUserResponse(ResponseType.Error, "A sponsor company is required.");
            SD.Core.Data.Model.Listing listing = new Listing();
            if (!string.IsNullOrEmpty(listingId))
            {
                listing = _listingService.FindById(listingId, null);
            }
            if (!string.IsNullOrEmpty(sponsoredListingId))
            {
                SD.Core.Data.Model.SponsoredListing sponsoredListing = _sponsoredListingService.FindById(sponsoredListingId, _sponsoredListingHydrationSettings);
                sponsoredListing.Status = relatedSponsoredListing.Status;
                sponsoredListing.RelatedListing = listing;
                sponsoredListing.UpdatedBy = MembershipHelper.GetActiveUser();
                sponsoredListing.UpdatedOn = DateTime.Now;
                sponsoredListing.RelatedTag = model.Tag;
                _sponsoredListingService.Save(sponsoredListing);
                model.RelatedSponsoredListing = sponsoredListing;
            }
            else
            {
                SD.Core.Data.Model.SponsoredListing newSponsoredListing = new SponsoredListing();
                newSponsoredListing.RelatedListing = listing;
                newSponsoredListing.RelatedTag = model.Tag;
                newSponsoredListing.CreatedBy = newSponsoredListing.UpdatedBy = MembershipHelper.GetActiveUser();
                newSponsoredListing.CreatedOn = newSponsoredListing.UpdatedOn = DateTime.Now;                
                _sponsoredListingService.Save(newSponsoredListing);
                model.RelatedSponsoredListing = newSponsoredListing;
            }
            var selectListItems = new List<SelectListItem>();
            selectListItems.Add(new SelectListItem { Text = "----", Value = "", Selected = true });
            var allTags = _tagsService.All();
            foreach (var t in allTags)
            {
                selectListItems.Add(new SelectListItem { Text = t.DisplayText, Value = t.Id });
            }
            ViewBag.TagsList = selectListItems.ToList();
            ViewBag.Msg = "Tag Saved";


            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS & AdminFunctions")]
        // [HttpPost]
        public ActionResult Delete(string id)
        {
            var deleteTag = _tagsService.FindByIds(id).FirstOrDefault();          
            _tagsService.Delete(_tagsService.FindByIds(id).FirstOrDefault().Id);
            return RedirectToAction("Index");
        }
    }
}