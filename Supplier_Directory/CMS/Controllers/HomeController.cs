﻿using System.Web.Mvc;
using SD.Core.Services.Security;
using SD.Core.Data;
using SD.Core.Common;
using SD.Core.Services.Core;
using SD.Core.Common.Extensions;
using SD.Core.Common.Extensions.Strings;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using System.Collections.Generic;
using System.Linq;
namespace CMS.Controllers
{
    public class HomeController : BaseController
    {
        private readonly SD.Core.Services.Core.ListingService _listingService;
        private IMongoRepository1 mongoRepository;
        public HomeController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
        }
        public HomeController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
        }
        [CMSAuthorize("UseCMS")]
        public ActionResult Index()
        {
            List<Listing> latestUnmoderatedListings = _listingService.GetAllListings(new[] { SD.Core.Common.Constants.PublicationStatus.UnPublished }, null);
            latestUnmoderatedListings = latestUnmoderatedListings.FindAll(x => !x.IsModerated).OrderByDescending(x => x.CreatedOn).ToList();
              
            return View(latestUnmoderatedListings);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public void UpdateCountryUrlSlugs()
        //{
        //    IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
        //    CountryService countryservice = new CountryService(mongoRepository);            
        //    var countries = countryservice.GetCountriesDTO();
        //    foreach(var c in countries)
        //    {
        //        c.UrlSlug = c.Name.GenerateSlug();
        //        countryservice.Update(c);
        //    }            
        //}

        //public void UpdateRegionUrlSlugs()
        //{
            
        //    IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
        //    RegionService regionservice = new RegionService(mongoRepository);
        //    ListingService service = new ListingService(mongoRepository);
        //    Listing listing = new Listing();
        //    service.Save(listing);
        //    var regions = regionservice.GetRegionsDTO();
        //    foreach (var region in regions)
        //    {
        //        region.UrlSlug = region.Name.GenerateSlug();
        //        regionservice.Update(region);
        //    }
        //}
    }
}