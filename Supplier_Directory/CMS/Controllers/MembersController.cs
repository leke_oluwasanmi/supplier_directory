﻿using AutoMapper;
using CMS.Helpers;
using CMS.Models;
using SD.Core.Common;
using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Security;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class MembersController : Controller
    {        
        private const string PaginationMainIndexInstanceKey = "MemberMainIndex";

        private const string UserResponseInstanceKey = "Member";

        private const string PaginationBaseUrl = "/Members/";
        private const string MemberSearchTextCacheKey = "MemberSearchTextCache";
        private const string MemberSearchCountCacheKey = "MemberSearchCountCache";
        private const int MaxResultsPerTabView = 5;
        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        //Carries Messages
        private readonly UserResponseHelper _responseHelper;

        //database entity
        private IMongoRepository1 mongoRepository;

        //
        private readonly MemberService _memberService;

        //
        private readonly ListingService _listingService;

        //
        private readonly UserService _userService;
        //        

        public readonly CountryService _countryService;
        // Hydration settings
        private static readonly MemberHydrationSettings _memberHydrationSettings;
        private int SearchCount
        {
            get
            {
                if (Session[MemberSearchCountCacheKey] == null)
                    Session[MemberSearchCountCacheKey] = MaxResultsPerTabView;
                return (int)Session[MemberSearchCountCacheKey];
            }
            set
            {
                Session[MemberSearchCountCacheKey] = value;
            }
        }

        static MembersController()
        {
            _memberHydrationSettings = new MemberHydrationSettings
            {
                MemberListings = new ListingHydrationSettings(),
                 Country = new CountryHydrationSettings(),
                  UpdatedBy = new UserHydrationSettings()
            };
        }

        public MembersController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _listingService = new ListingService(mongoRepository);

            //_savedSearchService = new SavedSearchService(mongoRepository);
            _memberService = new MemberService(mongoRepository);

            _userService = new UserService(mongoRepository);
            

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            _countryService = new CountryService(mongoRepository);
        }

        public MembersController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;

            _listingService = new ListingService(mongoRepository);

            //_savedSearchService = new SavedSearchService(mongoRepository);

            _userService = new UserService(mongoRepository);
            _memberService = new MemberService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            _countryService = new CountryService(mongoRepository);
        }

        //
        // GET: /Members/
        [CMSAuthorize("UseCMS & ViewMembers | EditMembers")]
        public ActionResult Index()
        {
            return Page(-1);
            //return View();
        }


        [CMSAuthorize("UseCMS & ViewMembers | EditMembers")]
        public ActionResult Page(int page = -1)
        {

            bool unmoderated = false;
            if (page != -1)
            {
                _mainlistPageHelper.SetPage(page);
            }
            
            var model = new MemberListModel { FilterType = PublicationStatus.Published };
            PublicationStatus[] showWithStatus;

            if (Request.QueryString["unmoderated"] != null)                            
            {
                unmoderated = true;
                showWithStatus = new[] { PublicationStatus.UnPublished };
            }
            else
                showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var hydrationSettings = new MemberHydrationSettings
            {
                MemberListings = new ListingHydrationSettings()
            };

            int skip = _mainlistPageHelper.PaginationModel.PageSize * (_mainlistPageHelper.PaginationModel.CurrentPage - 1);
            int take = _mainlistPageHelper.PaginationModel.PageSize;
            List<Member> results = new List<Member>();
            if (unmoderated)
            {
                results = _memberService.GetAllMembers().Where(x => x.IsModerated == false).ToList();
                _mainlistPageHelper.SetTotalItemCount(results.Count);                
                results = results.Skip(skip).Take(take).ToList();
                model.Members = results.OrderByDescending(x => x.CreatedOn).ToList();

                _mainlistPageHelper.PaginationModel.QueryString = "?unmoderated=true";
            }
            else
            {
                results = _memberService.GetAllMembers().Where(x => x.Status == PublicationStatus.Published || x.Status == PublicationStatus.UnPublished).ToList();
                _mainlistPageHelper.SetTotalItemCount(results.Count);
                results = results.Skip(skip).Take(take).ToList();
                model.Members = results.OrderByDescending(x => x.UpdatedOn).ToList();
            }

            
            // Put the view model together
            

            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;           
            model.Responses = _responseHelper.UserResponses;
            model.ShowDeletedMembers = ShowDeletedMembers;

            // If a search was previously made then reload the results
            if (string.IsNullOrWhiteSpace(SearchText)) return View("Index", model); 
            model.SearchText = SearchText;

            var searchCount = 30; //todo remove later
            model.SearchResults = search(SearchText, 0, searchCount);

            //model.SearchResults = model.ToList();

            return View("Index", model);
        }

        [CMSAuthorize("UseCMS & ViewMembers | EditMembers")]
        public ActionResult Edit(string id)
        {
            using (var editCache = new EntityEditCache<Member>(x => x.Id))
            {
                var model = new MemberEditModel { Member = _memberService.GetById(id, _memberHydrationSettings) };

                if (model.Member == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified member to edit");
                    model.Responses = _responseHelper.UserResponses;
                    return RedirectToAction("Index");
                    //return View("Index", getListingModel(-1));
                }
                ViewBag.Countries = _countryService.GetCountries().Select(x => new SelectListItem
                {
                    //Value = x.Id,
                    Value = x.Id, //temporarily use country name as value field till the Member model support countryId
                    Text = x.Name,
                    Selected = (model.Member.Id == x.Id)
                }).ToList();
                editCache.SetEntity(model.Member);
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditAction(Member member, string submit, string[] tags)
        {
            if (submit.ToLower() == "cancel")
                return RedirectToAction("Index");
            else if (submit.ToLower() == "save")
            {
                using (var editHelper = new EntityEditCache<Member>(x => x.Id))
                {
                   List<Member> duplicates = _memberService.GetAllMembers().Where(x => x.Email == member.Email && member.Id != x.Id).ToList();
                    if(duplicates.Count > 0)
                    {
                        foreach (var dup in duplicates)
                        {
                            _responseHelper.AddUserResponse(ResponseType.Error, string.Format("This email address is already in use by the member: <a href='/Member/edit?id={0}' target='blank'> {1} {2}</a>", 
                                dup.Id, dup.Firstname, dup.Surname));
                        }
                        var model = new MemberEditModel();
                        model.Responses = _responseHelper.UserResponses;
                        return View("Edit", model);
                    }
                    mergeBasicChanges(member, editHelper, member.Id);

                    MemberEditModel editModel = new MemberEditModel();
                    editModel.Member = editHelper.GetEntity(member.Id);
                    editModel.Member.UpdatedBy = MembershipHelper.GetActiveUser();
                    editModel.Member.UpdatedOn = DateTime.Now;               
                    _memberService.Save(editModel.Member);
                    
                    //get back te successful response message
                    _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated the member: {0} {1}", editModel.Member.Firstname, editModel.Member.Surname));                    
                    editModel.Responses = _responseHelper.UserResponses;

                    ViewBag.Countries = _countryService.GetCountries().Select(x => new SelectListItem
                    {
                        //Value = x.Id,
                        Value = x.Id, //temporarily use country name as value field till the Member model support countryId
                        Text = x.Name,
                        Selected = (editModel.Member.CountryId == x.Id)
                    }).ToList();

                    return View("Edit", editModel);
                }
            }
            else
                throw new Exception("Unrecognised EditAction");
        }


        private IList<Member> search(string searchText, int skip, int take)
        {
            PublicationStatus[] showWithStatus;
            //if (ShowDeletedArticles) showWithStatus = new[] { PublicationStatus.Deleted };
            //else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };
            showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var hydrationSettings = new MemberHydrationSettings
            {
                MemberListings = new ListingHydrationSettings()

            };

            var results = _memberService.Search(searchText, showWithStatus, false, hydrationSettings, skip, take);
                //_memberService.Search(searchText, skip, take, true, false, false, showWithStatus, false);

            SearchText = searchText;
            //if (!String.IsNullOrEmpty(SelectedBrand))
            //    return articles.Where(i => i.Brand == SelectedBrand).ToList();
            //else
                return results;
        }

        [HttpPost]
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public void CancelSearch()
        {
            SearchText = string.Empty;
            SearchCount = 0;
        }

        private void mergeBasicChanges(Member changedMember, EntityEditCache<Member> editHelper, string memberId)
        {
            var member = editHelper.GetEntity(memberId);
            member = Mapper.Map<Member, Member>(changedMember, member);
            member.Status = changedMember.Status;
            member.AccountType = changedMember.AccountType;
            member.IsModerated = true;
            member.RelatedCountry = _countryService.FindById(member.CountryId);
        }
        [CMSAuthorize("UseCMS & ViewMembers | EditMembers")]
        public PartialViewResult Search(string searchText, int skip)
        {
            var showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var count = skip + 15;

            var result = search(searchText, skip, 15);//_articleService.Search(searchText,skip, 15, true, false, false, showWithStatus);
            return PartialView("MemberList", result);
        }

        [HttpPost]
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public JsonResult GetTotalSearchResults(string searchText)
        {
            PublicationStatus[] showWithStatus;
            showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var resultCount = 0;

            return Json(resultCount);
        }

        //
        // GET: /Members/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Members/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Members/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Members/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Members/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        private bool ShowDeletedMembers
        {
            get
            {
                if (Session["ShowDeletedMembersCache"] == null) return false;
                else return (bool)Session["ShowDeletedMembersCache"];
            }
            set
            {
                Session["ShowDeletedMembersCache"] = value;
            }
        }
        
        private string SearchText
        {
            get
            {
                if (Session["MemberSearchTextCache"] == null) return string.Empty;
                return (string)Session["MemberSearchTextCache"];
            }
            set
            {
                Session["MemberSearchTextCache"] = value;
            }
        }
    }
}
