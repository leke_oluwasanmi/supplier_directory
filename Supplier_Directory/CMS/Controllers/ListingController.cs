﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Models;
using SD.Core.Common;
using SD.Core.Common.Constants;
using SD.Core.Common.Utilities;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using SD.Core.ViewModels;
using System.Data.Linq;
using System.Text;


namespace CMS.Controllers
{
    public class ListingController : Controller
    {
        private const int MaxResultsPerTabView = 5;
        private const string PaginationMainIndexInstanceKey = "ListingMainIndex";
        private const string UserResponseInstanceKey = "Listing";
        private const string ShowDeletedListingsCacheKey = "ShowDeletedListingsCache";
        private const string ShowListingTypeCacheKey = "ShowListingTypeCacheKey";
        private const string PaginationBaseUrl = "/Listing/";        
        private const string ListingSearchTextCacheKey = "ListingSearchTextCache";
        private const string ListingSearchCountCacheKey = "ListingSearchCountCache";
        
        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        //Carries Messages
        private readonly UserResponseHelper _responseHelper;

        //database entity
        private IMongoRepository1 mongoRepository;

        //
        private readonly MediaService _mediaService;

        //
        private readonly TagsService _tagsService;

        private readonly SD.Core.Services.Core.ListingService _listingService;

        private readonly UserService _userService;

        private static readonly ListingHydrationSettings _listingHydrationSettings;

        public readonly SD.Core.Services.Core.CountryService _countryService;

        private string SearchText
        {
            get
            {
                if (Session[ListingSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[ListingSearchTextCacheKey];
            }
            set
            {
                Session[ListingSearchTextCacheKey] = value;
            }
        }

        private int SearchCount
        {
            get
            {
                if (Session[ListingSearchCountCacheKey] == null)
                    Session[ListingSearchCountCacheKey] = MaxResultsPerTabView;
                return (int)Session[ListingSearchCountCacheKey];
            }
            set
            {
                Session[ListingSearchCountCacheKey] = value;
            }
        }

        private bool ShowDeletedListings
        {
            get
            {
                if (Session[ShowDeletedListingsCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedListingsCacheKey];
            }
            set
            {
                Session[ShowDeletedListingsCacheKey] = value;
            }
        }

        #region constructor
        static ListingController()
        {
            _listingHydrationSettings = new ListingHydrationSettings
            {
                PrimaryMedia = new MediaHydrationSettings(),
                RelatedMedia = new MediaHydrationSettings(),                
                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings(),
                RelatedTags = new TagHydrationSettings(),
                Country = new CountryHydrationSettings(),
                Member = new MemberHydrationSettings()                
            };
        }

        public ListingController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _userService = new UserService(mongoRepository);
            _countryService = new SD.Core.Services.Core.CountryService(mongoRepository);
            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);                        
        }

        public ListingController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _listingService = new SD.Core.Services.Core.ListingService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _userService = new UserService(mongoRepository);
            _countryService = new SD.Core.Services.Core.CountryService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }
        #endregion


        
        // GET: Listing
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public ActionResult Index(bool showUnpublishedOnly = false)
        {
            return Page(-1);
        }

        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public ActionResult Page(int page = -1)
        {
            bool unmoderated = false;
            if (page != -1)
            {
                _mainlistPageHelper.SetPage(page);
            }
            if (Request.QueryString["unmoderated"] != null)
                unmoderated = true;

            return View("Index", getListingModel(page, unmoderated));
        }

        private ListingModel getListingModel(int pageNumber, bool unmoderated = false)
        {            
            ListingModel model = new ListingModel();
            int count = 0;
            PublicationStatus[] showWithStatus;
            if (unmoderated)
            {
                
                showWithStatus = new[] { PublicationStatus.UnPublished };

                model.Listings = model.Listings.Where(x => x.IsModerated == false).ToList();
                count = _listingService.GetAllListings(showWithStatus, null).Where(x => x.IsModerated == false).Count();
                _mainlistPageHelper.SetTotalItemCount(count);
                _mainlistPageHelper.PaginationModel.QueryString = "?unmoderated=true";
            }
            else
            {
                showWithStatus = ShowDeletedListings ? new[] { PublicationStatus.Deleted } : new[] { PublicationStatus.Published, PublicationStatus.UnPublished };
                
                count = _listingService.GetCount("", showWithStatus);
                _mainlistPageHelper.SetTotalItemCount(count);
            }
            // Put the view model together
            

            model.Listings = _listingService.GetPagedOrderedBy(SortOrder.UpdatedOn, _mainlistPageHelper.PaginationModel.PageSize * (_mainlistPageHelper.PaginationModel.CurrentPage - 1),
              _mainlistPageHelper.PaginationModel.PageSize, new string[] { }, showWithStatus, false, false, "",
              new ListingHydrationSettings
              {
                  PrimaryMedia = new MediaHydrationSettings(),
                  RelatedTags = new TagHydrationSettings(),
                   Member = new MemberHydrationSettings(),
                   RelatedMedia = new MediaHydrationSettings(),
                   Country = new CountryHydrationSettings()
              }
              );
                       

            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;
                            
            model.Responses = _responseHelper.UserResponses;
            //if (string.IsNullOrWhiteSpace(SearchText)) return model;
            model.SearchText = SearchText;
            //model.SearchResults = search(SearchText, 0, SearchCount);

            return model;
        }
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public ActionResult Edit(string id)
        {
            using (var editCache = new EntityEditCache<Listing>(x => x.Id))
            {

                ListingEditModel model = new ListingEditModel();
                model.Listing = _listingService.FindById(id, _listingHydrationSettings);

                if (model.Listing == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified listing to edit");
                    return View("Index", getListingModel(-1));
                }
                editCache.SetEntity(model.Listing);
                model.Responses = _responseHelper.UserResponses;
                //get categories        

                PopulateViewCountries(model);
                PopulateViewTags(model);
                return View("Edit", model);
            }
        }

    
        [CMSAuthorize("UseCMS & EditListings")]
        public PartialViewResult AddPrimaryMedia(string listingId, string mediaId)
        {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<Listing>(x => x.Id))
            {
                var listing = editHelper.GetEntity(listingId);
                if (listing == null) throw new Exception("Listing is not in cache");
                // Find the specified media item
                var media = _mediaService.FindByIds(null, mediaId).FirstOrDefault();
                if (media == null) throw new Exception("Couldn't find the specified media item with id: " + mediaId);

                listing.PrimaryMedia = media;

                return PartialView("EditMediaItem", media);
            }
        }

        [CMSAuthorize("UseCMS & EditListings")]
        public PartialViewResult AddRelatedMedia(string listingId, string mediaId)
        {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<Listing>(x => x.Id))
            {
                var listing = editHelper.GetEntity(listingId);
                if (listing == null) throw new Exception("Listing is not in cache");
                // Check that the current article doesnt already have the specified media item with profile
                if (listing.RelatedMedia.Any(x => x.Id == mediaId))
                    throw new Exception("The specified media item already exist for the current article");
                // Get the media item by id
                var media = _mediaService.FindByIds(null, mediaId).FirstOrDefault();
                if (media == null) throw new Exception("Could not find a media item with the specified ID");
                // Add the media item to the current article
                listing.RelatedMedia.Add(media);
                // Render a new partial for the client
                return PartialView("EditMediaItem", media);
            }
        }
        [CMSAuthorize("UseCMS & EditListings")]
        public void RemovePrimaryMedia(string listingId)
        {
            using (var editHelper = new EntityEditCache<Listing>(x => x.Id))
            {
                // Remove the primary media item from the cached article
                var listing = editHelper.GetEntity(listingId);
                listing.PrimaryMedia = null;
            }
        }

        [CMSAuthorize("UseCMS & EditListings")]
        public void RemoveRelatedMedia(string listingId, string mediaId)
        {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<Listing>(x => x.Id))
            {
                var listing = editHelper.GetEntity(listingId);
                if (listing == null) throw new Exception("Listing is not in cache");
                var media = listing.RelatedMedia.SingleOrDefault(x => x.Id == mediaId);
                if (media == null) throw new Exception("Could not find the media item with the specified media ID");
                listing.RelatedMedia.Remove(media);
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditAction(Listing listing, string listingId, string submit, string[] tags)
        {
            if (submit.ToLower() == "cancel")
                return RedirectToAction("Index");
            else if (submit.ToLower() == "save")
            {
                using (var editHelper = new EntityEditCache<Listing>(x => x.Id))
                {
                    mergeBasicChanges(listing, editHelper, listingId);                                     

                    ListingEditModel editModel = new ListingEditModel();
                    editModel.Listing = editHelper.GetEntity(listingId);
                    editModel.Listing.UpdatedBy = MembershipHelper.GetActiveUser();
                    editModel.Listing.UpdatedOn = DateTime.Now;
                    PopulateViewCountries(editModel);
                    PopulateViewTags(editModel);

                    var _tags = _tagsService.FindByIds(tags);
                    editModel.Listing.RelatedTags = _tags;
                    editModel.Listing.Tags.Clear();
                    foreach (var _tag in _tags)
                    {
                        editModel.Listing.Tags.Add(_tag.UrlSlug);
                    }

                    if (!validate(editModel.Listing))
                    {                        
                        editModel.Responses = _responseHelper.UserResponses;
                        return View("Edit", editModel);
                    }
                    _listingService.Save(editModel.Listing);
                    //get back te successful response message
                    _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated the listing: {0}", editModel.Listing.CompanyName));   
                     
                    editModel.Responses = _responseHelper.UserResponses;                                      
                    return View("Edit", editModel);
                }
            }
            else
                throw new Exception("Unrecognised EditAction");
        }
        private void PopulateViewTags(ListingEditModel model)
        {
            var allTags = _tagsService.All().ToList();
            model.AvailableTags = allTags;
            List<string> categories = new List<string>();
            foreach (var tag in model.Listing.RelatedTags)
            {
                StringBuilder categoryText = new StringBuilder();
                var dbTag = allTags.Where(x => x.Id.ToLower() == tag.Id).SingleOrDefault();
                if (dbTag != null)
                {
                    if (!string.IsNullOrEmpty(dbTag.ParentId))
                    {
                        var parentTag = allTags.Where(x => x.Id == dbTag.ParentId).SingleOrDefault();
                        categoryText.Append(parentTag.DisplayText + " > " + dbTag.DisplayText);
                    }
                    else
                        categoryText.Append(dbTag.DisplayText);
                    categories.Add(categoryText.ToString());
                }
                model.Listing.Tags.Add(tag.UrlSlug);
            }

            model.Categories = categories;
        }

        private void PopulateViewCountries(ListingEditModel model)
        {
            ViewBag.Countries = _countryService.GetCountries().Select(x => new SelectListItem
            {
                //Value = x.Id,
                Value = x.Id, //temporarily use country name as value field till the Member model support countryId
                Text = x.Name,
                Selected = (model.Listing.CountryId == x.Id)
            }).ToList();
        }
        private bool validate(Listing Listing)
        {
            var isValid = true;
            if (!ModelState.IsValid)
            {                
                _responseHelper.AddValidationResponses(ModelState);
                isValid = false;
            }
            
            return isValid;
        }

        /// <summary>
        /// Persists aListing entity while skipping all properties that are managed
        /// individually (e.g. RelatedMedia, Tags)
        /// </summary>
        /// <param name="changedArticle"></param>
        private void mergeBasicChanges(Listing changedListing, EntityEditCache<Listing> editHelper, string listingId)
        {
            var listing = editHelper.GetEntity(listingId);
            listing = Mapper.Map<Listing, Listing>(changedListing, listing);
            listing.Status = changedListing.Status;
            listing.ListingType = changedListing.ListingType;
            listing.IsModerated = true;
            listing.RelatedCountry = _countryService.FindById(listing.CountryId);
        }

        [HttpPost]
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public void CancelSearch()
        {
            SearchText = string.Empty;
            SearchCount = 0;
        }

        [HttpPost]
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public JsonResult GetTotalSearchResults(string searchText)
        {
            PublicationStatus[] showWithStatus;
            showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var resultCount = 0;            

            return Json(resultCount);
        }
        [CMSAuthorize("UseCMS & ViewListings | EditListings")]
        public PartialViewResult Search(string searchText, int skip, int take = 0)
        {
            var showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            SearchCount = skip + 15;

            var listings = _listingService.Search(searchText,skip,take,true,false,null);
            return PartialView("Listings", listings);
        }
        /// <summary>
        /// Returns JSON of listings according to companyName
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public ActionResult GetAllListings(string companyName)
        {
            var results = _listingService.Search(companyName, 0, true, true, null);
            return Json(results.Select(x => new
            {
                label = x.CompanyName,
                value = x.Id
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}