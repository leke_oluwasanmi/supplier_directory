﻿using CMS.Helpers;
using CMS.Models;
using SD.Core.Common;
using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Security;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class PagesController : Controller
    {
        private const int MaxResultsPerTabView = 12;

        private const int MaxDiaristSearchResults = 12;

        private const string PaginationMainIndexInstanceKey = "PageMainIndex";

        private const string UserResponseInstanceKey = "Pages";

        private const string PaginationBaseUrl = "/Pages/";

        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        //Carries Messages
        private readonly UserResponseHelper _responseHelper;

        //database entity
        private IMongoRepository1 mongoRepository;

        //
        private readonly PageService _pageService;

        //
        private readonly UserService _userService;
        //
        

        // Hydration settings
        private static readonly PageHydrationSettings _pageHydrationSettings;

        static PagesController()
        {
            _pageHydrationSettings = new PageHydrationSettings
            {
                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings()
            };
        }

        public PagesController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _pageService = new PageService(mongoRepository);

            _userService = new UserService(mongoRepository);

            

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        public PagesController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;

            _pageService = new PageService(mongoRepository);

            _userService = new UserService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }



        //
        // GET: /Pages/
        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        public ActionResult Index(string saved = null, string error = null)
        {
            @ViewBag.ErrorMessage = error;
            @ViewBag.SuccessMessage = saved;
            return Page();
            //return View();
        }

        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        public ActionResult Page(int pageNumber = -1, bool? showDeletedPages = null)
        {
            if (showDeletedPages != null) ShowDeletedPages = (bool)showDeletedPages;

            // Update current page if necessary
            if (pageNumber != -1)
            {
                _mainlistPageHelper.SetPage(pageNumber);
            }


            var model = new PageViewModel { FilterType = PublicationStatus.Published };
            var showWithStatus = ShowDeletedPages ? new[] { PublicationStatus.Deleted } : new[] { PublicationStatus.Published, PublicationStatus.UnPublished };


            var hydrationSettings = new PageHydrationSettings
            {
                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings()
            };

            model.Pages = _pageService.GetAllPages();

            // Put the view model together
            _mainlistPageHelper.SetTotalItemCount(model.Pages.Count());

            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;

            model.Responses = _responseHelper.UserResponses;
            model.ShowDeletedPages = ShowDeletedPages;

            // If a search was previously made then reload the results
            if (string.IsNullOrWhiteSpace(SearchText)) return View("Index", model);
            model.SearchText = SearchText;

            var searchCount = 30; //todo remove later
            model.SearchResults = search(SearchText, 0, searchCount);

            //model.SearchResults = model.ToList();

            return View("Index", model);
        }

        //
        // GET: /Pages/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Pages/Create
        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        public ActionResult Create()
        {
            ViewBag.Title = "Create new page";
            @ViewBag.ErrorMessage = null;
            @ViewBag.SuccessMessage = null;
            var page = new Pages();
            //var model = new PageEditModel { Pages = page };
            return View(page);
        }

        //
        // POST: /Pages/Create
        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(FormCollection collection, Pages model)
        {
            ViewBag.Title = "Create new page";
            @ViewBag.ErrorMessage = null;
            @ViewBag.SuccessMessage = null;
            try
            {
                //if (ModelState.IsValid)

                if (!string.IsNullOrEmpty(model.Title))
                {
                    //encode / decode html 
                    //model.Body = HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(model.Body));
                    model.CreatedById = MembershipHelper.GetActiveUser().UpdatedById;
                    //save result
                    var result = _pageService.CreateNewPage(model.Title, model.Body, model.UrlSlug, model.CreatedById);
                    if (result != null)
                    {
                        //_responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully created the page: {0}", model.Title));

                        @ViewBag.SuccessMessage = string.Format("Successfully created the page: {0}", model.Title);

                        return RedirectToAction("Edit", new { id = model.Id, saved = "true" });
                        //return Page();
                    }
                    else
                    {
                        //ModelState.AddModelError("", "Internal Error Occured. Please try again later");
                        //_responseHelper.AddUserResponse(ResponseType.Error, "Internal Error Occured. Please try again later");

                        @ViewBag.ErrorMessage = "Internal Error Occured. Please try again later";
                    }
                }
                else
                {
                    //ModelState.AddModelError("", "Please review the error and try again");
                    //_responseHelper.AddUserResponse(ResponseType.Error, "Please review the error and try again");

                    @ViewBag.ErrorMessage = "Please review the error and try again";
                }

                var pageModel = new PageEditModel(model);
                pageModel.Responses = _responseHelper.UserResponses;
                return View(pageModel);
            }
            catch (Exception e)
            {
                //ModelState.AddModelError("", e.Message);
                //_responseHelper.AddUserResponse(ResponseType.Error, e.Message);
                @ViewBag.ErrorMessage = e.Message;
                //var pageModel = new PageEditModel(model);
                //pageModel.Responses = _responseHelper.UserResponses;
                return View(model);
            }
        }

        //
        // GET: /Pages/Edit/5
        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        [ValidateInput(false)]
        public ActionResult Edit(string id, string saved = null)
        {
            ViewBag.Title = "Edit Page";

            //var model = new PageEditModel { Pages = _pageService.GetById(id) };
            var model = _pageService.GetById(id);

            if (model.Id == null)
            {
                ViewBag.ErrorMessage = "Could not find the specified page to edit";

                _responseHelper.AddUserResponse(ResponseType.Error, ViewBag.ErrorMessage);
                //model.Responses = _responseHelper.UserResponses;

                return RedirectToAction("Index", new { error = ViewBag.ErrorMessage });
            }

            ViewBag.SuccessMessage = saved;            

            return View("Create", model);
        }

        //
        // POST: /Pages/Edit/5
        [HttpPost]
        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        [ValidateInput(false)]
        public ActionResult Edit(string id, FormCollection collection, Pages page, string saved = null)
        {
            ViewBag.Title = "Edit Page";
            try
            {
                if (!string.IsNullOrEmpty(page.Title))
                {
                    //encode / decode html 
                    //page.Body = HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(page.Body));


                    page.UpdatedById = MembershipHelper.GetActiveUser().UpdatedById;
                    //update
                    _pageService.EditPage(id, page.Title, page.Body, page.UrlSlug, page.UpdatedById);

                    //get the model
                    page = _pageService.GetById(page.Id);
                    //get back the successful response message
                    _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated the page: {0}", page.Title));

                    ViewBag.SuccessMessage = string.Format("Successfully updated the page: {0}", page.Title);


                    var pageModel = new PageEditModel(page);
                    pageModel.Responses = _responseHelper.UserResponses;

                    return View("Create", page);

                    //return RedirectToAction("Index");
                }
                else
                {
                    //ModelState.AddModelError("", "Please review the error and try again");
                    //_responseHelper.AddUserResponse(ResponseType.Error, "Please review the error and try again");
                    @ViewBag.ErrorMessage = "Please review the error and try again";

                    var pageModel = new PageEditModel(page);
                    pageModel.Responses = _responseHelper.UserResponses;
                    return View("Create", page);
                }
            }
            catch (Exception e)
            {
                //ModelState.AddModelError("", e.Message);

                _responseHelper.AddUserResponse(ResponseType.Error, e.Message);

                @ViewBag.ErrorMessage = e.Message;

                var pageModel = new PageEditModel(page);
                pageModel.Responses = _responseHelper.UserResponses;
                return View("Create", page);
            }
        }

        //
        // GET: /Pages/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Pages/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        private IList<Pages> search(string searchText, int skip, int take)
        {
            PublicationStatus[] showWithStatus;
            //if (ShowDeletedArticles) showWithStatus = new[] { PublicationStatus.Deleted };
            //else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };
            showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var hydrationSettings = new PageHydrationSettings
            {
                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings()
            };

            var results = _pageService.Search(searchText, showWithStatus, false, hydrationSettings, skip, take);

            SearchText = searchText;
            //if (!String.IsNullOrEmpty(SelectedBrand))
            //    return articles.Where(i => i.Brand == SelectedBrand).ToList();
            //else
            return results;
        }

        [CMSAuthorize("UseCMS & ViewArticles | EditArticles")]
        public PartialViewResult Search(string searchText, int skip)
        {
            var showWithStatus = ShowDeletedPages ?
                new[] { PublicationStatus.Deleted } :
                new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var count = skip + 15;

            var result = search(searchText, skip, 15);//_articleService.Search(searchText,skip, 15, true, false, false, showWithStatus);
            return PartialView("PageList", result);
        }

        private bool ShowDeletedPages
        {
            get
            {
                if (Session["ShowDeletedPagesCache"] == null) return false;
                else return (bool)Session["ShowDeletedPagesCache"];
            }
            set
            {
                Session["ShowDeletedPagesCache"] = value;
            }
        }

        private string SearchText
        {
            get
            {
                if (Session["PageSearchTextCache"] == null) return string.Empty;
                return (string)Session["PageSearchTextCache"];
            }
            set
            {
                Session["PageSearchTextCache"] = value;
            }
        }
    }
}
