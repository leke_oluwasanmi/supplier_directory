﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Models;
using CMS.Models.Constants;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.CMS;
using SD.Core.Services.Security;
using SD.Core.ViewModels;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Common.Utilities;

namespace CMS.Controllers
    {
    public class MediaController : BaseController
        {
        //private const string MediaModelsCacheKey = "MediaModelsCache";

        private const decimal CropBoxEditHeightCap = 400;
        private const decimal CropBoxEditWidthCap = 400;
        private const decimal CropBoxPreviewHeightCap = 400;
        private const decimal CropBoxPreviewWidthCap = 400;
        private const int DefaultPageRange = 10;
        private const int DefaultPageSize = 20;
        private const int DefaultStartPage = 1;
        private const int MaxFileSize = 104857600;
        private const string MediaFilterTypeCacheKey = "MediaFilterTypeCache";
        private const string MediaSearchCountCacheKey = "MediaSearchCountCache";
        private const string MediaSearchTextCacheKey = "MediaSearchTextCache";
        private const string MediaSearchTypeCacheKey = "MediaSearchTypeCache";
        private const string PaginationBaseUrl = "/Media/";
        private const string PaginationInstanceKey = "MediaIndex";
        private const string ShowDeletedMediaCacheKey = "ShowDeletedMediaCache";
        private const string UserResponseInstanceKey = "Media";
        /* 100 MB */

        // Helpers
        private readonly EntityEditHelper<Media> _mediaEditHelper = new EntityEditHelper<Media>();

        private readonly PaginationHelper _pageHelper;
        private readonly UserResponseHelper _responseHelper;

         private IMongoRepository1 mongoRepository;
        

        // Services
        private MediaService _mediaService;
        public MediaController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();            
            _mediaService = new MediaService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, PaginationBaseUrl, DefaultPageSize);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        public MediaController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;            
            _mediaService = new MediaService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, PaginationBaseUrl, DefaultPageSize);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        #region Index & Page

        //public ActionResult AudioFile()
        //    {
        //    var fileStorageProvider = new AmazonS3FileStorageProvider();

        //    var fileUploadViewModel = new FileUploadViewModel(fileStorageProvider.PublicKey,
        //                                                      fileStorageProvider.PrivateKey,
        //                                                      fileStorageProvider.BucketName,
        //                                                      string.Format("{0}/Media/Create?type=audio", VWGConfig.Instance.Audio.Redirct));

        //    fileUploadViewModel.SetPolicy(fileStorageProvider.GetPolicyString(
        //                    fileUploadViewModel.FileId, fileUploadViewModel.RedirectUrl));

        //    return View(fileUploadViewModel);
        //    }

        //[CMSAuthorize("UseCMS & EditMedia")]
        //public PartialViewResult SearchByDiscipline(string searchText, int skip)
        //{
        //    return PartialView("MediaList", search(searchText, skip, MediaSearchType.ByDiscipline));
        //}
        //[CMSAuthorize("UseCMS & EditMedia")]
        //public PartialViewResult SearchByCompetitor(string searchText, int skip)
        //{
        //    return PartialView("MediaList", search(searchText, skip, MediaSearchType.ByCompetitor));
        //}
        //[CMSAuthorize("UseCMS & EditMedia")]
        //public PartialViewResult SearchByEvent(string searchText, int skip)
        //{
        //    return PartialView("MediaList", search(searchText, skip, MediaSearchType.ByEvent));
        //}
        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        [CMSAuthorize("UseCMS & EditMedia")]
        public void CancelSearch()
            {
            SearchText = string.Empty;
            SearchCount = 0;
            SearchType = MediaSearchType.None;
            }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public ActionResult Filter(MediaFilterType type)
            {
            MediaFilter = type;
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        [HttpPost]
        public JsonResult GetTotalSearchResults(string searchText, MediaSearchType searchType)
            {
            PublicationStatus[] showWithStatus;
            if (ShowDeletedMedia) showWithStatus = new[] { PublicationStatus.Deleted };
            else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var resultCount = 0;

            switch (searchType)
                {
                case MediaSearchType.ByTitle:
                    resultCount = _mediaService.SearchCount(searchText, true, false, showWithStatus, false);
                    break;

                default:
                    throw new ArgumentException("Unrecognised MediaSearchType: " + searchType.ToString());
                }
            return Json(resultCount);
            }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public ActionResult Index()
            {
            return Page();
            }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public ActionResult Page(int pageNumber = -1, bool? showDeletedMedia = null)
            {
            if (showDeletedMedia != null) ShowDeletedMedia = (bool)showDeletedMedia;

            // Save any changes the user has made to the media item
            if (pageNumber != -1) _pageHelper.SetPage(pageNumber);

            return View("Index", getMediaListModel());
            }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public PartialViewResult Search(string searchText, int skip, MediaSearchType searchType)
            {
            SearchCount = skip + DefaultPageSize;
            return PartialView("MediaList", search(searchText, skip, DefaultPageSize, searchType));
            }

        #endregion Index & Page

        #region Create & Edit

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public JsonResult CancelJson()
            {
            _mediaEditHelper.ClearCache();
            return Json(new { success = true });
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public ActionResult ChangeStatus(string id, PublicationStatus? status, int? inDays)
            {
            if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException("id", "Id must be supplied to the ChangeStatus action method");
            var media = _mediaService.FindByIds(null, id).FirstOrDefault();
            if (status != null) media.Status = status.Value;
            if (inDays != null)
                {
                media.LiveFrom = DateTime.Today.AddDays(inDays.Value);
                media.LiveTo = null;
                }
            _mediaService.Save(media);
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public ActionResult Create(int relatedEventId = 0, string relatedDisciplineNameSlug = null)
            {
            if (Request.QueryString["bucket"] != null)
                return SaveAudioFile(Request.QueryString["key"]);

            _mediaEditHelper.ClearCache();
            var model = new MediaEditModel
            {
                Media = _mediaEditHelper.Cache,
            };
            model.Media.ShowInMedia = true;
            model.Media.LiveFrom = DateTime.UtcNow;

            return View("Edit", model);
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        [HttpGet]
        public JsonResult CreateJson()
            {
            _mediaEditHelper.ClearCache();
            return Json(_mediaEditHelper.Cache, JsonRequestBehavior.AllowGet);
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public ActionResult Delete(string id, FormCollection collection)
            {
            try
                {
                // Get the media to delete
                var media = _mediaService.FindByIds(null, id).FirstOrDefault();
                if (media == null)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to delete");
                    return RedirectToAction("Index");
                    }

                // Not working. Temporarily using save instead TODO: Use the DeleteById service method
                //_mediaService.Delete(media.UrlSlug);
                media.Status = PublicationStatus.Deleted;
                _mediaService.Save(media);

                // Clear the media cache
                _mediaEditHelper.ClearCache();
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted media: {0}", media.Title));
                return RedirectToAction("Index");
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
                }
            }

        /// <summary>
        /// Disables a media profile for the current media item and tries to remove the image from disk
        /// </summary>
        /// <param name="profileName">The name of the media profile to remove</param>
        /// <returns>The partial view Media/MediaProfileEditItem.cshtml</returns>
        [CMSAuthorize("UseCMS & EditMedia")]
        public PartialViewResult DisableMediaRatio(MediaRatio? ratio, string id)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to disable must be specified", "ratio");
            // Try to get the media item
            var media = _mediaEditHelper.Cache;
            // Check that the media item has the ratio available to start with
            if (media.AvailableRatios.Contains(ratio.Value))
                {
                // Get all profiles for the given ratio
                foreach (var profileConfig in SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio.Value))
                    {
                    if (profileConfig.ProfileName == MediaProfile.Original) continue;
                    if (MediaFileUtilities.MediaExists(profileConfig, media.FileName))
                        MediaFileUtilities.DeleteMedia(profileConfig, media.FileName);
                    }
                // Remove the ratio from the media items available ratios
                media.AvailableRatios.Remove(ratio.Value);
                _mediaEditHelper.Cache = media;
                }
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new MediaRatioEditItemModel
            {
                Media = media,
                RatioConfig = SDConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };

            return PartialView("MediaRatioEditItem", partialModel);
            }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public ActionResult Edit(string id)
            {
            var model = new MediaEditModel();

            if (string.IsNullOrEmpty(id) || _mediaEditHelper.Cache.Id == id)
                {
                model.Media = _mediaEditHelper.Cache;
                }
            else
                {
                // Find the media to edit
                model.Media = _mediaService.FindByIds(new MediaHydrationSettings
                {                    
                    CreatedBy = new UserHydrationSettings(),
                    UpdatedBy = new UserHydrationSettings()
                }, id).FirstOrDefault();
                if (model.Media == null)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to edit.");
                    return View("Index", getMediaListModel());
                    }
                }

            
            model.MediaRatioEditModels = buildMediaRatioEditModel(model.Media);
            model.Responses = _responseHelper.UserResponses;
            _mediaEditHelper.Cache = model.Media;
            return View("Edit", model);
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditAction(Media media, string submit, FormCollection formCollection, string versionComment)
            {
            mergeBasicChanges(media);
            switch (submit)
                {
                case "Upload Media": return traditionalUploadMedia();
                case "Save": return traditionalSave(null, versionComment);
                case "Publish": return traditionalSave(PublicationStatus.Published, versionComment);
                case "Un-Publish": return traditionalSave(PublicationStatus.UnPublished, versionComment);
                case "Cancel": return cancel();
                default: return traditionalUploadMedia();
                //default: throw new Exception("Unrecognised edit action");
                }
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public PartialViewResult EditMediaRatio(MediaRatio? ratio)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to edit must be specified", "ratio");
            // Try to get the media item
            var media = _mediaEditHelper.Cache;

            var ratioConfig = SDConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value);

            var partialModel = new MediaRatioEditPanelModel
            {
                Media = media,
                CropBoxEditWidth = GetCropBoxEditWidth(ratio.Value, media),
                CropBoxPreviewWidth = GetCropBoxPreviewWidth(ratio.Value),
                RatioConfig = ratioConfig
            };
            return PartialView("MediaRatioEditPanel", partialModel);
            }

        //[CMSAuthorize("UseCMS & EditMedia")]
        //public ActionResult Feature(string id, int position)
        //    {
        //    _mediaService.InsertNewFeatured(id, FeaturedArea.Media, position);
        //    return RedirectToAction("Index");
        //    }

        [CMSAuthorize("UseCMS & ViewMedia | EditMedia")]
        public JsonResult GetProfileConfig(string profileName)
            {
            var profile = MediaUtils.MediaProfileFromName(profileName);
            var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);

            return Json(profileConfig, JsonRequestBehavior.AllowGet);
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public ActionResult HardDelete(string id)
            {
            // Get the media to delete
            var media = _mediaService.FindByIds(null, id).FirstOrDefault();
            if (media == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to delete");
                return RedirectToAction("Index");
                }

            _mediaService.DeleteById(id, false);

            // Clear the media cache
            _mediaEditHelper.ClearCache();
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted media: {0}", media.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public ActionResult Restore(string id)
            {
            // Get the media to delete
            var media = _mediaService.FindByIds(null, id).FirstOrDefault();
            if (media == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to restore");
                return RedirectToAction("Index");
                }

            // Not working. Temporarily using save instead
            //_mediaService.Delete(media.UrlSlug);
            media.Status = PublicationStatus.UnPublished;
            _mediaService.Save(media);

            // Clear the media cache
            _mediaEditHelper.ClearCache();
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully restored media: {0}", media.Title));
            return RedirectToAction("Index");
            }

        

        [CMSAuthorize("UseCMS & EditMedia")]
        [HttpPost]
        public JsonResult SaveJson(Media media)
            {
            mergeBasicChanges(media);
            if (!validate())
                {
                var model = new JsonMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = false
                };
                return Json(model);
                }
            if (!save(PublicationStatus.Published))
                {
                var model = new JsonMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = false
                };
                return Json(model);
                }
            else
                {
                var model = new JsonMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = true,
                    MediaId = _mediaEditHelper.Cache.Id
                };
                // Clear the cache
                _mediaEditHelper.ClearCache();
                return Json(model);
                }
            }

        /// <summary>
        /// TODO: Remove any files that have been written to disk
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// For use when ajax uploading is required
        /// </summary>
        /// <returns></returns>

        [CMSAuthorize("UseCMS & EditMedia")]
        public PartialViewResult SaveMediaRatio(MediaRatio? ratio, int cropX1 = 0, int cropY1 = 0, int cropX2 = 0, int cropY2 = 0)
        {
            var media = _mediaEditHelper.Cache;
            if (!FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
            {

                if (ratio == null) throw new ArgumentException("a MediaRatio to save must be specified", "ratio");

                // First try to find the currently selected media items original file on disk
                //Image originalImage = Image.FromFile(Server.MapPath(MediaHelper.GetMediaPath(MediaProfile.Original, _mediaEditHelper.Cache)));

                var originalProfileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
                var originalImage = MediaFileUtilities.GetImage(originalProfileConfig, _mediaEditHelper.Cache.FileName);

                /* Quite important to make sure that the ratio is added to the media item cache here because otherwise,
                 * when we come to crop (and save) below, the default media path will be resolved and the default image
                 * will be overwritten.
                 */
                if (!media.AvailableRatios.Contains(ratio.Value))
                {
                    media.AvailableRatios.Add(ratio.Value);
                    _mediaEditHelper.Cache = media;
                }

                // Scale the coordinates for the original image size against the size of the client crop box
                decimal cropBoxEditWidth = GetCropBoxEditWidth(ratio.Value, media);
                if (cropBoxEditWidth > 0)
                {
                    var s = (decimal)originalImage.Width / cropBoxEditWidth;
                    cropX1 = (int)Math.Round((decimal)cropX1 * s);
                    cropX2 = (int)Math.Round((decimal)cropX2 * s);
                    cropY1 = (int)Math.Round((decimal)cropY1 * s);
                    cropY2 = (int)Math.Round((decimal)cropY2 * s);

                    crop(originalImage, ratio.Value, cropX1, cropY1, cropX2, cropY2);
                }
            }
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new MediaRatioEditItemModel
            {
                Media = _mediaEditHelper.Cache,
                RatioConfig = SDConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };
            return PartialView("MediaRatioEditItem", partialModel);
        }

        [CMSAuthorize("UseCMS & EditMedia")]
        public PartialViewResult SaveMediaResize(MediaRatio? ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to save must be specified", "ratio");

            // First try to find the currently selected media items original file on disk
            //Image originalImage = Image.FromFile(Server.MapPath(MediaHelper.GetMediaPath(MediaProfile.Original, _mediaEditHelper.Cache)));

            var originalProfileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = MediaFileUtilities.GetImage(originalProfileConfig, _mediaEditHelper.Cache.FileName);

            /* Quite important to make sure that the ratio is added to the media item cache here because otherwise,
             * when we come to crop (and save) below, the default media path will be resolved and the default image
             * will be overwritten.
             */
            var media = _mediaEditHelper.Cache;
            if (!media.AvailableRatios.Contains(ratio.Value))
                {
                media.AvailableRatios.Add(ratio.Value);
                _mediaEditHelper.Cache = media;
                }

            // Scale the coordinates for the original image size against the size of the client crop box
            var s = (decimal)originalImage.Width / (decimal)GetCropBoxEditWidth(ratio.Value, media);
            cropX1 = (int)Math.Round((decimal)cropX1 * s);
            cropX2 = (int)Math.Round((decimal)cropX2 * s);
            cropY1 = (int)Math.Round((decimal)cropY1 * s);
            cropY2 = (int)Math.Round((decimal)cropY2 * s);

            resize(originalImage, ratio.Value, cropX1, cropY1, cropX2, cropY2);
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new MediaRatioEditItemModel
            {
                Media = _mediaEditHelper.Cache,
                RatioConfig = SDConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };
            return PartialView("MediaRatioEditItem", partialModel);
            }

        [CMSAuthorize("UseCMS & EditMedia")]
        public JsonResult UploadMediaJson(string qqFile)
            {
            if (uploadMedia(qqFile))
                return Json(new JsonMediaUploadModel
                {
                    Success = true,
                    Media = _mediaEditHelper.Cache,
                    Responses = _responseHelper.UserResponses
                });
            else
                return Json(new JsonMediaUploadModel
                {
                    Success = false,
                    Media = _mediaEditHelper.Cache,
                    Responses = _responseHelper.UserResponses
                });
            }

        /// <summary>
        /// Returns the profile configuration, in json format, for the specified profile
        /// </summary>
        /// <param name="profileName">The name of the profile to return the configuration for</param>
        /// <returns>The <see cref="MediaProfileElement"/>, in json format, that relates to the specified profile</returns>
        /// <summary>
        /// Crops and resizes the original image and saves to disk. Also marks the media profile as enabled for the current media item if it isnt already.
        /// </summary>
        /// <param name="profileName">The name of the media profile to save for the current media item</param>
        /// <returns>The partial view Media/MediaProfileEditItem.cshtml</returns>

        #region Tags

        

        /// <summary>


        #endregion Tags

        #endregion Create & Edit

        #region Private Methods

        /// <summary>
        /// Auto crops the current media item for all ratios and profiles
        /// </summary>
        private void autoCrop()
            {
            var media = _mediaEditHelper.Cache;

            // First try to find the currently selected media items original file on disk
            var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = MediaFileUtilities.GetImage(profileConfig, media.FileName);

            // If we can't find it then throw an exceptions saying so. From the UI POV, you shouldnt be able to save an image profile before uploading the original
            if (originalImage == null) throw new Exception("Couldn't find the original image on disk. Please upload the original image before auto-cropping it");

            foreach (MediaRatioElement ratioConfig in SDConfig.Instance.MediaRatios)
                {
                var targetWidth = originalImage.Width;
                var targetHeight = originalImage.Height;

                if (((decimal)originalImage.Width / (decimal)originalImage.Height) > ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetWidth = (int)Math.Floor((((decimal)ratioConfig.Width / (decimal)ratioConfig.Height) * originalImage.Height));
                else if (((decimal)originalImage.Width / (decimal)originalImage.Height) <= ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetHeight = (int)Math.Floor((((decimal)ratioConfig.Height / (decimal)ratioConfig.Width) * originalImage.Width));

                // Get the crop coordinates from the target width and height values above
                var cropX1 = (int)Math.Round((((decimal)originalImage.Width - (decimal)targetWidth) / 2), 0, MidpointRounding.AwayFromZero);
                var cropY1 = (int)Math.Round((((decimal)originalImage.Height - (decimal)targetHeight) / 2), 0, MidpointRounding.AwayFromZero);
                var cropX2 = cropX1 + targetWidth;
                var cropY2 = cropY1 + targetHeight;

                crop(originalImage, ratioConfig.RatioName, cropX1, cropY1, cropX2, cropY2);
                }
            }

        private IList<MediaRatioEditItemModel> buildMediaRatioEditModel(Media mediaItem)
            {
            IList<MediaRatioEditItemModel> model = new List<MediaRatioEditItemModel>();
            // Build Media Profile view model data
            foreach (MediaRatio ratio in Enum.GetValues(typeof(MediaRatio)))
                {
                var ratioConfig = SDConfig.Instance.MediaRatios.GetRatioConfig(ratio);

                model.Add(new MediaRatioEditItemModel
                {
                    Media = mediaItem,
                    RatioConfig = ratioConfig
                });
                }
            return model;
            }

        /// <summary>
        /// TODO: Remove any files that have been written to disk
        /// </summary>
        /// <returns></returns>
        private ActionResult cancel()
            {
            _mediaEditHelper.ClearCache();
            return RedirectToAction("Index");
            }

        private void CapHeightGetWidth(Media media, ref int width, ref int height)
            {
            //decimal aspectRatio = media.SourceHeight > media.SourceWidth ?
            //        ((decimal)media.SourceHeight / (decimal)media.SourceWidth) :
            //        ((decimal)media.SourceWidth / (decimal)media.SourceHeight);
            var aspectRatio = ((decimal)media.SourceWidth / (decimal)media.SourceHeight);
            height = (int)Math.Floor(CropBoxEditHeightCap);
            width = (int)Math.Floor(height * aspectRatio);
            }

        private void CapWidthGetWidth(Media media, ref int width, ref int height)
            {
            //decimal aspectRatio = media.SourceHeight < media.SourceWidth ?
            //        ((decimal)media.SourceHeight / (decimal)media.SourceWidth) :
            //        ((decimal)media.SourceWidth / (decimal)media.SourceHeight);
            var aspectRatio = ((decimal)media.SourceHeight / (decimal)media.SourceWidth);
            width = (int)Math.Floor(CropBoxEditWidthCap);
            height = (int)Math.Floor(width * aspectRatio);
            }

        private void crop(Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            var media = _mediaEditHelper.Cache;

            // For each profile using the supplied ratio
            var profileConfigs = SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledMedia = MediaUtils.CropImage(originalImage, largestProfile.Width, largestProfile.Height, cropX1, cropX2, false, cropY1, cropY2);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
                {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledMedia, Server.MapPath(MediaHelper.GetMediaPath(profileConfig.ProfileName, media)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                MediaFileUtilities.SaveImage(profiledMedia, profileConfig, media.FileName);//, media.Title, media.OriginalFileName);
                }
            // Enable this ratio for the selected media item if it isnt already
            if (!media.AvailableRatios.Contains(ratio))
                {
                media.AvailableRatios.Add(ratio);
                _mediaEditHelper.Cache = media;
                }
            }

        private void resize(Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            var media = _mediaEditHelper.Cache;

            // For each profile using the supplied ratio
            var profileConfigs = SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledMedia = MediaUtils.ResizeImage(originalImage, largestProfile.Width, largestProfile.Height);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
                {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledMedia, Server.MapPath(MediaHelper.GetMediaPath(profileConfig.ProfileName, media)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                MediaFileUtilities.SaveImageNoResize(profiledMedia, profileConfig, media.FileName);//, media.Title, media.OriginalFileName);
                }
            // Enable this ratio for the selected media item if it isnt already
            if (!media.AvailableRatios.Contains(ratio))
                {
                media.AvailableRatios.Add(ratio);
                _mediaEditHelper.Cache = media;
                }
            }

        private int GetCropBoxEditWidth(MediaRatio ratio, Media media)
            {
            var newWidth = media.SourceWidth;
            var newHeight = media.SourceHeight;
            if (media.SourceWidth > CropBoxEditWidthCap)
                {
                CapWidthGetWidth(media, ref newWidth, ref newHeight);
                if (newHeight > CropBoxEditHeightCap) CapHeightGetWidth(media, ref newWidth, ref newHeight);
                }
            else if (newHeight > CropBoxEditHeightCap)
                {
                CapHeightGetWidth(media, ref newWidth, ref newHeight);
                if (newWidth > CropBoxEditWidthCap) CapWidthGetWidth(media, ref newWidth, ref newHeight);
                }
            if (newHeight > CropBoxEditHeightCap || newWidth > CropBoxEditWidthCap) throw new Exception("CropBox edit capping failed somehow");
            return newWidth;
            }

        private int GetCropBoxPreviewWidth(MediaRatio ratio)
            {
            var previewWidth = 0;
            var ratioConfig = SDConfig.Instance.MediaRatios.GetRatioConfig(ratio);
            if (ratioConfig.Width > ratioConfig.Height)
                {
                previewWidth = (int)Math.Floor(CropBoxEditWidthCap);
                }
            else
                {
                previewWidth = (int)Math.Floor(CropBoxEditHeightCap * ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height));
                }
            if (previewWidth > CropBoxEditWidthCap) throw new Exception("CropBox preview capping failed somehow");
            return previewWidth;
            }

        private MediaListModel getMediaListModel(int page = -1)
            {
            var model = new MediaListModel();

            var showWithStatus = ShowDeletedMedia ?
                new[] { PublicationStatus.Deleted } :
                new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            model.FilterType = MediaFilter;

            if (MediaFilter == MediaFilterType.Image ||
                MediaFilter == MediaFilterType.Video ||
                MediaFilter == MediaFilterType.Audio ||
                MediaFilter == MediaFilterType.Document)
                {
                // We're displaying a media list
                var formatGroup = MediaHelper.FileFormatGroupFromMediaFilterType(MediaFilter);
                model.Media = _mediaService.GetPagedOrderedByCreatedOn(
                    DefaultPageSize * (_pageHelper.PaginationModel.CurrentPage - 1),
                   DefaultPageSize,
                    formatGroup,
                    showWithStatus,
                    false,
                    false);

                _pageHelper.SetTotalItemCount(_mediaService.GetPagedCount(formatGroup, showWithStatus, ShowDeletedMedia));
                model.ShowDeletedMedia = ShowDeletedMedia;
                }

            model.Pagination = _pageHelper.PaginationModel;
            model.Responses = _responseHelper.UserResponses;

            // If a search was previously made then reload the results
            if (SearchType != MediaSearchType.None && !string.IsNullOrWhiteSpace(SearchText))
                {
                model.SearchType = SearchType;
                model.SearchText = SearchText;
                model.SearchResults = search(SearchText, 0, SearchCount, SearchType);
                }

            return model;
            }

        /// <summary>
        /// Merge any basic changes that have been made with the cached media item.
        /// We need to skip out the more complex members because they're managed
        /// independently by methods such as SaveMediaProfile, AddRelatedDiscipline, etc...
        /// </summary>
        /// <param name="changedMedia"></param>
        /// <returns></returns>
        private void mergeBasicChanges(Media changedMedia)
            {
            var cachedMedia = _mediaEditHelper.Cache;

            cachedMedia = Mapper.Map<Media, Media>(changedMedia, cachedMedia);
            cachedMedia.Hosting = changedMedia.Hosting;

            _mediaEditHelper.Cache = cachedMedia;
            }

        private bool save(PublicationStatus? publicationStatus, string versionComment = null)
            {
            var media = _mediaEditHelper.Cache;

            //hacky, but no way of knowing when it's in brightcove
            if (media.Hosting == MediaHosting.RemoteBrightCove)
                media.Format = FileFormat.AVI;

            // Update publication status if requested
            if (publicationStatus != null)
                {
                media.Status = publicationStatus.Value;
                _mediaEditHelper.Cache = media;
                }

            try
                {
                // Save the cached media item
                _mediaService.Save(media, versionComment);
                // Update the cache (mainly so that we can access the new id if it was inserted)
                _mediaEditHelper.Cache = media;
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully saved media: {0}", media.Title));
                return true;
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
                }
            }

        private ActionResult SaveAudioFile(string filename)
            {
            // Get the media item we're editing from the cache
            var media = _mediaEditHelper.Cache;

            // Update the cached media item
            media.FileName = filename.Replace("audio/", "");
            media.Format = FileFormat.MP3;

            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded media");
            // Update the media cache
            _mediaEditHelper.Cache = media;

            return RedirectToAction("Edit");
            }

        private IList<Media> search(string searchText, int skip, int take, MediaSearchType searchType)
            {
            PublicationStatus[] showWithStatus;
            if (ShowDeletedMedia) showWithStatus = new[] { PublicationStatus.Deleted };
            else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            IList<Media> media;

            switch (searchType)
                {
                case MediaSearchType.ByTitle:
                    media = _mediaService.Search(searchText, skip, take, true, false, showWithStatus, false);
                    break;

                default:
                    throw new ArgumentException("Unrecognised MediaSearchType: " + searchType.ToString());
                }

            SearchText = searchText;
            SearchType = searchType;

            return media;
            }

        private ActionResult traditionalSave(PublicationStatus? publicationStatus, string versionComment)
            {
            var model = new MediaEditModel();
            model.Media = _mediaEditHelper.Cache;

            if (!validate())
                {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
                }

            if (!save(publicationStatus, versionComment))
                {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
                }

            model.Responses = _responseHelper.UserResponses;
            return View("Edit", model); // Success
            }

        private ActionResult traditionalUploadMedia()
            {
            if (uploadMedia())
                return RedirectToAction("Edit");
            else
                return RedirectToAction("Edit");
            }

        private bool uploadMedia(string originalFilename = null)
            {
            var stream = Request.InputStream;
            byte[] buffer = null;
            try
                {
                if (originalFilename == null)
                    {
                    if (Request.Files[0].ContentLength > MaxFileSize)
                        {
                        _responseHelper.AddUserResponse(ResponseType.Error, "Error", "Maximum file size is 100MB. ");
                        return false;
                        }
                    // IE or traditional form post used with Webkit, Mozilla
                    var postedFile = Request.Files[0];
                    stream = postedFile.InputStream;
                    originalFilename = Path.GetFileName(Request.Files[0].FileName);
                    }
                else
                    {
                    //Webkit, Mozilla and valums file upload was used
                    //...
                    }

                buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", "There was a problem getting file data from input stream: " + ex.Message);
                return false;
                }

            // Get the media item we're editing from the cache
            var media = _mediaEditHelper.Cache;
            media.OriginalFileName = originalFilename;
            if (buffer.Length == 0 || string.IsNullOrWhiteSpace(originalFilename))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "No File specified", "Please select a media file to upload and then select upload");
                return false;
                }

            // Get configuration for the Original media profile
            var originalProfileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            // Check if this media item already has an original file on disk
            if (!string.IsNullOrWhiteSpace(media.FileName))
                {
                if (MediaFileUtilities.MediaExists(originalProfileConfig, media.FileName))
                    MediaFileUtilities.DeleteMedia(originalProfileConfig, media.FileName);
                }

            // Make sure the media item is of a recognised format
            if (!FileUtilities.ExtensionIsRecognised(Path.GetExtension(originalFilename)))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "The extension of the file you uploaded has not been recognised");
                return false;
                }
            // Update the cached media item
            //media.FileName = Guid.NewGuid().ToString() + Path.GetExtension(originalFilename).ToLower();
            media.FileName = Guid.NewGuid().ToString() + originalFilename.ToLower();
            media.Format = FileUtilities.FormatFromFilename(originalFilename);
            media.CreatedOn = DateTime.Now;
            try
                {
                // Save to disk
                if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                    {
                    MediaFileUtilities.SaveImage(stream, originalProfileConfig, media.FileName);//, media.Title, originalFilename);
                    }
                else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
                {
                    MediaFileUtilities.SaveDocument(stream, media.FileName);//, media.Title, originalFilename);
                }
                else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
                    {
                    MediaFileUtilities.SaveAudio(stream, media.FileName); //, media.Title, originalFilename);
                    }
                else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
                    {
                    throw new NotImplementedException("Local video uploading is not implemented. Use remote hosting.");
                    }
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
                }

            // If the media is an image then load it from disk so we can access its dimensions
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                {
                var originalImage = MediaFileUtilities.GetImage(originalProfileConfig, media.FileName);
                media.SourceWidth = originalImage.Width;
                media.SourceHeight = originalImage.Height;
                }

            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded media");
            // Update the media cache
            _mediaEditHelper.Cache = media;

            // If the media is an image then Auto crop for all ratios and profiles
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image)) autoCrop();

            return true;
            }

        private bool validate()
            {
            if (!ModelState.IsValid)
                {
                _responseHelper.AddValidationResponses(ModelState);
                return false;
                }

            var media = _mediaEditHelper.Cache;

            if (media.Hosting == MediaHosting.Local && FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("{0} media cannot be hosted locally. The media you upload must be an image.", media.Format.ToString()));
                return false;
                }
            if (media.Hosting == MediaHosting.Document && FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("{0} media cannot be hosted locally. The media you upload must be a document.", media.Format.ToString()));
                return false;
            }
            if ((media.Hosting != MediaHosting.Local && media.Hosting != MediaHosting.Document) &&
                string.IsNullOrWhiteSpace(media.RemoteItemCode))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("Remote item code is required for remotely hosted media"));
                return false;
                }

            return true;
            }

        #endregion Private Methods

        #region Private Properties

        private MediaFilterType MediaFilter
            {
            get
                {
                if (Session[MediaFilterTypeCacheKey] == null)
                    Session[MediaFilterTypeCacheKey] = MediaFilterType.Image;
                return (MediaFilterType)Session[MediaFilterTypeCacheKey];
                }
            set
                {
                Session[MediaFilterTypeCacheKey] = value;
                // Reset the pagination model everytime the MediaFilter is changed so that we start at the beginning again.
                _pageHelper.Reset();
                }
            }

        private int SearchCount
            {
            get
                {
                if (Session[MediaSearchCountCacheKey] == null)
                    Session[MediaSearchCountCacheKey] = DefaultPageSize;
                return (int)Session[MediaSearchCountCacheKey];
                }
            set
                {
                Session[MediaSearchCountCacheKey] = value;
                }
            }

        private string SearchText
            {
            get
                {
                if (Session[MediaSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[MediaSearchTextCacheKey];
                }
            set
                {
                Session[MediaSearchTextCacheKey] = value;
                }
            }

        private MediaSearchType SearchType
            {
            get
                {
                if (Session[MediaSearchTypeCacheKey] == null)
                    Session[MediaSearchTypeCacheKey] = MediaSearchType.None;
                return (MediaSearchType)Session[MediaSearchTypeCacheKey];
                }
            set
                {
                Session[MediaSearchTypeCacheKey] = value;
                }
            }

        private bool ShowDeletedMedia
            {
            get
                {
                if (Session[ShowDeletedMediaCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedMediaCacheKey];
                }
            set
                {
                Session[ShowDeletedMediaCacheKey] = value;
                }
            }

        #endregion Private Properties
        }
    }