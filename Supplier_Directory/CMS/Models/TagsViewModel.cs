﻿using SD.Core.Data.Model;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Models
{
    public class TagsViewModel
    {
        public List<SD.Core.Data.Model.Tags> Tags = new List<SD.Core.Data.Model.Tags>();
        public PaginationModel Pagination;
        public int TotalSearchResults;
        public IList<SD.Core.Data.Model.Tags> SearchResults;
        public TagsViewModel()
        {
            Tags = new List<SD.Core.Data.Model.Tags>();
        }
    }

    
    public class TagsEditModel
    {
        public SD.Core.Data.Model.Tags Tag;
        public SponsoredListing RelatedSponsoredListing;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public TagsEditModel()
        {
            Tag = new Tags();
            RelatedSponsoredListing = new SponsoredListing();
        }
    }
}