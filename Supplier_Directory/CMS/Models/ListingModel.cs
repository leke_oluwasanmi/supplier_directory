﻿using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;

namespace CMS.Models
{
    [Serializable]
    public class ListingModel
    {
        public IList<Listing> Listings = new List<Listing>();       
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public string SearchText;
        public int TotalSearchResults;
        public IList<Listing> SearchResults;        
    }


    [Serializable]
    public class ListingEditModel
    {
        public Listing Listing;
        public Test Test;        
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();        
        public IList<string> Categories = new List<string>();
        public IList<Tags> AvailableTags = new List<Tags>();       
        public ListingEditModel(Listing listing)
        {
            Listing = listing;
        }

        public ListingEditModel()
        {

        }
    }
}