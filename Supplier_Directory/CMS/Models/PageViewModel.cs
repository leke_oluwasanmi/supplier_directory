﻿using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Models
{
    [Serializable]
    public class PageViewModel
    {
        public IList<Pages> Pages = new List<Pages>();
        public IList<Pages> PublishedPages = new List<Pages>();
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        //public IList<Listing> Listing = new List<Listing>();
        //public IList<SavedSearch> SavedSearch = new List<SavedSearch>();
        public bool ShowDeletedPages = false;

        public PublicationStatus FilterType;
        public string SearchText;
        public int TotalSearchResults;
        public IList<Pages> SearchResults;
    }


    [Serializable]
    public class PageEditModel
    {
        public Pages Pages;        
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        //public IList<ArticleType> articleTypes = new List<ArticleType>();
        //public IList<Tags> availableTags = new List<Tags>();
        public User CreatedBy;
        public User UpdatedBy;
        public int ViewCount;
        public bool Versioned;

        //public IList<ArticleCommentItem> Comments = new List<ArticleCommentItem>();

        public string AuthorAuthenticatedUserName;
        public string AuthorAuthenticatedUserId;
        public PageEditModel(Pages pages)
        {
            Pages = pages;
        }

        public PageEditModel()
        {
            Pages = new Pages();
        }
    }
}