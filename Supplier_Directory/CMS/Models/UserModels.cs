﻿using SD.Core.Common.Config;
using SD.Core.Data.Model;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;

namespace CMS.Models
{
    [Serializable]
    public class UserEditModel
    {
        public User User { get; set; }

        public IList<PermissionsElement> AllPermissionConfigurations;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
    }

    [Serializable]
    public class PasswordResetModel
    {
        public User User { get; set; }

        public string PasswordResetTicketKey { get; set; }

        public string BaseUrl { get; set; }
    }

    public class UserListModel
    {
        public IList<User> Users = new List<User>();
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
    }
}