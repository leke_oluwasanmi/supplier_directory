﻿using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Models
{
    [Serializable]
    public class MemberListModel
    {
        public IList<Member> Members = new List<Member>();
        public IList<Member> PublishedMembers = new List<Member>();
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<Listing> Listing = new List<Listing>();        
        public bool ShowDeletedMembers = false;

        public PublicationStatus FilterType;
        public string SearchText;
        public int TotalSearchResults;
        public IList<Member> SearchResults;

        //public IList<Member> AreaFeatured { get; set; }
    }

    [Serializable]
    public class MemberEditModel
    {
        public Member Member;
        public Test Test;
        //public IList<VersionWrapper> VersionHistory;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        //public IList<string> Categories = new List<string>();
        //public User CreatedBy;
        //public User UpdatedBy;
        //public int ViewCount;
        //public bool Versioned;        

        //public string AuthorAuthenticatedUserName;
        //public string AuthorAuthenticatedUserId;
        public MemberEditModel(Member member)
        {
            Member = member;
        }

        public MemberEditModel()
        {

        }
    }
}