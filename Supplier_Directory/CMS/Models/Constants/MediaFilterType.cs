﻿
namespace CMS.Models.Constants
{
    public enum MediaFilterType
    {
        Image,
        Video,
        Audio,
        Resource,
        Document
    }
}