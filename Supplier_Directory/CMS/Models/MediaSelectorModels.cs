﻿using SD.Core.Data.Model;
using System;
using System.Collections.Generic;

namespace CMS.Models
{
    [Serializable]
    public class MediaResults
    {
        public IList<MediaResultItem> Results = new List<MediaResultItem>();
        public ResultSetType Type;
    }

    [Serializable]
    public class MediaResultItem
    {
        public Media Media { get; set; }

    }
    [Serializable]
    public enum ResultSetType
    {
        MediaRecent,
        MediaSearchAny,

        LogoMediaSearchByEvent,

        GalleryRecent,
        GallerySearchAny
    }

}
