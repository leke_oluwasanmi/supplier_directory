﻿using SD.Core.Data.Model;
using SD.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Models
{
    [Serializable]
    public class FeaturedListingEditModel
    {
        public FeaturedListing FeaturedListing;        
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();       

        public FeaturedListingEditModel()
        {
            FeaturedListing = new FeaturedListing();
        }
    }

    [Serializable]
    public class FeaturedListingModel
    {
        public List<FeaturedListing> FeaturedListings = new List<FeaturedListing>();
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public string SelectedPage = "";     
    }
}