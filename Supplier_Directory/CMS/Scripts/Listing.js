﻿$(function () {
    // Constants
    var searchUrls = {
        listings: "/Listing/Search",        
    };
   
    searchCountUrl = "/Listings/GetTotalSearchResults";
    searchCancelUrl = "/Listings/CancelSearch";

    var currentSearchType = "listings";

    // Global variables 
    var searchTimer;
    var resultCount = 0;

    // Initialisation
    bindEvents();
    initialiseViewState();

    function bindEvents() {
        $("#SearchBox").keyup(searchBoxChanged);
         $(".search-cancel").click(handleSearchCancel);
         $("#searchMore").click(moreResults);
        $("#searchListings").click(switchSearchView);        
    }



    function initialiseViewState() {
      if (hncms.showSearchResults) {
            $("#searchTermsLabel").html($("#SearchBox").val());
            resultCount = $("#searchedArticleList > li").length;
            showSearch(true);
        } else {
            showSearch(false);
        }
    }

    function handleSearchCancel(e) {
        articleSearchCancel();
        $("#SearchBox").val("");
        showSearch(false);
    }

    function moreResults(e) {
        makeSearch($("#SearchBox").val(), false);
        return false;
    }



    // Event handlers
    function searchBoxChanged(e) {
        var searchBox = $(e.currentTarget);
         resultCount = 0;
        if (searchBox.val() == "" || searchBox.val() == " ") {
            articleSearchCancel();
            $("#searchedList").html("");
            $("#ListContainer").show();
            $("#SearchResultsContainer").hide();
        } else {
            $("#ListContainer").hide();
            $("#SearchResultsContainer").show();
            $("#searchTermsLabel").html(searchBox.val());
            clearTimeout(searchTimer);
            var searchTerms = searchBox.val();
            searchTimer = setTimeout(function () { makeSearch(searchTerms, true) }, 1000);
        }
        return false;
    }

    function switchSearchView(e) {
        switch ($(e.currentTarget).attr("id")) {
            case "search":
                currentSearchType = "listings";
                break;            
        }
        makeSearch($("#SearchBox").val());
    }


    function showSearch(show) {
        if (show) {
            $(".search-cancel").removeClass("disabled");
            $("#ListContainer").hide();
            $("#SearchResultsContainer").show();
        } else {
            $(".search-cancel").addClass("disabled");
            $("#ListContainer").show();
            $("#SearchResultsContainer").hide();
        }
    }


 
    // Private methods
    function makeSearch(searchText, clearPreviousResults) {
        showSearch(true);
        var searchUrl = searchUrls[currentSearchType];
        $.ajax({
            url: searchUrl,
            data: {
                searchText: searchText ,
                skip: resultCount,               
            },           

            success: function (partialView) {

                // Check if there are any more results available
                getTotalResultCount(searchText, {
                    success: function (totalResultCount) {
                        if (resultCount >= totalResultCount) {
                            $("#searchMore").addClass("disabled");
                        } else {
                            $("#searchMore").removeClass("disabled");
                        }
                    }
                });

                var results = partialView;
                resultCount += $("<div />").html(results).find("li").length;
                if (clearPreviousResults) {
                    $("#searchedList").html(results);
                } else {
                    $("#searchedList").append(results);
                }
            },

            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    function articleSearchCancel() {
        showSearch(false);
        $.ajax({
            url: searchCancelUrl,
            type: "POST",
            success: function () {

            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    function getTotalResultCount(searchText, options) {
        $.ajax({
            url: searchCountUrl,
            type: "POST",
            data: {
                searchText: searchText
            },
            success: function (totalResultCount) {
                if (options && options.success) options.success(totalResultCount);
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }


});