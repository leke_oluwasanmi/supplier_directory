﻿$(function () {

    $("#listings").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/listing/getallListings", type: "POST", dataType: "json",
                data: { companyName: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.label, value: item.label, id: item.value };
                    }));
                },

            });
        },
        select: function (event, ui) {
            //update the jQuery selector here to your target hidden field
            $("#listingId").val(ui.item.id);
        }
    });
});


