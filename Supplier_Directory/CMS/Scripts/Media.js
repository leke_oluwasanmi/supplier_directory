﻿

$(function () {

    // Variable that holds the data which will be sent to the server when the user saves a media profile
    var cropData = {
        profileName: "",
        cropX1: 0,
        cropY1: 0,
        cropX2: 0,
        cropY2: 0
    };

    // Initialisations
    bindEvents();



    function bindEvents() {
        // User clicks enable/disable for a media profile
        $("#mediaProfiles > li > input[type=checkbox]").click(toggleMediaProfile);
        // User saves the edited media profile
        $("#saveMediaProfile").click(saveMediaProfileEdit);
        // User cancels media profile edit
        $("#cancelMediaProfileEdit").click(cancelMediaProfileEdit);
        // Hosting type changed
        $("#mediaHosting").change(hostingChanged);
    }

    function toggleMediaProfile(e) {
        // Get the profile name
        cropData.profileName = $(e.currentTarget).parent().attr("id").replace("mediaProfile_", "");
        switch ($(e.currentTarget).is(":checked")) {
            case true:
                // Edit the profile
                $.ajax({
                    url: "/Media/EditMediaProfile",
                    data: cropData,
                    success: function (data) {
                        $("#editMediaProfile").html(data);
                        $("#editMediaProfiles").hide();
                        $("#editMediaProfile").show();
                        initialiseCropTool();
                        bindEvents();
                    },
                    error: function (xhr, status, msg) {
                        alert("Error:" + msg);
                    }
                });
                break;
            case false:
                // Disable the profile
                $.ajax({
                    url: "/Media/DisableMediaProfile",
                    data: cropData,
                    success: function (data) {
                        replaceMediaProfileEditItem(data);
                        bindEvents();
                    },
                    error: function (xhr, status, msg) {
                        alert("Error:" + msg);
                    }
                });
                break;
        }
        return false;
    }



    function saveMediaProfileEdit() {
        $.ajax({
            url: "/Media/SaveMediaProfile",
            data: cropData,
            success: function (data) {
                replaceMediaProfileEditItem(data);
                bindEvents();
            },
            error: function (xhr, status, msg) {
                alert("Error:" + msg);
            }
        });
        cancelMediaProfileEdit();
        return false;
    }

    function replaceMediaProfileEditItem(newEditItem) {
        var newEditItem = $(newEditItem);
        // Find the equivalent element already in the media profile list
        var existingEditItem = $("#mediaProfiles > li[id=" + newEditItem.attr("id") + "]");
        existingEditItem.after(newEditItem);
        existingEditItem.remove();
    }



    function cancelMediaProfileEdit() {
        $("#editMediaProfiles").show();
        $("#editMediaProfile").hide();
        return false;
    }

    // Initialise the crop tool
    function initialiseCropTool() {
        // Get the aspect ratio from the size of mediaProfilePreview
        var previewBox = $("#mediaProfilePreview");
        var aspectRatio = previewBox.height() / previewBox.width();
        var stringRatio = "1:" + aspectRatio;
        $('#mediaFullCropbox').imgAreaSelect({
            zIndex: 20000,
            aspectRatio: stringRatio,
            handles: true,
            onSelectChange: previewProfiledImage,
            onSelectEnd: function (img, selection) {
                cropData.cropX1 = selection.x1;
                cropData.cropY1 = selection.y1;
                cropData.cropX2 = selection.x2;
                cropData.cropY2 = selection.y2;
            }
        });
    }

    function previewProfiledImage(img, selection) {

        var previewBoxWidth = $("#mediaProfilePreview").width();
        var previewBoxHeight = $("#mediaProfilePreview").height();

        var scaleX = previewBoxWidth / (selection.width || 1);
        var scaleY = previewBoxHeight / (selection.height || 1);

        var fullCropBoxWidth = $("#mediaFullCropbox").width();
        var fullCropBoxHeight = $("#mediaFullCropbox").height();

        $('#mediaProfilePreview img').css({

            width: Math.round(scaleX * fullCropBoxWidth) + 'px',
            height: Math.round(scaleY * fullCropBoxHeight) + 'px',
            marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
            marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
        });
    }

    function hostingChanged(e) {
        switch ($(e.currentTarget).val()) {
            case "Local":
                $(".local_hosting_properties").show();
                $(".remote_hosting_properties").hide();
                break;
            default:
                $(".local_hosting_properties").hide();
                $(".remote_hosting_properties").show();
                break;
        }
    }

});