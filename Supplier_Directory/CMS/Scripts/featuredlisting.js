﻿$(document).ready(function () {
    

    $("#sortable").sortable({ cursor: "move" });
    $("#sortable").on("sortupdate", function (event, ui) {
        $("#orders").val($(this).sortable('toArray').toString());
        $.ajax({
            url: '/featuredlisting/updateorders?orders=' + $("#orders").val() + "&selectedPage=" + $("#selectedPage").val()  //statscontroller/categoryviewed
        });
    });
});