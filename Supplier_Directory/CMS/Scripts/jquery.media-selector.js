﻿/*

Notes:
    
    - Based on the jQuery template from http://css-tricks.com/snippets/jquery/jquery-plugin-template/

Dependencies:

    - jqote2
    - Twitter Bootstrap framework (javascript and css)

*/

(function ($) {

    var instanceReference = 0;

    $.mediaSelector = function (trigger, options) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$trigger = $(trigger);
        base.trigger = trigger;

        // Store the instance reference number
        base.instanceNumber = instanceReference;
        // Declare a variable to hold the current tab
        base.currentTab;
        // Declare the timeout variable used for searching
        base.searchTimer;
        // Increment the instance reference
        instanceReference++;

        // Add a reverse reference to the DOM object
        base.$trigger.data("mediaSelector", base);

        // Instance variables
        base.cachedResults = {};

        base.init = function () {

            base.options = $.extend({}, $.mediaSelector.defaultOptions, options);
            // If media base url is missing then try and get it from the script registrars client variable namespace
            if (base.options.baseMediaUrl == "" && hncms.baseMediaUrl) base.options.baseMediaUrl = hncms.baseMediaUrl;

            // Put your initialization code here

            // Set the jqote tag
            $.jqotetag("*");

            // Render the media selector markup
            base.renderMediaSelector();

            // Load the initial tab. base.$widget is crucial for any DOM related tasks such as event binding
            base.loadTab(base.options.startupTab, function () {
                // Hide the widget to start with
                //if (!base.options.autoShow) base.$widget.hide();
            });

            // Set up the media selector as a modal
            $("#mediaSelector" + base.instanceNumber).modal({
                show: false
            });

        };

        // Sample Function, Uncomment to use
        // base.functionName = function(paramaters){
        // 
        // };
        base.tabClickHandler = function (e) {
            var tabName = $(e.currentTarget).attr("rel");
            base.loadTab(tabName);
            return false;
        };
        base.mediaClickHandler = function (e) {
            base.options.mediaSelected($(e.currentTarget).attr("rel"));
            return false;
        };
        base.galleryClickHandler = function (e) {

        };
        base.loadTab = function (tabName, success) {
            base.currentTab = tabName;
            getResults(success);
            console.log("Switched to tab " + base.currentTab);
        };
        base.renderMediaSelector = function () {
            // Package the data required to render the container markup
            var viewModel = {
                instanceNumber: base.instanceNumber,
                tabSettings: base.options.tabSettings
            };
            base.$widget = $($("#mediaSelectorTemplate").jqote(viewModel));
            // Get the current tab config
            var tabConfig = base.options.tabSettings[base.options.startupTab];
            // Display the correct tabs
            console.log("renderMediaSelector:" + tabConfig.queryType)
            switch (tabConfig.queryType) {
                case "Search":
                    console.log("switch to search mode:")
                    switchToSearchMode();
                    break;
                case "List":
                    console.log("switch to browse mode:")
                    switchToBrowseMode();
                    break;
            }
            // Bind layout events
            bindLayoutEvents();
            // Insert the widget into the document
            $("body").append(base.$widget);

            console.log("Rendered the core MediaSelector markup.");
        };
        base.renderTabView = function (viewModel) {
            // If the tab view already exists then remove it
            var tabViewContainer = base.$widget.find(".media-selector-tab-views");
            var tabView = tabViewContainer.find("." + viewModel.selectedTabSettings.viewClass);
            if (tabView.length != 0) tabView.remove();
            // Render the tab view with the supplied model
            var $tabViewMarkup = $($.jqote("#mediaSelectorTabView", viewModel));
            // Bind events to the new tab view
            bindTabViewEvents($tabViewMarkup);

            base.$widget.find(".media-selector-tab-views").html($tabViewMarkup);

        };
        base.open = function () {
            base.$widget.modal("show");
            // base.$widget.show();
            return false;
        };
        base.close = function () {
            base.$widget.modal("hide");
            // base.$widget.hide();
            return false;
        };

        // Private functions

        // Bind events for the media selector markup including tabs
        function bindLayoutEvents() {
            base.$trigger.click(base.open);
            //                $("#mediaSelector" + base.instanceNumber + " .media-selector-tab-container li a").live("click", base.tabClickHandler);
            //                $("a.add-media-item").live("click", base.mediaClickHandler);

            base.$widget.find(".media-selector-tab-container li a").click(base.tabClickHandler);
            base.$widget.find("#mediaSearch").keyup(searchTextChanged);
        }
        function bindTabViewEvents($tabViewMarkup) {
            $tabViewMarkup.find("a.add-media-item").click(base.mediaClickHandler);
        }
        function searchTextChanged(e) {
            console.log("Search Text Changed");
            var searchBox = $(e.currentTarget);
            if (searchBox.val() == "" || searchBox.val() == " ") {
                getResults();
            } else {
                clearTimeout(base.searchTimer);
                base.searchTimer = setTimeout(function () {
                    getResults();
                }, base.options.searchTimeout);
            }
            return false;
        }
        function getResults(success) {
            // Get the tab config
            var tabConfig = base.options.tabSettings[base.currentTab];

            var searchBox = base.$widget.find("#mediaSearch");

            // Make sure that the correct tab type is active
            if ((searchBox.val() == "" || searchBox.val() == " ") && tabConfig.queryType == "Search") {
                console.log("switching to browse mode");
                switchToBrowseMode();
                base.loadTab(base.options.defaultBrowseTab);
                return;
            } else if (searchBox.val() != "" && searchBox.val() != " " && searchBox.val() != undefined && tabConfig.queryType == "List") {
                console.log("searchBox.val() = " + searchBox.val() + ":");
                console.log("switching to search mode");
                switchToSearchMode();
                base.loadTab(base.options.defaultSearchTab);
                return;
            }

            // Build the query data for a search or list request to the server
            var queryData = {
                skip: 0
            };


            switch (tabConfig.queryType) {
                case "Search":
                    // Search tab is selected so build a search query
                    queryData.searchText = searchBox.val();
                    break;
                case "List":

                    break;
            }


            // Check if the tab requires new data to be loaded or if it can use results from the cache
            if (tabConfig.cache && base.cachedResults[base.currentTab] != undefined) {
                base.renderTabView(base.cachedResults[base.currentTab]);
                return;
            }

            $.ajax({
                // url: base.options.tabSettings[base.options.defaultBrowseTab].queryUrl,
                url: tabConfig.queryUrl,
                dataType: "JSON",
                data: queryData,
                success: function (data) {

                    // Build the view model
                    var viewModel = $.extend({
                        selectedTabSettings: tabConfig,
                        baseMediaUrl: base.options.baseMediaUrl,
                        defaultGalleryThumbnail: base.options.defaultGalleryThumbnail
                    }, data);

                    // Cache data if required
                    if (tabConfig.cache) base.cachedResults[base.currentTab] = viewModel;

                    // Render the data
                    base.renderTabView(viewModel);

                    if (success) return success();
                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
        }
        function switchToBrowseMode() {
            base.$widget.find(".browse-tab").show();
            base.$widget.find(".search-tab").hide();
        }
        function switchToSearchMode() {
            base.$widget.find(".browse-tab").hide();
            base.$widget.find(".search-tab").show();
        }


        // Run initializer
        base.init();
    };

    $.mediaSelector.defaultOptions = {
        autoShow: false,
        baseMediaUrl: "",
        defaultGalleryThumbnail: "default_gallery_icon.jpg",
        loaded: function () { },
        mediaSelected: function (mediaId) { },
        startupTab: "RecentMedia",
        defaultSearchTab: "AnyMedia",
        defaultBrowseTab: "RecentMedia",
        searchTimeout: 1000,
        tabSettings: {
            "RecentMedia": {
                label: "Recent Media",
                labelClass: "media-selector-tab-recent-media",
                viewClass: "media-selector-tab-view-recent-media",
                queryUrl: "/MediaSelector/RecentMedia",
                queryType: "List",
                cache: true
            },
            "AnyMedia": {
                label: "Any Media",
                labelClass: "media-selector-tab-any-media",
                viewClass: "media-selector-tab-view-any-media",
                queryUrl: "/MediaSelector/SearchByTitle",
                queryType: "Search",
                cache: false
            },
        }
    };

    $.fn.mediaSelector = function (options, action) {
        return this.each(function () {
            var selector = new $.mediaSelector(this, options, action);

            // HAVE YOUR PLUGIN DO STUFF HERE

            if (action) {
                selector[action]();
            }

            // END DOING STUFF

        });
    };
    return false;
})(jQuery);