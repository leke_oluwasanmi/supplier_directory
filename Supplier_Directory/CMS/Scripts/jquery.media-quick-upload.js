﻿/*

Notes:
    
- Based on the jQuery template from http://css-tricks.com/snippets/jquery/jquery-plugin-template/

Dependencies:

- JQuery: JQote2
- Twitter Bootstrap framework (javascript and css)
- JQuery: ImgAreaSelect
- JQuery: ToDictionary (http://erraticdev.blogspot.co.uk/2010/12/sending-complex-json-objects-to-aspnet.html)
- JQuery: ParseDates (http://erraticdev.blogspot.co.uk/2010/12/converting-dates-in-json-strings-using.html)

*/

(function ($) {

    var instanceReference = 0;

    $.mediaQuickUpload = function (trigger, options) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // *** Constants ***

        var ADD_TAGS_HTML = "<li><a href='#' class='add-tags'>Edit</a></li>";
        var NO_RESULTS_HTML = "<li class='no-results'>No Results</li>";

        // *** Instance Variables ***

        // Access to jQuery and DOM versions of element
        base.$trigger = $(trigger);
        base.trigger = trigger;

        // Store the instance reference number
        base.instanceNumber = instanceReference;
        // Increment the instance reference
        instanceReference++;

        base.mediaItem = null;

        base.cropData = null;

        base.queuedMediaRatios = null;

        base.uploader = null;

        // Add a reverse reference to the DOM object
        base.$trigger.data("mediaQuickUpload", base);

        base.init = function () {

            base.options = $.extend({}, $.mediaQuickUpload.defaultOptions, options);

            // *** Initialisation Code ***
            renderWidget();
            bindEvents();
            createUploader();
            // Start at stage 1
            switchStage(1);
            // Set the jqote tag
            $.jqotetag("*");

            // Set up the tag selector as a modal
            $("#mediaQuickUpload" + base.instanceNumber).modal({
                show: false
            });

        };

        // Public methods

        base.open = function () {
            if (base.mediaItem == null) { create(); }
            base.$widget.modal("show");
            return false;
        };
        base.close = function () {
            base.$widget.modal("hide");
            return false;
        };

        // Private methods

        function renderWidget() {
            var viewModel = {
                instanceNumber: base.instanceNumber
            };
            // Render the widget
            base.$widget = $($("#mediaQuickUploadTemplate").jqote(viewModel));
            // Insert the widget into the DOM
            $("body").append(base.$widget);
            //console.log("Rendered the core MediaQuickUpload markup.");
        }

        function initialiseCropTool() {
            // Get the original image
            var $fullImg = base.$widget.find('#mediaFullCropbox');

            // Get the aspect ratio from the size of mediaRatioPreview
            var $previewBox = base.$widget.find("#mediaRatioPreview");
            var aspectRatio = $previewBox.height() / $previewBox.width();
         
            var stringRatio = "1:" + aspectRatio;

            //console.log("Width: " + $fullImg.width());
            //console.log("Height: " + $fullImg.height());
            
            // Calculate the initial crop area coordinates
            if (aspectRatio < ($fullImg.height() / $fullImg.width())) {
                // Use the full img width as the starting point
                base.cropData.cropX2 = $fullImg.width();
                base.cropData.cropY2 = Math.round(Number($fullImg.width() * $previewBox.height() / $previewBox.width()));
            } else {
                // Use the full img height as the starting point
                base.cropData.cropY2 = $fullImg.height();
                base.cropData.cropX2 = Math.round(Number($fullImg.height() * $previewBox.width() / $previewBox.height()));
            }

            $fullImg.imgAreaSelect({
                zIndex: 20000,
                aspectRatio: stringRatio,
                handles: true,
                x1: base.cropData.cropX1,
                x2: base.cropData.cropX2,
                y1: base.cropData.cropY1,
                y2: base.cropData.cropY2,
                onSelectChange: function (img, selection) {
                    if (isNaN(selection.x1) || isNaN(selection.y1) || isNaN(selection.width) || isNaN(selection.height)) return;
                    updateCropPreview(selection.x1, selection.y1, selection.width, selection.height);

                },
                onSelectEnd: function (img, selection) {
                    base.cropData.cropX1 = selection.x1;
                    base.cropData.cropY1 = selection.y1;
                    base.cropData.cropX2 = selection.x2;
                    base.cropData.cropY2 = selection.y2;
                }
            });
            updateCropPreview(
                base.cropData.cropX1,
                base.cropData.cropY1,
                base.cropData.cropX2 - base.cropData.cropX1,
                base.cropData.cropY2 - base.cropData.cropY1);
        }

        function bindEvents() {
            base.$trigger.click(base.open);
            base.$widget.on("click", ".cancel, .close", cancel);
            base.$widget.on("click", ".save", save);
            base.$widget.on("click", ".disable-ratio", disableMediaRatio);
            base.$widget.on("click", ".next-crop", saveMediaRatio);
            base.$widget.on("click", ".next-resize", resizeMediaRatio);
            base.$widget.on("click", ".next-final", saveDetails);
        }

        function createUploader() {
            base.uploader = new qq.FileUploader({
                element: base.$widget.find(".media-uploader").get()[0],
                action: '/Media/UploadMediaJson',
                debug: true,
                extraDropzones: [qq.getByClass(document, "media-uploader-drop-area")[0]],
                onComplete: uploadComplete
            });
        }

        function create() {
            $.ajax({
                url: base.options.createMediaUrl,
                dataType: "json",
                success: function () {
                    //...
                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
        }

        function switchStage(stageNumber) {
            $(".stage-label").removeClass("current");
            $("#stage" + stageNumber + "Label").addClass("current");
            $(".stage-view").hide();
            $(".stage-" + stageNumber + "-view").show();
            switch (stageNumber) {
                case 1:
                    // Stage 1 means we're starting over so reset any data from the previous run
                    base.queuedMediaRatios = hncms.mediaRatios.slice();
                    resetCropData();
                    break;
                case 4:
                    // Stage 4 means we're done editing details so update the cached media item

                    break;
            }
        }

        function uploadComplete(id, filename, jsonResponse) {
            base.mediaItem = jsonResponse.Media;
            switchStage(2);
           // stringRepresentationOfRatio(base.cropData.ratio);
            cropNextRatio();
        }

        function updateCropPreview(x1, y1, width, height) {
            var previewBoxWidth = base.$widget.find("#mediaRatioPreview").width();
            var previewBoxHeight = base.$widget.find("#mediaRatioPreview").height();

            var scaleX = previewBoxWidth / (width || 1);
            var scaleY = previewBoxHeight / (height || 1);

            var fullCropBoxWidth = base.$widget.find("#mediaFullCropbox").width();
            var fullCropBoxHeight = base.$widget.find("#mediaFullCropbox").height();

            base.$widget.find('#mediaRatioPreview img').css({

                width: Math.round(scaleX * fullCropBoxWidth) + 'px',
                height: Math.round(scaleY * fullCropBoxHeight) + 'px',
                marginLeft: '-' + Math.round(scaleX * x1) + 'px',
                marginTop: '-' + Math.round(scaleY * y1) + 'px'
            });
        }

        function editMediaRatio(ratio) {
            base.cropData.ratio = ratio
            $.ajax({
                url: base.options.editMediaRatioUrl,
                data: {
                    ratio: base.cropData.ratio
                },
                success: function (data) {
                    base.$widget.find(".crop-area").html(data);
                    // Wait until the image is loaded before initialising the crop tool
                    base.$widget.find(".crop-area img").bind("load", function () {
                        initialiseCropTool();
                    });
                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
            return false;
        }

        function saveMediaRatio() {
            $.ajax({
                url: base.options.saveMediaRatioUrl,
                data: base.cropData,
                success: function (data) {
                    clearQueuedRatio(base.cropData.ratio);
                    cropNextRatio();
                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
            closeMediaRatioEdit();
            return false;
        }

        function stringRepresentationOfRatio(ratio)
        {
           // alert("Ratio " + ratio);
            if (ratio == 0 || ratio == "0")
            {
                $("#spanRatioInfo").html("Editing ratio 6x4")
            }
            else if (ratio == 1 || ratio == "1")
            {
                $("#spanRatioInfo").html("Editing ratio 6x5")
            }
            else if (ratio == 2 || ratio == "2")
            {
                $("#spanRatioInfo").html("Editing ratio banner")
            }
        }
        function resizeMediaRatio() {
            var ratioText = base.cropData.ratio;
            $.ajax({
                url: base.options.resizeMediaRatioUrl,
                data: base.cropData,
                success: function (data) {
                    clearQueuedRatio(base.cropData.ratio);
                    cropNextRatio();
                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
            closeMediaRatioEdit();
            // stringRepresentationOfRatio(base.cropData.ratio);
        
            var testy = "";
            if (ratioText == 0)
                testy = "Ratio 6x4";
            if (ratioText == 1)
                testy = "Ratio 6x5";
            if (ratioText == 2)

            $("<span style='color:white !important; font-size: 14px !important;'>Resized " + testy + "</span>").insertBefore(".btn-group").fadeOut(3000);
            return false;
        }

        function disableMediaRatio(ratio) {
            alert('disable');
            $.ajax({
                url: base.options.disableMediaRatioUrl,
                data: base.cropData,
                success: function (data) {
                    clearQueuedRatio(base.cropData.ratio);
                    cropNextRatio();
                },
                error: function (a, b, c) {
                    alert("Error");
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
        }

        function cropNextRatio() {
            resetCropData();
            switch (base.queuedMediaRatios.length) {
                case 0:
                    switchStage(3);
                    break;
                default:
                    editMediaRatio(base.queuedMediaRatios[0]);
                    break;
            }
            stringRepresentationOfRatio(base.cropData.ratio);
            return false;
        }

        function clearQueuedRatio(ratio) {
            base.queuedMediaRatios.splice(base.queuedMediaRatios.indexOf(ratio), 1);
        }

        function closeMediaRatioEdit() {
            // Clean up cropping tool overlay elements. Dont know why we have to do this and dont have time to look into it...
            $(".imgareaselect-outer").remove();
            $(".imgareaselect-selection").parent().remove();
            return false;
        }

        function resetCropData() {
            base.cropData = {
                ratio: 0,
                cropX1: 0,
                cropY1: 0,
                cropX2: 0,
                cropY2: 0
            };
        }

        function saveDetails() {
            base.mediaItem.Title = base.$widget.find("input[name='title']").val();
            base.mediaItem.SEOTitle = base.$widget.find("input[name='seoTitle']").val();
            base.mediaItem.MetaDescription = base.$widget.find("textarea[name='metaDescription']").val();
            base.mediaItem.Credit = base.$widget.find("input[name='credit']").val();
            switchStage(4);
            return false;
        }

        function save() {
            // Process base.mediaItem before sending it back

            //base.mediaItem.UpdatedOn = (new Date(parseInt(base.mediaItem.UpdatedOn.substr(6))));toString("dd MM yyyy");
            base.mediaItem.UpdatedOn = null;
            base.mediaItem.CreatedOn = null;
            base.mediaItem.LiveFrom = (new Date(parseInt(base.mediaItem.LiveFrom.substr(6)))).toString("dd MM yyyy");
            $.ajax({
                url: base.options.saveMediaUrl,
                type: "POST",
                dataType: "json",
                data: $.toDictionary({ media: base.mediaItem }),
                success: function (result) {
                    if (result.Success) {
                        displayUserResponses(result.Responses);
                        base.options.complete(result.MediaId);
                        setTimeout(function () {
                            // Close the modal
                            base.close();
                            // Reset the plugin
                            reset();
                        }, 1000);
                    } else {
                        displayUserResponses(result.Responses)
                    }

                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
            return false;
        }

        function cancel() {
            // Close the modal
            base.close();
            // Reset the plugin
            reset();
            // Tell server to cancel the edit
            $.ajax({
                url: base.options.cancelMediaUrl,
                type: "POST",
                dataType: "json",
                data: {},
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                }
            });
            return false;
        }

        function reset() {
            // Clear crop data
            resetCropData();
            // Clear details fields
            base.$widget.find("input[name='title']").val("");
            base.$widget.find("input[name='seoTitle']").val("");
            base.$widget.find("textarea[name='metaDescription']").val("");
            base.$widget.find("input[name='credit']").val("");
            // Tidy up crop tool elements
            closeMediaRatioEdit();
            base.$widget.find(".crop-area").html("");
            // Switch back to stage 1
            switchStage(1);
            // Reset the file uploader
            createUploader();
            // Reset user responses
            base.$widget.find(".user-responses").html("");
        }

        function displayUserResponses(responses) {
            base.$widget.find(".user-responses").html($("#mediaQuickUploadResponseTemplate").jqote(responses));
        }

        // Run initializer
        base.init();
    };

    $.mediaQuickUpload.defaultOptions = {
        editMediaRatioUrl: "/Media/EditMediaRatio",
        disableMediaRatioUrl: "/Media/DisableMediaRatio",
        saveMediaRatioUrl: "/Media/SaveMediaRatio",
        resizeMediaRatioUrl: "/Media/SaveMediaResize",
        createMediaUrl: "/Media/CreateJson",
        saveMediaUrl: "/Media/SaveJson",
        cancelMediaUrl: "/Media/CancelJson",
        complete: function () { }
    };

    $.fn.mediaQuickUpload = function (options, action) {
        return this.each(function () {
            (new $.mediaQuickUpload(this, options, action))

            // HAVE YOUR PLUGIN DO STUFF HERE

            if (action) {
                selector[action]();
            }

            // END DOING STUFF

        });
    };

    $.fn.getMediaQuickUpload = function () {
        return this.data("mediaQuickUpload");
    };

})(jQuery);