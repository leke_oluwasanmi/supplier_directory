﻿$(function () {
    // Constants
    var searchUrls = {
        article: "/Article/Search",
        byDiscipline: "/Article/SearchByDiscipline",
        byCompetitor: "/Article/SearchByCompetitor",
        byEvent: "/Article/SearchByEvent"
    };

    var UPDATE_FEATURED_ORDER_URL = "/Article/UpdateFeaturedOrder";

    searchCountUrl = "/Article/GetTotalSearchResults";
    searchCancelUrl = "/Article/CancelSearch";

    var currentSearchType = "article";

    // Global variables 
    var searchTimer;
    var resultCount = 0;

    // Initialisation
    bindEvents();
    initialiseViewState();

    function bindEvents() {
        $("#articleSearchBox").keyup(searchBoxChanged);
         $(".search-cancel").click(handleSearchCancel);
         $("#searchMoreArticles").click(moreResults);
        $("#searchArticles").click(switchSearchView);
        $("#searchDiscipline").click(switchSearchView);
        $("#searchCompetitor").click(switchSearchView);
        $("#searchEvent").click(switchSearchView);
    }

    function setupSortableFeaturedList() {
        $("ul#featuredList").sortable({
            update: function (e, ui) {
                updateFeaturedOrder();
            }
        });
        $("ul#featuredList").disableSelection();
    }

    function initialiseViewState() {
      if (hncms.showSearchResults) {
            $("#searchTermsLabel").html($("#articleSearchBox").val());
            resultCount = $("#searchedArticleList > li").length;
            showSearch(true);
        } else {
            showSearch(false);
        }
    }

    function handleSearchCancel(e) {
        articleSearchCancel();
        $("#articleSearchBox").val("");
        showSearch(false);
    }

    function moreResults(e) {
        makeSearch($("#articleSearchBox").val(), false);
        return false;
    }



    // Event handlers
    function searchBoxChanged(e) {
        var searchBox = $(e.currentTarget);
         resultCount = 0;
        if (searchBox.val() == "" || searchBox.val() == " ") {
            articleSearchCancel();
            $("#searchedArticleList").html("");
            $("#articleListContainer").show();
            $("#articleSearchResultsContainer").hide();
        } else {
            $("#articleListContainer").hide();
            $("#articleSearchResultsContainer").show();
            $("#searchTermsLabel").html(searchBox.val());
            clearTimeout(searchTimer);
            var searchTerms = searchBox.val();
            searchTimer = setTimeout(function () { makeSearch(searchTerms, true) }, 1000);
        }
        return false;
    }

    function switchSearchView(e) {
        switch ($(e.currentTarget).attr("id")) {
            case "searchArticles":
                currentSearchType = "article";
                break;
            case "searchDisciplines":
                currentSearchType = "byDiscipline";
                break;
            case "searchCompetitor":
                currentSearchType = "byCompetitor";
                break;
            case "searchEvent":
                currentSearchType = "byEvent";
                break;
        }
        makeSearch($("#articleSearchBox").val());
    }


    function showSearch(show) {
        if (show) {
            $(".search-cancel").removeClass("disabled");
            $("#articleListContainer").hide();
            $("#articleSearchResultsContainer").show();
        } else {
            $(".search-cancel").addClass("disabled");
            $("#articleListContainer").show();
            $("#articleSearchResultsContainer").hide();
        }
    }


    function updateOrder() {

        // Build an array of discipline codes in order
        var articleIdArr = [];
        $("ul#featuredList > li").each(function () {
            featuredCodeArr.push($(this).children("input.hidden-featured-id").val());
        });

        // Send to server to update
        $.ajax({
            url: UPDATE_FEATURED_ORDER_URL,
            type: "POST",
            dataType: "json",
            data: { orderedFeaturedArticleIds: articleIdArr },
            traditional: true,
            success: function () {
                // Notification?
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    // Private methods
    function makeSearch(searchText, clearPreviousResults) {
        showSearch(true);
        var searchUrl = searchUrls[currentSearchType];
        $.ajax({
            url: searchUrl,
            data: {
                searchText: searchText ,
                skip: resultCount,               
            },           

            success: function (partialView) {

                // Check if there are any more results available
                getTotalResultCount(searchText, {
                    success: function (totalResultCount) {
                        if (resultCount >= totalResultCount) {
                            $("#searchMoreArticles").addClass("disabled");
                        } else {
                            $("#searchMoreArticles").removeClass("disabled");
                        }
                    }
                });

                var results = partialView;
                resultCount += $("<div />").html(results).find("li").length;
                if (clearPreviousResults) {
                    $("#searchedArticleList").html(results);
                } else {
                    $("#searchedArticleList").append(results);
                }
            },

            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    function articleSearchCancel() {
        showSearch(false);
        $.ajax({
            url: searchCancelUrl,
            type: "POST",
            success: function () {

            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    function getTotalResultCount(searchText, options) {
        $.ajax({
            url: searchCountUrl,
            type: "POST",
            data: {
                searchText: searchText
            },
            success: function (totalResultCount) {
                if (options && options.success) options.success(totalResultCount);
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }


});