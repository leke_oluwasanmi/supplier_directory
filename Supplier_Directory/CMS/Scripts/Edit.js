﻿//scrolling to anchor links
function backToTopFunction() {

    var scrolltotop = function () {

        if ($(window).scrollTop() > 100) {
            $(".backtotopbutton").fadeIn();
        } else {
            $(".backtotopbutton").fadeOut();
        }
    }

    $(".backtotopbutton").click(function () {
        $('html,body').animate({ scrollTop: 0 }, 750, 'swing');
        return false;
    });

    scrolltotop();

    $(window).scroll(function () {
        scrolltotop();
    });

};


//scrolling to anchor links
function contentTabsArea() {

    //tabs throughout website
    $('.span9 form fieldset').hide();
    $('.span9 form fieldset:first').show();
    $('.span3 .nav-list li:first a').addClass('active');

    $('.span3 .nav-list li a').click(function () {
        $('.span3 .nav-list li').removeClass('active');
        $(this).parent("li").addClass('active');
        var currentTab = $(this).attr('id').replace("sm", "").toLowerCase();
        var amalgatab = currentTab + "Tab";
        $('.span9 form fieldset').hide();
        $('.span9 form #' + amalgatab).each(function () {
            $(this).show();
        });

//        var id = $(this).attr('id');
//        if (id) {
//            var currentTab = id.replace("sm", "").toLowerCase();
//            var amalgatab = currentTab + "Tab";
//            $('.span9 form fieldset').hide();
//            $('.span9 form #' + amalgatab).each(function () {
//                $(this).show();
//            });
//        }

        // alert(amalgatab);
        return false;
    });

};
/*
function alignButtons() {

    var $topSideBar = $(".row-fluid .span3 .sidebar-nav");

    var offset = $topSideBar.offset();
    var thisheight = $topSideBar.innerHeight();
    var buttonplacement = (offset.top + thisheight) + 20;
    $topSideBar.css({ 'marginBottom': 105 });

    $(".btn-group-fixed").each(function () {
        $(this).css({ 'top': buttonplacement, 'left': offset.left });
    });
};
*/
$(function () {
    // Set up tool tips
    setupToolTips();

    function setupToolTips() {
        $(".media_thumbnail_container").on("mouseover", ".media_item img", function (e) {
            $(e.currentTarget).tooltip("show");
        });
    }
});



function alignButtons() {

    var CONTROL_PANEL_MARGIN = 20;
    var BUTTON_GROUP_MARGIN = 20;
    var BUTTON_GROUP_HEIGHT = 95;

    var $topSideBar = $(".row-fluid .span3 .sidebar-nav").first();
    var $controlPanel = $("#controlPanel");
    var $standardButtonGroup = $(".btn-group-fixed");

    var topSideBarOffset = $topSideBar.offset();
    var topSideBarHeight = $topSideBar.innerHeight();
    var topSideBarWidth = $topSideBar.innerWidth();

    var topSideBarBottomMargin = -1;

    if ($controlPanel.length != 0) {
        var offsetTop = (topSideBarOffset.top + topSideBarHeight) + CONTROL_PANEL_MARGIN;
        topSideBarBottomMargin = $controlPanel.innerHeight() + (CONTROL_PANEL_MARGIN * 2);

        $controlPanel.css({
            "top": offsetTop,
            "left": topSideBarOffset.left,
            "width": topSideBarWidth
        });

        var $versionComment = $controlPanel.children("textarea").first();
        var versionCommentWidth = $topSideBar.width() - 22;

        $versionComment.css({
            "width": versionCommentWidth
        });

    } else if ($standardButtonGroup.length != 0) {
        var offsetTop = (topSideBarOffset.top + topSideBarHeight) + BUTTON_GROUP_MARGIN;
        topSideBarBottomMargin = BUTTON_GROUP_HEIGHT;

        $standardButtonGroup.css({ 'top': offsetTop, 'left': topSideBarOffset.left });
    }

    if (topSideBarBottomMargin != -1) {
        $topSideBar.css({
            "marginBottom": topSideBarBottomMargin
        });
    }
}; 




function maxChars() {

    $(".maxchars").each(function () {

        thismaxcharset = parseInt($(this).attr("maxchars"));

        $(this).children("label").append("<span class='maxcharsblock'>(<span class='charcount'>" + thismaxcharset + "</span> / " + thismaxcharset + " characters maximum)</span>");

        $(this).children("input").each(function () {

            valuelength = $(this).val().length;

        });

        calculation = parseInt(thismaxcharset - valuelength);

        //on load
        $(this).children("label").children("span.maxcharsblock").children("span.charcount").html(calculation);

        origcharcountlocation = $(this).children("label").children("span.maxcharsblock").children("span.charcount");
        originputlocation = $(this).children("input");

        if (calculation >= 0) {
            origcharcountlocation.removeClass("redtext");
            originputlocation.removeClass("borderredtext");
        } else if (calculation <= -1) {
            origcharcountlocation.addClass("redtext");
            originputlocation.addClass("borderredtext");
        }



        $(this).children("input").keyup(function () {

            newvaluelength = $(this).val().length;

            newcalculation = parseInt(thismaxcharset - newvaluelength);

            charcountlocation = $(this).parent().children("label").children("span.maxcharsblock").children("span.charcount");
            inputlocation = $(this).parent().children("input");

            charcountlocation.html(newcalculation);

            if (newcalculation >= 0) {
                charcountlocation.removeClass("redtext");
                inputlocation.removeClass("borderredtext");
            } else if (newcalculation <= -1) {
                charcountlocation.addClass("redtext");
                inputlocation.addClass("borderredtext");
            }

        });


    });





}








