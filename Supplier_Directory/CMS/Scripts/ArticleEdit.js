﻿$(function () {
    var additionalRequestData = {};
    if (hncms.articleId) additionalRequestData["articleId"] = hncms.articleId;

    $("#primaryMedia").on("click", ".media_remove", function (e) {
        var data = {};
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/Article/RemovePrimaryMedia",
            data: data,
            success: function () {
                $("#primaryMedia > li").first().remove();
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
        return false;
    });

    $("#addPrimaryMedia").mediaSelector({
        mediaSelected: function (mediaId) {
            // Send to the server
            addPrimaryMedia(mediaId);
        }
    });

    $("#quickUploadPrimaryMedia").mediaQuickUpload({
        complete: function (mediaId) {
            addPrimaryMedia(mediaId);
        }
    });

    function addPrimaryMedia(mediaId) {
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/Article/AddPrimaryMedia",
            data: data,
            success: function (viewUpdate) {
                // Make sure the .no-results element is removed
                $("#primaryMedia").find(".no-results").remove();
                // Add the ExistingArticleMediaItem partial view to the update element
                if ($("#primaryMedia > li").length == 2) {
                    $("#primaryMedia").prepend(viewUpdate);
                } else {
                    $("#primaryMedia > li").first().replaceWith(viewUpdate);
                }
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
    }



    $("#secondaryMedia").on("click", ".media_remove", function (e) {
        var mediaId = $(e.currentTarget).attr("rel");
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/Article/RemoveRelatedMedia",
            data: data,
            success: function () {
                $(e.currentTarget).parent().remove();
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "|c: " + c)
            }
        });
        return false;
    });

    $("#addSecondaryMedia").mediaSelector({
        mediaSelected: function (mediaId) {
            // Send to the server
            addSecondaryMedia(mediaId);
        }
    });

    $("#quickUploadSecondaryMedia").mediaQuickUpload({
        complete: function (mediaId) {
            addSecondaryMedia(mediaId);
        }
    });

    function addSecondaryMedia(mediaId) {
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/Article/AddRelatedMedia",
            data: data,
            success: function (viewUpdate) {
                // Make sure the .no-results element is removed
                $("#secondaryMedia").find(".no-results").remove();
                // Add the ExistingArticleMediaItem partial view to the update element
                $("#secondaryMedia").prepend(viewUpdate);
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
    }


    /***************SPONSOR************************/
    $("#sponsorMedia").on("click", ".media_remove", function (e) {
        var data = {};
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/Article/RemoveSponsorMedia",
            data: data,
            success: function () {
                $("#sponsorMedia > li").first().remove();
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
        return false;
    });

    $("#addSponsorMedia").mediaSelector({
        mediaSelected: function (mediaId) {
            // Send to the server
            addSponsorMedia(mediaId);
        }
    });

    $("#quickUploadSponsorMedia").mediaQuickUpload({
        complete: function (mediaId) {
            addSponsorMedia(mediaId);
        }
    });

    function addSponsorMedia(mediaId) {
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/Article/AddSponsorMedia",
            data: data,
            success: function (viewUpdate) {
                // Make sure the .no-results element is removed
                $("#sponsorMedia").find(".no-results").remove();
                // Add the ExistingArticleMediaItem partial view to the update element
                if ($("#sponsorMedia > li").length == 2) {
                    $("#sponsorMedia").prepend(viewUpdate);
                } else {
                    $("#sponsorMedia > li").first().replaceWith(viewUpdate);
                }
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
    }


    /********************In line content *************************/


    $("#inline-media-selector").inlineMediaSelector({});




     $("#relatedTags").tagSelector({
        heading: "Search for related tags",
        searchUrl: "/TagSelector/SearchTags",
        listSelectedUrl: "/Article/ListRelatedTags",
        addSelectedUrl: "/Article/SaveRelatedTags",
        removeSelectedUrl: "/Article/RemoveRelatedTags",  
        idArgumentName: "tagId",
        additionalRequestData: additionalRequestData,
        resultAdded: function () {
            $("#relatedTags").getTagSelector().reload();
        }
    });    

     $("#relatedEvents").tagSelector({
         heading: "Search for related events",
         searchUrl: "/TagSelector/SearchEvents",
         listSelectedUrl: "/Article/ListRelatedEvents",
         addSelectedUrl: "/Article/SaveRelatedEvent",
         removeSelectedUrl: "/Article/RemoveRelatedEvent",
         idArgumentName: "eventId",
         additionalRequestData: additionalRequestData,
         resultAdded: function () {
             $("#relatedEvents").getTagSelector().reload();
         }
     });
    

     $("#relatedPolicy").tagSelector({
         heading: "Search for related Publications",
         searchUrl: "/TagSelector/SearchPolicy",
         listSelectedUrl: "/Article/ListRelatedPolicy",
         addSelectedUrl: "/Article/SaveRelatedPolicy",
         removeSelectedUrl: "/Article/RemoveRelatedPolicy",
         idArgumentName: "policyId",
         additionalRequestData: additionalRequestData,
         resultAdded: function () {
             $("#relatedPolicy").getTagSelector().reload();
         }

     });    

     $("#relatedArticles").tagSelector({
         heading: "Search for related articles",
         searchUrl: "/TagSelector/SearchArticles",
         listSelectedUrl: "/Article/ListRelatedArticles",
         addSelectedUrl: "/Article/SaveRelatedArticle",
         removeSelectedUrl: "/Article/RemoveRelatedArticle",
         idArgumentName: "relatedarticleId",
         additionalRequestData: additionalRequestData,
         resultAdded: function () {
             $("#relatedArticles").getTagSelector().reload();
         }
     });

     
   
});

//$(function () {
//    // Constants
//    var searchSettings = {
//        article: {
//            url: "/Article/Search",
//            resultsElement: "#articleSearchResults"
//        },
//        articleLink: {
//            url: "/ArticleLink/Search",
//            resultsElement: "#articleLinkSearchResults"
//        },
//        competitor: {
//            url: "/Athlete/Search",
//            resultsElement: "#competitorSearchResults"
//        },
//        author: {
//            url: "/Author/Search",
//            resultsElement: "#authorSearchResults"
//        },
//        event: {
//            url: "/Event/Search",
//            resultsElement: "#@eventSearchResults"
//        },
//        discipline: {
//            url: "/Discipline/Search",
//            resultsElement: "#disciplineSearchResults"
//        },
//        tag: {
//            url: "/Tag/Search",
//            resultsElement: "#tagSearchResults"
//        }
//    };

//    // Initialisations

//    bindEvents()

//    function bindEvents() {
//        $(".search_textbox").keyup(searchTextBoxChange);
//    }

//
//    // Handlers

//    function searchTextBoxChange() {
//        var thisContext = this;

//        var sSettings = _getSearchSettings(this.id);

//        if ($(this).val() == "") {
//            $(sSettings.resultsElement).fadeOut();
//        } else {
//            $.ajax({
//                url: sSettings.url,
//                cache: false,
//                data: { searchText: $(this).val() },
//                success: function (html) {
//                    $(sSettings.resultsElement).html(html);
//                    $(sSettings.resultsElement).fadeIn();
//                }
//            });
//        }
//        return false;
//    }

//    // Helpers

//    function _getSearchSettings(textBoxId) {
//        switch (textBoxId) {
//            case "articleSearch": return searchSettings.article;
//            case "articleLinkSearch": return searchSettings.articleLink;
//            case "competitorSearch": return searchSettings.competitor;
//            case "authorSearch": return searchSettings.author;
//            case "eventSearch": return searchSettings.@event;
//            case "disciplineSearch": return searchSettings.discipline;
//            case "mediaSearch": return searchSettings.media;
//            case "tagSearch": return searchSettings.tag;
//        }
//    }

//});