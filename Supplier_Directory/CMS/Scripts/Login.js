﻿$(function () {

    // Security advice
    $("#securityAdivceModal").modal({
        show: false
    });
    $("#securityAdvice").click(function () {
        $("#securityAdivceModal").modal("show");
        return false;
    });
    $("#closeSecurityAdviceModal").click(function () {
        $("#securityAdivceModal").modal("hide");
        return false;
    });

    // Password reset request
    $("#passwordResetModal").modal({
        show: false
    });
    $("#passwordReset").click(function () {
        $("#passwordResetModal").modal("show");
        return false;
    });
    $("#closePasswordResetModal").click(function () {
        $("#passwordResetModal").modal("hide");
        return false;
    });
    $("#requestPasswordReset").click(function () {
        $("#passwordResetRequestResponse").html("");
        $("#passwordResetRequestResponse").hide();
        $.ajax({
            url: "/User/RequestPasswordReset",
            data: { email: $("#passwordResetEmailAddress").val() },
            type: "POST",
            success: function (result) {
                if (result.Success) {
                    $("#passwordResetRequestResponse").show();
                    $("#passwordResetRequestResponse").html("<strong>Success</strong> - " + result.Message);
                    $("#passwordResetRequestResponse").css("color", "green");
                    // Clear the email text box
                    $("#passwordResetEmailAddress").val("");
                } else {
                    $("#passwordResetRequestResponse").show();
                    $("#passwordResetRequestResponse").html("<strong>Warning</strong> - " + result.Message);
                    $("#passwordResetRequestResponse").css("color", "orange");
                }
            },
            error: function () {
                $("#passwordResetRequestResponse").show();
                $("#passwordResetRequestResponse").html("<strong>Error</strong> - Something went wrong on the server. Please refresh the page and try again. Contact andrew.macharg@haymarket.com if the problem persists.");
                $("#passwordResetRequestResponse").css("color", "red");
            }
        });
        return false;
    });

    // Password reset complete
    $("#passwordResetCompleteModal").modal({
        show: true
    });
    $("#closePasswordResetCompleteModal").click(function () {
        $("#passwordResetCompleteModal").modal("hide");
        return false;
    });
    $("#completePasswordReset").click(function () {
        $("#passwordResetCompleteResponse").html("");
        $("#passwordResetCompleteResponse").hide();
        $.ajax({
            url: "/User/CompletePasswordReset",
            type: "POST",
            data: {
                newPassword: $("#passwordResetNewPassword").val(),
                newPasswordConfirmation: $("#passwordResetNewPasswordConfirmation").val(),
                key: $("#passwordResetTicketKey").val()
            },
            success: function (result) {
                if (result.Success) {
                    $("#passwordResetCompleteResponse").show();
                    $("#passwordResetCompleteResponse").html("<strong>Success</strong> - " + result.Message);
                    $("#passwordResetCompleteResponse").css("color", "green");
                    setTimeout(function () {
                        $("#passwordResetCompleteModal").modal("hide");
                        // Clear the password textboxes
                        $("#passwordResetNewPassword").val(""),
                        $("#passwordResetNewPasswordConfirmation").val("")
                    }, 2000);
                } else {
                    $("#passwordResetCompleteResponse").show();
                    $("#passwordResetCompleteResponse").html("<strong>Warning</strong> - " + result.Message);
                    $("#passwordResetCompleteResponse").css("color", "orange");
                }
            },
            error: function () {
                $("#passwordResetCompleteResponse").show();
                $("#passwordResetCompleteResponse").html("<strong>Error</strong> - Something went wrong on the server. Please refresh the page and try again. Contact andrew.macharg@haymarket.com if the problem persists.");
                $("#passwordResetCompleteResponse").css("color", "red");
            }
        });
        return false;
    });
});