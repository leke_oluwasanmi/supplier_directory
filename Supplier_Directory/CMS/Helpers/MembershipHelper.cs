﻿using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using SD.Core.Services.CMS;
using System;
using System.Web;
using System.Web.Security;
using SD.Core.Common;
using SD.Core.Data;

namespace CMS.Helpers
{
    /// <summary>
    /// TODO: Make this a singleton
    /// </summary>
    public class MembershipHelper
    {
         private IMongoRepository1 mongoRepository;

        
        public MembershipHelper()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            
        }

        public MembershipHelper(IMongoRepository1 _mongoRepository)
        {
             this.mongoRepository = _mongoRepository;
            
        }
        private static string tempAuthId;

        public static bool Login(string email, string password, bool persistent)
        {
            var userService = new UserService(AppKernel.GetInstance<IMongoRepository1>());

            var user = userService.FindWithCredentials(email, password, Permission.UseCMS);
            if (user != null)
            {
                FormsAuthentication.SetAuthCookie(user.Id, persistent);
                tempAuthId = user.Id;
                return true;
            }
            else return false;
        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
            tempAuthId = string.Empty;
        }

        public static bool IsAuthenticated()
        {
            return !string.IsNullOrEmpty(GetActiveUserId);
        }

        public static User GetActiveUser()
        {
            if (!IsAuthenticated())
                throw new Exception("Cannot get active user when no use is logged in. Use the IsAuthenticated() method first if there is any doubt.");

            var userService = new UserService(AppKernel.GetInstance<IMongoRepository1>());
            return userService.FindById(GetActiveUserId);
        }

        public static string GetActiveUserId
        {
            get
            {
                if (HttpContext.Current.User == null) return null;
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name)) return HttpContext.Current.User.Identity.Name as string;
                if (!string.IsNullOrWhiteSpace(tempAuthId)) return tempAuthId;
                return null;
            }
        }
    }
}