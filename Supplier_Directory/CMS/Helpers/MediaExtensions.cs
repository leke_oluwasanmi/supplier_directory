﻿using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using System.Web.Mvc;

namespace CMS.Helpers.HtmlExtensions
{
    public static class MediaExtensions
    {
        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaProfile profile, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetMediaUri(profile, media));
        }

        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaRatio ratio, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetMediaUri(ratio, media));
        }

        public static MvcHtmlString GetVideoThumbnailUri(this HtmlHelper helper, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetVideoThumbnailUri(media));
        }

        public static MvcHtmlString GetMediaMarkup(this HtmlHelper helper, MediaProfile profile, Media media, string attributes="")
        {
            string src = "";
            string markup = "";
            switch (media.Format)
            {
                case FileFormat.JPEG:
                case FileFormat.PNG:
                case FileFormat.Bitmap:
                case FileFormat.GIF:
                    src = MediaHelper.GetMediaUri(profile, media);
                    markup = string.Format("<img src='{0}' alt='{1}' title='{2}' {3} class='edit-media-item-image' />", src, media.Title,
                        media.Title, attributes);
                    break;
                case FileFormat.PDF:
                case FileFormat.Excel:
                case FileFormat.Word:
                case FileFormat.Text:
                case FileFormat.PPT:
                case FileFormat.PPTX:
                    string icon="";
                    src = MediaHelper.GetDocumentUri(profile, media,out icon);
                    markup = string.Format("<a href='{0}' alt='{1}' title='{1}' target='_blank' class='edit-media-item-image'><img src='{2}' alt='{1}' title='{1}' class='edit-media-item-image' style='width:10%' />{1}</a>", 
                        src, media.OriginalFileName, icon);                    
                    break;
                default:
                    src = MediaHelper.GetMediaUri(profile, media);
                    markup = string.Format("<img src='{0}' alt='{1}' title='{2}' {3} class='edit-media-item-image' />", src, media.Title,
                        media.Title, attributes);
                    break;
            }

            return new MvcHtmlString(markup);
        }
    }
}