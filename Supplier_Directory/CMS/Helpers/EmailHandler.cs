﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;

namespace CMS.Helpers
{
    public class EmailHandler
    {
        /// <summary>
        /// Sends an email.
        /// </summary>
        /// <param name="addressTo"></param>
        /// <param name="nameTo"></param>
        /// <param name="addressFrom"></param>
        /// <param name="nameFrom"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="priority"></param>
        /// <param name="isHtml"></param>
        /// <returns></returtns>
        public async static  Task SendMail(String addressTo, String nameTo, String addressFrom, String nameFrom, String subject, String body, MailPriority priority, Boolean isHtml)
        {

            MailAddress objFrom = new MailAddress(addressFrom, nameFrom);
            MailAddress objTo = new MailAddress(addressTo, nameTo);
            MailMessage objMessage = new MailMessage(objFrom, objTo);
            objMessage.Subject = subject;
            objMessage.Body = body;
            objMessage.Priority = priority;
            objMessage.IsBodyHtml = isHtml;
            string env = ConfigurationManager.AppSettings["ServerEnvironment"];
            if (env == "staging")
            {
                ServicePointManager.ServerCertificateValidationCallback =
        delegate (object s, X509Certificate certificate,
                 X509Chain chain, SslPolicyErrors sslPolicyErrors)
        { return true; };
                using (var client = new SmtpClient("mail.blakeshore.com"))
                {
                    client.Credentials = new System.Net.NetworkCredential("haymarket@blakeshore.com", "haymarket1*");
                    client.Port = 587;
                    await client.SendMailAsync(objMessage);
                }
            }
            else
            {
                using (var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]))
                {
                    await client.SendMailAsync(objMessage);
                }

            }                       
        }
    }
}