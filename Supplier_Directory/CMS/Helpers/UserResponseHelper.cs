﻿using SD.Core.ViewModels;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace CMS.Helpers
{
    public class UserResponseHelper
    {
        private const string InstanceKeyPrefix = "UserResponses_";

        private string _instanceKey;

        public UserResponseHelper(string instanceKey)
        {
            _instanceKey = instanceKey;
        }

        public void AddUserResponse(ResponseType type, string headline, string details = "")
        {
            var userResponse = UserResponses;
            userResponse.Add(new UserResponseItem
            {
                Type = type,
                Headline = headline,
                Details = details
            });
            UserResponses = userResponse;
        }

        public void AddValidationResponse(string fieldName, string validationMessage)
        {
            AddUserResponse(ResponseType.Error, validationMessage);
        }

        public void AddValidationResponses(ModelStateDictionary modelStateDictionary)
        {
            if (modelStateDictionary.IsValid) return;
            foreach (var modelState in modelStateDictionary)
            {
                foreach (var modelError in modelState.Value.Errors)
                {
                    var dotIndexPlusOne = modelState.Key.IndexOf('.') + 1;// Plus one to get the value starting after the dot
                    var fieldName = dotIndexPlusOne != 0 ? modelState.Key.Substring(dotIndexPlusOne, modelState.Key.Length - dotIndexPlusOne) : modelState.Key;
                    AddValidationResponse(fieldName, modelError.ErrorMessage);
                }
            }
        }

        /// <summary>
        /// Returns any stored responses and clears the response stack while it's at it. Each response will only ever be retrieved once.
        /// </summary>
        public IList<UserResponseItem> UserResponses
        {
            get
            {
                var userResponses = HttpContext.Current.Session[InstanceKeyPrefix + _instanceKey] as IList<UserResponseItem>;

                if (userResponses == null) return new List<UserResponseItem>();
                else
                {
                    HttpContext.Current.Session[InstanceKeyPrefix + _instanceKey] = null;
                    return userResponses;
                }
            }
            set
            {
                HttpContext.Current.Session[InstanceKeyPrefix + _instanceKey] = value;
            }
        }
    }
}