﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Helpers
{
    public class GlobalVariables
    {
        public static string CurrentBrand
        {
            get
            {
                if (HttpContext.Current.Session["ChangeBrand"] != null)
                    return HttpContext.Current.Session["ChangeBrand"].ToString();
                return "vw";
                ///TODO: return according to URL
            }
        }
    }
}