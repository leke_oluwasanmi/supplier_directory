﻿using System;
using System.Web.Mvc;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data.Model;

namespace SD.Core.Common.Utilities.Extensions.Html
{
    public static class MediaExtensions
    {
        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaProfile profile, Media media, string brand)
        {
            return new MvcHtmlString(MediaUtilities.GetMediaUri(profile, media, brand));
        }

        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaProfile profile, string fileName, string brand)
        {
            var profileConfig = SD.Core.Common.Config.SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);

            return new MvcHtmlString(MediaUtilities.GetMediaUri(profileConfig, fileName, brand));
        }

        public static MvcHtmlString GetDocumentUri(this HtmlHelper helper, string fileName)
        {
            return new MvcHtmlString(MediaUtilities.GetDocumentUri(fileName));
        }
        public static MvcHtmlString GetDocumentUri(this HtmlHelper helper, string id, string brand, bool isDownload)
        {
            return new MvcHtmlString(MediaUtilities.GetDocumentUri(id, brand, isDownload));
        }

        public static MvcHtmlString GetMediaImage(this HtmlHelper helper, MediaProfile profile, Media media,
            int? height = null, int? width = null, bool showDefault = false, bool showCaption = false)
        {
            return
                new MvcHtmlString(MediaUtilities.GetMediaImageMarkup(profile, media, height, width, showDefault,
                    showCaption));
        }

        public static MvcHtmlString GetMedia(this HtmlHelper helper, MediaProfile profile, Media media,
            int? height = null, int? width = null, bool showDefault = false, bool showCaption = false,
            bool justImage = false)
        {
            return
                new MvcHtmlString(MediaUtilities.GetMediaMarkup(profile, media, height, width, showDefault,
                    showCaption, justImage));
        }
        public static MvcHtmlString GetMedia(this HtmlHelper helper, MediaProfile profile, Media media,
           int? height = null, int? width = null, bool showDefault = false, bool showCaption = false,
           bool justImage = false, string brand = "")
        {
            return
                new MvcHtmlString(MediaUtilities.GetMediaMarkup(profile, media, height, width, showDefault,
                    showCaption, justImage, brand));
        }

        //public static MvcHtmlString GetAudioUri(this HtmlHelper helper, Media media)
        //{
        //    if (!FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
        //        throw new Exception("Media item is not an audio file");
        //    return new MvcHtmlString(MediaUtilities.GetAudioUri(media.FileName));
        //}

        public static string GetMediaTypeClass(this HtmlHelper helper, Media media)
        {
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image)) return "popup-photo-icon";
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video)) return "popup-video-icon";
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio)) return "popup-audio-icon";
            else throw new Exception("Unrecognised media format");
        }

        public static MvcHtmlString GetLargeMediaUri(this HtmlHelper helper, Media media, string brand)
        {
            if (media == null) return new MvcHtmlString(string.Empty);
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
            {
                //if (media.SourceHeight < media.SourceWidth)
                return new MvcHtmlString(MediaUtilities.GetMediaUri(MediaProfile.size_600x300, media, brand));
                //else return new MvcHtmlString(MediaUtilities.GetMediaUri(MediaProfile.size_400x600, media));
            }
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                switch (media.Hosting)
                {
                    case MediaHosting.RemoteYoutube:
                        return
                            new MvcHtmlString(string.Format("http://www.youtube.com/embed/{0}?hl=en_GB",
                                media.RemoteItemCode));
                        break;

                    case MediaHosting.RemoteVimeo:
                        return new MvcHtmlString("");
                        break;

                    default:
                        return new MvcHtmlString("");
                        break;
                }
            }

            //else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
            //{
            //    return new MvcHtmlString(MediaUtilities.GetAudioUri(media.FileName));
            //}
            else throw new Exception("Unrecognised media format");
        }
    }
}