﻿using System;
using System.Web.Mvc;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data.Model;

namespace SD.Core.Common.Utilities
{
    public static partial class MediaUtilities
    {
        public static MediaProfile MediaProfileFromName(string profileName)
        {
            var profile = MediaProfile.size_300x200;
            var foundProfile = false;
            foreach (MediaProfile mediaProfile in Enum.GetValues(typeof(MediaProfile)))
            {
                if (mediaProfile.ToString().ToLower() == profileName.ToLower())
                {
                    profile = mediaProfile;
                    foundProfile = true;
                    break;
                }
            }
            if (!foundProfile) throw new Exception(string.Format("Couldn't find profile called {0}", profileName));
            return profile;
        }

        public static bool MediaRatioHasProfiles(MediaRatio ratio)
        {
            return SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio).Count > 0;
        }

        public static string GetMediaUri(MediaProfileElement profileConfig, string filename, string brand)
        {
            return string.Format("{0}/{1}/{3}/{4}",
                    SDConfig.Instance.Media.baseUri,                   
                    SDConfig.Instance.Media.Directory,
                    brand,
                    profileConfig.DirectoryName,
                    filename);
        }

        public static string GetMediaUri(MediaProfile profile, Media media, string brand)
        {
            var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);
            if (media == null) return MediaUtilities.GetDefaultMediaUri(profileConfig); // TODO: Write a log entry
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
            {
                if (!media.AvailableRatios.Contains(profileConfig.Ratio))
                {
                    // The specified media item doesnt support the requested profile so return the default image url for the profile
                    return MediaUtilities.GetDefaultMediaUri(profileConfig);
                    // TODO: Write a log entry
                }
                if (!string.IsNullOrWhiteSpace(media.FileName))
                {
                    // Hosted locally so return the local media uri
                    return MediaUtilities.GetMediaUri(profileConfig, media.FileName, brand) + "?v=" + media.UpdatedOn.Ticks.GetHashCode();
                }
                else
                {
                    throw new Exception("No uploaded file is available to get the MediaUri for.");
                }
            }
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
            {
                return  String.Format("/file.axd?fid={0}&brand={1}", media.Id, brand, media.Title);

            }
            //else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
            //{
            //    return MediaUtilities.GetAudioCoverImageUri(profileConfig);
            //}
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                return string.Format("http://img.youtube.com/vi/{0}/0.jpg", media.RemoteItemCode);
                //return string.Format("http://www.youtube.com/embed/{0}?hl=en_GB", media.RemoteItemCode);
            }
            throw new ArgumentException("Unknown media format: " + media.Format, "media");
        }

        //public static string GetAudioUri(string filename)
        //{
        //    return string.Format("{0}/{1}/{2}",
        //            VWGConfig.Instance.Media.BaseUri,
        //            VWGConfig.Instance.Audio.Directory,
        //            filename);
        //}

        //public static string GetAudioCoverImageUri(MediaProfileElement profileConfig)
        //{
        //    return string.Format("{0}/{1}/{2}/{3}",
        //        VWGConfig.Instance.Media.BaseUri,
        //        VWGConfig.Instance.Media.Directory,
        //        profileConfig.DirectoryName,
        //        VWGConfig.Instance.Audio.CoverImageFilename);
        //}

        public static string GetDefaultMediaUri(MediaProfileElement profileConfig)
        {
            return string.Format("{0}/{1}/{2}/{3}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Media.Directory,
                    profileConfig.DirectoryName,
                    profileConfig.DefaultFilename);
        }

        public static string GetDocumentUri(string id, string brand, bool isDownload = false)
        {
            return String.Format("/file.axd?fid={0}&brand={1}{2}", id, brand, isDownload ? "&dl=1" :"");

        }
        public static string GetDocumentUri(MediaProfile profile, Media media, out string icon)
        {
            var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);
            string uri = "";
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
            {
                uri = MediaUtils.GetDocumentUri(media.FileName, out icon);
                return uri;
            }

            throw new Exception("Failed to resolve media uri");
        }
        public static string GetDocumentUri(string fileName)
        {
            return string.Format("{0}/{1}/{2}",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Document.Directory,
                    fileName);
        }

        public static string GetDocumentBasePath()
        {
            return string.Format("{0}/{1}/",
                    SDConfig.Instance.Media.baseUri,
                    SDConfig.Instance.Document.Directory);
        }

        public static string GetYouTubeThumbnailUri(string videoCode)
        {
            return string.Format("http://img.youtube.com/vi/{0}/1.jpg", videoCode);
        }

        public static string GetBrightCoveThumbnailUri(string videoCode)
        {
            throw new NotImplementedException(); //TODO: Get the BrightCove thumnail uri
        }

        public static string GetMediaImageMarkup(MediaProfile profile, Media media, int? height = null, int? width = null, bool showDefault = false, bool showCaption = false, string brand = "")
        {
            var src = Common.Utilities.MediaUtilities.GetMediaUri(profile, media, brand);
            var caption = "";

            if (media != null)
            {
                var builder = new TagBuilder("img");

                builder.MergeAttribute("src", src);
                builder.MergeAttribute("class", "img-responsive");
                //builder.MergeAttribute("rel", string.Format("{0} ({1}) &copy; Copyright", media.Title, media.Credit ?? "VWG"));

                //builder.MergeAttribute("itemprop", "image");

                if (media.Title != "")
                {
                    if (media.Credit != "")
                    {
                        caption = string.Format("{0} ({1})", media.Title, media.Credit);
                        builder.MergeAttribute("alt", caption);
                        //caption = string.Format("<span><em>{0} &copy; Copyright</em></span>", caption);
                    }
                    else
                    {
                        caption = media.Title;
                        builder.MergeAttribute("alt", caption);
                       // caption = string.Format("<span><em>{0} &copy; Copyright</em></span>", caption);
                    }
                }
                if (profile == MediaProfile.Original)
                {
                    builder.MergeAttribute("width", "100%");
                }
                else
                {
                    if (height.HasValue)
                        builder.MergeAttribute("height", height.ToString());
                    if (width.HasValue)
                        builder.MergeAttribute("width", width.ToString());
                }
                if (showCaption)
                {
                    return builder.ToString(TagRenderMode.SelfClosing) + caption;
                }
                else
                    return builder.ToString(TagRenderMode.SelfClosing);
            }
            else if (showDefault)
            {
                var builder = new TagBuilder("img");
                var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);
                builder.MergeAttribute("src", Common.Utilities.MediaUtilities.GetMediaUri(profileConfig, "placeholder.png", brand));
                if (height.HasValue)
                    builder.MergeAttribute("height", height.ToString());
                if (width.HasValue)
                    builder.MergeAttribute("width", width.ToString());

                return builder.ToString(TagRenderMode.SelfClosing);
            }
            else
                return string.Empty;
        }
        public static string GetMediaMarkup(MediaProfile profile, Media media, int? height = null, int? width = null, bool showDefault = false, bool showCaption = false, bool justImage = false, string brand = "")
        {
            if (media != null)
            {
                if (Common.Utils.FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                {
                    return GetMediaImageMarkup(profile, media, height, width, showDefault, showCaption, brand);
                }
                else if (Common.Utils.FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
                {
                    return GetMediaImageMarkup(profile, media, height, width, showDefault, showCaption, brand);
                }
                else if (Common.Utils.FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
                {
                    #region Video

                    switch (media.Hosting)
                    {
                        case MediaHosting.Local:
                            var url = string.Format("/file.axd?fid={0}&brand={1}", media.Id, brand);
                            var toFormat = "<div class=\"thumbnail\"><div class=\"document-group text-center{0}\"><h3><a href = \"{1}\"><i class=\"fa fa-file-{2}-o\"></i>{3}</a></h3><div class=\"text-center\"><div class=\"btn-group\" role=\"group\"><a class=\"btn btn-primary btn-lg nrDownload\" href=\"{1}&dl=1\"><i class=\"fa fa-cloud-download\"></i></a><a class=\"btn btn-primary btn-lg nrOpenDownload\" href=\"{1}\")\"><i class=\"fa fa-eye\"></i></a></div></div></div></div>";

                            string ext = System.IO.Path.GetExtension(media.FileName);

                            return String.Format(toFormat, ext, url, ext, media.Title);
                            break;

                        case MediaHosting.RemoteBrightCove:
                            var newPlayerString = "<video id=\"{0}\" style=\"width:640px; height:360px;\" data-video-id=\"{0}\" data-account=\"1842992633001\" data-player=\"B1M2VUPz\" data-embed=\"default\"" +
                                " class=\"video-js\" controls></video>" +
                                "<script type=\"text/javascript\">" +
                                    "require([\"bc\", \"bcga\"], function () {{" +
                                        "var myPlayer{0};" +
                                        "videojs(\"{0}\").on('loadstart', function () {{" +
                                            "myPlayer{0} = this;" +
                                            "console.log(\"mediainfo object: \", myPlayer{0}.mediainfo);" +
                                            "myPlayer{0}.ga(" +
                                            "{{" +
                                                "\"eventNames\": {{" +
                                                    "\"play\": \"Video Playback\"," +
                                                    "\"percent_played\": \"Percent played\"," +
                                                    "\"pause\": \"Media Pause\"," +
                                                    "\"error\": \"Media Error\"," +
                                                    "\"fullscreen_exit\": \"Fullscreen exited\"," +
                                                    "\"fullscreen_enter\": \"Fullscreen entered\"," +
                                                    "\"resize\": \"Media Resize\"," +
                                                    "\"end\": \"Media complete\"" +
                                                "}}, " +
                                                "\"eventsToTrack\": [\"start\", \"end\", \"play\", \"pause\", \"resize\", \"error\", \"fullscreen\", \"percent_played\", \"video_load\", \"player_load\", \"volume_change\"]" +
                                            "}}" +
                                            ");" +
                                        "}});" +
                                    "}});" +
                                "</script>";
                            var playerString = string.Format(newPlayerString, media.RemoteItemCode, width, height);
                            return playerString;
                            break;

                        case MediaHosting.RemoteVimeo:

                            #region

                            var srcV = string.Format("//player.vimeo.com/video/{0}?title=0&amp;byline=0&amp;portrait=0&amp;color=63aa9c", media.RemoteItemCode);
                            var jsonV = string.Format("http://vimeo.com/api/v2/video/{0}.json", media.RemoteItemCode);

                            var imgAltV = media.MetaDescription;
                            var builderV = new TagBuilder("test");

                            var videoWrapV = new TagBuilder("div");
                            videoWrapV.AddCssClass("videoWrapper");
                            videoWrapV.MergeAttribute("data-url", jsonV);

                            if (height.HasValue || width.HasValue)
                            {
                                builderV = new TagBuilder("iframe");

                                //builder.Attributes.Add("webkitallowfullscreen mozallowfullscreen allowfullscreen");

                                builderV.SetInnerText("webkitallowfullscreen mozallowfullscreen allowfullscreen");

                                if (height.HasValue)
                                    builderV.MergeAttribute("height", height.ToString());
                                if (width.HasValue)
                                    builderV.MergeAttribute("width", width.ToString());

                                builderV.MergeAttribute("id", media.RemoteItemCode);
                                builderV.MergeAttribute("src", srcV);
                                builderV.MergeAttribute("frameborder", "0");

                                videoWrapV.InnerHtml = builderV.ToString(TagRenderMode.SelfClosing) + "</iframe>";

                                return videoWrapV.ToString();
                            }
                            else
                            {
                                switch (profile)
                                {
                                    case MediaProfile.Original:

                                        return string.Empty;
                                        break;

                                    case MediaProfile.size_600x300:
                                        builderV = new TagBuilder("iframe");
                                        builderV.MergeAttribute("src", srcV);
                                        builderV.MergeAttribute("frameborder", "0");
                                        builderV.MergeAttribute("height", "440");
                                        builderV.MergeAttribute("width", "645");
                                        videoWrapV.InnerHtml = builderV.ToString(TagRenderMode.SelfClosing) + "</iframe>";

                                        return videoWrapV.ToString();
                                        break;

                                    case MediaProfile.size_300x200:
                                        builderV = new TagBuilder("iframe");
                                        builderV.MergeAttribute("src", srcV);
                                        builderV.MergeAttribute("frameborder", "0");
                                        builderV.MergeAttribute("height", "250");
                                        builderV.MergeAttribute("width", "320");
                                        videoWrapV.InnerHtml = builderV.ToString(TagRenderMode.SelfClosing) + "</iframe>";
                                        return videoWrapV.ToString();
                                        break;

                                    default:
                                        return string.Empty;
                                        break;
                                }
                            }

                        #endregion Video

                        case MediaHosting.RemoteYoutube:

                            #region

                            var src = string.Format("http://www.youtube.com/embed/{0}?hl=en_GB&wmode=opaque&autohide=1&showinfo=0", media.RemoteItemCode);
                            var imgSrc = string.Format("http://img.youtube.com/vi/{0}/0.jpg", media.RemoteItemCode);
                            var imgAlt = media.MetaDescription;
                            var builder = new TagBuilder("test");

                            var videoWrap = new TagBuilder("div");
                            videoWrap.AddCssClass("videoWrapper");
                            if (height.HasValue || width.HasValue)
                            {
                                builder = new TagBuilder("iframe");

                                if (height.HasValue)
                                    builder.MergeAttribute("height", height.ToString());
                                if (width.HasValue)
                                    builder.MergeAttribute("width", "100%");

                                builder.MergeAttribute("id", media.RemoteItemCode);
                                builder.MergeAttribute("imgsrc", imgSrc);
                                builder.MergeAttribute("src", src);
                                builder.MergeAttribute("frameborder", "0");
                                builder.MergeAttribute("allowfullscreen", "");
                                //builder.MergeAttribute("itemprop", "video");
                                videoWrap.InnerHtml = builder.ToString(TagRenderMode.SelfClosing) + "</iframe>";
                                if (showCaption)
                                {
                                    videoWrap.InnerHtml = videoWrap.InnerHtml + "<p>" + media.Title + "</p>";
                                }
                                return videoWrap.ToString();
                            }
                            else
                            {
                                switch (profile)
                                {
                                    case MediaProfile.Original:
                                        return string.Empty;
                                        break;

                                    case MediaProfile.size_600x300:
                                        builder = new TagBuilder("iframe");
                                        builder.MergeAttribute("src", src);
                                        builder.MergeAttribute("frameborder", "0");
                                        builder.MergeAttribute("height", "267");
                                        builder.MergeAttribute("width", "100%");
                                        videoWrap.InnerHtml = builder.ToString(TagRenderMode.SelfClosing) + "</iframe>";
                                        if (showCaption)
                                        {
                                            videoWrap.InnerHtml = videoWrap.InnerHtml + "<p>" + media.Title + "</p>";
                                        }
                                        return videoWrap.ToString();
                                        break;

                                    case MediaProfile.size_300x200:
                                        builder = new TagBuilder("iframe");
                                        builder.MergeAttribute("src", src);
                                        builder.MergeAttribute("frameborder", "0");
                                        builder.MergeAttribute("height", "200");
                                        builder.MergeAttribute("width", "300");
                                        videoWrap.InnerHtml = builder.ToString(TagRenderMode.SelfClosing) + "</iframe>";
                                        if (showCaption)
                                        {
                                            videoWrap.InnerHtml = videoWrap.InnerHtml + "<p>" + media.Title + "</p>";
                                        }
                                        return videoWrap.ToString();
                                        break;

                                    default:
                                        return string.Empty;
                                        break;
                                }
                            }

                        #endregion

                        default:
                            return string.Empty;
                    }

                    #endregion Video
                }
                else if (Common.Utils.FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
                {
                    //var url = string.Format("/file.axd?fid={0}&brand={1}", media.Id, brand);
                    string icon = "";
                    var url = MediaUtilities.GetDocumentUri(profile,media,out icon);
                     var toFormat = "<div class=\"thumbnail\"><div class=\"document-group text-center{0}\"><h3><a href = \"{1}\"><i class=\"fa fa-file-{2}-o\"></i>{3}</a></h3><div class=\"text-center\"><div class=\"btn-group\" role=\"group\"><a class=\"btn btn-primary btn-lg nrDownload\" target='_blank' href=\"{1}\"><i class=\"fa fa-cloud-download\"></i></a><a class=\"btn btn-primary btn-lg nrOpenDownload\" target='_blank' href=\"{1}\")\"><i class=\"fa fa-eye\"></i></a></div></div></div></div>";

                    string ext = System.IO.Path.GetExtension(media.FileName);
                    
                    return String.Format(toFormat, ext, url, ext, media.Title);


                }
                return string.Empty;
            }
            else
                return string.Empty;
        }

        public static string GetMediaMarkup(MediaProfile profile, Media media, int? height = null, int? width = null, bool showDefault = false, bool showCaption = false, bool justImage = false)
        {

            return GetMediaMarkup(profile, media, height, width, showDefault, showCaption, justImage, "");
           
        }

        public static string GetYouTubeMarkup(MediaProfile profile, string YouTubeId, string MetaDescription, int? height = null, int? width = null)
        {
            var src = string.Format("http://www.youtube.com/embed/{0}?hl=en_GB", YouTubeId);
            var imgSrc = string.Format("http://img.youtube.com/vi/{0}/0.jpg", YouTubeId);
            var imgAlt = MetaDescription;
            var builder = new TagBuilder("test");

            if (height.HasValue || width.HasValue)
            {
                builder = new TagBuilder("iframe");

                if (height.HasValue)
                    builder.MergeAttribute("height", height.ToString());
                if (width.HasValue)
                    builder.MergeAttribute("width", width.ToString());

                builder.MergeAttribute("src", src);
                builder.MergeAttribute("frameborder", "0");
                return builder.ToString(TagRenderMode.SelfClosing) + "</iframe>";
            }
            else
            {
                switch (profile)
                {
                    case MediaProfile.Original:
                        return string.Empty;

                    case MediaProfile.size_600x300:
                        builder = new TagBuilder("iframe");
                        builder.MergeAttribute("src", src);
                        builder.MergeAttribute("frameborder", "0");
                        builder.MergeAttribute("width", "600");
                        builder.MergeAttribute("height", "300");
                        return builder.ToString(TagRenderMode.SelfClosing) + "</iframe>";

                    case MediaProfile.size_300x200:
                        builder = new TagBuilder("iframe");
                        builder.MergeAttribute("src", src);
                        builder.MergeAttribute("frameborder", "0");
                        builder.MergeAttribute("height", "300");
                        builder.MergeAttribute("width", "200");
                        return builder.ToString(TagRenderMode.SelfClosing) + "</iframe>";

                    case MediaProfile.size_240x160:
                        builder = new TagBuilder("img");
                        builder.MergeAttribute("src", imgSrc);
                        builder.MergeAttribute("id", YouTubeId);
                        builder.MergeAttribute("height", "160");
                        builder.MergeAttribute("width", "240");
                        builder.MergeAttribute("class", "youtubepopup");
                        return builder.ToString();

                    case MediaProfile.size_100x67:
                        builder = new TagBuilder("img");
                        builder.MergeAttribute("src", imgSrc);
                        builder.MergeAttribute("id", YouTubeId);
                        builder.MergeAttribute("height", "67");
                        builder.MergeAttribute("width", "100");
                        builder.MergeAttribute("class", "youtubepopup");
                        return builder.ToString();

                    default:
                        return string.Empty;
                }
            }
        }
    }
}