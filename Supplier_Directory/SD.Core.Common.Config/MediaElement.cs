﻿using System;
using System.Configuration;

namespace SD.Core.Common.Config
{
    public class MediaElement : ConfigurationElement
    {
        private const string DirectoryAttributeName = "directory";
        private const string BaseURIAttributeName = "baseUri";

        [ConfigurationProperty(DirectoryAttributeName, IsRequired = true)]
        public Uri Directory
        {
            get
            {
                return this[DirectoryAttributeName] as Uri;
            }
        }


       

        [ConfigurationProperty(BaseURIAttributeName, IsRequired = true)]
        public string baseUri
        {
            get
            {
                return (string)this[BaseURIAttributeName];
            }
        }
        public override bool IsReadOnly()
        {
            return false;
        }
    }
}