﻿using System;
using System.Configuration;
using System.Net;

namespace SD.Core.Common.Config
{
    public class ErrorElementCollection : ConfigurationElementCollection
    {
        public ErrorElement GetErrorConfig(HttpStatusCode httpStatus)
        {
            var errorConfig = BaseGet(httpStatus) as ErrorElement;
            if (errorConfig == null) throw new Exception(string.Format("Couldn't find error configuration for status code: {0}", httpStatus.ToString()));
            return errorConfig;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ErrorElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ErrorElement)element).Name;
        }
    }
}