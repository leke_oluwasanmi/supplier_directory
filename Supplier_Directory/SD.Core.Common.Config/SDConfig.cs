﻿using System.Configuration;
using System.Web.Configuration;

namespace SD.Core.Common.Config
{
    public class SDConfig : ConfigurationSection
    {
        private const string SectionName = "SDSettings";
        private const string InstallationElementName = "installation";
        private const string MediaElementName = "media";
        private const string AudioElementName = "audio";
        private const string GettyImageElementName = "gettyImage";
        private const string MediaProfileElementCollectionName = "mediaProfiles";
        private const string MediaRatioElementCollectionName = "mediaRatios";
        private const string ErrorElementCollectionName = "errors";
        private const string DocumentElementName = "document";
        private const string DatabaseElementName = "database";
        private const string FileStoreElementName = "fileStore";
        private const string SNSElementName = "sns";
        private const string AWSElementName = "aws";
        private const string CFElementName = "cf";
        private const string ContactAreaElementCollectionName = "contactAreas";
        private const string PressElementName = "press";
        private const string NewsletterElementName = "newsletter";
        private const string ImportElementName = "import";
        private const string PermissionsElementName = "permissions";
        private const string LiveBlogElementName = "liveBlog";
        private const string LiveBlogFeederElementName = "liveBlogFeeder";
        private const string LiveBlogPublisherElementName = "liveBlogPublisher";
        private const string MongoOutputCacheElementName = "mongoOutputCache";
        private const string LiveBlogWebElementName = "liveBlogWeb";
        private const string LiveBlogCmsElementName = "liveBlogCms";
        private const string EventSubCategoryRecordElementCollectionName = "eventSubCategoryRecords";
        private const string FoxyCartElementName = "foxyCart";

        private static bool _useConfigurationManager = false;
        private static readonly object _syncObj = new object();

        private SDConfig()
        {
        }

        public static SDConfig Instance
        {
            get
            {
                if (_useConfigurationManager)
                    return (SDConfig)ConfigurationManager.GetSection(SectionName);
                else
                    return (SDConfig)WebConfigurationManager.GetSection(SectionName);
            }
        }

        [ConfigurationProperty(InstallationElementName)]
        public InstallationElement Installation
        {
            get
            {
                return (InstallationElement)this[InstallationElementName];
            }
        }

        [ConfigurationProperty(MediaElementName)]
        public MediaElement Media
        {
            get
            {
                return (MediaElement)this[MediaElementName];
            }
        }

        [ConfigurationProperty(MongoOutputCacheElementName)]
        public MongoOutputCacheElement MongoOutputCache
        {
            get
            {
                return (MongoOutputCacheElement)this[MongoOutputCacheElementName];
            }
        }

        [ConfigurationProperty(MediaProfileElementCollectionName)]
        public MediaProfileElementCollection MediaProfiles
        {
            get
            {
                return this[MediaProfileElementCollectionName] as MediaProfileElementCollection;
            }
        }

        [ConfigurationProperty(MediaRatioElementCollectionName)]
        public MediaRatioElementCollection MediaRatios
        {
            get
            {
                return this[MediaRatioElementCollectionName] as MediaRatioElementCollection;
            }
        }

        [ConfigurationProperty(ErrorElementCollectionName)]
        public ErrorElementCollection Errors
        {
            get
            {
                return this[ErrorElementCollectionName] as ErrorElementCollection;
            }
        }

        [ConfigurationProperty(DocumentElementName)]
        public DocumentElement Document
        {
            get
            {
                return (DocumentElement)this[DocumentElementName];
            }
        }

        [ConfigurationProperty(AudioElementName)]
        public AudioElement Audio
        {
            get
            {
                return this[AudioElementName] as AudioElement;
            }
        }

        [ConfigurationProperty(DatabaseElementName)]
        public DatabaseElement Database
        {
            get
            {
                return (DatabaseElement)this[DatabaseElementName];
            }
        }

        [ConfigurationProperty(FileStoreElementName)]
        public FileStoreElement FileStore
        {
            get
            {
                return (FileStoreElement)this[FileStoreElementName];
            }
        }

        [ConfigurationProperty(SNSElementName, IsRequired = false)]
        public SNSElement SNS
        {
            get
            {
                return (SNSElement)this[SNSElementName];
            }
        }

        [ConfigurationProperty(AWSElementName)]
        public AWSElement AWS
        {
            get
            {
                return (AWSElement)this[AWSElementName];
            }
        }

        [ConfigurationProperty(ImportElementName, IsRequired = false)]
        public ImportElement Import
        {
            get
            {
                return this[ImportElementName] as ImportElement;
            }
        }

        [ConfigurationProperty(PermissionsElementName, IsRequired = false)]
        [ConfigurationCollection(typeof(PermissionsElementCollection))]
        public PermissionsElementCollection Permissions
        {
            get
            {
                return this[PermissionsElementName] as PermissionsElementCollection;
            }
        }

        public void Save()
        {
            WebConfigurationManager.OpenWebConfiguration("~/").Save();
        }

        public static void UseConfigurationManager()
        {
            lock (_syncObj)
            {
                _useConfigurationManager = true;
            }
        }
    }
}