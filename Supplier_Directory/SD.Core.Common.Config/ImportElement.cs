﻿using System.Configuration;

namespace SD.Core.Common.Config
{
    public class ImportElement : ConfigurationElement
    {
        private const string AdminEmailAttributeName = "adminEmail";
        private const string EmailNotificationsEnabledAttributeName = "emailNotificationsEnabled";
        private const string NotificationSnsArnAttributeName = "notificationSnsArn";
        private const string DefaultEventIdAttributeName = "eventId";
        private const string EnableLiveDataImportAttributeName = "enableLiveDataImport";
        private const string EnableSnsNotificationsAttributeName = "enableSnsNotifications";

        [ConfigurationProperty(AdminEmailAttributeName, IsRequired = true)]
        public string AdminEmail
        {
            get
            {
                return (string)this[AdminEmailAttributeName];
            }
        }

        [ConfigurationProperty(DefaultEventIdAttributeName, IsRequired = false)]
        public string DefaultEventId
        {
            get
            {
                var valObj = this[DefaultEventIdAttributeName];
                return valObj == null ? "" : (string)valObj;
            }
        }

        [ConfigurationProperty(EmailNotificationsEnabledAttributeName, IsRequired = false)]
        public bool EmailNotificationsEnabled
        {
            get
            {
                var valObj = this[EmailNotificationsEnabledAttributeName];
                return valObj == null ? false : (bool)valObj;
            }
        }

        [ConfigurationProperty(NotificationSnsArnAttributeName, IsRequired = false)]
        public string NotificationSnsArn
        {
            get
            {
                var valObj = this[NotificationSnsArnAttributeName];
                return valObj == null ? "" : (string)valObj;
            }
        }

        [ConfigurationProperty(EnableLiveDataImportAttributeName, IsRequired = false)]
        public bool EnableLiveDataImport
        {
            get
            {
                var valObj = this[EnableLiveDataImportAttributeName];
                return valObj == null ? false : (bool)valObj;
            }
        }

        [ConfigurationProperty(EnableSnsNotificationsAttributeName, IsRequired = false)]
        public bool EnableSnsNotifications
        {
            get
            {
                var valObj = this[EnableSnsNotificationsAttributeName];
                return valObj == null ? false : (bool)valObj;
            }
        }
    }
}