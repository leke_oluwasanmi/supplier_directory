﻿using System.Configuration;

namespace SD.Core.Common.Config
{
    public class AudioElement : ConfigurationElement
    {
        private const string DirectoryAttributeName = "directory";
        private const string CoverImageFilenameAttributeName = "coverImageFilename";
        private const string RedirctAttributeName = "redirect";

        [ConfigurationProperty(DirectoryAttributeName, IsRequired = true)]
        public string Directory
        {
            get
            {
                return (string)this[DirectoryAttributeName];
            }
        }

        [ConfigurationProperty(CoverImageFilenameAttributeName, IsRequired = true)]
        public string CoverImageFilename
        {
            get
            {
                return (string)this[CoverImageFilenameAttributeName];
            }
        }

        [ConfigurationProperty(RedirctAttributeName)]
        public string Redirct
        {
            get
            {
                return (string)this[RedirctAttributeName];
            }
        }
    }
}