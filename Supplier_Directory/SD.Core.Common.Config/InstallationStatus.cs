﻿namespace SD.Core.Common.Config
{
    public enum InstallationStatus
    {
        NotInstalled,
        Installed,
        Installing,
        InstalledWithErrors
    }
}