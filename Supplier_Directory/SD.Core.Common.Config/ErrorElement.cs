﻿using System.Configuration;
using System.Net;

namespace SD.Core.Common.Config
{
    public class ErrorElement : ConfigurationElement
    {
        private const string ErrorNameAttributeName = "name";
        private const string ActionAttributeName = "action";
        private const string ControllerAttributeName = "controller";

        [ConfigurationProperty(ErrorNameAttributeName, IsRequired = true)]
        public HttpStatusCode Name
        {
            get
            {
                return (HttpStatusCode)this[ErrorNameAttributeName];
            }
        }

        [ConfigurationProperty(ActionAttributeName, IsRequired = true)]
        public string Action
        {
            get
            {
                return this[ActionAttributeName] as string;
            }
        }

        [ConfigurationProperty(ControllerAttributeName, IsRequired = true)]
        public string Controller
        {
            get
            {
                return this[ControllerAttributeName] as string;
            }
        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}