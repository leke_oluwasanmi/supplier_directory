﻿using System;
using System.Configuration;

namespace SD.Core.Common.Config
{
    public class DatabaseElement : ConfigurationElement
    {
        private const string NameAttributeName = "name";
        private const string TempNameAttributeName = "tempName";
        private const string VersionsNameAttributeName = "versionsName";
        private const string LiveFeedAttributeName = "liveFeedName";
        private const string OutputCacheAttributeName = "outputCacheName";

        [ConfigurationProperty(NameAttributeName, IsRequired = false)]
        public string Name
        {
            get
            {
                var nameObj = this[NameAttributeName];
                if (nameObj == null) throw new Exception("Main-site entity database name has not been specified which is either a configuration oversight or an indication that you should not be using this functionality in your project");
                return nameObj.ToString();
            }
        }

        [ConfigurationProperty(TempNameAttributeName, IsRequired = false)]
        public string TempName
        {
            get
            {
                var nameObj = this[TempNameAttributeName];
                if (nameObj == null) throw new Exception("Temporary entity database name has not been specified which is either a configuration oversight or an indication that you should not be using this functionality in your project");
                return nameObj.ToString();
            }
        }

        [ConfigurationProperty(VersionsNameAttributeName, IsRequired = false)]
        public string VersionsName
        {
            get
            {
                var nameObj = this[VersionsNameAttributeName];
                if (nameObj == null) throw new Exception("Version entity database name has not been specified which is either a configuration oversight or an indication that you should not be using this functionality in your project");
                return nameObj.ToString();
            }
        }

        [ConfigurationProperty(LiveFeedAttributeName, IsRequired = false)]
        public string LiveFeedName
        {
            get
            {
                var nameObj = this[LiveFeedAttributeName];
                if (nameObj == null) throw new Exception("LiveFeed entity database name has not been specified which is either a configuration oversight or an indication that you should not be using this functionality in your project");
                return nameObj.ToString();
            }
        }

        [ConfigurationProperty(OutputCacheAttributeName, IsRequired = false)]
        public string OutputCacheName
        {
            get
            {
                var nameObj = this[OutputCacheAttributeName];
                if (nameObj == null) throw new Exception("OutputCache entity database name has not been specified which is either a configuration oversight or an indication that you should not be using this functionality in your project");
                return nameObj.ToString();
            }
        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}