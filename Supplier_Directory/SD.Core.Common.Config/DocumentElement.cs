﻿using System.Configuration;

namespace SD.Core.Common.Config
{
    public class DocumentElement : ConfigurationElement
    {
        private const string DirectoryAttributeName = "directory";
        private const string IconAttributeName = "icon";

        [ConfigurationProperty(DirectoryAttributeName, IsRequired = true)]
        public string Directory
        {
            get
            {
                return this[DirectoryAttributeName] as string;
            }
        }

        [ConfigurationProperty(IconAttributeName, IsRequired = true)]
        public string Icon
        {
            get
            {
                return this[IconAttributeName] as string;
            }
        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}