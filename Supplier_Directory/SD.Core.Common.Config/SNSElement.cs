﻿using System.Configuration;

namespace SD.Core.Common.Config
{
    public class SNSElement : ConfigurationElement
    {
        private const string PrivateAccessKeyAttributeName = "privateKey";
        private const string PublicAccessKeyAttributeName = "publicKey";
        private const string RegionAttributeName = "region";

        [ConfigurationProperty(PrivateAccessKeyAttributeName, IsRequired = true)]
        public string PrivateAccessKey
        {
            get
            {
                return (string)this[PrivateAccessKeyAttributeName];
            }
        }

        [ConfigurationProperty(PublicAccessKeyAttributeName, IsRequired = true)]
        public string PublicAccessKey
        {
            get
            {
                return (string)this[PublicAccessKeyAttributeName];
            }
        }

        [ConfigurationProperty(RegionAttributeName, IsRequired = true)]
        public string Region
        {
            get
            {
                return (string)this[RegionAttributeName];
            }
        }
    }
}