﻿using System.Configuration;
using SD.Core.Common.Constants;

namespace SD.Core.Common.Config
{
    public class MediaProfileElement : ConfigurationElement
    {
        private const string ProfileNameAttributeName = "profileName";
        private const string ProfilePrettyNameAttributeName = "prettyProfileName";
        private const string ProfileDefaultFileLocationAttributeName = "defaultFilename";
        private const string MediaWidthAttributeName = "width";
        private const string MediaHeightAttributeName = "height";
        private const string DirectoryNameAttributeName = "directoryName";
        private const string MediaRatioAttributeName = "ratio";

        [ConfigurationProperty(ProfileNameAttributeName, IsKey = true, IsRequired = true)]
        public MediaProfile ProfileName
        {
            get { return (MediaProfile)this[ProfileNameAttributeName]; }
        }

        [ConfigurationProperty(ProfilePrettyNameAttributeName, IsRequired = true)]
        public string PrettyProfileName
        {
            get { return this[ProfilePrettyNameAttributeName] as string; }
        }

        [ConfigurationProperty(ProfileDefaultFileLocationAttributeName, IsRequired = true)]
        public string DefaultFilename
        {
            get { return this[ProfileDefaultFileLocationAttributeName] as string; }
        }

        [ConfigurationProperty(MediaWidthAttributeName, IsRequired = true)]
        public int Width
        {
            get { return (int)this[MediaWidthAttributeName]; }
        }

        [ConfigurationProperty(MediaHeightAttributeName, IsRequired = true)]
        public int Height
        {
            get { return (int)this[MediaHeightAttributeName]; }
        }

        [ConfigurationProperty(DirectoryNameAttributeName, IsRequired = true)]
        public string DirectoryName
        {
            get { return this[DirectoryNameAttributeName] as string; }
        }

        [ConfigurationProperty(MediaRatioAttributeName, IsRequired = true)]
        public MediaRatio Ratio
        {
            get { return (MediaRatio)this[MediaRatioAttributeName]; }
        }
    }
}