﻿using System;
using System.Configuration;
using SD.Core.Common.Constants;

namespace SD.Core.Common.Config
{
    public class MediaRatioElementCollection : ConfigurationElementCollection
    {
        public MediaRatioElement GetRatioConfig(MediaRatio ratio)
        {
            var mediaRatio = BaseGet(ratio) as MediaRatioElement;
            if (mediaRatio == null) throw new Exception(string.Format("Couldn't find configuration for media ratio: {0}", ratio.ToString()));
            return mediaRatio;
        }

        public MediaRatioElement GetRatioConfig(MediaProfile profile)
        {
            var mediaProfile = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile) as MediaProfileElement;
            return GetRatioConfig(mediaProfile.Ratio);
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new MediaRatioElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MediaRatioElement)element).RatioName;
        }
    }
}