﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class PagesViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }

        public string UrlSlug { get; set; }

        [DataType(DataType.Html)]
        public string Body { get; set; }

        public string PageUrls { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        public dynamic Status { get; set; }
    }
}