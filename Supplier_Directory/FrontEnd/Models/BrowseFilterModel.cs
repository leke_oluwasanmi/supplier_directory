﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class BrowseFilterModel
    {
        /// <summary>
        /// Selected Tag
        /// </summary>
        public List<SD.Core.Data.Model.Tags> SelectedTags { get; set; }

        /// <summary>
        /// Selected Country
        /// </summary>
        public List<SD.Core.Data.Model.Country> SelectedCountries { get; set; }

        /// <summary>
        /// Selected Region
        /// </summary>
        public List<SD.Core.Data.Model.Region> SelectedRegions { get; set; }

        /// <summary>
        /// Child Tags
        /// </summary>
        public List<SD.Core.Data.Model.Tags> Tags { get; set; }
        

        /// <summary>
        /// Regions
        /// </summary>
        public List<SD.Core.Data.Model.Region> Regions { get; set; }

        

        /// <summary>
        /// Selected Countries
        /// </summary>
        public List<SD.Core.Data.Model.Country> Countries { get; set; }
        

        public BrowseFilterModel()
        {
            Tags = new List<SD.Core.Data.Model.Tags>();
            Countries = new List<SD.Core.Data.Model.Country>();
            Regions = new List<SD.Core.Data.Model.Region>();
            SelectedCountries = new List<SD.Core.Data.Model.Country>();
            SelectedRegions = new List<SD.Core.Data.Model.Region>();
            SelectedTags = new List<SD.Core.Data.Model.Tags>();
        }
    }
}