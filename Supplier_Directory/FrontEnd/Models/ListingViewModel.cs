﻿using SD.Core.Data.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Models
{
    public class ListingViewModel
    {
        public Listing Listing { get; set; }
        public string FullAddress
        {
            get
            {
                string fullAddress = "";
                if(Listing !=null)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendFormat("{0},{1},{2},{3},{4},{5}",
                        Listing.Address1, 
                        Listing.Address2, 
                        Listing.Address3, 
                        Listing.County,
                        Listing.RelatedCountry != null ? Listing.RelatedCountry.Name : "", 
                        Listing.PostCode);
                    fullAddress = builder.ToString();               
                }
                return fullAddress;
            }
        }
        public double? Latitude { get; set; }
        public double? Longitude { get; set;}
    }

    public class ListingFormModel
    {
        public Listing Listing { get; set; }
        
        public IEnumerable<SelectListItem> MainCategoryList { get; set; }

        public IEnumerable<SelectListItem> SubCategoryList { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Please select Category")]
        public string MainCategoryId { get; set; }
        public string SubCategoryId { get; set; }
        public string SelectedTags { get; set; }
    }
}