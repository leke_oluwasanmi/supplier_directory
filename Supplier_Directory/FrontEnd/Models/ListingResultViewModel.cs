﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class ListingResultViewModel
    {
        /// <summary>
        /// Results
        /// </summary>
        public List<SD.Core.Data.Model.Listing> Results { get; set; }

        /// <summary>
        /// Latest
        /// </summary>
        public List<SD.Core.Data.Model.Listing> LatestListings { get; set; }
        
        /// <summary>
        /// Popular Categories
        /// </summary>
        public List<SD.Core.Data.Model.Tags>  PopularCategories { get; set; }   
        
        /// <summary>
        /// Regions
        /// </summary>
        public List<SD.Core.Data.Model.Region> Regions { get; set; }          

        /// <summary>
        /// search or browse
        /// </summary>
        public string SearchType { get; set; }

        /// <summary>
        /// Handles the searching
        /// </summary>
        public SearchFilterModel SearchFilter { get; set; }

        /// <summary>
        /// Handles the browsing
        /// </summary>
        public BrowseFilterModel BrowseFilter { get; set; }

        public List<SD.Core.Data.Model.FeaturedListing> FeaturedListings { get; set; }

        public SD.Core.Data.Model.SponsoredListing SponsoredListing { get; set; }

        public ListingResultViewModel()
        {
            Results = new List<SD.Core.Data.Model.Listing>();
            LatestListings = new List<SD.Core.Data.Model.Listing>();
            PopularCategories = new List<SD.Core.Data.Model.Tags>();
            //default to search
            SearchType = "search";
            SearchFilter = new SearchFilterModel();
            BrowseFilter = new BrowseFilterModel();
            Regions = new List<SD.Core.Data.Model.Region>();
            FeaturedListings = new List<SD.Core.Data.Model.FeaturedListing>();
            SponsoredListing = new SD.Core.Data.Model.SponsoredListing();
        }        

    }
}