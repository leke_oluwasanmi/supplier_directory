﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class SearchFilterModel
    {
        /// <summary>
        /// Search Keyword
        /// </summary>
        public string Keyword { get; set; }
        /// <summary>
        /// Search Category
        /// </summary>
        public SD.Core.Data.Model.Tags Category { get; set; }
        /// <summary>
        /// Search Location
        /// </summary>
        public SD.Core.Data.Model.Country Location { get; set; }

        public SearchFilterModel() { }
    }
}