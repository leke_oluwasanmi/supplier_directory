﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;
using SD.Core.Data.Model;
using SD.Core.Services.Security;
using System.ComponentModel.DataAnnotations;
using SD.Core.Common.Constants;

namespace Frontend.Models
{
    public class ProfileCreateViewModel
    {
        [Required]
        [EmailAddressAttribute]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} character long.", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "The Password and Confirm password do not matched.")]
        public string ConfirmPassword { get; set; }

        //terms and conditions
        [Frontend.Helpers.Validation.BooleanRequired(ErrorMessage = "You must accept the terms and conditions.")]
        [Display(Name = "I accept the terms and conditions")]
        public bool accept_terms { get; set; }
    }

    public class ProfileLoginViewModel
    {
        [Required]
        [EmailAddressAttribute]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} character long.", MinimumLength = 6)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ProfileForgetPasswordViewModel
    {
        [Required]
        [EmailAddressAttribute]
        [Display(Name = "Email address")]
        public string Email { get; set; }
    }


    public class ProfileChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Old Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} character long.", MinimumLength = 6)]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} character long.", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }
    }


    public class ProfilePasswordRecoveryViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} character long.", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    
    public class ProfileEditModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Telephone { get; set; }

        [Display(Name = "Account Type")]
        public AccountType AccountType { get; set; }


        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Display(Name = "Address 3")]
        public string Address3 { get; set; }
        [Display(Name = "Address 4")]
        public string Address4 { get; set; }
        public string Town { get; set; }
        public string CountryId { get; set; }
        public string Country { get; set; }
        public string County { get; set; }

        [Display(Name = "Postal Code")]
        public string Postcode { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Company Description")]
        public string CompanyDescription { get; set; }
        //public string Password { get; set; }
        public virtual string ActivationToken { get; set; }
        //public virtual IList<Listing> Listings { get; set; }
        //public virtual IList<SavedSearch> SavedSearches { get; set; }
        public int LoginAttempts { get; set; }
        public PublicationStatus Published { get; set; } // Active, Not Active
        public Boolean IsActivated { get; set; }        
        
    }    
    
}