﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models
{
    public class EmailViewModel
    {
        public string To { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
        public string Subject { get; set; }
        public string Username { get; set; }
        public string UrlToken { get; set; }
        public DateTime Date { get; set; }

    }
}