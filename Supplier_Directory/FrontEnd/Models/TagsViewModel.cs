﻿using System.Collections.Generic;
using SD.Core.Data.DTOs;

namespace Frontend.Models
{
    public class TagsViewModel
    {
        public IList<Tags> PopularTags { get; set; }
    }

    public class Tag
    {
        public string tagName { get; set; }
        public string tagSlug { get; set; }
        public string tagCount { get; set; }
    }
}