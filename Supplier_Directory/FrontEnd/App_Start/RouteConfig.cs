﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using LowercaseRoutesMVC;

namespace Frontend
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            
            routes.MapRouteLowercase(
                name: "ListingDetail",
                url: "listings/detail/{urlSlug}",
                defaults: new { controller = "Listings", action = "Detail" }
            );
            routes.MapRouteLowercase(
                name: "ListingResults",
                url: "listings/results",
                defaults: new { controller = "Listings", action = "Results" }
            );

            routes.MapRouteLowercase(
                 name: "ListingCategory",
                url: "listings/category/{categoryUrl}",
                defaults: new { controller = "Listings", action = "Category", categoryUrl = UrlParameter.Optional }
            );


            routes.MapRouteLowercase(
                name: "AccountActivation",
                url: "Profile/Activation/{id}/{token}",
                defaults: new { controller = "Profile", action = "Activation", id = UrlParameter.Optional, token = UrlParameter.Optional }
            );


            routes.MapRouteLowercase(
                name: "RecoverPassword",
                url: "Profile/RecoverPassword/{id}/{token}",
                defaults: new { controller = "Profile", action = "RecoverPassword", id = UrlParameter.Optional, token = UrlParameter.Optional }
            );

            //about us
            routes.MapRouteLowercase(
                name: "About",
                url: "About",
                defaults: new { controller = "Home", action = "About" },
                namespaces: new[] { "Frontend.Controllers" }
            );


            routes.MapRouteLowercase(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces: new [] { "Frontend.Controllers" });
        }
    }
}
