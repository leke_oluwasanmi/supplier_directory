﻿using Frontend.Models;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Frontend.handlers
{
    public class EmailHandler
    {
        public async static Task SendMail(string template, EmailViewModel model)
        {
            model.SenderName = ConfigurationManager.AppSettings["EmailSenderName"];
            model.SenderEmail = ConfigurationManager.AppSettings["EmailSenderAddress"];            
            MailMessage message = new MailMessage();
            message.To.Add(model.To);
            message.From = new System.Net.Mail.MailAddress(model.SenderEmail, model.SenderName);
            message.Subject = model.Subject;
            message.Body = GetEMailContent(
                                model.To,
                                model.SenderEmail,
                                model.Subject,
                                model.Date.ToShortDateString(),
                                model.Username,
                                model.UrlToken,
                                template
                           );
            //using (var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]))
            string env = ConfigurationManager.AppSettings["ServerEnvironment"];
            if (env == "staging")
            {
                ServicePointManager.ServerCertificateValidationCallback =
        delegate (object s, X509Certificate certificate,
                 X509Chain chain, SslPolicyErrors sslPolicyErrors)
        { return true; };
                using (var client = new SmtpClient("mail.blakeshore.com"))
                {
                    client.Credentials = new System.Net.NetworkCredential("haymarket@blakeshore.com", "haymarket1*");
                    client.Port = 587;
                    await client.SendMailAsync(message);
                }
            }
            else
            {
                using (var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]))
                {                    
                    await client.SendMailAsync(message);
                }

            }

        }

        private static string GetEMailContent(string to, string sender, string subject, string date, string username, string urlToken, string template)
        {
            var fileName = HostingEnvironment.MapPath("~/Views/Emails/" + template + ".cshtml");
            //get the file
            StreamReader objStreamReader = new StreamReader(fileName);
            //read html template file
            string bodyMsg = objStreamReader.ReadToEnd();
            //replace the dynamic string at run-time
            bodyMsg = bodyMsg.Replace("##To##", (!string.IsNullOrEmpty(to))? to : "");
            bodyMsg = bodyMsg.Replace("##Sender##", (!string.IsNullOrEmpty(sender)) ? sender : "");
            bodyMsg = bodyMsg.Replace("##Subject##", (!string.IsNullOrEmpty(subject)) ? subject : "");
            bodyMsg = bodyMsg.Replace("##Date##", (!string.IsNullOrEmpty(date)) ? date : "");
            bodyMsg = bodyMsg.Replace("##Username##", (!string.IsNullOrEmpty(username)) ? username : "");
            bodyMsg = bodyMsg.Replace("##UrlToken##", (!string.IsNullOrEmpty(urlToken)) ? urlToken : "");
            return bodyMsg;
        }
    }
}