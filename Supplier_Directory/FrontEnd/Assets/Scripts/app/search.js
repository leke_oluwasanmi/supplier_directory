﻿
$(function() {

    $("#location").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/home/LookUpCountry", type: "POST", dataType: "json",
                //original code
                //data: { searchText: request.id, maxResults: 10 },
                //updated code; updated to request.term 
                //and removed the maxResults since you are not using it on the server side
                data: { country: request.term },

                success: function (data) {
                    response($.map(data, function (item) {
                        //original code
                        //return { label: item.FullName, value: item.FullName, id: item.TagId }; 
                        //updated code
                        return { label: item.name, value: item.name, category : item.region, id : item.name};
                    }));
                },
                
            });
        },
        select: function (event, ui) {
            //update the jQuery selector here to your target hidden field
            $("#countryId").val(ui.item.id);
        }
    });

    $("#category").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/home/GetAllTags", type: "POST", dataType: "json",                
                data: { tag: request.term },
                success: function (data) {
                    response($.map(data, function (item) {                        
                        return { label: item.label, value: item.label, id: item.name };
                    }));
                },
              
            });
        },
        select: function (event, ui) {
            //update the jQuery selector here to your target hidden field
            $("#categoryId").val(ui.item.id);
        }
    });
});

