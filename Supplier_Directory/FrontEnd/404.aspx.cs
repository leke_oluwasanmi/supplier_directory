﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.Core.Services.Core;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Common;

namespace Frontend
{
    public partial class _404 : System.Web.UI.Page
    {
        private IMongoRepository1 mongoRepository;
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = Request.RawUrl;

            string[] splitUrl = url.Split('/');
            string urlSlug = splitUrl[splitUrl.Length - 1];

            if (urlSlug.Length > 45)
            {
                urlSlug = urlSlug.Substring(0, 45);

            }

            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();            


        }
    }
}