﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data;
using SD.Core.Common;
using MongoDB.Driver;

namespace Frontend
{
    public class SDNinjectModule : NinjectModule
    {
        public override void Load()
        {

            AppKernel.kernel.Bind<IMongoRepository1>()
              .To<MongoRepository>();
            AppKernel.kernel.Bind<IMongoOutputCacheRepository>()
                .To<MongoOutputCacheRepository>();
            AppKernel.kernel.Settings.AllowNullInjection = true;


            //AppKernel.kernel.Bind<IMongoCollection<VWG.Core.Data.DTOs.Article>>()
            //.To < MongoCollectionBase<VWG.Core.Data.DTOs.Article>>();

        }
    }
}
