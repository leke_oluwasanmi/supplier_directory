﻿using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using System;
using System.Web;
using System.Web.Security;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Services.Core;
using SD.Core.Services.Security;
using SD.Core.Data.Model.HS;
namespace Frontend.Helpers
{
    /// <summary>
    /// TODO: Make this a singleton
    /// </summary>
    public class MembershipHelper
    {
        private static string userAuthId;

        public static string Login(string email, string password, bool persistent, MemberHydrationSettings hydrationSettings = null)
        {
            var memberService = new MemberService(AppKernel.GetInstance<IMongoRepository1>());

            var user = memberService.GetLogin(email, password, hydrationSettings);
            if (user != null)
            {
                if (user.IsActivated)
                {
                    FormsAuthentication.SetAuthCookie(user.Id, persistent);
                    userAuthId = user.Id;
                    
                    return "true";
                }
                return "inactive";
            }
            else return "false";
        }
        //We don't need this as it is in the loginManager already
        public static void Logout()
        {
            FormsAuthentication.SignOut();
            userAuthId = string.Empty;
        }


        public static bool IsAuthenticated()
        {
            return !string.IsNullOrEmpty(GetActiveUserId);
        }

        public static Member GetActiveUser()
        {
            //if (!IsAuthenticated())
            //    throw new Exception("No user is authenticated. Please login first");
            if (IsAuthenticated())
            {
                var memberService = new MemberService(AppKernel.GetInstance<IMongoRepository1>());

                return memberService.GetById(GetActiveUserId);
            }
            return null;
        }

        public static string GetActiveUserId
        {
            get
            {
                if (HttpContext.Current.User == null) return null;
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name)) return HttpContext.Current.User.Identity.Name as string;
                if (!string.IsNullOrWhiteSpace(userAuthId)) return userAuthId;
                return null;
            }
        }
    }
}