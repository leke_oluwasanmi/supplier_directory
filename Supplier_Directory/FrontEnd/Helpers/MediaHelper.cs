﻿using SD.Core.Common.Config;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Helpers
{
    public class MediaHelper
    {        
        public static string GetMediaUri(MediaProfile profile, SD.Core.Data.Model.Media media)
        {
            var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);
            if (media == null) return MediaUtils.GetDefaultMediaUri(profileConfig);
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
            {
                if (!media.AvailableRatios.Contains(profileConfig.Ratio))
                {
                    // The specified media item doesnt support the requested profile so return the default image url for the profile
                    return MediaUtils.GetDefaultMediaUri(profileConfig);
                }

                if (!string.IsNullOrWhiteSpace(media.FileName))
                {
                    // Hosted locally so return the local media uri
                    return MediaUtils.GetMediaUri(profileConfig, media.FileName);
                }
                else
                {
                    throw new Exception("No uploaded file is available to get the MediaUri for.");
                }
            }
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                switch (media.Hosting)
                {

                    case MediaHosting.RemoteYoutube: return MediaUtils.GetYouTubeThumbnailUri(media.RemoteItemCode);
                    case MediaHosting.RemoteBrightCove: return MediaUtils.GetBrightCoveThumbnailUri(media.RemoteItemCode);
                }
            }
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
            {
                return MediaUtils.GetAudioCoverImageUri(profileConfig);
            }
            else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
            {
                return MediaUtils.GetDefaultMediaUri(profileConfig);
            }

            throw new Exception("Failed to resolve media uri");
        }

        /// <summary>
        /// Gets a uri for the largest possible media profile available to the specified Media item and for the given ratio
        /// </summary>
        /// <param name="ratio"></param>
        /// <param name="media"></param>
        /// <returns></returns>
        public static string GetMediaUri(MediaRatio ratio, Media media)
        {
            if (media.Hosting == MediaHosting.Local)
            {
                // Get the media profile
                IList<MediaProfileElement> profileConfigs = SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio)
                    .Where(x => x.Ratio == ratio)
                    .ToList();
                if (profileConfigs == null || profileConfigs.Count == 0) throw new Exception("No profiles of the given ratio are available to the media item."); //TODO: Will probably need to return a default image.
                var largestProfileConfig = profileConfigs[0];
                for (var i = 1; i < profileConfigs.Count; i++)
                {
                    var profileConfig = profileConfigs[i];
                    if ((profileConfig.Width * profileConfig.Height) > (largestProfileConfig.Width * largestProfileConfig.Height) &&
                        media.AvailableRatios.Contains(profileConfig.Ratio))
                        largestProfileConfig = profileConfig;
                }
                return GetMediaUri(largestProfileConfig.ProfileName, media);
            }
            else if (media.Hosting == MediaHosting.RemoteYoutube)
            {
                return MediaUtils.GetYouTubeThumbnailUri(media.RemoteItemCode);
            }
            else if (media.Hosting == MediaHosting.RemoteBrightCove)
            {
                return MediaUtils.GetBrightCoveThumbnailUri(media.RemoteItemCode);
            }

            else
            {
                throw new ArgumentException("No media url resolution defined for the value of Media.Hosting (MediaHosting) property", "media");
            }
        }

        public static string GetVideoThumbnailUri(Media media)
        {
            if (!FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                throw new Exception("Media item is not a video.");
            }
            switch (media.Hosting)
            {
                case MediaHosting.RemoteYoutube:
                    return MediaUtils.GetYouTubeThumbnailUri(media.RemoteItemCode);

                case MediaHosting.RemoteBrightCove:
                    return MediaUtils.GetBrightCoveThumbnailUri(media.RemoteItemCode);

                default:
                    throw new Exception("Media item is not hosted remotely. Videos should always be hosted remotely");
            }
        }       
    }
}