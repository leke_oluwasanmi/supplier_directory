﻿using SD.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SD.Core.Data.Model;
namespace Frontend.Helpers.HtmlExtensions
{
    public static class MediaExtensions
    {
        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaProfile profile, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetMediaUri(profile, media));
        }

        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaRatio ratio, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetMediaUri(ratio, media));
        }

        public static MvcHtmlString GetVideoThumbnailUri(this HtmlHelper helper, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetVideoThumbnailUri(media));
        }
    }
}