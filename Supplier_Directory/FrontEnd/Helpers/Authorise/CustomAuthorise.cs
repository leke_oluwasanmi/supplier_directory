﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using SD.Core.Services.Security;
using SD.Core.Common;
using System.Web.Routing;
using System;

namespace Frontend.Helpers.Authorise
{
    public class CustomAuthorise : AuthorizeAttribute
    {

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!HasValidUser())
            {
                if (!HasValidUser())
                {                    
                    filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(new { controller = "Profile", action = "Signin", returnUrl = filterContext.HttpContext.Request.Url.GetComponents(UriComponents.PathAndQuery, UriFormat.SafeUnescaped) }));
                }

            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return HasValidUser();
        }

        private bool HasValidUser()
        {
            if (MembershipHelper.IsAuthenticated())
            {
                return true;
            }
            else
                return false;
        }

    }
}