﻿using System.Web.Mvc;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Services.Core;
using SD.Core.Services.Security;

namespace Frontend.Controllers
    {
    public class StatsController : BaseController
    {        
        private readonly TagsService _tagService = new TagsService(AppKernel.GetInstance<IMongoRepository1>());

        [Route("category-viewed/{id}")]
        [HttpGet]
        public void CategoryViewed(string id)
        {
            if (!Request.Browser.Crawler)
                _tagService.RegisterTagViewed(id);
        }
    }

}