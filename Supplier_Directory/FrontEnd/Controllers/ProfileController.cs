﻿using System.Linq;
using System.Web.Mvc;
using Frontend.Helpers.Authorise;
using Frontend.Models;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using SD.Core.Services.Security;
using System;
using Frontend.handlers;
using System.Configuration;
using Frontend.Helpers;
using System.Threading.Tasks;

namespace Frontend.Controllers
{
    public class ProfileController : BaseController
    {
        private IMongoRepository1 mongoRepository;

        public readonly MemberService _memberService;
        public readonly ListingService _listingService;        
        public readonly CountryService _countryService;
        public MemberHydrationSettings hydrationSettings = new MemberHydrationSettings { Country = new CountryHydrationSettings(),
         MemberListings = new ListingHydrationSettings(), UpdatedBy = new UserHydrationSettings()};
        public ProfileController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _listingService = new ListingService(mongoRepository);            
            _memberService = new MemberService(mongoRepository);
            _countryService = new CountryService(mongoRepository);
        }

        public ProfileController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _memberService = new MemberService(mongoRepository);
            _listingService = new ListingService(mongoRepository);            
            _countryService = new CountryService(mongoRepository);
        }
        
        [CustomAuthorise]
        public ActionResult Index()
        {
            var model = getProfile(MembershipHelper.GetActiveUserId);
            if (model == null)
            {
                return RedirectToAction("Signin");
            }
            ViewBag.Countries = _countryService.GetCountries().OrderBy(a => a.Name).Select(x => new SelectListItem
            {                
                Value = x.Id, //temporarily use country name as value field till the Member model support countryId
                Text = x.Name
            });
            return View(model);
        }

        ProfileEditModel getProfile(string id)
        {
            var user = _memberService.FindDTOById(id);
            if (user != null)
            {
                var model = new ProfileEditModel()
                {
                    Id = user.Id,
                    Title = user.Title,
                    Telephone = user.Telephone,
                    AccountType = user.AccountType,
                    Address1 = user.Address1,
                    Address2 = user.Address2,
                    Address3 = user.Address3,
                    Address4 = user.Address4,
                    Town = user.Town,
                    CountryId = user.CountryId,
                    County = user.County,
                    Postcode = user.Postcode,
                    Firstname = user.Firstname,
                    Surname = user.Surname,
                    Email = user.Email,
                    //Password = user.Password,
                    ActivationToken = user.ActivationToken,
                    LoginAttempts = user.LoginAttempts,
                    Published = user.Status,
                    IsActivated = user.IsActivated,
                };
                var country = _countryService.FindById(model.CountryId);
                model.Country = country.Name;
                return model;
            }
            return null;
        }

        public ActionResult Register()
        {
            var model = new ProfileCreateViewModel();
            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> Register(FormCollection collection, ProfileCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //check if account already exist
                    var exist = _memberService.GetAllMembers().Where(x => x.Email == model.Email).Count() > 0;
                    if (exist)
                    {
                        ModelState.AddModelError("", "This email address is already exist");
                    }
                    else
                    {

                        var insert = _memberService.AddNewSignup(model.Email, model.Password);
                        if (insert != null && !String.IsNullOrEmpty(insert.Email))
                        {
                            var template = "AccountActivation";
                            var emailData = new EmailViewModel
                            {
                                To = insert.Email,

                                Subject = "Account Activation",
                                Username = insert.Firstname,
                                UrlToken = Url.Action("Activation", "Profile", new { id = insert.Id, token = insert.ActivationToken }, Request.Url.Scheme),
                                Date = insert.CreatedOn,
                            };

                            //send email
                            await EmailHandler.SendMail(template, emailData);

                        }
                        return RedirectToAction("Completed");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Error Occured. Please check and try again later");
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //ModelState.AddModelError("", ex.Message);
            }
            return View(model);
        }

        [HttpGet]
        [CustomAuthorise]
        public ActionResult Completed()
        {
            ViewBag.Message = "We have sent you an email to the email address provided. Follow the link in the email to activate your account";
            return View();
        }

        [HttpGet]        
        public ActionResult Activation(string id, string token)
        {
            //check if valid
            var data = _memberService.FindDTOById(id);
            if (data != null)
            {
                if (data.IsActivated)
                {
                    ViewBag.Message = "You have already completed this process. Please login to access your profile";
                    ViewBag.MessageType = "completed";
                }
                else if (data.ActivationToken == token) //set verification to true if not already been set
                {
                    var model = data;

                    model.IsActivated = true;
                    model.ActivationToken = "";
                    model.LiveFrom = DateTime.Now;
                    model.Status = SD.Core.Common.Constants.PublicationStatus.Published;
                    _memberService.Edit(model);
                    ViewBag.Message = "Your account has been activated successfully.";
                    ViewBag.MessageType = "success";
                }
                else
                {
                    ViewBag.Message = "Invalid verification link. Please check your email for valid link to continue your application";
                    ViewBag.MessageType = "failed";
                }
            }
            else
            {
                ViewBag.Message = "Invalid Url. Please try again";
                ViewBag.MessageType = "failed";
            }

            return View();
        }

        public ActionResult Signin(string returnUrl)
        {
            var model = new ProfileLoginViewModel();
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        public ActionResult Logout()
        {
            MembershipHelper.Logout();
            return RedirectToAction("Signin");
        }

        [HttpPost]
        public ActionResult Signin(FormCollection collection, ProfileLoginViewModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //check if valid
                    //var data = _memberService.GetLogin(model.Email, model.Password);
                    var login = MembershipHelper.Login(model.Email, model.Password, model.RememberMe);
                    if (login == "true")
                    {
                        if (!string.IsNullOrEmpty(returnUrl))
                            return RedirectToLocal(returnUrl);
                        else
                            return RedirectToAction("Index");
                    }
                    else if (login == "inactive")
                    {
                        ModelState.AddModelError("", "Your account is not yet activated. Check your email and click on the activation link sent to you during registration to continue");
                    }
                    else
                    {
                        //TODO: apply login attempt count
                        ModelState.AddModelError("", "Invalid username or password");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid details entered. Please try again");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

            }
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }


        [HttpPost]
        [CustomAuthorise]
        public ActionResult Index(FormCollection collection, ProfileEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //check if account already exist
                    var record = _memberService.FindDTOById(MembershipHelper.GetActiveUserId);                    
                    if (record != null)
                    {
                        record.AccountType = model.AccountType;
                        record.Address1 = model.Address1;
                        record.Address2 = model.Address2;
                        record.Address3 = model.Address3;
                        record.Address4 = model.Address4;
                        record.CountryId = model.CountryId;
                        record.County = model.County;
                        if (!string.IsNullOrEmpty(model.Email))
                        {
                            record.Email = model.Email;
                        }
                        record.Firstname = model.Firstname;
                        record.Postcode = model.Postcode;
                        record.Surname = model.Surname;
                        record.Telephone = model.Telephone;
                        record.Title = model.Title;
                        record.Town = model.Town;
                        record.UpdatedById = MembershipHelper.GetActiveUser().UpdatedById;
                        record.UpdatedOn = DateTime.Now;
                        //save
                        _memberService.Edit(record);
                        //get updated record
                        model = getProfile(model.Id);
                        ModelState.AddModelError("", "Record saved successfully");
                    }
                    else
                    {
                        ModelState.AddModelError("", "We cannot find your record. Please try again.");

                        return RedirectToAction("Signin");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Error Occured. Please check and try again later");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            ViewBag.Countries = _countryService.GetCountries().Select(x => new SelectListItem
            {
                //Value = x.Id,
                Value = x.Id, //temporarily use country name as value field till the Member model support countryId
                Text = x.Name,
                Selected = (model.CountryId == x.Id)
            }).ToList();

            return View(model);
        }

        [HttpGet]
        [CustomAuthorise]
        public ActionResult ChangePassword()
        {
            var model = new ProfileChangePasswordModel();
            return View();
        }

        [HttpPost]
        [CustomAuthorise]
        public async Task<ActionResult> ChangePassword(FormCollection collection, ProfileChangePasswordModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //check if valid
                    var data = MembershipHelper.GetActiveUser();
                    var passwordChanged = _memberService.ChangePassword(data.Id, model.Password);
                    if (passwordChanged)
                    {
                        var template = "PasswordChanged";
                        var emailData = new EmailViewModel
                        {
                            To = data.Email,
                            Subject = "Password Changed",
                            Username = data.Firstname,
                            UrlToken = "",
                            Date = DateTime.UtcNow,
                        };

                        //send email
                        await EmailHandler.SendMail(template, emailData);
                        ViewBag.Message = "Your password has been changed successfully. Please check your email for detail";
                        //clear the model
                        model = null;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Your password could not be changed, Please try again");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Error occured. Please check your input and try again");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

            }
            return View(model);
        }


        [HttpGet]
        public ActionResult RecoverPassword(string id, string passwordToken)
        {
            ViewBag.IsValid = true;
            if (string.IsNullOrEmpty(passwordToken) || string.IsNullOrEmpty(id))
            {
                ViewBag.IsValid = false;
            }

            var user = _memberService.GetById(id);
            if (user == null || user.PasswordToken != passwordToken)
            {
                ViewBag.IsValid = false;
            }

            if (ViewBag.IsValid)
            {
                var model = new ProfilePasswordRecoveryViewModel();
                ViewBag.PasswordToken = user.PasswordToken;
                ViewBag.MemberId = user.Id;
                return View(model);
            }
            else
            {
                ViewBag.Message = "Invalid password token, Please try again";
                return View();
            }
        }

        [HttpPost]        
        public async Task<ActionResult> RecoverPassword(string id, string passwordToken, ProfilePasswordRecoveryViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //check if valid
                    var data = _memberService.GetById(id);
                    if (data != null)
                    {
                        var passwordChanged = _memberService.ChangePassword(id, model.Password);
                        if (passwordChanged)
                        {
                            var template = "PasswordChanged";
                            var emailData = new EmailViewModel
                            {
                                To = data.Email,
                                Subject = "Password Changed",
                                Username = data.Firstname,
                                UrlToken = "",
                                Date = DateTime.UtcNow,
                            };

                            //send email
                            await EmailHandler.SendMail(template, emailData);
                            ViewBag.Message = "Your password has been changed successfully. Please check your email for details";
                            //clear the model
                            model = null;
                            return RedirectToAction("Signin");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Your password could not be changed, Please try again");
                        }
                    }
                    ModelState.AddModelError("", "Invalid request, Please use a valid link from your email ");
                }
                else
                {
                    ModelState.AddModelError("", "Error occured. Please check your input and try again");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

            }
            return View(model);
        }

        [CustomAuthorise]
        public ActionResult ForgetPassword()
        {
            var model = new ProfileForgetPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [CustomAuthorise]
        public async Task<ActionResult> ForgetPassword(FormCollection collection, ProfileForgetPasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //check if valid
                    var data = _memberService.GetByEmail(model.Email);
                    if (data != null)
                    {
                        var post = _memberService.FindDTOById(data.Id);
                        post.PasswordToken = Guid.NewGuid().ToString("N");

                        //pass to model
                        data.PasswordToken = post.PasswordToken;

                        mongoRepository.Update(post);

                        var template = "RecoverPassword";
                        var emailData = new EmailViewModel
                        {
                            To = data.Email,
                            //SenderEmail = ConfigurationManager.AppSettings["EmailSenderName"],
                            Subject = "Recover Password",
                            Username = data.Firstname,
                            UrlToken = Url.Action("RecoverPassword", "Profile", new { id = data.Id, token = data.ActivationToken }, Request.Url.Scheme),
                            Date = DateTime.UtcNow,
                        };

                        //send email
                        await EmailHandler.SendMail(template, emailData);
                        ViewBag.Message = "Please check your email for a password recovery link";
                        //clear the model
                        model = null;
                    }
                    else
                    {
                        ModelState.AddModelError("", "This email address does not exist on our record");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please check your input");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

            }
            return View(model);
        }


        #region Private Methods

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        #endregion


        #region History
        [CustomAuthorise]
        public ActionResult ViewedArticleHistory(string id)
        {
            return View();
        }

        #endregion
    }
}