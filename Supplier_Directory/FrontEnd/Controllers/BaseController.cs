﻿using System;
using System.Web.Mvc;
using SD.Core.Common;
using SD.Core.Data;

namespace Frontend.Controllers
{
    public class BaseController : Controller
    {
        private IMongoRepository1 mongoRepository;

        public BaseController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
        }

        public BaseController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;

        }


    }

}