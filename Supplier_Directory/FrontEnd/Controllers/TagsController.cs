﻿using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend.Controllers
{
    public class TagsController : Controller
    {
        // GET: Tags
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PopularTags(int take)
        {
            IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            TagHydrationSettings settings = new TagHydrationSettings() { ParentTag = new TagHydrationSettings() };
            TagsService tagsService = new TagsService(mongoRepository);
            var tags = tagsService.GetPopularTags(take, settings);
            ViewBag.PopularCategories = tags.OrderByDescending(x => x.DisplayText);
            return PartialView("PopularTags");
        }
    }
}