﻿using System.Web.Mvc;
using SD.Core.Services.Core;
using SD.Core.Data.Model.HS;
using Frontend.Helpers.Authorise;
using SD.Core.Services.Security;
using System;
using System.Linq;
using Frontend.Models;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Common.Constants;
using System.Collections.Generic;
using Frontend.Helpers;
using SD.Core.Data.Model;
using X.PagedList;

namespace Frontend.Controllers
{
    public class HomeController : BaseController
    {
        private IMongoRepository1 mongoRepository;
        public readonly CountryService _countryService;
        public readonly TagsService _tagsService;
        public readonly FeaturedListingService _featuredListingService;
        public readonly ListingService _listingService;
        private const int _pageSize = 10;
        public ListingHydrationSettings _listingHydrationSettings = new ListingHydrationSettings()
        {
            PrimaryMedia = new MediaHydrationSettings(),
            RelatedMedia = new MediaHydrationSettings(),
            RelatedTags = new TagHydrationSettings(),
            Country = new CountryHydrationSettings()
        };
        public HomeController()
        {           
             mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _countryService = new CountryService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _featuredListingService = new FeaturedListingService(mongoRepository);
            _listingService = new ListingService(mongoRepository);
        }
        public HomeController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _countryService = new CountryService(_mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _featuredListingService = new FeaturedListingService(mongoRepository);
            _listingService = new ListingService(mongoRepository);
        }
        
        public int DefaultPageSize = 20;
                  
        public ActionResult Index(string urlSlug, int page=1)
        {
            ListingResultViewModel model = new ListingResultViewModel();
            TagHydrationSettings settings = new TagHydrationSettings() { ParentTag = new TagHydrationSettings() };
            FeaturedListingHydrationSettings featuredSettings = new FeaturedListingHydrationSettings() { ListingHydrationSettings = new ListingHydrationSettings() };
            //get featured lisitngs
            List<FeaturedListing> featuredListings = _featuredListingService.GetAllByPage(new[] { PublicationStatus.Published }, Page.Home, featuredSettings,true, true);
            model.FeaturedListings = featuredListings;

            IList<SD.Core.Data.Model.Listing> listings = _listingService.Search(new List<string>(), "","", 0, 0, true, true, _listingHydrationSettings, true);
            model.Results = listings.ToList();
            var resultsPage = model.Results.ToPagedList(page, _pageSize);
            ViewBag.ResultsPage = resultsPage;
            return View(model);
        }

        public ActionResult About()
        {            
            return View();
        }

        public ActionResult TermsAndConditions()
        {           
            string identifier = "terms-and-conditions";
            var pageService = new PageService(mongoRepository);
            var page = pageService.GetAllPages().Where(x => x.UrlSlug == identifier).SingleOrDefault();
            if (page != null)
            {
                var model = new PagesViewModel()
                {
                    Id = page.Id,
                    Title = page.Title,
                    UrlSlug = page.UrlSlug,
                    PageUrls = page.PageUrls,
                    Body = page.Body,
                    CreatedOn = page.CreatedOn,
                    Status = page.Status,
                    UpdatedOn = page.UpdatedOn
                };
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }  


        #region Logout

        public ActionResult SignOut()
        {
            Request.Headers.Remove("Cache-Control");
            System.Web.Security.FormsAuthentication.SignOut();
            HttpContext.Session.Abandon();

            return Redirect(string.Format(ConfigHelper.IMLogOutUrl,
                "Logout?realm=vwg&goto=https%3A%2F%2F",
                Request.Url.DnsSafeHost));
        }

        #endregion

        #region Json methods
        /// <summary>
        /// Returns JSON of related tags, according to the current filter
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        
        public ActionResult GetAllTags(string tag)
        {
            var results = _tagsService.AllTagsThatContains(tag);            
            return Json(results.Select(x => new
            {
                label = x.DisplayText,
                value = x.Id
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult LookUpCountry(string country)
        {
            var countries = _countryService.GetCountries().Where(x => x.Name.ToLower().Contains(country.ToLower()));
            if (countries.Count() > 0)
            {
                var matchingCountries = countries.Select(x => new { name = x.Name, region = x.RegionName, value = x.Id });
                return Json(matchingCountries, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<Country>(), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}