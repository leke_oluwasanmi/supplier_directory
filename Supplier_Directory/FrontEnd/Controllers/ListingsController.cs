﻿using Frontend.Helpers;
using Frontend.Helpers.Authorise;
using Frontend.Models;
using GoogleMaps.LocationServices;
using SD.Core.Common;
using SD.Core.Common.Config;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace Frontend.Controllers
{
    public class ListingsController : BaseController
    {
        private IMongoRepository1 mongoRepository;
        public readonly CountryService _countryService;
        public readonly TagsService _tagsService;
        public readonly MemberService _memberService;
        public readonly ListingService _listingService;
        public readonly RegionService _regionService;
        public readonly MediaService _mediaService;
        private readonly SponsoredListingService _sponsoredListingService;
        private const int _pageSize = 10;
        public RegionHydrationSettings _regionHydrationSettings = new RegionHydrationSettings() { Countries = new CountryHydrationSettings() };
        public TagHydrationSettings _tagHydrationSettings = new TagHydrationSettings() { ParentTag = new TagHydrationSettings() };
        public ListingHydrationSettings _listingHydrationSettings = new ListingHydrationSettings()
        {
            PrimaryMedia = new MediaHydrationSettings(),
            RelatedMedia = new MediaHydrationSettings(),
            RelatedTags = new TagHydrationSettings(),
            Country = new CountryHydrationSettings()
        };
        private readonly SD.Core.Data.Model.HS.SponsoredListingHydrationSettings _sponsoredListingHydrationSettings =
            new SD.Core.Data.Model.HS.SponsoredListingHydrationSettings { RelatedTags = new SD.Core.Data.Model.HS.TagHydrationSettings(), RelatedListings = new SD.Core.Data.Model.HS.ListingHydrationSettings() };

        public ListingsController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _countryService = new CountryService(mongoRepository);
            _tagsService = new TagsService(mongoRepository);
            _listingService = new ListingService(mongoRepository);
            _regionService = new RegionService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _sponsoredListingService = new SponsoredListingService(mongoRepository);
        }

        public ListingsController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _countryService = new CountryService(_mongoRepository);
            _tagsService = new TagsService(_mongoRepository);
            _memberService = new MemberService(_mongoRepository);
            _listingService = new ListingService(_mongoRepository);
            _regionService = new RegionService(_mongoRepository);
            _mediaService = new MediaService(_mongoRepository);
            _sponsoredListingService = new SponsoredListingService(mongoRepository);
        }

        //
        // GET: /Listings/

        public ActionResult Index()
        {
            Results(null, null, null, null);
            return View();
        }

        public ActionResult Results(string keyword, string countryName, string categoryName, string categoryUrl, int page = 1, string submit = "search")
        {
            ListingResultViewModel model = new ListingResultViewModel();
            string countryId = "", categoryId = "";
            List<string> tags = new List<string>();

            #region SearchFilter
            if (submit == "search")
            {
                //get countries
                if (!string.IsNullOrEmpty(countryName))
                {
                    var country = _countryService.FindByName(countryName);
                    if (country != null)
                        countryId = country.Id;
                    model.SearchFilter.Location = country;
                }
                //get category
                if (!string.IsNullOrEmpty(categoryName))
                {
                    var tag = _tagsService.FindByDisplayText(categoryName);
                    if (tag != null)
                    {
                        categoryId = tag.Id;
                        tags = _tagsService.GetChildTags(categoryId);
                        tags.Add(categoryId);
                    }
                    model.SearchFilter.Category = tag;
                }

                model.SearchFilter.Keyword = keyword;

                //Brings back listings with the featured ones first.
                IList<SD.Core.Data.Model.Listing> listings = _listingService.Search(tags, countryId, keyword, 0, 0, true, true, _listingHydrationSettings, true);                
                model.Results = listings.ToList();
                var resultsPage = model.Results.ToPagedList(page, _pageSize);
                ViewBag.ResultsPage = resultsPage;

                //set the filter defaults
                model.BrowseFilter.Tags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
                model.BrowseFilter.Regions = _regionService.GetAllRegions(_regionHydrationSettings);
            }
            #endregion

            #region BrowseFilter    
            if (submit == "browse")
            {
                //check if a filter is selected
                if (Request.QueryString["t"] != null || Request.QueryString["c"] != null || Request.QueryString["r"] != null)
                {
                    string tagQuery = Request.QueryString["t"];
                    string countryQuery = Request.QueryString["c"];
                    string regionQuery = Request.QueryString["r"];

                    #region search selected countries
                    string[] searchCountriesQuery = string.IsNullOrEmpty(countryQuery) ? new string[] { } : countryQuery.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    string[] searchRegionsQuery = string.IsNullOrEmpty(regionQuery) ? new string[] { } : regionQuery.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    List<string> searchCountries = new List<string>();

                    if (searchRegionsQuery.ToList().Count > 0 || searchCountriesQuery.ToList().Count > 0)
                    {
                        foreach (var r in searchRegionsQuery.ToList())
                        {
                            var region = _regionService.FindBySlug(r, _regionHydrationSettings);
                            var browseCountries = region.Countries;
                            if (browseCountries.Count > 0)
                            {
                                var browseCountryStrings = browseCountries.Select(x => x.Id);
                                searchCountries.AddRange(browseCountryStrings);
                            }
                            //set filter countries
                            foreach (var browseCountry in browseCountries)
                            {
                                if (!searchCountriesQuery.ToList().Exists(x => x == browseCountry.UrlSlug))
                                    model.BrowseFilter.Countries.Add(browseCountry);
                            }
                            //set the selected countries
                            model.BrowseFilter.SelectedRegions.Add(region);
                        }
                        foreach (var c in searchCountriesQuery.ToList())
                        {
                            var country = _countryService.FindBySlug(c, null);

                            if (!searchCountriesQuery.ToList().Exists(x => x == country.UrlSlug))
                                model.BrowseFilter.Countries.Add(country);

                            if (c == searchCountriesQuery.Last())
                                searchCountries.Add(country.Id);

                            //set the selected countries
                            model.BrowseFilter.SelectedCountries.Add(country);
                        }
                    }
                    else
                        model.BrowseFilter.Regions = _regionService.GetAllRegions(_regionHydrationSettings);
                    #endregion
                    #region search selected tags
                    string[] searchtagsQuery = string.IsNullOrEmpty(tagQuery) ? new string[] { } : tagQuery.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    List<string> searchtags = new List<string>();
                    //if there is a tags query
                    if (searchtagsQuery.ToList().Count > 0)
                    {
                        foreach (var t in searchtagsQuery)
                        {
                            var tag = _tagsService.FindByUrlSlug(t);
                            var browseTags = _tagsService.GetChildTags(tag.Id, null);
                            //add child tags
                            if (browseTags.Count > 0)
                            {
                                var browsetagStrings = browseTags.Select(x => x.Id);
                                //set search tags
                                //only get the last tag. e.g business consultant in consultant+ business consultant                             
                                if (t == searchtagsQuery.Last())
                                    searchtags.AddRange(browsetagStrings);
                            }

                            //set search tags
                            //only get the last tag. e.g business consultant in consultant+ business consultant                             
                            if (t == searchtagsQuery.Last())
                                searchtags.Add(tag.Id);

                            //set filter tags
                            foreach (var browseTag in browseTags)
                            {
                                if (!searchtagsQuery.ToList().Exists(x => x == browseTag.UrlSlug))
                                    model.BrowseFilter.Tags.Add(browseTag);
                            }
                            //set the selected tags
                            model.BrowseFilter.SelectedTags.Add(tag);
                        }
                    }
                    else
                    {
                        //var defaultTags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
                        model.BrowseFilter.Tags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
                        //foreach (var parentTag in defaultTags)
                        //{
                        //    var browseTags = _tagsService.GetChildTags(parentTag.Id, null);
                        //    if (browseTags.Count > 0)
                        //        model.BrowseFilter.Tags.AddRange(browseTags);
                        //}
                    }
                    #endregion
                    
                    var results = _listingService.Search(searchtags.ToList(), searchCountries.ToList(), "", 0, 0, true, true, _listingHydrationSettings, true);
                    model.Results = results.ToList();
                    var resultsPage = model.Results.ToPagedList(page, _pageSize);
                    ViewBag.ResultsPage = resultsPage;

                    #region tag, country filters found
                    List<Tags> tagSearchResults = new List<Tags>();
                    List<Tags> childTagSearchResults = new List<Tags>();
                    List<Country> countrySearchResults = new List<Country>();
                    foreach (var result in model.Results)
                    {
                        //get tag results
                        foreach (var t in result.RelatedTags)
                        {
                            if (!tagSearchResults.Exists(x => x.UrlSlug == t.UrlSlug))
                            {
                                //add the parentTags
                                if (!string.IsNullOrEmpty(t.ParentId))
                                {
                                    var parent = _tagsService.FindById(t.ParentId);
                                    if (parent != null)
                                        tagSearchResults.Add(parent);
                                }
                            }

                            if (!childTagSearchResults.Exists(x => x.UrlSlug == t.UrlSlug))
                            {
                                childTagSearchResults.Add(t);
                            }
                        }
                        //get country results
                        if (!countrySearchResults.Exists(x => result.RelatedCountry.UrlSlug == x.UrlSlug))
                            countrySearchResults.Add(result.RelatedCountry);
                    }
                    #endregion

                    if (searchCountries.Count > 0 || searchtags.Count > 0)
                    {
                        if (searchtagsQuery.Count() > 0)
                        {
                            model.BrowseFilter.Tags.RemoveAll(x => !childTagSearchResults.Exists(y => y.UrlSlug == x.UrlSlug));
                        }
                        else
                            model.BrowseFilter.Tags.RemoveAll(x => !tagSearchResults.Exists(y => y.UrlSlug == x.UrlSlug));
                        model.BrowseFilter.Countries.RemoveAll(x => !countrySearchResults.Exists(y => y.UrlSlug == x.UrlSlug));
                    }
                    //order tags, regions, countries
                    model.BrowseFilter.Tags = model.BrowseFilter.Tags.OrderBy(x => x.DisplayText).ToList();
                    model.BrowseFilter.Countries = model.BrowseFilter.Countries.OrderBy(x => x.Name).ToList();


                }
            }
            #endregion
            
            return View("Index", model);
        }

        public ActionResult Detail(string urlSlug)
        {
            ListingViewModel model = new ListingViewModel();
            ListingHydrationSettings hydrationSettings = new ListingHydrationSettings() { PrimaryMedia = new MediaHydrationSettings(), RelatedMedia = new MediaHydrationSettings(), RelatedTags = new TagHydrationSettings(), Country = new CountryHydrationSettings() };
            model.Listing = _listingService.FindBySlug(urlSlug, hydrationSettings, null, true);

            #region get longitude latitude
            if (model.Listing.RelatedCountry != null && !string.IsNullOrEmpty(model.Listing.PostCode) && !string.IsNullOrEmpty(model.Listing.County))
            {                
                var locationService = new GoogleLocationService();
                var addressData = new AddressData();
                addressData.City = model.Listing.County;
                addressData.Country = model.Listing.RelatedCountry.Name;
                addressData.Zip = model.Listing.PostCode;

                var point = locationService.GetLatLongFromAddress(addressData);
                model.Longitude = point.Longitude;
                model.Latitude = point.Latitude;
            }
            
            #endregion

            return View(model);
        }

        public ActionResult Category(string keyword, string countryName, string categoryName, string categoryUrl, int page = 1, string submit = "search")
        {
            ListingResultViewModel model = new ListingResultViewModel();
            string countryId = "", categoryId = "";
            List<string> tags = new List<string>();

            #region SearchFilter
            if (submit == "search")
            {
                //get countries
                if (!string.IsNullOrEmpty(countryName))
                {
                    var country = _countryService.FindByName(countryName);
                    if (country != null)
                        countryId = country.Id;
                    model.SearchFilter.Location = country;
                }
                //get category
                if (!string.IsNullOrEmpty(categoryName))
                {
                    var tag = _tagsService.FindByDisplayText(categoryName);
                    if (tag != null)
                    {
                        categoryId = tag.Id;
                        tags = _tagsService.GetChildTags(categoryId);
                        tags.Add(categoryId);
                    }
                    model.SearchFilter.Category = tag;
                }

                model.SearchFilter.Keyword = keyword;

                //Brings back listings with the featured ones first.
                IList<SD.Core.Data.Model.Listing> listings = _listingService.Search(tags, countryId, keyword, 0, 0, true, true, _listingHydrationSettings, true);
                listings.ToList().RemoveAll(x => x.ListingType == SD.Core.Common.Constants.ListingType.Standard);
                model.Results = listings.ToList();
                var resultsPage = model.Results.ToPagedList(page, _pageSize);
                ViewBag.ResultsPage = resultsPage;

                //set the filter defaults
                model.BrowseFilter.Tags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
                model.BrowseFilter.Regions = _regionService.GetAllRegions(_regionHydrationSettings);
            }
            #endregion

            #region BrowseFilter    
            if (submit == "browse")
            {
                //check if a filter is selected
                if (Request.QueryString["t"] != null || Request.QueryString["c"] != null || Request.QueryString["r"] != null || !string.IsNullOrEmpty(categoryUrl))
                {
                    string tagQuery = "";
                    if (!string.IsNullOrEmpty(categoryUrl))
                    {
                        tagQuery = categoryUrl;
                    }
                    else
                    {
                        tagQuery = Request.QueryString["t"];
                    }
                    string countryQuery = Request.QueryString["c"];
                    string regionQuery = Request.QueryString["r"];

                    #region search selected countries
                    string[] searchCountriesQuery = string.IsNullOrEmpty(countryQuery) ? new string[] { } : countryQuery.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    string[] searchRegionsQuery = string.IsNullOrEmpty(regionQuery) ? new string[] { } : regionQuery.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    List<string> searchCountries = new List<string>();

                    if (searchRegionsQuery.ToList().Count > 0 || searchCountriesQuery.ToList().Count > 0)
                    {
                        foreach (var r in searchRegionsQuery.ToList())
                        {
                            var region = _regionService.FindBySlug(r, _regionHydrationSettings);
                            var browseCountries = region.Countries;
                            if (browseCountries.Count > 0)
                            {
                                var browseCountryStrings = browseCountries.Select(x => x.Id);
                                searchCountries.AddRange(browseCountryStrings);
                            }
                            //set filter countries
                            foreach (var browseCountry in browseCountries)
                            {
                                if (!searchCountriesQuery.ToList().Exists(x => x == browseCountry.UrlSlug))
                                    model.BrowseFilter.Countries.Add(browseCountry);
                            }
                            //set the selected countries
                            model.BrowseFilter.SelectedRegions.Add(region);
                        }
                        foreach (var c in searchCountriesQuery.ToList())
                        {
                            var country = _countryService.FindBySlug(c, null);

                            if (!searchCountriesQuery.ToList().Exists(x => x == country.UrlSlug))
                                model.BrowseFilter.Countries.Add(country);

                            if (c == searchCountriesQuery.Last())
                                searchCountries.Add(country.Id);

                            //set the selected countries
                            model.BrowseFilter.SelectedCountries.Add(country);
                        }
                    }
                    else
                        model.BrowseFilter.Regions = _regionService.GetAllRegions(_regionHydrationSettings);
                    #endregion
                    #region search selected tags
                    string[] searchtagsQuery = string.IsNullOrEmpty(tagQuery) ? new string[] { } : tagQuery.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    List<string> searchtags = new List<string>();
                    //if there is a tags query
                    if (searchtagsQuery.ToList().Count > 0)
                    {
                        foreach (var t in searchtagsQuery)
                        {
                            var tag = _tagsService.FindByUrlSlug(t);
                            var browseTags = _tagsService.GetChildTags(tag.Id, null);
                            //add child tags
                            if (browseTags.Count > 0)
                            {
                                var browsetagStrings = browseTags.Select(x => x.Id);
                                //set search tags
                                //only get the last tag. e.g business consultant in consultant+ business consultant                             
                                if (t == searchtagsQuery.Last())
                                    searchtags.AddRange(browsetagStrings);
                            }

                            //set search tags
                            //only get the last tag. e.g business consultant in consultant+ business consultant                             
                            if (t == searchtagsQuery.Last())
                                searchtags.Add(tag.Id);

                            //set filter tags
                            foreach (var browseTag in browseTags)
                            {
                                if (!searchtagsQuery.ToList().Exists(x => x == browseTag.UrlSlug))
                                    model.BrowseFilter.Tags.Add(browseTag);
                            }
                            //set the selected tags
                            model.BrowseFilter.SelectedTags.Add(tag);
                        }
                    }
                    else
                    {
                        //var defaultTags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
                        model.BrowseFilter.Tags = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).ToList();
                        //foreach (var parentTag in defaultTags)
                        //{
                        //    var browseTags = _tagsService.GetChildTags(parentTag.Id, null);
                        //    if (browseTags.Count > 0)
                        //        model.BrowseFilter.Tags.AddRange(browseTags);
                        //}
                    }
                    #endregion
                    //if (!string.IsNullOrEmpty(regionQuery))
                    //{
                    //    SD.Core.Data.Model.Region region = _regionService.FindBySlug(regionQuery, _regionHydrationSettings);
                    //    List<Country> regionCountries = _countryService.FindAllCountriesByRegion(region.Id, null);
                    //    searchCountries = regionCountries.Select(x => x.Id).ToList();
                    //}
                    //else


                    var results = _listingService.Search(searchtags.ToList(), searchCountries.ToList(), "", 0, 0, true, true, _listingHydrationSettings, true);
                    model.Results = results.ToList();
                    var resultsPage = model.Results.ToPagedList(page, _pageSize);
                    ViewBag.ResultsPage = resultsPage;

                    #region tag, country filters found
                    List<Tags> tagSearchResults = new List<Tags>();
                    List<Tags> childTagSearchResults = new List<Tags>();
                    List<Country> countrySearchResults = new List<Country>();
                    foreach (var result in model.Results)
                    {
                        //get tag results
                        foreach (var t in result.RelatedTags)
                        {
                            if (!tagSearchResults.Exists(x => x.UrlSlug == t.UrlSlug))
                            {
                                //add the parentTags
                                if (!string.IsNullOrEmpty(t.ParentId))
                                {
                                    var parent = _tagsService.FindById(t.ParentId);
                                    if (parent != null)
                                        tagSearchResults.Add(parent);
                                }
                            }

                            if (!childTagSearchResults.Exists(x => x.UrlSlug == t.UrlSlug))
                            {
                                childTagSearchResults.Add(t);
                            }
                        }
                        //get country results
                        if (!countrySearchResults.Exists(x => result.RelatedCountry.UrlSlug == x.UrlSlug))
                            countrySearchResults.Add(result.RelatedCountry);
                    }
                    #endregion

                    if (searchCountries.Count > 0 || searchtags.Count > 0)
                    {
                        if (searchtagsQuery.Count() > 0)
                        {
                            model.BrowseFilter.Tags.RemoveAll(x => !childTagSearchResults.Exists(y => y.UrlSlug == x.UrlSlug));
                        }
                        else
                            model.BrowseFilter.Tags.RemoveAll(x => !tagSearchResults.Exists(y => y.UrlSlug == x.UrlSlug));
                        model.BrowseFilter.Countries.RemoveAll(x => !countrySearchResults.Exists(y => y.UrlSlug == x.UrlSlug));
                    }
                    //order tags, regions, countries
                    model.BrowseFilter.Tags = model.BrowseFilter.Tags.OrderBy(x => x.DisplayText).ToList();
                    model.BrowseFilter.Countries = model.BrowseFilter.Countries.OrderBy(x => x.Name).ToList();
                    
                    if(model.BrowseFilter.SelectedTags.Count > 0)
                    {                        
                        var sponsoredListing = _sponsoredListingService.FindByTagId(model.BrowseFilter.SelectedTags[0].Id, _sponsoredListingHydrationSettings);
                        if(sponsoredListing.Status == PublicationStatus.Published)
                        {
                            model.SponsoredListing = sponsoredListing;
                            ViewBag.SelectedTagName = model.BrowseFilter.SelectedTags[0].DisplayText;
                        }
                    }

                }
            }
            #endregion            

            return View("CategoryLanding", model);
        }


        public ActionResult ListWithUs()
        {            
            return View();
        }

        [CustomAuthorise]
        public ActionResult MyListings()
        {
            var currentUser = MembershipHelper.GetActiveUserId;

            var hydrationSettings = new ListingHydrationSettings() { 
                PrimaryMedia = new MediaHydrationSettings(), 
                RelatedMedia = new MediaHydrationSettings(), 
                RelatedTags = new TagHydrationSettings() ,
                 Country = new CountryHydrationSettings(),
                  Member = new MemberHydrationSettings()
            };

            var listing = _listingService.GetListingByMember(currentUser, hydrationSettings);

            return View(listing);
        }


        [CustomAuthorise]
        public ActionResult Post()
        {
            return FormView(new Listing());
        }

        [CustomAuthorise]
        [HttpPost]
        public ActionResult Post(FormCollection collections, Listing listing, string[] SelectedTags, string[] mediaFiles, string submit, HttpPostedFileBase CompanyLogo)
        {
            try
            {               
                if (ModelState.IsValidField("CompanyName")
                    && ModelState.IsValidField("Body"))
                {
                    if (CompanyLogo != null && CompanyLogo.ContentLength > 0)
                    {                       
                        if (!FileUtilities.ExtensionIsRecognised(Path.GetExtension(CompanyLogo.FileName)))
                        {
                            ModelState.AddModelError("CompanyLogo", "The extension of the file you uploaded has not been recognised");
                            return FormView(listing);
                        }
                        Media media;
                        SaveFile(CompanyLogo, out media);
                        listing.PrimaryMedia = media;                        
                    }

                    if (SelectedTags != null && SelectedTags.Count() > 0)
                    {
                        listing.RelatedTags = _tagsService.FindByIds(SelectedTags);
                    }

                    if (mediaFiles != null && mediaFiles.Count() > 0)
                    {
                        listing.RelatedMedia = _mediaService.FindByIds(null, mediaFiles);
                    }

                    //draft or submit
                    if (submit.ToLower() == "draft")
                    {
                        listing.Status = PublicationStatus.Draft;                        
                    }
                    else if (submit.ToLower() == "submit" || submit.ToLower() == "upload")
                    {
                        listing.Status = PublicationStatus.UnPublished; 
                    }                    
                    listing.MemberId = MembershipHelper.GetActiveUserId;
                    listing.Member = MembershipHelper.GetActiveUser();
                    listing.IsModerated = false;
                    //listing.Status = SD.Core.Common.Constants.PublicationStatus.UnPublished;
                    listing.CreatedOn = listing.UpdatedOn = DateTime.UtcNow;
                   
                    //set country
                    if (!string.IsNullOrEmpty(listing.CountryId))
                    {
                        var country = _countryService.FindById(listing.CountryId);
                        if (country != null)
                        {
                            listing.Country = country.Name;
                            listing.RelatedCountry = country;
                        }
                    }
                    //save
                    var recordId = _listingService.Save(listing);

                    if (submit.ToLower() == "submit")
                    {
                        //return RedirectToAction("Preview", new { id = recordId });
                        return RedirectToAction("Confirmation");
                    }                    
                    else
                    {
                        return RedirectToAction("Edit", new { id = recordId });
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please check for error and try again");
                }
            }
            catch (Exception ex)
            {
                //TODO : create error log and log all exceptions
                ModelState.AddModelError("", ex.Message);
            }

            return FormView(listing);
        }

        public ActionResult FormView(Listing listing)
        {
            var formmodel = new ListingFormModel()
            {
                Listing = listing
            };
            formmodel.Listing.MemberId = MembershipHelper.GetActiveUserId;

            formmodel.MainCategoryList = _tagsService.All().Where(x => string.IsNullOrEmpty(x.ParentId)).Select(
                y => new SelectListItem
                {
                    Value = y.Id,
                    Text = y.DisplayText
                }
            ).ToList();

            formmodel.SubCategoryList = _tagsService.All().Where(x => !string.IsNullOrEmpty(x.ParentId)).Select(
                y => new SelectListItem
                {
                    Value = y.Id,
                    Text = y.DisplayText
                }
            ).ToList();

            ViewBag.Countries = _countryService.GetCountries().OrderBy(a => a.Name).Select(x => new SelectListItem
            {
                Value = x.Id,
                Text = x.Name,
                //Selected = (listing.CountryId == x.Id)
            }).ToList();

            return View("Post", formmodel);
        }

        [CustomAuthorise]
        public ActionResult Edit(string id)
        {
            var listing = _listingService.FindById(id, _listingHydrationSettings);
            if (listing == null)
            {
                return RedirectToAction("MyListings");
            }
            return FormView(listing);
        }

        [CustomAuthorise]
        public ActionResult Confirmation()
        {
            return View();
        }

        [CustomAuthorise]
        public ActionResult Preview(string id)
        {
            var model = _listingService.FindById(id, _listingHydrationSettings);

            return View(model);
        }

        [CustomAuthorise]
        public ActionResult SavedSearch()
        {
            //get saved searches
            return View();
        }

        public JsonResult GetSubTagLists(String selectedId)
        {            
            var result = _tagsService.All().Where(x => x.ParentId == selectedId).Select(y => new
            {
                y.Id,
                y.DisplayText
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);            
        }
        
        [HttpPost]
        public ActionResult UploadFiles()
        {
            if (Request.Files.Count > 0)
            {
                var isFileSaved = true;
                string Msg = "";
                List<string> uploadedFiles = new List<string>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase httpPostedfileBase = Request.Files[file];
                    try
                    {
                        //rename file
                        var fileName = Path.GetFileName(httpPostedfileBase.FileName);
                        if (!FileUtilities.ExtensionIsRecognised(Path.GetExtension(fileName)))
                        {
                            Msg = "The extension of the file you uploaded has not been recognised"; // +ex.Message;
                            isFileSaved = false;
                        }
                        else
                        {
                            Media media;
                            SaveFile(httpPostedfileBase, out media);
                            uploadedFiles.Add(media.Id);
                            Msg = "File(s) Saved successfully";
                        }
                    }
                    catch (Exception ex)
                    {
                        Msg = "Error in Saving File. Try Again"; // +ex.Message;
                        isFileSaved = false;
                    }
                }
                return (isFileSaved == true ? Json(new { Message = Msg, Files = uploadedFiles }, JsonRequestBehavior.AllowGet) : Json(new { Message = Msg, Files = uploadedFiles }, JsonRequestBehavior.AllowGet));
            }
            else
            {
                return View();
            }
        }

        public ActionResult DeleteMedia(string mediaId)
        {
            try
            {
                var media = _mediaService.FindByIds(null, new string[] { mediaId });
                if (media.Count > 0)
                {
                    if (FileUtilities.FormatIsInGroup(media[0].Format, FileFormatGroup.Image))
                    {
                        var profilesToDelete = EnumUtils.ToList<MediaProfile>();
                        foreach (var profile in profilesToDelete)
                        {
                            var originalProfileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(profile);
                            if (MediaFileUtilities.MediaExists(originalProfileConfig, media[0].FileName))
                                MediaFileUtilities.DeleteMedia(originalProfileConfig, media[0].FileName);
                        }
                    }
                    if (FileUtilities.FormatIsInGroup(media[0].Format, FileFormatGroup.Document))
                    {
                        if (MediaFileUtilities.DocumentExists(media[0].FileName))
                            MediaFileUtilities.DeleteDocument(media[0].FileName);
                    }
                    
                    _mediaService.DeleteById(mediaId);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Message = "Error Deleting File" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Message = "File has been deleted" }, JsonRequestBehavior.AllowGet);
        }

        private FileFormat getFileFormat(string format)
        {
            string[] ext = format.Split('/');

            return (FileFormat)System.Enum.Parse(typeof(FileFormat), ext[1], true);
        }

        private void autoCrop(Media media)
        {
            // First try to find the currently selected media items original file on disk
            var profileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = MediaFileUtilities.GetImage(profileConfig, media.FileName);

            // If we can't find it then throw an exceptions saying so. From the UI POV, you shouldnt be able to save an image profile before uploading the original
            if (originalImage == null) throw new Exception("Couldn't find the original image on disk. Please upload the original image before auto-cropping it");

            foreach (MediaRatioElement ratioConfig in SDConfig.Instance.MediaRatios)
            {
                var targetWidth = originalImage.Width;
                var targetHeight = originalImage.Height;

                if (((decimal)originalImage.Width / (decimal)originalImage.Height) > ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetWidth = (int)Math.Floor((((decimal)ratioConfig.Width / (decimal)ratioConfig.Height) * originalImage.Height));
                else if (((decimal)originalImage.Width / (decimal)originalImage.Height) <= ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetHeight = (int)Math.Floor((((decimal)ratioConfig.Height / (decimal)ratioConfig.Width) * originalImage.Width));

                // Get the crop coordinates from the target width and height values above
                var cropX1 = (int)Math.Round((((decimal)originalImage.Width - (decimal)targetWidth) / 2), 0, MidpointRounding.AwayFromZero);
                var cropY1 = (int)Math.Round((((decimal)originalImage.Height - (decimal)targetHeight) / 2), 0, MidpointRounding.AwayFromZero);
                var cropX2 = cropX1 + targetWidth;
                var cropY2 = cropY1 + targetHeight;

                crop(media, originalImage, ratioConfig.RatioName, cropX1, cropY1, cropX2, cropY2);
            }
        }
        private void crop(Media media, Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
        {
            // For each profile using the supplied ratio
            var profileConfigs = SDConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledMedia = MediaUtils.CropImage(originalImage, largestProfile.Width, largestProfile.Height, cropX1, cropX2, false, cropY1, cropY2);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
            {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledMedia, Server.MapPath(MediaHelper.GetMediaPath(profileConfig.ProfileName, media)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                MediaFileUtilities.SaveImage(profiledMedia, profileConfig, media.FileName);//, media.Title, media.OriginalFileName);
            }
            // Enable this ratio for the selected media item if it isnt already
            if (!media.AvailableRatios.Contains(ratio))
            {
                media.AvailableRatios.Add(ratio);
            }
        }

        private void SaveFile(HttpPostedFileBase file, out Media media)
        {
            var fileName = Path.GetFileName(file.FileName);
            var guid = Guid.NewGuid().ToString("N");

            var rename = guid.ToString() + "_" + fileName;
            rename.Replace(" ", "_");
            //save media
            media = new Media();
            media.OriginalFileName = fileName;
            media.FileName = rename;
            // Get configuration for the Original media profile
            var originalProfileConfig = SDConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            // Check if this media item already has an original file on disk
            if (!string.IsNullOrWhiteSpace(media.FileName))
            {
                if (MediaFileUtilities.MediaExists(originalProfileConfig, media.FileName))
                    MediaFileUtilities.DeleteMedia(originalProfileConfig, media.FileName);
            }
            media.Format = FileUtilities.FormatFromFilename(media.OriginalFileName);
            media.CreatedOn = DateTime.Now;

            var stream = Request.InputStream;
            byte[] buffer = null;
            stream = file.InputStream;
            buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);

            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
            {
                MediaFileUtilities.SaveImage(stream, originalProfileConfig, media.FileName);//, media.Title, originalFilename);
            }
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
            {
                MediaFileUtilities.SaveDocument(stream, media.FileName);
            }
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
            {
                var originalImage = MediaFileUtilities.GetImage(originalProfileConfig, media.FileName);
                media.SourceWidth = originalImage.Width;
                media.SourceHeight = originalImage.Height;
            }
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                autoCrop(media);
            var mediaService = new SD.Core.Services.CMS.MediaService(mongoRepository);
            var mediaId = mediaService.Save(media);
            media.Id = mediaId;
        }

    }
}