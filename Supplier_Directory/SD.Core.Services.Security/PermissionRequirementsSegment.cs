﻿using System.Collections.Generic;
using System.Linq;
using SD.Core.Common.Constants;

namespace SD.Core.Services.Security
{
    internal class PermissionRequirementsSegment
    {
        private SegmentType _type;
        private IList<Permission> _permissions;

        internal PermissionRequirementsSegment(SegmentType type, params Permission[] permissions)
        {
            _type = type;
            _permissions = permissions;
        }

        internal bool Check(IList<Permission> userPermissions)
        {
            if (userPermissions == null) return true;
            return _type == SegmentType.Or || _type == SegmentType.AndAny ?
                _permissions.Any(x => userPermissions.Contains(x)) :
                _permissions.All(x => userPermissions.Contains(x));
        }

        internal IList<Permission> Permissions
        {
            get
            {
                return _permissions;
            }
        }

        internal SegmentType Type { get { return _type; } }
    }
    internal enum SegmentType
    {
        And,
        AndAny,
        Or,
        OrAll
    }
}
