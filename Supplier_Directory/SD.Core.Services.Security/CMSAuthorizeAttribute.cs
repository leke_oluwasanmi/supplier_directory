﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SD.Core.Common.Interfaces;

namespace SD.Core.Services.Security
{
    public class CMSAuthorizeAttribute : AuthorizeAttribute
    {
        private static IDictionary<string, PermissionRequirementsBuilder> _requirementsCache = new Dictionary<string, PermissionRequirementsBuilder>();
        private static IMembershipProvider _membershipProvider;
        private static object _requirementsLockObj = new object();
        private static object _membershipLockObj = new object();

        public new string Roles { get { throw new NotSupportedException(); } set { throw new NotSupportedException(); } } // Hide roles

        public new object TypeId { get { throw new NotSupportedException(); } }

        public new string Users { get { throw new NotSupportedException(); } set { throw new NotSupportedException(); } }

        private PermissionRequirementsBuilder _requirements;

        /// <summary>
        ///
        /// </summary>
        /// <param name="permissionRequirements">
        /// <para>A string defining permissions requirements for this method. A complex example</para>
        /// <example>
        ///  a & b | c & d & e & f | g | h
        ///  t   f   t   t   t   t   f   f   == t
        ///  t   f   f   t   t   t   f   f   == f
        ///  ----------------------------------------
        ///  a & ( b | c & d & e & f | g | h )
        ///  t     f   t   t   t   t   f   f   == t
        ///  t     f   f   t   t   t   f   f   == f
        /// </example>
        /// <para>This is translated into: .OrAll(a, b).OrAll(c, d, e, f).OrAll(g).OrAll(h). </para>
        /// <para>Convention increases performance via PermissionRequirmentsBuilder caching which uses the
        /// permissionRequirements string as key. Make sure there is a single space between lower-case operators.</para>
        /// <para>Permission enum values must be PascalCase as per the enum labels.</para>
        /// TODO: There are some combinations that are not possible with this syntax. Grouping might be needed at some point.
        /// </param>
        public CMSAuthorizeAttribute(string permissionRequirements)
        {
            // Check cache for existing builder object
            if (!_requirementsCache.ContainsKey(permissionRequirements))
            {
                _requirements = new PermissionRequirementsBuilder();
                // - Remove spaces
                var permissionRequirementsNoWhitespace = permissionRequirements.Replace(" ", string.Empty);
                // - Break up by ors
                var ors = permissionRequirementsNoWhitespace.Split('|');
                // - Iterate through ors
                foreach (var or in ors)
                {
                    // -- break each into ands
                    var andPermissions = or.Split('&');
                    // -- turn andPermissions strings into permission enum values
                    var permissions = PermissionUtilities.PermissionsFromString(andPermissions);
                    // -- add to permissions requirements builder
                    _requirements.OrAll(permissions);
                }
                // Thread safely add the builder
                lock (_requirementsLockObj) _requirementsCache.Add(permissionRequirements, _requirements);
            }
            else
            {
                _requirements = _requirementsCache[permissionRequirements];
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
#if DEBUG
            if (_membershipProvider == null) throw new Exception("Membership provider must be set before authorization checks can be made");
#endif
            return base.AuthorizeCore(httpContext) && _requirements.Check(_membershipProvider.ActiveUser);
        }

        public static void SetMembershipProvider(IMembershipProvider membershipProvider)
        {
            lock (_membershipLockObj) _membershipProvider = membershipProvider;
        }

        public static void ClearRequirementsCache()
        {
            lock (_requirementsLockObj) _requirementsCache.Clear();
        }
    }
}