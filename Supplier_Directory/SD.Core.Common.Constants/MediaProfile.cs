﻿using System.ComponentModel;

namespace SD.Core.Common.Constants
{
    /// <summary>
    /// Describes a . Dimensions and other associated configuration are defined in <see cref="HN.Core.Common.Config.HNConfig"/>.
    /// </summary>
    public enum MediaProfile
    {
        [Description("Original")]
        Original,

        [Description("600x300")]
        size_600x300,

        [Description("300x200")]
        size_300x200,

        [Description("240x160")]
        size_240x160,

        [Description("100x67")]
        size_100x67,
    }
}