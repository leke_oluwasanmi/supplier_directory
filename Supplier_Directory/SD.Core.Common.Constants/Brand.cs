﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Common.Constants
{
    public enum Brand
    {
        vw,
        seat,
        skoda,
        cv,
        vwcv,
        audi,
        pb,
        tps
    }
}
