﻿namespace SD.Core.Common.Constants
{
    public enum ArticleType
        {
        article,
        video,
        document,
        page,
        group,
        news,
        bulletin
        }
}