﻿namespace SD.Core.Common.Constants
{
    public enum VersionRecordType
    {
        Insert,
        Update,
        Delete,
    }
}