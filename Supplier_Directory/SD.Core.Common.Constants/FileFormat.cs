﻿namespace SD.Core.Common.Constants
{
    public enum FileFormat
    {
        JPEG,
        PNG,
        Bitmap,
        GIF,
        PDF,
        AVI,
        WMV,
        FLV,
        MP3,
        MPG,
        SWF,
        MOV,
        WMA,
        MP4,
        RAM,
        Excel,
        Word,
        Text,
        PPT,
        PPTX
    }

    /// <summary>
    /// TODO: Delete
    /// </summary>
    public enum DocumentFileFormat
    {
        PDF,
        Word
    }
}