﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Common.Constants
{
    public enum HistoryGroup
    {
         Today,
        Yesterday,
        LastWeek,
        LastMonth,
        Older
    }
}
