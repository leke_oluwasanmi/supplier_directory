﻿
using System.ComponentModel.DataAnnotations;

namespace SD.Core.Common.Constants
{
    public enum Page
    {
        [Display(Name = "Home")]
        Home,
        [Display(Name = "About Us")]
        AboutUs,
        [Display(Name = "ListWithUs")]
        ListWithUs
    }
}