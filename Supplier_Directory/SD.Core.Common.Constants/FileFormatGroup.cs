﻿namespace SD.Core.Common.Constants
{
    public enum FileFormatGroup
    {
        Image,
        Video,
        Audio,
        Document,
        Resource
    }
}