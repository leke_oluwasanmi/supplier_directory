﻿namespace SD.Core.Common.Constants
{
    public enum PublicationStatus
    {
        Deleted,
        UnPublished,
        Published,
        Draft
    }
}