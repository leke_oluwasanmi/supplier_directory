﻿namespace SD.Core.Common.Constants
{
    public enum SortOrder
    {
        CreatedOn,
        UpdatedOn,
        LiveFrom,
        VersionDate,
    }
}