﻿
using System.ComponentModel.DataAnnotations;

namespace SD.Core.Common.Constants
{
    public enum ListingType
    {
        [Display(Name = "Standard")]
        Standard,
        [Display(Name = "Premium Plus")]
        PremiumPlus,
        [Display(Name = "Premium")]
        Premium
    }    
}