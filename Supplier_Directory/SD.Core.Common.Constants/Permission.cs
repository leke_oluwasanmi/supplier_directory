﻿namespace SD.Core.Common.Constants
{
    public enum Permission
    {
        UseCMS, //0
        ViewUsers,//1
        EditUsers, //1,2
        ViewArticles,
        EditArticles,
        ViewListings,
        EditListings, //3,4
        ViewMedia,
        EditMedia, //5,6

        ViewMediaGalleries,
        EditMediaGalleries, //7,8

        ViewDocuments,
        EditDocuments, //9,10

        AdminFunctions,
        ViewMembers,
        EditMembers,
    }
}