﻿using System;
using System.Collections.Generic;
using System.Linq;
using SD.Core.Common.Constants;
using SD.Core.Common.Extensions.Strings;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core;

namespace SD.Core.Services.CMS
{
    public class MediaService : Core.MediaService
    {
        public MediaService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
            
        }

        private const int MaxUniqueUrlSlugChecks = 100;
        private const int MaxFeaturedMedia = 3;
        

        /// <summary>
        /// Model.Media contains bi-directional relationships which need to be persisted
        /// (i.e. a discipline has primary and related media and those media items are related to that discipline item)
        /// Relationship enforcement must occur after the media item is saved because, in the case of an insert, it won't have an id
        /// which is required to update the entities in which the relationships are defined.
        /// The enforcement process for each type of relationship must do the following:
        /// - Remove relationships that no longer exist
        /// - Add new relationships that have been created
        ///
        /// TBH this smells quite bad.
        /// </summary>
        /// <param name="viewModel"></param>


        

        #region Version History
            

        #endregion Version History

        
    }
}