﻿using AutoMapper;
using System;
using System.Linq;

namespace SD.Core.Services.Helpers
{
    public class EntityMapper
    {
        #region By Owain Wrags

        public static T Map<T>(params object[] sources) where T : class
        {
            if (!sources.Any())
            {
                return default(T);
            }

            var initialSource = sources[0];

            var mappingResult = Map<T>(initialSource);

            // Now map the remaining source objects
            if (sources.Count() > 1)
            {
                Map(mappingResult, sources.Skip(1).ToArray());
            }

            return mappingResult;
        }

        private static void Map(object destination, params object[] sources)
        {
            if (!sources.Any())
            {
                return;
            }

            var destinationType = destination.GetType();

            foreach (var source in sources)
            {
                if (source == null) continue;
                var sourceType = source.GetType();
                Mapper.Map(source, destination, sourceType, destinationType);
            }
        }

        private static T Map<T>(object source) where T : class
        {
            var destinationType = typeof(T);
            var sourceType = source.GetType();
            var mappingResult = Mapper.Map(source, sourceType, destinationType);
            return mappingResult as T;
        }

        #endregion By Owain Wrags

        public static void InitialiseMappings()
        {           
            TagMappings();           
            UserMappings();
            MediaMappings();            
            ListingMapping();
            MemberMappings();            
            PageMapping();
            CountryMapping();
            RegionMapping();
            FeaturedListingMapping();
            SponsoredListingMapping();
        }
        private static void FeaturedListingMapping()
        {
            Mapper.CreateMap<Data.Model.FeaturedListing, Data.Model.FeaturedListing>()
                           .ForMember(x => x.Id, opt => opt.Ignore())
                           .ForMember(dest => dest.RelatedListing, opt => opt.Ignore())
                           .ForMember(dest => dest.PageName, opt => opt.Ignore())
                           .ForMember(dest => dest.Order, opt => opt.Ignore())
            .ForMember(dest => dest.Status, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.FeaturedListing, Data.Model.FeaturedListing>();

            Mapper.CreateMap<Data.Model.FeaturedListing, Data.DTOs.FeaturedListing>()
               .ForMember(x => x.CreatedById,
                   opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
              .ForMember(x => x.ListingId, opt => opt.MapFrom(origin => origin.RelatedListing))
               .ForMember(x => x.UpdatedById,
                   opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.FeaturedListing, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        private static void SponsoredListingMapping()
        {
            Mapper.CreateMap<Data.Model.SponsoredListing, Data.Model.SponsoredListing>()
                           .ForMember(x => x.Id, opt => opt.Ignore())
                           .ForMember(dest => dest.RelatedTag, opt => opt.Ignore())
                           .ForMember(dest => dest.RelatedListing, opt => opt.Ignore())
                           .ForMember(dest => dest.Status, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.SponsoredListing, Data.Model.SponsoredListing>();

            Mapper.CreateMap<Data.Model.SponsoredListing, Data.DTOs.SponsoredListing>()
               .ForMember(x => x.CreatedById,
                   opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
              .ForMember(x => x.TagId, opt => opt.MapFrom(origin => origin.RelatedTag))
              .ForMember(x => x.ListingId, opt => opt.MapFrom(origin => origin.RelatedListing))
               .ForMember(x => x.UpdatedById,
                   opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.SponsoredListing, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        private static void RegionMapping()
        {
            Mapper.CreateMap<Data.Model.Region, Data.Model.Region>().ForMember(x => x.Id, opt => opt.Ignore());
            Mapper.CreateMap<Data.DTOs.Region, Data.Model.Region>();
            Mapper.CreateMap<Data.Model.Region, Data.DTOs.Region>();
            Mapper.CreateMap<SD.Core.Data.Model.Region, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void CountryMapping()
        {
            Mapper.CreateMap<Data.Model.Country, Data.Model.Country>().ForMember(x => x.Id, opt => opt.Ignore());
            Mapper.CreateMap<Data.DTOs.Country, Data.Model.Country>();
            Mapper.CreateMap<Data.Model.Country, Data.DTOs.Country>();
            Mapper.CreateMap<SD.Core.Data.Model.Country, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        
        private static void TagMappings()
        {
            Mapper.CreateMap<Data.Model.Tags, Data.Model.Tags>()
                 .ForMember(x => x.Id, opt => opt.Ignore())
                 .ForMember(dest => dest.ParentTag, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Tags, Data.Model.Tags>();
            Mapper.CreateMap<Data.Model.Tags, Data.DTOs.Tags>()
                .ForMember(x => x.ParentId, opt => opt.MapFrom(origin => origin.ParentTag));
            Mapper.CreateMap<SD.Core.Data.Model.Tags, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
      
        private static void MediaMappings()
        {
            Mapper.CreateMap<Data.Model.Media, Data.Model.Media>()
                .ForMember(x => x.Id, opt => opt.Ignore())                
                .ForMember(dest => dest.AvailableRatios, opt => opt.Ignore())
                .ForMember(dest => dest.FileName, opt => opt.Ignore())
                .ForMember(dest => dest.Format, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore());
            Mapper.CreateMap<Data.DTOs.Media, Data.Model.Media>();
            Mapper.CreateMap<Data.Model.Media, Data.DTOs.Media>()
                .ForMember(x => x.CreatedById, opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
                .ForMember(x => x.UpdatedById, opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));
            Mapper.CreateMap<Data.Model.Media, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void UserMappings()
        {
            Mapper.CreateMap<Data.Model.User, Data.Model.User>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedById, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedById, opt => opt.Ignore())                
                .ForMember(dest => dest.Password, opt => opt.Ignore());
            Mapper.CreateMap<Data.Model.User, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void ListingMapping()
        {
            Mapper.CreateMap<Data.Model.Listing, Data.Model.Listing>()
                           .ForMember(x => x.Id, opt => opt.Ignore())
            .ForMember(dest => dest.MemberId, opt => opt.Ignore())
            .ForMember(dest => dest.RelatedTags, opt => opt.Ignore())
            .ForMember(dest => dest.ListingType, opt => opt.Ignore())
            .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
            .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
            .ForMember(dest => dest.RelatedMedia, opt => opt.Ignore())
            .ForMember(dest => dest.PrimaryMedia, opt => opt.Ignore())
            .ForMember(dest => dest.RelatedCountry, opt => opt.Ignore())
            .ForMember(dest => dest.Status, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Listing, Data.Model.Listing>();

            Mapper.CreateMap<Data.Model.Listing, Data.DTOs.Listing>()

               .ForMember(x => x.PrimaryMediaId, opt => opt.MapFrom(origin => origin.PrimaryMedia))
               .ForMember(x => x.MemberId, opt => opt.MapFrom(origin => origin.Member))
               .ForMember(x => x.CountryId, opt => opt.MapFrom(origin => origin.RelatedCountry))
               .ForMember(x => x.RelatedMediaIds, opt => opt.MapFrom(origin => origin.RelatedMedia))
               .ForMember(x => x.Tags, opt => opt.MapFrom(origin => origin.RelatedTags))
               .ForMember(x => x.CreatedById,
                   opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
               .ForMember(x => x.UpdatedById,
                   opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.Listing, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void MemberMappings()
        {
            Mapper.CreateMap<Data.Model.Member, Data.Model.Member>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Listings, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedCountry, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.IsActivated, opt => opt.Ignore())
                .ForMember(dest => dest.AccountType, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Member, Data.Model.Member>();
            Mapper.CreateMap<Data.Model.Member, Data.DTOs.Member>()

                .ForMember(x => x.ListingIds, opt => opt.MapFrom(origin => origin.Listings))
                .ForMember(x => x.CountryId, opt => opt.MapFrom(origin => origin.RelatedCountry))
               .ForMember(x => x.CreatedById,
                   opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
               .ForMember(x => x.UpdatedById,
                   opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.Member, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

      
        private static void PageMapping()
        {
            Mapper.CreateMap<Data.Model.Pages, Data.Model.Pages>()
                           .ForMember(x => x.Id, opt => opt.Ignore())

            //.ForMember(dest => dest.PrimaryMedia, opt => opt.Ignore())
            //.ForMember(dest => dest.MemberId, opt => opt.Ignore())
            //.ForMember(dest => dest.Tags, opt => opt.Ignore())
            ;

            Mapper.CreateMap<Data.DTOs.Pages, Data.Model.Pages>();

            Mapper.CreateMap<Data.Model.Pages, Data.DTOs.Pages>()

               //.ForMember(x => x.PrimaryMediaId, opt => opt.MapFrom(origin => origin.PrimaryMedia))
               //.ForMember(x => x.MemberId, opt => opt.MapFrom(origin => origin.ArticleType))
               //.ForMember(x => x.RelatedMediaIds, opt => opt.MapFrom(origin => origin.RelatedMedia))
               //.ForMember(x => x.Tags, opt => opt.MapFrom(origin => origin.Tags))
               .ForMember(x => x.CreatedById,
                   opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
               .ForMember(x => x.UpdatedById,
                   opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.Pages, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
    }
}