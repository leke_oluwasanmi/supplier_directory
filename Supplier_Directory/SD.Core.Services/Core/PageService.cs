﻿using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using SD.Core.Services.Core.Wrapped.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core
{
    public class PageService : BaseService
    {
        private readonly UserService _userService;

        private readonly IPageServiceWrapped _pageServiceWrapped;

        public PageService(IMongoRepository1 mongoRepository, IPageServiceWrapped pageServiceWrapped = null)
            : base(mongoRepository)
        {
            _userService = new UserService(mongoRepository);

            if (pageServiceWrapped == null)
                pageServiceWrapped = new PageServiceWrapped(mongoRepository);
            _pageServiceWrapped = pageServiceWrapped;
        }

        public List<Pages> GetAllPages()
        {
            var data = _pageServiceWrapped.GetAllPages();
            return data.ToList();
        }

        public Pages GetById(string id, PageHydrationSettings hydrationSettings = null)
        {
            return _pageServiceWrapped.GetById(id, hydrationSettings);
        }
        
        public List<Pages> FindByIds(PageHydrationSettings hydrationSettings = null, params string[] ids)
        {
            return _pageServiceWrapped.FindByIds(hydrationSettings, ids).ToList();
        }


        public Pages CreateNewPage(string Title, string Body, string PageUrls, string CreatedById)
        {
            return _pageServiceWrapped.CreateNewPage(Title, Body, PageUrls, CreatedById);
        }

        public void EditPage(string id, string Title, string Body, string PageUrls, string UpdatedById)
        {
            _pageServiceWrapped.EditPage(id, Title, Body, PageUrls, UpdatedById);
        }

        public int PublishPage(string id)
        {
            return _pageServiceWrapped.PublishPage(id);
        }

        public int DeactivatePage(string id)
        {
            return _pageServiceWrapped.DeactivatePage(id);
        }

        public List<Pages> Search(string searchText, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, PageHydrationSettings hydrationSettings = null, int? skip = null, int? take = null)
        {
            return _pageServiceWrapped.Search(searchText, showWithStatus, showOnlyLive, hydrationSettings, skip, take).ToList();
        }
    }
}
