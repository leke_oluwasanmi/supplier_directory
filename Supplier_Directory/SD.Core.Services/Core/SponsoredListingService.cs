﻿using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core
{
    public class SponsoredListingService : BaseService
    {
        private readonly SponsoredListingServiceWrapped _sponsoredListingServiceWrapped;

        public SponsoredListingService(IMongoRepository1 mongoRepository, SponsoredListingServiceWrapped featuredListingServiceWrapped = null)
            : base(mongoRepository)
        {
            _sponsoredListingServiceWrapped = new SponsoredListingServiceWrapped(mongoRepository);
        }


        public void Save(Data.Model.SponsoredListing sponsoredListing)
        {
            var sponsoredListingDTO = _sponsoredListingServiceWrapped.deHydrate(sponsoredListing);

            if (sponsoredListingDTO.Id == null)
            {
                MongoRepository.Insert(sponsoredListingDTO);
                sponsoredListing.Id = sponsoredListingDTO.Id;
            }
            else MongoRepository.Update(sponsoredListingDTO);
        }

        public Data.Model.SponsoredListing FindById(string id, SponsoredListingHydrationSettings hydrationSettings)
        {
            var model = _sponsoredListingServiceWrapped.FindById(id, hydrationSettings);
            return model;
        }


        public Data.Model.SponsoredListing FindByTagId(string tagId, SponsoredListingHydrationSettings hydrationSettings)
        {
            var model = _sponsoredListingServiceWrapped.FindByTagId(tagId, hydrationSettings);
            return model;
        }
    }
}
