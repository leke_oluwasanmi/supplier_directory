﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using SD.Core.Services.Core.Wrapped.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core
{
    public class MemberService : BaseService
    {
        private readonly ListingService _listingService;

        //private readonly SavedSearchService _savedSearchSerice;

        private readonly MemberServiceWrapped _memberServiceWrapped;
        public MemberService(IMongoRepository1 mongoRepository, MemberServiceWrapped memberServiceWrapped = null)
            : base(mongoRepository)
        {
            _listingService = new ListingService(mongoRepository);
            //_savedSearchSerice = new SavedSearchService(mongoRepository);
            if (memberServiceWrapped == null)
                memberServiceWrapped = new MemberServiceWrapped(mongoRepository);
            _memberServiceWrapped = memberServiceWrapped;
        }

        //public List<Member> GetAllMembers(MemberHydrationSettings hydrationSettings = null)
        public List<Member> GetAllMembers()
        {
            var data = _memberServiceWrapped.GetAllMembers();
            return data.ToList();
        }

        public Member GetById(string id, MemberHydrationSettings hydrationSettings = null)
        {
            return _memberServiceWrapped.GetById(id, hydrationSettings);
        }

        public Member GetByEmail(string email, MemberHydrationSettings hydrationSettings = null)
        {
            var result = _memberServiceWrapped.GetByEmail(email, hydrationSettings);

            return result;
        }

        public List<Member> FindByIds(MemberHydrationSettings hydrationSettings = null, params string[] ids)
        {
            return _memberServiceWrapped.FindByIds(hydrationSettings, ids).ToList();
        }

        public int PublishMember(string id)
        {
            return _memberServiceWrapped.PublishMember(id);
        }

        public Member AddNewSignup(string email, string password)
        {
            return _memberServiceWrapped.AddNewSignup(email, password);
        }

        public bool ChangePassword(string id, string password)
        {
            return _memberServiceWrapped.ChangePassword(id, password);
        }

        public void Edit(Data.DTOs.Member member)
        {
            _memberServiceWrapped.EditMember(member.Id, member);
            return;
        }

        public void Save(Data.Model.Member viewModel)
        {
            var memberDTO = _memberServiceWrapped.deHydrate(viewModel);
            if (string.IsNullOrEmpty(memberDTO.Id))
            {
                MongoRepository.Insert(memberDTO);
                viewModel.Id = memberDTO.Id;
            }
            else
            {
                MongoRepository.UpdateWithComment(memberDTO);
            }
        }
        public Data.DTOs.Member FindDTOById(string id)
        {
            return _memberServiceWrapped.FindDTOById(id);
        }

        //public Data.DTOs.Member GetLogin(string email, string password)
        //{
        //    return _memberServiceWrapped.Login(email, password);
        //}
        public Data.Model.Member GetLogin(string email, string password, MemberHydrationSettings hydrationSettings = null)
        {
            return _memberServiceWrapped.Login(email, password, hydrationSettings);
        }

        public List<Member> Search(string searchText, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, MemberHydrationSettings hydrationSettings = null, int? skip = null, int? take = null)
        {
            return _memberServiceWrapped.Search(searchText, showWithStatus, showOnlyLive, hydrationSettings, skip, take).ToList();
        }
        public int GetCount(PublicationStatus[] showWithStatus = null,
                   bool showOnlyLive = false)
        {
            return _memberServiceWrapped.GetCount(showWithStatus, showOnlyLive);
        }
    }
}
