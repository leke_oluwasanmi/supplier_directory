﻿using AutoMapper;
using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Data.DTOs;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core
{
    public class CountryService : BaseService
    {
        private readonly CountryServiceWrapped _countryServiceWrapped;
        public CountryService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
            _countryServiceWrapped = new CountryServiceWrapped(mongoRepository);
        }
        public IList<SD.Core.Data.Model.Country> GetCountries()
        {
            var countries = _countryServiceWrapped.GetAllCountries(null);
            return countries;
        }

        public IList<SD.Core.Data.DTOs.Country> GetCountriesDTO()
        {
            IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            var countries = mongoRepository.AsQueryable<Data.DTOs.Country>().ToList();
            return countries;
        }
        public void Update(SD.Core.Data.DTOs.Country country)
        {
            IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            mongoRepository.UpdateWithComment(country);
        }

        public Data.Model.Country FindById(string id)
        {
            var country = _countryServiceWrapped.FindById(id, null);
            return country;
        }

        public Data.Model.Country FindByName(string name)
        {
            var country = _countryServiceWrapped.FindByName(name, null);
            return country;
        }

        /// <summary>
        /// Find all countries for a particular region
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public List<Data.Model.Country> FindAllCountriesByRegion(string regionId, CountryHydrationSettings hydrationSettings)
        {
            List<Data.Model.Country> countries = _countryServiceWrapped.FindAllCountriesByRegion(regionId, hydrationSettings);
            return countries;
        }

        /// <summary>
        /// Finds a country by url
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <param name="hydrationSettings"></param>        
        /// <returns></returns>
        public Data.Model.Country FindBySlug(string urlSlug, CountryHydrationSettings hydrationSettings = null)
        {
            var country = _countryServiceWrapped.FindBySlug(urlSlug, hydrationSettings);
            return country;
        }
    }
}