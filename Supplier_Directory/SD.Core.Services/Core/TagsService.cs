﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.EqualityComparers;
using SD.Core.Services.Core.Wrapped;
using System;
using SD.Core.Data.Model.HS;

namespace SD.Core.Services.Core
{
    public class TagsService : BaseService
    {
        private readonly ITagsServiceWrapped _tagsServiceWrapped;

        public TagsService(IMongoRepository1 mongoRepository, ITagsServiceWrapped tagsServiceWrapped = null)
            : base(mongoRepository)
        {
            if (tagsServiceWrapped == null)
                tagsServiceWrapped = new TagsServiceWrapped(mongoRepository);
            _tagsServiceWrapped = tagsServiceWrapped;
        }

        public IList<Tags> All()
        {
            return _tagsServiceWrapped.All();
        }

        public IList<Tags> AllTagsThatStartWith(string text)
        {
            return MongoRepository.AsQueryable<SD.Core.Data.DTOs.Tags>().Where(x => x.DisplayText.ToLower().StartsWith(text.ToLower())).OrderBy(x => x.DisplayText).ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<Tags> AllTagsThatContains(string text)
        {
            return MongoRepository.AsQueryable<SD.Core.Data.DTOs.Tags>().Where(x => x.DisplayText.ToLower().Contains(text.ToLower()))
                .OrderBy(x => x.DisplayText).ToList().Select(x => hydrate(x)).ToList();
        }
        public Data.Model.Tags hydrate(Data.DTOs.Tags tag, TagHydrationSettings hydrationSettings = null)
        {
            return _tagsServiceWrapped.hydrate(tag, hydrationSettings);
        }

        public IList<Tags> AllTagsThatContainsWithoutStartWith(string text)
        {
            return MongoRepository.AsQueryable<SD.Core.Data.DTOs.Tags>().Where(x => x.DisplayText.ToLower().Contains(text.ToLower()) && !x.DisplayText.ToLower().StartsWith(text.ToLower())).OrderBy(x=>x.DisplayText).ToList().Select(x => hydrate(x)).ToList();
        }

        /// <summary>
        /// Increase the view count 
        /// </summary>
        /// <param name="urlSlug"></param>
        public void RegisterTagViewed(string id)
        {            
            var tagDTO = MongoRepository.AsQueryable<Data.DTOs.Tags>()
                .Where(x => x.Id == id).ToList()
                .SingleOrDefault();
            if (tagDTO != null)
            {
                tagDTO.ViewCount = tagDTO.ViewCount + 1;
                MongoRepository.Update(tagDTO);
            }            
        }

        public IList<Tags> Search(string searchText, bool searchName = true, int resultLimit = 10)
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchName)) return new List<Tags>();
            var foundTags = new List<Tags>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");

            foreach (var term in searchTerms.Where(term => !string.IsNullOrWhiteSpace(term)))
                sb.Append(string.Format("(?=.*?({0}))", term));

            sb.Append(".*$");

            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var eventQuery = MongoRepository.AsQueryable<Tags>();

            if (searchName)
            {
                var nameQuery = eventQuery
                    .Where(x =>
                        re.IsMatch(x.DisplayText));
                foundTags.AddRange(nameQuery.ToList()
                    );
            }

            foundTags = foundTags.OrderByDescending(x => x.DisplayText).ToList();

            // Remove duplicates and crop to result limit
            foundTags = foundTags
                .Distinct(new TagEqualityComparer())
                .Take(resultLimit)
                .ToList();
            
            return foundTags;
        }

        public IList<Tags> FindByIds(params string[] ids)
        {
            return _tagsServiceWrapped.FindByIds(ids);
        }

        public Tags FindById(string id)
        {
            return _tagsServiceWrapped.FindById(id);
        }

        public Tags FindByUrlSlug(string UrlSlug)
        {
            return _tagsServiceWrapped.FindByUrlSlug(UrlSlug);
        }

        public List<Data.Model.Tags> FindByUrlSlug(string[] urlSlugs)
        {
            return _tagsServiceWrapped.FindByUrlSlug(urlSlugs);
        }

        public Tags FindByDisplayText(string displayText)
        {
            return _tagsServiceWrapped.FindByDisplayText(displayText);
        }

        public Data.DTOs.Tags deHydrate(Data.Model.Tags viewModel)
        {
            var article = Mapper.Map<Data.Model.Tags, Data.DTOs.Tags>(viewModel);
            return article;
        }
        

        public void Save(Tags tag)
        {
            var _tag = deHydrate(tag);

            var previousTag = FindById(tag.Id);

            #region Bulk Update Tags in Articles
            if (previousTag != null)
            { 
            //UPDATES RELATED TAGS
            var filter = Builders<Listing>.Filter.ElemMatch(x => x.RelatedTags, x => x.DisplayText == previousTag.DisplayText);
            var update = Builders<Listing>.Update.Set(x => x.RelatedTags[-1].DisplayText, tag.DisplayText)
                .Set(x => x.RelatedTags[-1].UrlSlug, tag.UrlSlug);
           
                MongoRepository.GetCollection<Listing>().UpdateMany(filter, update);



                //UPDATES TAGS ARRAY
                if (previousTag.UrlSlug != tag.UrlSlug)
                {
                    var filterForUrlSlug = Builders<Listing>.Filter.AnyIn(x => x.Tags, new[] { previousTag.UrlSlug });
                     var updateForUrlSlugPush = Builders<Listing>.Update.Push(x => x.Tags, tag.UrlSlug);
                
                    var updateForUrlSlugPull = Builders<Listing>.Update.Pull(x => x.Tags, previousTag.UrlSlug);


                    MongoRepository.GetCollection<Listing>().UpdateMany(filterForUrlSlug, updateForUrlSlugPush);
                    MongoRepository.GetCollection<Listing>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);
                }

            }
            #endregion

            if (tag.Id == null)
            {
                MongoRepository.Insert(_tag);
                tag.Id = _tag.Id;
            }
            else MongoRepository.Update(_tag);
        }

        public void Insert(Tags tag)
        {
            var _tag = deHydrate(tag);
            MongoRepository.Insert(_tag);
        }

        public void Delete(string id)
        {
            var deleteTag = FindById(id);

            #region Bulk Update Tags in Articles
            if (deleteTag != null)
            {             

                //remove anywhere using the tag
                var filterForUrlSlug = Builders<Listing>.Filter.AnyIn(x => x.Tags, new[] { deleteTag.UrlSlug });
                //this bit works
                var updateForUrlSlugPull = Builders<Listing>.Update.Pull(x => x.Tags, deleteTag.UrlSlug);
                MongoRepository.GetCollection<Listing>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);


                var filter3 = Builders<Listing>.Filter.ElemMatch(x => x.RelatedTags, x => x.DisplayText == deleteTag.DisplayText);
                
                var update = Builders<Listing>.Update.Pull("RelatedTags", filter3);
                
                var query = MongoRepository.GetCollection<Listing>().Find(filter3, new FindOptions() { });

            }
            #endregion
            var filter = Builders<Tags>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<Tags>().DeleteOne(filter);
        }
        /// <summary>
        /// Get Popular Tags
        /// </summary>
        /// <param name="take"></param>
        /// <returns></returns>
        public List<Data.Model.Tags> GetPopularTags(int take, TagHydrationSettings hydrationSettings)
        {
            var tags = _tagsServiceWrapped.GetPopularTags(take, hydrationSettings);
            return tags;
        }

        /// <summary>
        /// Get Child tags
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public List<string> GetChildTags(string parentId)
        {
            var childTags = _tagsServiceWrapped.GetChildTags(parentId);
            return childTags;

        }
        public List<Data.Model.Tags> GetChildTags(string parentId, TagHydrationSettings hydrationSettings)
        {
            var childTags = _tagsServiceWrapped.GetChildTags(parentId, hydrationSettings);
            return childTags;
        }

    }
}