﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using Media = SD.Core.Data.Model.Media;
using SD.Core.Common.Extensions.Strings;

namespace SD.Core.Services.Core

{
    public class MediaService : BaseService
    {
        private IMediaServiceWrapped _mediaServiceWrapped;

        public MediaService(IMongoRepository1 mongoRepository, IMediaServiceWrapped mediaServiceWrapped = null)
            : base(mongoRepository)
        {
            
            if (mediaServiceWrapped == null)
                mediaServiceWrapped = new MediaServiceWrapped(mongoRepository);
            _mediaServiceWrapped = mediaServiceWrapped;
        }

        //protected new MongoRepository MongoRepository = new MongoRepository();

        

        private const int MaxUniqueUrlSlugChecks = 100;

        public IList<Media> FindByIds(MediaHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null || !ids.Any()) return new List<Media>();
            return MongoRepository.AsQueryable<Data.DTOs.Media>()
                .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
                .OrderByDescending(x => x.LiveFrom)
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public IList<Media> All(MediaHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.Media>()
                .ToList().AsQueryable()
                .OrderByDescending(x => x.LiveFrom)
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public IList<Media> FindByImportId(string importId, MediaHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.Media>()
                .Where(x => x.ImportId == importId).AsQueryable().ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public IList<Data.Model.Media> GetPagedOrderedByCreatedOn(int skip, int take, FileFormatGroup? showFromFileFormatGroup = null, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, MediaHydrationSettings hydrationSettings = null)
        {
            return _mediaServiceWrapped.GetPagedOrderedByCreatedOn(skip, take, showFromFileFormatGroup, showWithStatus, showOnlyLive, ascending, showOnlyMultiMedia, hydrationSettings);
        }

        public int GetPagedCount(FileFormatGroup? showFromFileFormatGroup = null, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool? showOnlyMultiMedia = null)
        {
            return _mediaServiceWrapped.GetPagedCount(showFromFileFormatGroup, showWithStatus, showOnlyLive, showOnlyMultiMedia);
        }

        public Media GetBySlug(string urlSlug, MediaHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.Media>()
                .Where(x => x.UrlSlug == urlSlug).ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .SingleOrDefault();
        }

        public Media FindByYouTubeId(string youTubeId, MediaHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.Media>()
                .Where(x => x.RemoteItemCode == youTubeId)
                .Select(x => hydrate(x, hydrationSettings))
                .FirstOrDefault();
        }

        #region

        //Todo: Readd

        //public int GetPagedCount(FileFormatGroup? showFromFileFormatGroup = null,
        //    IEnumerable<PublicationStatus> showWithStatus = null, bool showOnlyLive = false,
        //    bool? showOnlyMultiMedia = null)
        //    {
        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (!showFromFileFormatGroup.HasValue) return query.Count();
        //    var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
        //    query = query.Where(x => showFileFormats.Contains(x.Format));
        //    return query.Count();
        //    }

        //public IList<Media> GetPagedOrderedByLiveFromDate(int skip, int take,
        //    FileFormatGroup? showFromFileFormatGroup = null, IEnumerable<PublicationStatus> showWithStatus = null,
        //    bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null,
        //    MediaHydrationSettings hydrationSettings = null)
        //    {
        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (showFromFileFormatGroup != null)
        //        {
        //        var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
        //        query = query.Where(x => showFileFormats.Contains(x.Format));
        //        }

        //    query = ascending ? query = query.OrderBy(x => x.LiveFrom) : query.OrderByDescending(x => x.LiveFrom);

        //    query = query.Skip(skip).Take(take);

        //    return query
        //        .Select(x => hydrate(x, hydrationSettings))
        //        .ToList();
        //    }

        //public IList<Media> GetForEventPagedOrderedByLiveFromDate(int eventId, int skip, int take,
        //    IEnumerable<FileFormatGroup> showFromFileFormatGroup = null,
        //    IEnumerable<PublicationStatus> showWithStatus = null, bool showOnlyLive = false, bool ascending = true,
        //    bool? showOnlyMultiMedia = null, MediaHydrationSettings hydrationSettings = null)
        //    {
        //    var eventMediaIds = _eventService.FindEventMediaIds(eventId);
        //    if (eventMediaIds == null || !eventMediaIds.Any()) return new List<Media>();

        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (showFromFileFormatGroup != null)
        //        {
        //        //FileFormat[] showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
        //        var showFileFormats = new ArrayList();
        //        foreach (var item in showFromFileFormatGroup)
        //            {
        //            showFileFormats.AddRange(FileUtilities.FileFormatsFromGroup(item));
        //            }
        //        query = query.Where(x => showFileFormats.Contains(x.Format));
        //        }

        //    query = query.Where(x => eventMediaIds.Contains(x.Id));

        //    query = ascending ? query = query.OrderBy(x => x.LiveFrom) : query.OrderByDescending(x => x.LiveFrom);

        //    query = query.Skip(skip).Take(take);

        //    return query
        //        .Select(x => hydrate(x, hydrationSettings))
        //        .ToList();
        //    }

        //public IEnumerable<Media> GetPagedOrderedByCreatedOn(int skip, int take,
        //    IEnumerable<FileFormatGroup> showFromFileFormatGroup,
        //    IEnumerable<PublicationStatus> showWithStatus = null, bool showOnlyLive = false, bool ascending = true,
        //    bool? showOnlyMultiMedia = null, MediaHydrationSettings hydrationSettings = null)
        //    {
        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    query = query.Where(x => x.Hosting != MediaHosting.S3360Video);
        //    if (showFromFileFormatGroup != null)
        //        {
        //        var showFileFormats = new ArrayList();
        //        foreach (var item in showFromFileFormatGroup)
        //            {
        //            showFileFormats.AddRange(FileUtilities.FileFormatsFromGroup(item));
        //            }
        //        //FileFormat[] showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
        //        query = query.Where(x => showFileFormats.ToArray().Contains(x.Format));
        //        }

        //    query = ascending ? query = query.OrderBy(x => x.CreatedOn) : query.OrderByDescending(x => x.CreatedOn);

        //    query = query.Skip(skip).Take(take);

        //    return query
        //        .Select(x => hydrate(x, hydrationSettings))
        //        .ToList();
        //    }

        //public IList<Media> GetForEventPagedOrderedByLiveFromDate(int eventId, int skip, int take,
        //    FileFormatGroup? showFromFileFormatGroup = null, IEnumerable<PublicationStatus> showWithStatus = null,
        //    bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null,
        //    MediaHydrationSettings hydrationSettings = null)
        //    {
        //    var eventMediaIds = _eventService.FindEventMediaIds(eventId);
        //    if (eventMediaIds == null || !eventMediaIds.Any()) return new List<Media>();

        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (showFromFileFormatGroup != null)
        //        {
        //        var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);

        //        query = query.Where(x => showFileFormats.Contains(x.Format));
        //        }

        //    query = query.Where(x => eventMediaIds.Contains(x.Id));

        //    query = ascending ? query.OrderBy(x => x.LiveFrom) : query.OrderByDescending(x => x.LiveFrom);

        //    query = query.Skip(skip).Take(take);

        //    return query
        //        .Select(x => hydrate(x, hydrationSettings))
        //        .ToList();
        //    }

        //public IList<Media> GetPagedOrderedByCreatedOn(int skip, int take,
        //    FileFormatGroup? showFromFileFormatGroup = null, PublicationStatus[] showWithStatus = null,
        //    bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null,
        //    MediaHydrationSettings hydrationSettings = null)
        //    {
        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (showFromFileFormatGroup != null)
        //        {
        //        var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
        //        query = query.Where(x => showFileFormats.Contains(x.Format));
        //        }

        //    query = ascending ? query.OrderBy(x => x.CreatedOn) : query.OrderByDescending(x => x.CreatedOn);

        //    query = query.Skip(skip).Take(take);

        //    return query
        //        .Select(x => hydrate(x, hydrationSettings))
        //        .ToList();
        //    }

        //public IList<Media> GetFileteredOrderedByLiveFromDate(
        //    int skip, int take,
        //    string competitionSlug, string disciplineSlug, string competitorSlug, string eventSlug,
        //    bool? visibleInMultimedia, DateTime? startDate, DateTime? endDate, FileFormatGroup? formatGroup,
        //    IEnumerable<PublicationStatus> showWithStatus = null, bool showOnlyLive = false, bool ascending = true,
        //    bool? showOnlyMultiMedia = null, MediaHydrationSettings hydrationSettings = null)
        //    {
        //    // Return a (skipped and taken) list of media items
        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (!string.IsNullOrEmpty(competitionSlug) || !string.IsNullOrEmpty(disciplineSlug) ||
        //        !string.IsNullOrEmpty(competitorSlug) || !string.IsNullOrEmpty(eventSlug))
        //        {
        //        // Get a list of media ids related to the specified Competition, Discipline and/or Competitor
        //        var mediaIds = getFiltered(competitionSlug, disciplineSlug, competitorSlug, eventSlug);
        //        query = query.Where(x => mediaIds.Contains(x.Id));
        //        }

        //    if (startDate.HasValue)
        //        query = query.Where(x => x.LiveFrom > startDate.Value);
        //    if (endDate.HasValue)
        //        query = query.Where(x => x.LiveFrom < endDate.Value);

        //    if (visibleInMultimedia.HasValue)
        //        query = query.Where(x => x.ShowInMedia == visibleInMultimedia.Value);

        //    if (formatGroup != null)
        //        {
        //        IEnumerable<FileFormat> formatsInGroup = FileUtilities.FileFormatsFromGroup(formatGroup.Value);
        //        query = query.Where(x => formatsInGroup.Contains(x.Format));
        //        }

        //    query = ascending ? query.OrderBy(x => x.LiveFrom) : query.OrderByDescending(x => x.LiveFrom);

        //    return query
        //        .Skip(skip)
        //        .Take(take)
        //        .Select(x => hydrate(x, null))
        //        .ToList();
        //    }

        //public int GetFilteredCount(
        //    string competitionSlug, string disciplineSlug, string competitorSlug, string eventSlug,
        //    bool? visibleInMultimedia, DateTime? startDate, DateTime? endDate, FileFormatGroup? formatGroup,
        //    PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool? showOnlyMultiMedia = null)
        //    {
        //    // Return a (skipped and taken) list of media items
        //    var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia);

        //    if (!string.IsNullOrEmpty(competitionSlug) || !string.IsNullOrEmpty(disciplineSlug) ||
        //        !string.IsNullOrEmpty(competitorSlug) || !string.IsNullOrEmpty(eventSlug))
        //        {
        //        // Get a list of media ids related to the specified Competition, Discipline and/or Competitor
        //        var mediaIds = getFiltered(competitionSlug, disciplineSlug, competitorSlug, eventSlug);
        //        query = query.Where(x => mediaIds.Contains(x.Id));
        //        }

        //    if (startDate != null) query = query.Where(x => x.LiveFrom > startDate.Value);
        //    if (endDate != null) query = query.Where(x => x.LiveFrom < endDate.Value);

        //    if (formatGroup != null)
        //        {
        //        IEnumerable<FileFormat> formatsInGroup = FileUtilities.FileFormatsFromGroup(formatGroup.Value);
        //        query = query.Where(x => formatsInGroup.Contains(x.Format));
        //        }

        //    if (visibleInMultimedia != null) query = query.Where(x => x.ShowInMedia == visibleInMultimedia.Value);

        //    return query.Count();
        //    }

        public IList<Media> Search(string searchText, int skip, int take, bool searchTitle = true,
            bool searchMetaDescription = true, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            MediaHydrationSettings hydrationSettings = null)
        {
            // If the search text is empty or the user specified that no fields should be searched then we know that nothing is coming back
            if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMetaDescription))
                return new List<Media>();

            var foundMedia = new List<Data.DTOs.Media>();
            // Split into search terms
            var searchTerms = searchText.ToLower().Split(' ');

            // Build the regular expression
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrWhiteSpace(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

            // Search
            var query = MongoRepository.AsQueryable<Data.DTOs.Media>();

            if (showWithStatus != null)
                query = query.Where(x => showWithStatus.Contains(x.Status));
            if (showOnlyLive)
                query = query.Where(x => x.LiveFrom < DateTime.UtcNow);

            if (searchTitle)
            {
                var titleQuery = query
                    .Where(x =>
                        re.IsMatch(x.Title));
                foundMedia.AddRange(titleQuery
                    .ToList());
            }

            if (searchMetaDescription)
            {
                var metaQuery = query
                    .Where(x =>
                        re.IsMatch(x.MetaDescription));
                foundMedia.AddRange(metaQuery
                    .ToList());
            }

            foundMedia = foundMedia.OrderByDescending(x => x.CreatedOn).ToList();

            // Remove duplicates and skip/take
            return foundMedia
                .OrderByDescending(x => x.CreatedOn)
                //.Distinct(new MediaEqualityComparer()) //Todo:readd
                .Skip(skip)
                .Take(take)
                .Select(x => hydrate(x, hydrationSettings)) // Only hydrate the final set of results
                .ToList();
        }

        //public IList<Media> Search(string searchText, int skip, int take,
        //    FileFormatGroup? showFromFileFormatGroup = null, bool searchTitle = true,
        //    bool searchMetaDescription = true, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
        //    MediaHydrationSettings hydrationSettings = null)
        //    {
        //    // If the search text is empty or the user specified that no fields should be searched then we know that nothing is coming back
        //    if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMetaDescription))
        //        return new List<Media>();

        //    var foundMedia = new List<Data.DTOs.Media>();
        //    // Split into search terms
        //    var searchTerms = searchText.ToLower().Split(' ');

        //    // Build the regular expression
        //    var sb = new StringBuilder("^");
        //    foreach (var term in searchTerms.Where(term => !string.IsNullOrWhiteSpace(term)))
        //        {
        //        sb.Append(string.Format("(?=.*?({0}))", term));
        //        }
        //    sb.Append(".*$");
        //    var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

        //    // Search
        //    var query = MongoRepository.AsQueryable<Data.DTOs.Media>();

        //    if (showWithStatus != null)
        //        query = query.Where(x => showWithStatus.Contains(x.Status));
        //    if (showOnlyLive)
        //        query = query.Where(x => x.LiveFrom < DateTime.UtcNow);
        //    // File format group
        //    if (showFromFileFormatGroup != null)
        //        {
        //        var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
        //        query = query.Where(x => showFileFormats.Contains(x.Format));
        //        }
        //    // Search fields
        //    if (searchTitle)
        //        {
        //        var titleQuery = query
        //            .Where(x =>
        //                re.IsMatch(x.Title));
        //        foundMedia.AddRange(titleQuery
        //            .ToList());
        //        }
        //    if (searchMetaDescription)
        //        {
        //        var metaQuery = query
        //            .Where(x =>
        //                re.IsMatch(x.MetaDescription));
        //        foundMedia.AddRange(metaQuery
        //            .ToList());
        //        }

        //    foundMedia = foundMedia.OrderByDescending(x => x.CreatedOn).ToList();

        //    // Remove duplicates and skip/take
        //    return foundMedia
        //        .OrderByDescending(x => x.CreatedOn)
        //        .Distinct(new MediaEqualityComparer())
        //        .Skip(skip)
        //        .Take(take)
        //        .Select(x => hydrate(x, hydrationSettings)) // Only hydrate the final set of results
        //        .ToList();
        //    }

        /// <summary>
        /// Investigate if this is actually a faster way of doing the search. i.e.: a) get the unique ids of found media items b) then get the actual entities from the db in one call
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="searchTitle"></param>
        /// <param name="searchMetaDescription"></param>
        /// <param name="showWithStatus"></param>
        /// <param name="showOnlyLive"></param>
        /// <returns></returns>
        public int SearchCount(string searchText, bool searchTitle = true, bool searchMetaDescription = true,
            PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            // If the search text is empty or the user specified that no fields should be searched then we know that nothing is coming back
            if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMetaDescription)) return 0;

            var foundMediaIds = new List<string>();
            // Split into search terms
            var searchTerms = searchText.ToLower().Split(' ');

            // Build the regular expression
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrWhiteSpace(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

            // Search
            var query = MongoRepository.AsQueryable<Data.DTOs.Media>();

            if (showWithStatus != null)
                query = query.Where(x => showWithStatus.Contains(x.Status));
            if (showOnlyLive)
                query = query.Where(x => x.LiveFrom < DateTime.UtcNow);

            if (searchTitle)
            {
                query = query
                    .Where(x =>
                        re.IsMatch(x.Title));
            }

            if (searchMetaDescription)
            {
                query = query
                    .Where(x =>
                        re.IsMatch(x.MetaDescription));
            }

            query = query.OrderByDescending(x => x.UpdatedOn);

            // Remove duplicates and skip/take
            return query
                .Select(x => x.Id)
                .ToList()
                .Distinct()
                .Count();
        }

        public IList<Media> GetForDay(DateTime dayDate, int take)
        {
            return _mediaServiceWrapped.GetForDay(dayDate, take);
        }

        #endregion

        #region Private Methods

        protected Media hydrate(Data.DTOs.Media media, MediaHydrationSettings hydrationSettings)
        {
            if (media == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new MediaHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Media, Media>(media);

            //if (hydrationSettings.RelatedArticles != null)
            //    viewModel.RelatedArticles = FindRelatedArticles(hydrationSettings.RelatedArticles, media.Id);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(media.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(media.CreatedById);
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(media.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(media.UpdatedById);
            }
            return viewModel;
        }

        public Data.DTOs.Media deHydrate(Media viewModel)
        {
            return Mapper.Map<Media, Data.DTOs.Media>(viewModel);
        }

        // Helper methods for resolving related entities
        //private IList<Article> FindRelatedArticles(ArticleHydrationSettings hydrationSettings, string mediaId)
        //    {
        //    //var articles = new List<Model.Article>(_articleService.FindWithPrimaryMedia(hydrationSettings.RelatedArticles, mediaId));
        //    //articles.AddRange(_articleService.FindWithRelatedMedia(hydrationSettings, mediaId));
        //    var articles = _articleService.FindWithRelatedMedia(hydrationSettings, mediaId);
        //    return articles
        //        .Distinct(new ArticleEqualityComparer())
        //        .ToList();
        //    }

        #endregion Private Methods

        public string Add(Media media)
        {
            var _media = deHydrate(media);
            MongoRepository.Insert(_media);
            return _media.Id;
        }

        public void DeleteById(string id, bool soft = true)
        {
            // We need to remove the article from any entities that reference it so:
            // - Hydrate the media item
            var media = FindByIds(new MediaHydrationSettings
            {                
            }, id).SingleOrDefault();

            if (media == null) throw new Exception(string.Format("Couldn't find a media item with id {0} to delete.", id));
                        

            if (soft)
            {
                // - Change the status to deleted
                media.Status = PublicationStatus.Deleted;
                // - Update the media item
                Save(media);
            }
            else
            {
                // - Enforce relationships
                enforceArticleRelationships(media);

                // - Delete from db
                var mediaDto = MongoRepository.AsQueryable<Data.DTOs.Media>()
                    .SingleOrDefault(x => x.Id == media.Id);
                MongoRepository.Remove(mediaDto, mediaDto.Id);
            }
        }

        public string Save(SD.Core.Data.Model.Media viewModel, string versionComment = null)
        {
            var media = deHydrate(viewModel);

            //Check that a liveFrom date has been supplied
            if (media.LiveFrom.Year == 0001)
                media.LiveFrom = DateTime.Today;

            //Update the url slug with check for duplicates
            media.UrlSlug = media.Title.GenerateSlug();
            var checkCount = 1;
            var urlSlugIsUnique = false;
            while (!urlSlugIsUnique && checkCount < MaxUniqueUrlSlugChecks)
            {
                if (MongoRepository.AsQueryable<Data.DTOs.Listing>().Any(x => x.UrlSlug == media.UrlSlug && x.Id != media.Id))
                    media.UrlSlug = media.Title.GenerateSlug() + checkCount;
                else urlSlugIsUnique = true;
                checkCount++;
            }

            // Save the media item so we know it has an id
            if (string.IsNullOrEmpty(media.Id))
            {
                MongoRepository.Insert(media);
                viewModel.Id = media.Id;
            }
            else
            {
                MongoRepository.UpdateWithComment(media, versionComment);
            }

            viewModel.Id = media.Id;

            // Enfoce it's relationships
            enforceArticleRelationships(viewModel);

            return media.Id;
        }
        private void enforceArticleRelationships(Data.Model.Media viewModel)
        {

        }
    }
}