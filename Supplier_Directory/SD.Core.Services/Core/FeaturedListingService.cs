﻿using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core
{
    public class FeaturedListingService : BaseService
    {        
        private readonly FeaturedListingServiceWrapped _featuredListingServiceWrapped;
        
        public FeaturedListingService(IMongoRepository1 mongoRepository, FeaturedListingServiceWrapped featuredListingServiceWrapped = null)
            : base(mongoRepository)
        {
            _featuredListingServiceWrapped = new FeaturedListingServiceWrapped(mongoRepository);            
        }

        public List<Data.Model.FeaturedListing> GetAll(PublicationStatus[] showWithStatus, FeaturedListingHydrationSettings hydrationSettings)
        {
            var featuredListings = _featuredListingServiceWrapped.GetAll(showWithStatus, hydrationSettings);
            return featuredListings.ToList();
        }

        public List<Data.Model.FeaturedListing> GetAllByPage(PublicationStatus[] showWithStatus, Page pageName, FeaturedListingHydrationSettings hydrationSettings, bool loadRelatedHydrationSettings = false, bool liveOnly = false)
        {
            var featuredListings = _featuredListingServiceWrapped.GetAllByPage(showWithStatus, pageName, hydrationSettings, loadRelatedHydrationSettings, liveOnly = false);
            return featuredListings;
        }

        public void Save(Data.Model.FeaturedListing featuredListing)
        {
            var featuredListingDTO = _featuredListingServiceWrapped.deHydrate(featuredListing);

            if (featuredListingDTO.Id == null)
            {
                MongoRepository.Insert(featuredListingDTO);
                featuredListing.Id = featuredListingDTO.Id;
            }
            else MongoRepository.Update(featuredListingDTO);
        }        

        public Data.Model.FeaturedListing FindById(string id, FeaturedListingHydrationSettings hydrationSettings)
        {
            var model = _featuredListingServiceWrapped.FindById(id, hydrationSettings);
            return model;
        }

        /// <summary>
        /// Reorder
        /// </summary>
        /// <param name="orderedIds"></param>
        /// <param name="page"></param>
        public void Reorder(List<string> orderedIds, Page page, FeaturedListingHydrationSettings hydrationSettings)
        {
            var modelsForPage = GetAllByPage(null, page, hydrationSettings);
            int i = 1;
            foreach (string orderId in orderedIds)
            {
                var model = modelsForPage.Find(x => x.Id == orderId);
                model.Order = i;
                Save(model);
                i++;
            }
        }

        public bool DeleteById(string id, bool soft = true)
        {
            var featuredListing = MongoRepository.AsQueryable<Data.DTOs.FeaturedListing>()
                    .SingleOrDefault(x => x.Id == id);

            if (featuredListing == null) return false;

            MongoRepository.Remove(featuredListing, featuredListing.Id);
            return true;
        }
    }
}
