﻿using SD.Core.Common;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core
{
   public class RegionService : BaseService
    {
        private readonly RegionServiceWrapped _regionServiceWrapped;
        public RegionService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
            _regionServiceWrapped = new RegionServiceWrapped(mongoRepository);
        }

        public List<Data.Model.Region> GetAllRegions(RegionHydrationSettings hydrationSettings)
        {
            return _regionServiceWrapped.GetAllRegions(hydrationSettings);
        }

        public IList<SD.Core.Data.DTOs.Region> GetRegionsDTO()
        {
            IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            var regions = mongoRepository.AsQueryable<Data.DTOs.Region>().ToList();
            return regions;
        }
        public void Update(SD.Core.Data.DTOs.Region region)
        {
            IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            mongoRepository.UpdateWithComment(region);
        }

        public void Remove(SD.Core.Data.DTOs.Region region)
        {
            IMongoRepository1 mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            mongoRepository.Remove<SD.Core.Data.DTOs.Region>(region, region.Id);
        }

        /// <summary>
        /// Finds a region by url
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <param name="hydrationSettings"></param>        
        /// <returns></returns>
        public Region FindBySlug(string urlSlug, RegionHydrationSettings hydrationSettings = null)
        {
            var region = _regionServiceWrapped.FindBySlug(urlSlug, hydrationSettings);
            return region;
        }
    }
    
}
