﻿using AutoMapper;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped.Interfaces;
using System.Text.RegularExpressions;
using Omu.Encrypto;

namespace SD.Core.Services.Core.Wrapped
{
    public class MemberServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;


        public MemberServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;

        }

        public IList<Member> GetAllMembers()
        {
            var records = new List<Member>();
            var dbRecords = _mongoRepository.AsQueryable<Data.DTOs.Member>().ToList();

            foreach (var dbRecord in dbRecords)
            {
                var record = hydrate(dbRecord, new MemberHydrationSettings());
                records.Add(record);
            }

            return records;
        }


        public Member GetById(string id, MemberHydrationSettings hydrationSettings = null)
        {
            var member = _mongoRepository.AsQueryable<Data.DTOs.Member>().Where(x => x.Id == id).SingleOrDefault();
            Data.Model.Member memberModel = new Member();
            if (member != null)
            {
                memberModel = hydrate(member, hydrationSettings);
            }

            return memberModel;
        }

        public Member GetByEmail(string email, MemberHydrationSettings hydrationSettings = null)
        {
            var member = _mongoRepository.AsQueryable<Data.DTOs.Member>().Where(x => x.Email == email).SingleOrDefault();
            Data.Model.Member memberModel = new Member();
            if (member != null)
            {
                memberModel = hydrate(member, hydrationSettings);
            }

            return memberModel;
        }

        public IList<Member> FindByIds(MemberHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null || !ids.Any()) return new List<Member>();

            var filter = Builders<Data.DTOs.Member>.Filter.In("_id", ids.ToList());
            return _mongoRepository.GetCollection<Data.DTOs.Member>().Find(filter).ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }
        public Data.DTOs.Member FindDTOById(string id)
        {
            return _mongoRepository.AsQueryable<Data.DTOs.Member>().Where(x => x.Id == id).ToList()
                .SingleOrDefault();
        }
        
        public Data.Model.Member Login(string email, string password, MemberHydrationSettings settings = null)
        {
            email = email.ToLower();
            var user = GetByEmail(email, settings);
            //if empty
            if (user == null) return null;

            // hash password
            var hasher = new Hasher();
            hasher.SaltSize = 10;

            // Check password
            if (string.IsNullOrEmpty(user.Password)) return null;
            if (!hasher.CompareStringToHash(password, user.Password)) return null;

            return user;
        }

        public Data.Model.Member CreateNewMember(string Title, string Telephone, string AccountType, string Address1, string Address2, string Address3, string Address4, string Town, string Country, string Postcode, string Firstname, string Surname, string Email, string Password)
        {
            throw new NotImplementedException();
        }

        public Member AddNewSignup(string email, string password)
        {
            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var data = new Data.DTOs.Member()
            {
                Email = email.ToLower(),
                Password = hasher.Encrypt(password),
                ActivationToken = GenerateToken(),

            };
            //insert
            _mongoRepository.Insert(data);
            //get record
            var result = _mongoRepository.AsQueryable<Data.DTOs.Member>().Where(x => x.Email == data.Email).SingleOrDefault();

            //return result
            return new Member()
            {
                Id = result.Id,
                Email = result.Email,
                ActivationToken = result.ActivationToken,
                CreatedOn = result.CreatedOn,
                Firstname = "",
                Surname = "",
                Status = PublicationStatus.UnPublished,
                IsModerated = false
            };
        }

        public bool ChangePassword(string id, string password)
        {
            var hasher = new Hasher();
            hasher.SaltSize = 10;

            var model = _mongoRepository.AsQueryable<Data.DTOs.Member>().Where(x => x.Id == id).ToList()
                .SingleOrDefault();
            if (model == null)
            {
                return false;
            }

            //assign password
            model.Password = hasher.Encrypt(password);
            model.PasswordToken = "";

            _mongoRepository.Update(model);

            return true;
        }

        public string GenerateToken()
        {
            var urlCode = Guid.NewGuid().ToString("N");
            bool isDuplicate = _mongoRepository.AsQueryable<Data.DTOs.Member>().Where(x => x.ActivationToken == urlCode).Count() > 0;
            do
            {
                urlCode = Guid.NewGuid().ToString("N");
            }
            while (isDuplicate);
            return urlCode.ToString();
        }

        public void EditMember(string id, Data.DTOs.Member member)
        {
            var model = GetById(id);
            if (model != null)
            {
                _mongoRepository.Update(member);
            }
            return;
        }

        public int PublishMember(string id)
        {
            var model = GetById(id);
            if (model != null)
            {
                model.Status = PublicationStatus.Published;
                _mongoRepository.Update(model);

                return 1;
            }
            return 0;
        }

        public int DeactivateMember(string id)
        {
            var model = GetById(id);
            if (model != null)
            {
                model.Status = PublicationStatus.UnPublished;
                _mongoRepository.Update(model);

                return 1;
            }
            return 0;
        }

        public bool VerifyByCode(string id, string code)
        {
            throw new NotImplementedException();
        }


        public Member hydrate(Data.DTOs.Member member, MemberHydrationSettings hydrationSettings)
        {
            if (member == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new MemberHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Member, Member>(member);

            if (hydrationSettings.MemberListings != null && member.ListingIds != null)
            {
                var listingService = new ListingService(_mongoRepository);
                listingService.FindByIds(null, member.ListingIds.ToArray());
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(member.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(member.UpdatedById);
            }
            if (hydrationSettings.Country != null && !string.IsNullOrEmpty(member.CountryId))
            {
                CountryService countryService = new CountryService(_mongoRepository);
                viewModel.RelatedCountry = countryService.FindById(member.CountryId);
            }
            viewModel.LiveFrom = (member.LiveFrom == DateTime.MinValue) ? null : member.LiveFrom as DateTime?;

            if (member.LiveFrom == DateTime.MinValue) viewModel.LiveFrom = null;
            else viewModel.LiveFrom = member.LiveFrom;

            return viewModel;
        }


        public IFindFluent<Data.DTOs.Member, Data.DTOs.Member> GenerateBasicQuery(PublicationStatus[] showWithStatus = null, bool showOnlyLive = true, int? skip = null, int? take = null)
        {
            var filter = Builders<Data.DTOs.Member>.Filter.Empty;

            if (showWithStatus != null)
            {
                var showWithStatusInts = showWithStatus.Select(x => (int)x);

                filter = filter & Builders<Data.DTOs.Member>.Filter.In("Status", showWithStatusInts.ToList());
            }

            if (showOnlyLive)
            {
                var dt = new BsonDateTime(DateTime.UtcNow);

                //this line causes the serialization error
                filter = filter & Builders<Data.DTOs.Member>.Filter.Lt("LiveFrom", new BsonDateTime(DateTime.UtcNow));
                //filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveTo", new BsonDateTime(DateTime.UtcNow));
                filter = filter & Builders<Data.DTOs.Member>.Filter.Eq("Status", PublicationStatus.Published);
            }

            var query = _mongoRepository.GetCollection<Data.DTOs.Member>().Find(filter, new FindOptions() { });

            return query;
        }
        public int GetCount(PublicationStatus[] showWithStatus = null,
                    bool showOnlyLive = false)
        {

            var query = GenerateBasicQuery(showWithStatus, showOnlyLive);
            var filter = query.Filter;
            var orfilter = Builders<Data.DTOs.Member>.Filter.Empty;

            query.Filter = filter & orfilter;
            return int.Parse(query.Count().ToString());
        }

        public IList<Member> Search(string searchText, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, MemberHydrationSettings hydrationSettings = null, int? skip = null, int? take = null)
        {

            if (string.IsNullOrWhiteSpace(searchText) || string.IsNullOrEmpty(searchText))
                return new List<Member>();

            var records = new List<Member>();

            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

            var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

            var nameQuery = query.ToList()
                .Where(x => re.IsMatch(x.Firstname) || re.IsMatch(x.Surname) || re.IsMatch(x.Email));
            records.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            return records;
        }

        public Data.DTOs.Member deHydrate(Data.Model.Member viewModel)
        {
            var member = Mapper.Map<Data.Model.Member, Data.DTOs.Member>(viewModel);

            member.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            DateTimeUtilities.ToUtcPreserved(member.LiveFrom);
            DateTimeUtilities.ToUtcPreserved(member.LiveTo);

            return member;
        }
    }
}
