﻿using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data.DTOs;
using AutoMapper;
using SD.Core.Common.Utils;
using SD.Core.Common.Constants;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SD.Core.Services.Core.Wrapped
{
    public class FeaturedListingServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;
        private readonly ListingService _listingService;
        public FeaturedListingServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
            _listingService = new ListingService(mongoRepository);
        }

        public Data.Model.FeaturedListing FindById(string id, FeaturedListingHydrationSettings hydrationSettings = null)
        {
            var featuredListingDTO = _mongoRepository.AsQueryable<Data.DTOs.FeaturedListing>().Where(x => x.Id == id).SingleOrDefault();
            Data.Model.FeaturedListing featuredListingModel = new Data.Model.FeaturedListing();
            if (featuredListingDTO != null)
            {
                featuredListingModel = hydrate(featuredListingDTO, hydrationSettings);
            }
            return featuredListingModel;
        }

        private Data.Model.FeaturedListing hydrate(Data.DTOs.FeaturedListing featuredListingDTO, FeaturedListingHydrationSettings hydrationSettings, bool loadRelatedHydrationSettings = false)
        {
            if (featuredListingDTO == null) return null;
            if (hydrationSettings == null) hydrationSettings = new FeaturedListingHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.FeaturedListing, Data.Model.FeaturedListing>(featuredListingDTO);
            if (hydrationSettings.ListingHydrationSettings != null && !string.IsNullOrEmpty(featuredListingDTO.ListingId))
            {
                if (loadRelatedHydrationSettings)
                    viewModel.RelatedListing = _listingService.FindById(viewModel.ListingId, new ListingHydrationSettings
                    {
                        Country = new CountryHydrationSettings(),
                        CreatedBy = new UserHydrationSettings(),
                        Member = new MemberHydrationSettings(),
                        PrimaryMedia = new MediaHydrationSettings(),
                        RelatedMedia = new MediaHydrationSettings(),
                        RelatedTags = new TagHydrationSettings(),
                        UpdatedBy = new UserHydrationSettings()
                    });
                else
                    viewModel.RelatedListing = _listingService.FindById(viewModel.ListingId, null);
            }
            return viewModel;
        }
        public Data.DTOs.FeaturedListing deHydrate(Data.Model.FeaturedListing viewModel)
        {
            var featuredListing = Mapper.Map<Data.Model.FeaturedListing, Data.DTOs.FeaturedListing>(viewModel);

            featuredListing.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            DateTimeUtilities.ToUtcPreserved(featuredListing.LiveFrom);
            DateTimeUtilities.ToUtcPreserved(featuredListing.LiveTo);

            return featuredListing;
        }

        public List<Data.Model.FeaturedListing> GetAll(PublicationStatus[] showWithStatus, FeaturedListingHydrationSettings hydrationSettings, bool loadRelatedHydrationSettings = false)
        {
            var query = GenerateBasicQuery(showWithStatus);

            IEnumerable<Data.DTOs.FeaturedListing> results = query.ToList();

            List<Data.DTOs.FeaturedListing> result = results.ToList();

            return result
                .Select(x => hydrate(x, hydrationSettings, loadRelatedHydrationSettings))
                .ToList();
        }
        /// <summary>
        /// Gets all by pages on the website e.g Home, About us
        /// </summary>
        /// <param name="showWithStatus"></param>
        /// <param name="pageName"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public List<Data.Model.FeaturedListing> GetAllByPage(PublicationStatus[] showWithStatus, Page pageName, FeaturedListingHydrationSettings hydrationSettings, bool loadRelatedHydrationSettings = false, bool liveOnly = false)
        {            
            var query = GenerateBasicQuery(showWithStatus, liveOnly);
            var filter = query.Filter;
            var orfilter = Builders<Data.DTOs.Listing>.Filter.Empty;

            IEnumerable<Data.DTOs.FeaturedListing> results = query.ToList().Where(x => x.PageName == pageName);

            List<Data.DTOs.FeaturedListing> result = results.ToList();

            return result
                .Select(x => hydrate(x, hydrationSettings, loadRelatedHydrationSettings))
                .ToList();
        }        

        public IFindFluent<Data.DTOs.FeaturedListing, Data.DTOs.FeaturedListing> GenerateBasicQuery(PublicationStatus[] showWithStatus = null, bool liveOnly = false,
             int? skip = null, int? take = null)
        {
            var filter = Builders<Data.DTOs.FeaturedListing>.Filter.Empty;

            if (liveOnly)
            {
                filter = filter & Builders<Data.DTOs.FeaturedListing>.Filter.Lt("LiveFrom", new BsonDateTime(DateTime.UtcNow));                
            }
            if (showWithStatus != null)
            {
                var showWithStatusInts = showWithStatus.Select(x => (int)x);

                filter = filter & Builders<Data.DTOs.FeaturedListing>.Filter.In("Status", showWithStatusInts.ToList());
            }

            var dt = new BsonDateTime(DateTime.UtcNow);            
            var query = _mongoRepository.GetCollection<Data.DTOs.FeaturedListing>().Find(filter, new FindOptions() { });

            return query;
        }

        
    }
}
