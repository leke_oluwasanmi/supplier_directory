﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data.DTOs;
using AutoMapper;
using SD.Core.Common.Utils;
using SD.Core.Common.Constants;
using MongoDB.Bson;
using MongoDB.Driver;
using SD.Core.Data;
using SD.Core.Data.Model.HS;

namespace SD.Core.Services.Core.Wrapped
{
    public class SponsoredListingServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;
        private readonly TagsService _tagService;
        private readonly ListingService _listingService;
        public SponsoredListingServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
            _tagService = new TagsService(mongoRepository);
            _listingService = new ListingService(mongoRepository);
        }

        public Data.Model.SponsoredListing FindById(string id, SponsoredListingHydrationSettings hydrationSettings = null)
        {
            var sponsoredListingDTO = _mongoRepository.AsQueryable<Data.DTOs.SponsoredListing>().Where(x => x.Id == id).SingleOrDefault();
            Data.Model.SponsoredListing sponsoredListingModel = new Data.Model.SponsoredListing();
            if (sponsoredListingDTO != null)
            {
                sponsoredListingModel = hydrate(sponsoredListingDTO, hydrationSettings);
            }
            return sponsoredListingModel;
        }
        public Data.Model.SponsoredListing FindByTagId(string tagId, SponsoredListingHydrationSettings hydrationSettings = null)
        {
            var sponsoredListingDTO = _mongoRepository.AsQueryable<Data.DTOs.SponsoredListing>().Where(x => x.TagId == tagId).SingleOrDefault();
            Data.Model.SponsoredListing sponsoredListingModel = new Data.Model.SponsoredListing();
            if (sponsoredListingDTO != null)
            {
                sponsoredListingModel = hydrate(sponsoredListingDTO, hydrationSettings);
            }
            return sponsoredListingModel;
        }
        private Data.Model.SponsoredListing hydrate(Data.DTOs.SponsoredListing sponsoredListingDTO, SponsoredListingHydrationSettings hydrationSettings)
        {
            if (sponsoredListingDTO == null) return null;
            if (hydrationSettings == null) hydrationSettings = new SponsoredListingHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.SponsoredListing, Data.Model.SponsoredListing>(sponsoredListingDTO);
            if (hydrationSettings.RelatedTags != null && !string.IsNullOrEmpty(sponsoredListingDTO.TagId))
            {
                viewModel.RelatedTag = _tagService.FindById(sponsoredListingDTO.TagId);
            }

            if (hydrationSettings.RelatedListings != null && !string.IsNullOrEmpty(sponsoredListingDTO.ListingId))
            {
                viewModel.RelatedListing = _listingService.FindById(viewModel.ListingId, null);
            }
            return viewModel;
        }
        public Data.DTOs.SponsoredListing deHydrate(Data.Model.SponsoredListing viewModel)
        {
            var sponsoredListing = Mapper.Map<Data.Model.SponsoredListing, Data.DTOs.SponsoredListing>(viewModel);

            sponsoredListing.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            DateTimeUtilities.ToUtcPreserved(sponsoredListing.LiveFrom);
            DateTimeUtilities.ToUtcPreserved(sponsoredListing.LiveTo);

            return sponsoredListing;
        }
    }
}
