﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;

namespace SD.Core.Services.Core.Wrapped
{
    public class ListingServiceWrapped 
    {
        private IMongoRepository1 _mongoRepository;
        public ListingServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;

        }
        public IList<SD.Core.Data.Model.Listing> GetPagedOrderedBy(SortOrder sortOrder, int skip, int take, string[] tags, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false, string listingType = "standard", ListingHydrationSettings hydrationSettings = null, bool allTags = true)
        {
            var query = GenerateBasicQuery(listingType, showWithStatus, showOnlyLive, skip, take);
            
            var filter = query.Filter;

            var orfilter = Builders<Data.DTOs.Listing>.Filter.Empty;
     
            if (tags.Any())
            {
                if (allTags)
                {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Listing>.Filter.Eq("Tags", tag));
                }
                else
                {
                    orfilter = tags.Aggregate(filter,
                        (current, tag) => current | Builders<Data.DTOs.Listing>.Filter.Eq("Tags", tag));
                }
            }

            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.Listing>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.Listing>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.Listing>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.Listing>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.Listing>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.Listing>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;                
                default:
                    break;
            }

            query.Filter = filter & orfilter;

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();

            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public IFindFluent<Data.DTOs.Listing, Data.DTOs.Listing> GenerateBasicQuery(string listingType, PublicationStatus[] showWithStatus = null, 
      bool showOnlyLive = true, int? skip = null, int? take = null, bool orderedByFeatured = false)
        {
            var filter = Builders<Data.DTOs.Listing>.Filter.Empty;            

            if (showWithStatus != null)
            {
                var showWithStatusInts = showWithStatus.Select(x => (int)x);

                filter = filter & Builders<Data.DTOs.Listing>.Filter.In("Status", showWithStatusInts.ToList());
            }
            if (showOnlyLive)
            {
                var dt = new BsonDateTime(DateTime.UtcNow);

                //this line causes the serialization error
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Lt("LiveFrom", new BsonDateTime(DateTime.UtcNow));
                //filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveTo", new BsonDateTime(DateTime.UtcNow));
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("Status", PublicationStatus.Published);
            }


            if (!string.IsNullOrEmpty(listingType))
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("ListingType", listingType);

            var query = _mongoRepository.GetCollection<Data.DTOs.Listing>().Find(filter, new FindOptions() { });                                 

            return query;
        }

        public Data.Model.Listing hydrate(Data.DTOs.Listing listing, ListingHydrationSettings hydrationSettings)
        {
            if (listing == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new ListingHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Listing, Listing>(listing);

            if (hydrationSettings.PrimaryMedia != null || hydrationSettings.RelatedMedia != null)
            {
                var mediaService = new MediaService(_mongoRepository);
                if (hydrationSettings.PrimaryMedia != null)
                    viewModel.PrimaryMedia =
                        mediaService.FindByIds(hydrationSettings.PrimaryMedia, listing.PrimaryMediaId).FirstOrDefault();
                if (hydrationSettings.RelatedMedia != null && listing.RelatedMediaIds != null)
                    viewModel.RelatedMedia = mediaService.FindByIds(hydrationSettings.RelatedMedia,
                        listing.RelatedMediaIds.ToArray());
            }

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(listing.CreatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.CreatedBy = userService.FindById(listing.CreatedById);
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(listing.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(listing.UpdatedById);
            }
            if (hydrationSettings.RelatedTags != null && listing.Tags != null && listing.Tags.Count > 0)
            {
                var tagService = new TagsService(_mongoRepository);
                viewModel.RelatedTags = tagService.FindByIds(listing.Tags.ToArray());
            }
            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(listing.CreatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.CreatedBy = userService.FindById(listing.CreatedById);
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(listing.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(listing.UpdatedById);
            }
            if(hydrationSettings.Member != null && !string.IsNullOrEmpty(listing.MemberId))
            {
                var memberService = new MemberService(_mongoRepository);
                viewModel.Member = memberService.GetById(listing.Id);
            }
            if(hydrationSettings.Country != null && !string.IsNullOrEmpty(listing.CountryId))
            {
                CountryService countryService = new CountryService(_mongoRepository);
                viewModel.RelatedCountry = countryService.FindById(listing.CountryId);
            }

            viewModel.LiveFrom = (listing.LiveFrom == DateTime.MinValue) ? null : listing.LiveFrom as DateTime?;

            if (listing.LiveFrom == DateTime.MinValue) viewModel.LiveFrom = null;
            else viewModel.LiveFrom = listing.LiveFrom;

            //include viewcount            

            return viewModel;
        }

        
        public IList<Data.Model.Listing> FindByIds(ListingHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null || !ids.Any()) return new List<Listing>();
            return _mongoRepository.AsQueryable<Data.DTOs.Listing>()
                .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
                .OrderByDescending(x => x.LiveFrom)
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public int GetCount(string listingType, PublicationStatus[] showWithStatus = null,
            bool showOnlyLive = false)
        {

            var query = GenerateBasicQuery(listingType,showWithStatus, showOnlyLive);
            var filter = query.Filter;
            var orfilter = Builders<Data.DTOs.Listing>.Filter.Empty;            

            query.Filter = filter & orfilter;
            return int.Parse(query.Count().ToString());
        }

        /// <summary>
        /// Searches By Company Name
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="searchCompanyName"></param>
        /// <param name="showOnlyLive"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public IList<Data.Model.Listing> Search(string companyName, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null)
        {
            if (string.IsNullOrWhiteSpace(companyName))
                return new List<Data.Model.Listing>();
            var foundListings = new List<Data.Model.Listing>();
            var searchTerms = companyName.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = GenerateBasicQuery(null,null,showOnlyLive, skip, take);
            
                var nameQuery = query.ToList()
                    .Where(x => re.IsMatch(x.CompanyName));
                foundListings.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            return foundListings;
        }
        /// <summary>
        /// Searches By Company Name
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="skip"></param>        
        /// <param name="searchCompanyName"></param>
        /// <param name="showOnlyLive"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public IList<Data.Model.Listing> Search(string companyName, int skip, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null)
        {
            if (string.IsNullOrWhiteSpace(companyName))
                return new List<Data.Model.Listing>();
            var foundListings = new List<Data.Model.Listing>();
            var searchTerms = companyName.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = GenerateBasicQuery(null, null, showOnlyLive, skip);

            var nameQuery = query.ToList()
                .Where(x => re.IsMatch(x.CompanyName));
            foundListings.AddRange(nameQuery
                .Select(x => hydrate(x, hydrationSettings))
                .ToList());
            return foundListings;
        }
        public IList<Data.Model.Listing> Search(string categoryId, string countryId, string keyword, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null, bool orderByListingType = false)
        {            
            var foundListings = new List<Data.Model.Listing>();
            var filter = Builders<Data.DTOs.Listing>.Filter.Empty;

            if(showOnlyLive)
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("Status", PublicationStatus.Published);
            //filter by the country and categoryId if available
            if (!string.IsNullOrEmpty(categoryId))
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("Tags", categoryId);            
            if(!string.IsNullOrEmpty(countryId))
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("CountryId", countryId);
            var query = _mongoRepository.GetCollection<Data.DTOs.Listing>().Find(filter, new FindOptions() { });

            //then filter by keyword
            if (string.IsNullOrEmpty(keyword))
                keyword = "";
            var searchTerms = keyword.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var nameQuery = query.ToList()
                .Where(x => re.IsMatch(x.CompanyName) || re.IsMatch(x.Body) || re.IsMatch(x.StandFirst));

            if (orderByListingType)
            {
                var premiumPlusListings = nameQuery.Where(x => x.ListingType == "PremiumPlus");
                var premium = nameQuery.Where(x => x.ListingType == "Premium");
                var standard = nameQuery.Where(x => x.ListingType == "Standard");

                //add premium listings
                foundListings.AddRange(premiumPlusListings
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

                foundListings.AddRange(premium
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

                foundListings.AddRange(standard
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

            }

            else
            {
                foundListings.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }
            return foundListings;
        }

        public IList<Data.Model.Listing> Search(List<string> categoryIds, string countryId, string keyword, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null, bool orderByListingType = false)
        {
            var foundListings = new List<Data.Model.Listing>();
            var filter = Builders<Data.DTOs.Listing>.Filter.Empty;

            if (showOnlyLive)
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("Status", PublicationStatus.Published);
            //filter by the country and categoryId if available
            if (categoryIds.Count > 0)
                filter = filter & Builders<Data.DTOs.Listing>.Filter.In("Tags", categoryIds);
            if (!string.IsNullOrEmpty(countryId))
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("CountryId", countryId);
            var query = _mongoRepository.GetCollection<Data.DTOs.Listing>().Find(filter, new FindOptions() { });

            //then filter by keyword
            if (string.IsNullOrEmpty(keyword))
                keyword = "";
            var searchTerms = keyword.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var nameQuery = query.ToList()
                .Where(x => re.IsMatch(x.CompanyName) || re.IsMatch(x.Body) || re.IsMatch(x.StandFirst));

            if (orderByListingType)
            {
                var premiumPlusListings = nameQuery.Where(x => x.ListingType == "PremiumPlus");
                var premium = nameQuery.Where(x => x.ListingType == "Premium");
                var standard = nameQuery.Where(x => x.ListingType == "Standard");

                //add premium listings
                foundListings.AddRange(premiumPlusListings
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

                foundListings.AddRange(premium
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

                foundListings.AddRange(standard
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

            }

            else
            {
                foundListings.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }
            return foundListings;
        }
        public IList<Data.Model.Listing> Search(List<string> categoryIds, List<string> countryIds, string keyword, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null, bool orderByListingType = false)
        {
            var foundListings = new List<Data.Model.Listing>();
            var filter = Builders<Data.DTOs.Listing>.Filter.Empty;            
            if (showOnlyLive)
                filter = filter & Builders<Data.DTOs.Listing>.Filter.Eq("Status", PublicationStatus.Published);
            //filter by the country and categoryId if available
            if (categoryIds.Count > 0)
            {
                if (categoryIds.Any())
                {
                    //filter = categoryIds.Aggregate(filter,
                    //      (current, tag) => current | Builders<Data.DTOs.Listing>.Filter.Eq("Tags", tag));                    
                    //if (allTags)
                    //{
                    //    filter = tags.Aggregate(filter,
                    //        (current, tag) => current & Builders<Data.DTOs.Listing>.Filter.Eq("Tags", tag));
                    //}
                    //else
                    //{
                    //    orfilter = tags.Aggregate(filter,
                    //        (current, tag) => current | Builders<Data.DTOs.Listing>.Filter.Eq("Tags", tag));
                    //}
                    filter = filter & Builders<Data.DTOs.Listing>.Filter.In("Tags", categoryIds);
                }
            }
                //filter = filter & Builders<Data.DTOs.Listing>.Filter.In("Tags", categoryIds);
            if (countryIds.Count > 0)
                filter = filter & Builders<Data.DTOs.Listing>.Filter.In("CountryId", countryIds);

            var query = _mongoRepository.GetCollection<Data.DTOs.Listing>().Find(filter, new FindOptions() { });

            //then filter by keyword
            if (string.IsNullOrEmpty(keyword))
                keyword = "";
            var searchTerms = keyword.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var nameQuery = query.ToList()
                .Where(x => re.IsMatch(x.CompanyName) || re.IsMatch(x.Body) || re.IsMatch(x.StandFirst));

            if (orderByListingType)
            {
                var premiumPlusListings = nameQuery.Where(x => x.ListingType == "PremiumPlus");
                var premium = nameQuery.Where(x => x.ListingType == "Premium");
                var standard = nameQuery.Where(x => x.ListingType == "Standard");

                //add premium listings
                foundListings.AddRange(premiumPlusListings
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

                foundListings.AddRange(premium
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

                foundListings.AddRange(standard
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());

            }

            else
            {
                foundListings.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }
            return foundListings;
        }
        public Data.DTOs.Listing deHydrate(Data.Model.Listing viewModel)
        {
            var listing = Mapper.Map<Data.Model.Listing, Data.DTOs.Listing>(viewModel);

           listing.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            DateTimeUtilities.ToUtcPreserved(listing.LiveFrom);
            DateTimeUtilities.ToUtcPreserved(listing.LiveTo);

            return listing;
        }

        public SD.Core.Data.Model.Listing FindById(string id, ListingHydrationSettings hydrationSettings)
        {
            return _mongoRepository.AsQueryable<Data.DTOs.Listing>()
                .Where(x => x.Id == id).ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .SingleOrDefault();
        }

        public SD.Core.Data.DTOs.Listing FindDTOById(string id)
        {
            return _mongoRepository.AsQueryable<Data.DTOs.Listing>()
                .Where(x => x.Id == id).ToList()
                .SingleOrDefault();
        }

        /// <summary>
        /// Finds a listing by url
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <param name="hydrationSettings"></param>
        /// <param name="showWithStatus"></param>
        /// <param name="showOnlyLive"></param>
        /// <returns></returns>
        public Listing FindBySlug(string urlSlug, ListingHydrationSettings hydrationSettings, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            urlSlug = urlSlug.ToLower();

            var query = GenerateBasicQuery("",showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.Listing>.Filter.Eq("UrlSlug", urlSlug);

            query.Filter = filter;

            var listing = query.FirstOrDefault();

            return hydrate(listing, hydrationSettings);
        }

        /// <summary>
        /// Get latest 5 companies
        /// </summary>
        /// <param name="listingType"></param>
        /// <param name="hydrationSettings"></param>
        /// <param name="showOnlyLive"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        public List<Data.Model.Listing> GetLatest(string listingType, ListingHydrationSettings hydrationSettings, bool showOnlyLive, int take)
        {
            var query = GenerateBasicQuery(listingType,null, showOnlyLive, 0, take);
            IEnumerable<SD.Core.Data.DTOs.Listing> results = query.ToList().OrderByDescending(x => x.LiveFrom);

            //take latest 5
            List< SD.Core.Data.DTOs.Listing> latest = results.Take(take).ToList();            

            return latest
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }


        public List<Data.Model.Listing> GetListingByMember(string memberId, ListingHydrationSettings hydrationSettings)
        {
            var query = GenerateBasicQuery("", null, false, null);
            
            IEnumerable<SD.Core.Data.DTOs.Listing> results = query.ToList();

            List<SD.Core.Data.DTOs.Listing> result = results.Where(m => m.MemberId == memberId).ToList();

            return result
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public List<Data.Model.Listing> GetAllListings(PublicationStatus[] showWithStatus, ListingHydrationSettings hydrationSettings)
        {
            var query = GenerateBasicQuery("", showWithStatus, false);

            IEnumerable<SD.Core.Data.DTOs.Listing> results = query.ToList();

            List<SD.Core.Data.DTOs.Listing> result = results.ToList();

            return result
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

    }
}
