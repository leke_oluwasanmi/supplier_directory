﻿using System;

namespace SD.Core.Services.Core.Wrapped
{
    public interface IMediaServiceWrapped
    {
        SD.Core.Data.DTOs.Media deHydrate(SD.Core.Data.Model.Media viewModel);

        System.Collections.Generic.IList<SD.Core.Data.Model.Media> GetForDay(DateTime dayDate, int take);

        int GetPagedCount(SD.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, SD.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool? showOnlyMultiMedia = null);

        System.Collections.Generic.IList<SD.Core.Data.Model.Media> GetPagedOrderedByCreatedOn(int skip, int take, SD.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, SD.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, SD.Core.Data.Model.HS.MediaHydrationSettings hydrationSettings = null);
    }
}