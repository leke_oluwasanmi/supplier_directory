﻿using SD.Core.Data.DTOs;
using SD.Core.Data.Model.HS;
using System.Collections.Generic;

namespace SD.Core.Services.Core.Wrapped
{
    public interface ITagsServiceWrapped
    {
        System.Collections.Generic.IList<SD.Core.Data.Model.Tags> All();

        SD.Core.Data.DTOs.Tags deHydrate(SD.Core.Data.Model.Tags viewModel);

        SD.Core.Data.Model.Tags FindByDisplayText(string displayText);

        SD.Core.Data.Model.Tags FindById(string id);

        System.Collections.Generic.IList<SD.Core.Data.Model.Tags> FindByIds(params string[] ids);

        SD.Core.Data.Model.Tags FindByUrlSlug(string UrlSlug);

        System.Collections.Generic.List<SD.Core.Data.Model.Tags> FindByUrlSlug(string[] urlSlugs);

        SD.Core.Data.Model.Tags hydrate(SD.Core.Data.DTOs.Tags tag, TagHydrationSettings hydrationSettings);
        List<Data.Model.Tags> GetPopularTags(int take, TagHydrationSettings hydrationSettings);
        List<string> GetChildTags(string parentId);
        List<SD.Core.Data.Model.Tags> GetChildTags(string parentId, TagHydrationSettings hydrationSettings);
    }
}