﻿using MongoDB.Driver;
using SD.Core.Common.Constants;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core.Wrapped.Interfaces
{
    public interface IPageServiceWrapped
    {
        IList<Pages> GetAllPages();

        Pages GetById(string id, PageHydrationSettings hydrationSettings = null);

        IList<Pages> FindByIds(PageHydrationSettings hydrationSettings = null, params string[] ids);

        Pages CreateNewPage(string Title, string Body, string PageUrls, string CreatedById);

        Data.DTOs.Pages deHydrate(Data.Model.Pages viewModel);


        void EditPage(string id, string Title, string Body, string PageUrls, string UpdatedById);

        int PublishPage(string id);

        int DeactivatePage(string id);

        Pages hydrate(Data.DTOs.Pages Page, PageHydrationSettings hydrationSettings);

        IFindFluent<Data.DTOs.Pages, Data.DTOs.Pages> GenerateBasicQuery(PublicationStatus[] showWithStatus = null, bool showOnlyLive = true, int? skip = null, int? take = null);

        IList<Pages> Search(string searchText, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, PageHydrationSettings hydrationSettings = null, int? skip = null, int? take = null);
    }
}
