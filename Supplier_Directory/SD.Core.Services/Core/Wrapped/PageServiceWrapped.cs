﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using SD.Core.Common.Constants;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SD.Core.Services.Core.Wrapped
{
    public class PageServiceWrapped : IPageServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        //private readonly ListingService _listingService;

        public PageServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Pages> GetAllPages()
        {
            var records = new List<Pages>();
            var dbRecords = _mongoRepository.AsQueryable<Data.DTOs.Pages>().ToList();

            foreach (var dbRecord in dbRecords)
            {
                var record = hydrate(dbRecord, new PageHydrationSettings());
                records.Add(record);
            }

            return records;
        }

        public Pages GetById(string id, Data.Model.HS.PageHydrationSettings hydrationSettings = null)
        {           
            var page = _mongoRepository.AsQueryable<Data.DTOs.Pages>().Where(x => x.Id == id).SingleOrDefault();
            Data.Model.Pages pageModel = new Pages();
            if (page != null)
            {
                pageModel = hydrate(page, hydrationSettings);
            }

            return pageModel;
        }

        public IList<Pages> FindByIds(Data.Model.HS.PageHydrationSettings hydrationSettings = null, params string[] ids)
        {
            throw new NotImplementedException();
        }

        public Pages CreateNewPage(string Title, string Body, string PageUrls, string CreatedById)
        {
            var model = new Pages()
            {
                Title = Title,
                Body = Body,
                PageUrls = PageUrls,
                CreatedById = CreatedById
            };

            _mongoRepository.Insert(deHydrate(model));
            return model;
        }

        public Data.DTOs.Pages deHydrate(Data.Model.Pages viewModel)
        {
            var page = Mapper.Map<Data.Model.Pages, Data.DTOs.Pages>(viewModel);

            page.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            //DateTimeUtilities.ToUtcPreserved(page.LiveFrom);
            //DateTimeUtilities.ToUtcPreserved(article.LiveTo);

            return page;
        }

        public void EditPage(string id, string Title, string Body, string PageUrls, string UpdatedById)
        {
            //var data = GetById(id);
            var data = _mongoRepository.AsQueryable<Data.DTOs.Pages>().Where(x => x.Id == id).ToList()
                .SingleOrDefault();
            if (data != null)
            {
                data.Title = Title;
                data.Body = Body;
                data.PageUrls = PageUrls;
                data.UpdatedById = UpdatedById;
                data.UpdatedOn = DateTime.Now;

                //update
                _mongoRepository.Update(data);
            }
            return ;
        }

        public int PublishPage(string id)
        {
            var model = GetById(id);
            if (model != null)
            {
                model.Status = PublicationStatus.Published;
                _mongoRepository.Update(model);

                return 1;
            }
            return 0;
        }

        public int DeactivatePage(string id)
        {
            var model = GetById(id);
            if (model != null)
            {
                model.Status = PublicationStatus.UnPublished;
                _mongoRepository.Update(model);

                return 1;
            }
            return 0;
        }

        public Pages hydrate(Data.DTOs.Pages page, Data.Model.HS.PageHydrationSettings hydrationSettings)
        {
            if (page == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new PageHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Pages, Pages>(page);

            //viewModel.CreatedOn = (page.CreatedOn == DateTime.MinValue) ? null : page.CreatedOn as DateTime?;

            //if (page.CreatedOn == DateTime.MinValue) viewModel.CreatedOn = null;
            //else viewModel.CreatedOn = page.CreatedOn;

            return viewModel;
        }

        public MongoDB.Driver.IFindFluent<Data.DTOs.Pages, Data.DTOs.Pages> GenerateBasicQuery(Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = true, int? skip = null, int? take = null)
        {
            var filter = Builders<Data.DTOs.Pages>.Filter.Empty;

            if (showWithStatus != null)
            {
                var showWithStatusInts = showWithStatus.Select(x => (int)x);

                filter = filter & Builders<Data.DTOs.Pages>.Filter.In("Status", showWithStatusInts.ToList());
            }

            if (showOnlyLive)
            {
                var dt = new BsonDateTime(DateTime.UtcNow);

                //this line causes the serialization error
                //filter = filter & Builders<Data.DTOs.Page>.Filter.Lt("LiveFrom", new BsonDateTime(DateTime.UtcNow));
                //filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveTo", new BsonDateTime(DateTime.UtcNow));
                filter = filter & Builders<Data.DTOs.Pages>.Filter.Eq("Status", PublicationStatus.Published);
            }

            var query = _mongoRepository.GetCollection<Data.DTOs.Pages>().Find(filter, new FindOptions() { });

            // query = query.Skip(skip);

            //query = query.Limit(take);

            return query;
        }

        public IList<Pages> Search(string searchText, Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, Data.Model.HS.PageHydrationSettings hydrationSettings = null, int? skip = null, int? take = null)
        {
            if (string.IsNullOrWhiteSpace(searchText) || string.IsNullOrEmpty(searchText))
                return new List<Pages>();

            var records = new List<Pages>();

            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

            var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

            //if (searchTitle)
            //{
            //    var nameQuery = query.ToList()
            //        .Where(x => re.IsMatch(x.SEOTitle));
            //    foundArticles.AddRange(nameQuery
            //        .Select(x => hydrate(x, hydrationSettings))
            //        .ToList());
            //}

            //// Remove duplicates and limit result count
            //foundArticles = foundArticles
            //    .Distinct(new Data.Model.EqualityComparers.ArticleEqualityComparer())
            //    //.OrderByDescending(x => x.LiveFrom)
            //    .Skip(skip)
            //    .Take(take)
            //    .ToList();

            var dbRecords = _mongoRepository.AsQueryable<Data.DTOs.Pages>().ToList();

            foreach (var dbRecord in dbRecords)
            {
                var record = hydrate(dbRecord, new PageHydrationSettings());
                records.Add(record);
            }

            return records;
        }
    }
}
