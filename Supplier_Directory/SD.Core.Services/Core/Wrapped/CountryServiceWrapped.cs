﻿using SD.Core.Services.Core.Wrapped.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Core.Data.Model;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using AutoMapper;

namespace SD.Core.Services.Core.Wrapped
{
    public class CountryServiceWrapped 
    {
        private IMongoRepository1 _mongoRepository;
        public CountryServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        /// <summary>
        /// Get all countries
        /// </summary>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public IList<Country> GetAllCountries( CountryHydrationSettings hydrationSettings)
        {
            return _mongoRepository.AsQueryable<Data.DTOs.Country>().ToList()
                .Select(x => hydrate(x, hydrationSettings)).ToList();

        }

        public Country FindById(string id, CountryHydrationSettings hydrationSettings = null)
        {
            var countryDTO = _mongoRepository.AsQueryable<Data.DTOs.Country>().Where(x => x.Id == id).SingleOrDefault();
            Country countryModel = new Country();
            if(countryDTO !=null)
            {
                countryModel = hydrate(countryDTO, hydrationSettings);
            }
            return countryModel;
        }
        public Country FindByName(string name, CountryHydrationSettings hydrationSettings = null)
        {
            var countryDTO = _mongoRepository.AsQueryable<Data.DTOs.Country>().Where(x => x.Name.ToLower() == name.Trim().ToLower()).SingleOrDefault();
            Country countryModel = new Country();
            if (countryDTO != null)
            {
                countryModel = hydrate(countryDTO, hydrationSettings);
            }
            return countryModel;
        }
        public Country hydrate(Data.DTOs.Country country, CountryHydrationSettings hydrationSettings)
        {
            if (country == null) return null;
            if (hydrationSettings == null) hydrationSettings = new CountryHydrationSettings();
            var viewModel = Mapper.Map<Data.DTOs.Country, Data.Model.Country>(country);
            return viewModel;
        }

        /// <summary>
        /// Find all countries for a particular region
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public List<Country> FindAllCountriesByRegion(string regionId, CountryHydrationSettings hydrationSettings)
        {
            var countriesDTO = _mongoRepository.AsQueryable<Data.DTOs.Country>().Where(x => x.RegionId == regionId);
            List<Country> countriesModel = new List<Country>();
            foreach(var country in countriesDTO)
            {
                Country countryModel = hydrate(country, hydrationSettings);
                countriesModel.Add(countryModel);
            }
            return countriesModel;
        }

        /// <summary>
        /// Finds a country by url
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <param name="hydrationSettings"></param>        
        /// <returns></returns>
        public Country FindBySlug(string urlSlug, CountryHydrationSettings hydrationSettings = null)
        {

            var countryDTO = _mongoRepository.AsQueryable<Data.DTOs.Country>().Where(x => x.UrlSlug == urlSlug).SingleOrDefault();
            Country countryModel = new Country();
            if (countryDTO != null)
            {
                countryModel = hydrate(countryDTO, hydrationSettings);
            }
            return countryModel;            
        }
    }
}
