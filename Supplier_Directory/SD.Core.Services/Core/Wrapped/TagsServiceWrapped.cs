﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using MoreLinq;
namespace SD.Core.Services.Core.Wrapped
{
    public class TagsServiceWrapped : SD.Core.Services.Core.Wrapped.ITagsServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        public TagsServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Tags> All()
        {
            var query = GenerateBasicQuery();

            var tags = query.ToList();
            return tags.ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<Tags> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Tags>();

            var filter = Builders<Data.DTOs.Tags>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.Tags>().Find(filter).ToList()
                .Select(x => hydrate(x))
                .OrderByDescending(x => x.DisplayText)
                .ToList();
        }

        public Tags FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public Tags FindByUrlSlug(string UrlSlug)
        {
            var query = GenerateBasicQuery();

            // var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);
            var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Regex("UrlSlug", new BsonRegularExpression("/^" + UrlSlug + "$/i"));
            query.Filter = filter;

            var tag = query.FirstOrDefault();

            return hydrate(tag);
        }

        public List<Data.Model.Tags> FindByUrlSlug(string[] urlSlugs)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Tags>.Filter.In("UrlSlug", new BsonArray(urlSlugs));

            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);

            query.Filter = filter;

            var tags = query.ToList().Select(x => hydrate(x));

            return tags.ToList();
        }

        public Tags FindByDisplayText(string displayText)
        {
            var query = GenerateBasicQuery();

            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("DisplayText", displayText);
            var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Regex("DisplayText", new BsonRegularExpression("/^" + displayText + "$/i"));

            query.Filter = filter;

            var tag = query.FirstOrDefault();


            return tag == null ? new Tags() : hydrate(tag);

            //return MongoRepository.AsQueryable<Data.DTOs.Tags>()
            //    .Select(x => hydrate(x)).FirstOrDefault(x => x.DisplayText == displayText);
        }

        protected IFindFluent<Data.DTOs.Tags, Data.DTOs.Tags> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Tags>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Tags>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Tags deHydrate(Data.Model.Tags viewModel)
        {
            var article = Mapper.Map<Data.Model.Tags, Data.DTOs.Tags>(viewModel);
            return article;
        }

        public Data.Model.Tags hydrate(Data.DTOs.Tags tag, TagHydrationSettings hydrationSettings = null)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new TagHydrationSettings();
            var viewModel = Mapper.Map<Data.DTOs.Tags, Tags>(tag);
            if (hydrationSettings.ParentTag != null && !string.IsNullOrEmpty(viewModel.ParentId))
            {
                if (viewModel.ParentId != viewModel.Id)
                {
                    var tagService = new TagsService(_mongoRepository);
                    viewModel.ParentTag = tagService.FindById(viewModel.ParentId);
                }
            }           
            return viewModel;
        }      
        /// <summary>
        /// Returns top taps
        /// </summary>
        /// <param name="take"></param>
        /// <returns></returns>
        public List<Data.Model.Tags> GetPopularTags(int take, TagHydrationSettings hydrationSettings)
        {
            var query = GenerateBasicQuery();
            var topDTOTags = query.ToList().OrderByDescending(x => x.ViewCount).Take(take);
            var topTags = topDTOTags.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
            List<Tags> topParentTags = new List<Tags>();
            foreach (var topTag in topTags)
            {
                if (topTag.ParentTag != null)
                    topParentTags.Add(topTag.ParentTag);
                else
                    topParentTags.Add(topTag);
            }

            topParentTags = topParentTags.OrderByDescending(x => x.ViewCount).ToList();
            topParentTags = topParentTags.DistinctBy(x => x.Id).ToList();
            return topParentTags;
        }
        /// <summary>
        /// GetChildTags
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public List<string> GetChildTags(string parentId)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Tags>.Filter.Eq("ParentId", parentId);

            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);

            query.Filter = filter;

            var tags = query.ToList().Select(x => hydrate(x));
            //var tags = _mongoRepository.AsQueryable<Data.DTOs.Tags>().Where(x => x.ParentId == parentId)
            //    .Select(x => hydrate(x, hydrationSettings)).ToList();
            List<string> ids = new List<string>();
            foreach (var t in tags)
                ids.Add(t.Id);
            return ids;
        }

        /// <summary>
        /// Returns Tags for a parentid
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public List<Tags> GetChildTags(string parentId, TagHydrationSettings hydrationSettings)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Tags>.Filter.Eq("ParentId", parentId);

            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);

            query.Filter = filter;


            var tags = _mongoRepository.AsQueryable<Data.DTOs.Tags>().Where(x => x.ParentId == parentId).ToList();
            List<Tags> tagList = new List<Tags>();
            foreach(var t in tags)
            {
                var tag = hydrate(t, hydrationSettings);
                tagList.Add(tag);
            }                
            return tagList;
        }
    }
}