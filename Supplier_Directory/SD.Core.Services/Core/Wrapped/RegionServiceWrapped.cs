﻿using AutoMapper;
using SD.Core.Data;
using SD.Core.Data.Model;
using SD.Core.Data.Model.HS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD.Core.Services.Core.Wrapped
{
    public class RegionServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;
        private CountryService _countryService;
        public RegionServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
            _countryService = new CountryService(_mongoRepository);
        }

        public List<Data.Model.Region> GetAllRegions(RegionHydrationSettings hydrationSettings)
        {
            return _mongoRepository.AsQueryable<Data.DTOs.Region>().ToList()
                .Select(x => hydrate(x, hydrationSettings)).ToList();
        }
        public SD.Core.Data.Model.Region hydrate(Data.DTOs.Region region, Data.Model.HS.RegionHydrationSettings hydrationSettings)
        {
            if (region == null) return null;
            if (hydrationSettings == null) hydrationSettings = new RegionHydrationSettings();
            var viewModel = Mapper.Map<Data.DTOs.Region, Data.Model.Region>(region);
            if (hydrationSettings.Countries != null)
            {
                viewModel.Countries = _countryService.FindAllCountriesByRegion(region.Id, null);
            }

            return viewModel;
        }

        /// <summary>
        /// Finds a region by url
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <param name="hydrationSettings"></param>        
        /// <returns></returns>
        public Region FindBySlug(string urlSlug, RegionHydrationSettings hydrationSettings = null)
        {

            var regionDTO = _mongoRepository.AsQueryable<Data.DTOs.Region>().Where(x => x.UrlSlug == urlSlug).SingleOrDefault();
            Region regionModel = new Region();
            if (regionDTO != null)
            {
                regionModel = hydrate(regionDTO, hydrationSettings);
            }
            return regionModel;
        }
    }
}
