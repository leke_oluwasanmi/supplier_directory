﻿using System.Linq;
using SD.Core.Data;
using SD.Core.Data.Model;
using System.Text;
using System;

namespace SD.Core.Services.Core
{
    public class UserService : BaseService
    {
        public UserService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public User FindById(string id)
        {
            var user = MongoRepository.AsQueryable<User>()
                .SingleOrDefault(x => x.Id == id.ToLower());
            user.Password = string.Empty;
            return user;
        }
      
    }
}