﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using SD.Core.Common.Constants;
using SD.Core.Common.Utils;
using SD.Core.Data;
using SD.Core.Data.Model.HS;
using SD.Core.Services.Core.Wrapped;
using Tags = SD.Core.Data.DTOs.Tags;
using SD.Core.Data.Model;
using SD.Core.Common.Extensions.Strings;
namespace SD.Core.Services.Core
{
    public class ListingService : BaseService
    {
        private readonly ListingServiceWrapped _listingServiceWrapped;
        private const int MaxUniqueUrlSlugChecks = 100;
        public ListingService(IMongoRepository1 mongoRepository, ListingServiceWrapped articleServiceWrapped = null)
            : base(mongoRepository)
        {
            _listingServiceWrapped = new ListingServiceWrapped(mongoRepository);
        }
        public IList<SD.Core.Data.Model.Listing> GetPagedOrderedBy(SortOrder sortOrder, int skip, int take, string[] tags,
            PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false, string listingType = "", ListingHydrationSettings hydrationSettings = null, bool allTags = true)
        {
            return _listingServiceWrapped.GetPagedOrderedBy(sortOrder, skip, take, tags, showWithStatus, showOnlyLive, ascending, listingType, hydrationSettings, allTags);
        }

        public int GetCount(string listingType, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            return _listingServiceWrapped.GetCount(listingType, showWithStatus, showOnlyLive);
        }

        public SD.Core.Data.Model.Listing FindById(string id, ListingHydrationSettings hydrationSettings)
        {
            return _listingServiceWrapped.FindById(id, hydrationSettings);
        }
        public SD.Core.Data.DTOs.Listing FindDTOById(string id)
        {
            return _listingServiceWrapped.FindDTOById(id);
        }
        public void Update(SD.Core.Data.DTOs.Listing listing)
        {
            MongoRepository.Update(listing);
        }

        public string Save(Listing viewModel)
        {
            var listingDTO = _listingServiceWrapped.deHydrate(viewModel);
            //Update ur and check for duplicates
            if (listingDTO.UrlSlug == null)
            {
                listingDTO.UrlSlug = listingDTO.SEOTitle.GenerateSlug();
                var checkCount = 1;
                var urlSlugIsUnique = false;
                while (!urlSlugIsUnique && checkCount < MaxUniqueUrlSlugChecks)
                {
                    if (MongoRepository.AsQueryable<Data.DTOs.Listing>().Any(x => x.UrlSlug == listingDTO.UrlSlug && x.Id != listingDTO.Id))
                        listingDTO.UrlSlug = listingDTO.CompanyName.GenerateSlug() + checkCount;
                    else urlSlugIsUnique = true;
                    checkCount++;
                }
                if (!urlSlugIsUnique) listingDTO.UrlSlug = listingDTO.CompanyName.GenerateSlug() + listingDTO.Id;

                viewModel.UrlSlug = listingDTO.UrlSlug;
            }

            if (string.IsNullOrEmpty(listingDTO.Id))
            {
                MongoRepository.Insert(listingDTO);
                viewModel.Id = listingDTO.Id;
            }
            else
            {
                MongoRepository.UpdateWithComment(listingDTO);
            }

            return listingDTO.Id;
        }

        public IList<Data.Model.Listing> Search(string searchText, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null)
        {
            return _listingServiceWrapped.Search(searchText, skip, take, searchCompanyName, showOnlyLive, hydrationSettings);
        }
        public IList<Data.Model.Listing> Search(string searchText, int skip, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null)
        {
            return _listingServiceWrapped.Search(searchText, skip, searchCompanyName, showOnlyLive, hydrationSettings);
        }

        public IList<Data.Model.Listing> FindByIds(ListingHydrationSettings hydrationSettings = null, params string[] ids)
        {
            return _listingServiceWrapped.FindByIds(hydrationSettings, ids);
        }


        /// <summary>
        /// Searches By Company Name
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="searchCompanyName"></param>
        /// <param name="showOnlyLive"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        public IList<Data.Model.Listing> Search(List<string> categoryIds, string countryId, string keyword, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null, bool orderByListingType = false)
        {
            IList<Data.Model.Listing> listings = _listingServiceWrapped.Search(categoryIds, countryId, keyword, skip, take, searchCompanyName, showOnlyLive, hydrationSettings, orderByListingType);
            return listings;
        }
        public IList<Data.Model.Listing> Search(List<string> categoryIds, List<string> countryIds, string keyword, int skip, int take, bool searchCompanyName = true, bool showOnlyLive = false, ListingHydrationSettings hydrationSettings = null, bool orderByListingType = false)
        {
            var listings = _listingServiceWrapped.Search(categoryIds, countryIds, keyword, skip, take, searchCompanyName, showOnlyLive, hydrationSettings, orderByListingType);
            return listings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <param name="hydrationSettings"></param>
        /// <param name="showWithStatus"></param>
        /// <param name="showOnlyLive"></param>
        /// <returns></returns>
        public Listing FindBySlug(string urlSlug, ListingHydrationSettings hydrationSettings, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            Listing listing = _listingServiceWrapped.FindBySlug(urlSlug, hydrationSettings, showWithStatus, showOnlyLive);
            return listing;
        }

        /// <summary>
        /// Gets the latest listings
        /// </summary>
        /// <param name="listingType"></param>
        /// <param name="hydrationSettings"></param>
        /// <param name="showOnlyLive"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        public List<Data.Model.Listing> GetLatest(string listingType, ListingHydrationSettings hydrationSettings, bool showOnlyLive, int take)
        {
            List<Data.Model.Listing> listings = _listingServiceWrapped.GetLatest(listingType, hydrationSettings, showOnlyLive, take);
            return listings;
        }

        public List<Data.Model.Listing> GetListingByMember(string memberId, ListingHydrationSettings hydrationSettings)
        {
            List<Data.Model.Listing> listings = _listingServiceWrapped.GetListingByMember(memberId, hydrationSettings);
            return listings;
        }

        public List<Data.Model.Listing> GetAllListings(PublicationStatus[] showWithStatus, ListingHydrationSettings hydrationSettings)
        {
            List<Data.Model.Listing> listings = _listingServiceWrapped.GetAllListings(showWithStatus, hydrationSettings);
            return listings;
        }
    }
}
