﻿using SD.Core.Data;

namespace SD.Core.Services
{
    public abstract class BaseService
    {
        private Data.IMongoRepository1 _mongoRepository;

        protected BaseService(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        protected Data.IMongoRepository1 MongoRepository { get { return _mongoRepository; } }
    }
}